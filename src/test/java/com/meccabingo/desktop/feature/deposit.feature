Feature: Deposit

@DesktopReg99
Scenario: My Account Deposit Debit Card_Check whether system displays success message for deposit when user enters all correct details for Debit Card and click on Deposit button
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
And Get current balance from Deposit screen
Then Click on Deposit button besides myaccount
Then Select "Card" payment method
Then Verify following errors are displayed when fields are empty
|Card Number is required|
|Expiry Date is required|
|CVN is required|
When Enter Card number "4026201382933139"
And Enter expiry month year as "03/2030"
And Enter CVN as "000"
And Click on Submit
Then "Payment Method Added" message displayed on screen
Then Click on "Deposit" button from deposit screen
Then Enter Deposit Amount as "5"
And Again enter CVN as "000" and hit deposit button
Then Verify Deposit Successful confirmation message
Then Verify Deposit amount is credited to account

@Desktop21
Scenario: Check whether user is able to deposit using Paypal payment method
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
And Get current balance from Deposit screen
Then Click on Deposit button besides myaccount
Then Select "Paypal" payment method
Then Enter Deposit Amount as "5"

Then Verify 'Add new payment Method' screen is displayed
Then Select "paypal" payment method
Then Choose " No bonus for me " option
Then Click on "Next" button
Then Again Enter Deposit Amount as "10"
#And Click on "Deposit" button
Then Click on "Next" button
Then Enter email address "paypal1@aquagaming.com" and Password "Qqq.111!"
And Click on Login from Paypal screen
And Click on 'Pay Now' button
Then Verify Deposit Successful confirmation message
Then Verify Deposit amount is credited to account

@Desktop21
Scenario: Check whether user is able to deposit using PaySafeCard payment method
Given Invoke the Bella site on Desktop
When User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
And Get current playable balance
Then Navigate to "Deposit" tab from my account screen
Then Verify 'Add new payment Method' screen is displayed
Then Select "paysafecard" payment method
#Then Select payment method
Then Choose " No bonus for me " option
Then Click on "Next" button
Then Again Enter Deposit Amount as "10"
#And Click on "Deposit" button
Then Click on "Next" button
#Then Verify it redirect to "Pay with paysafecard" window
Then Enter Pin number "0000 0000 0990 5165" and Accept TnC 
And Click on Pay
Then Verify Deposit Successful confirmation message
Then Verify Deposit amount is credited to account


@Desktop21
Scenario: Check that system displays an error message on trying to withdraw an amount less than the minimum withdraw limit of the site
Given Invoke the Bella site on Desktop
When User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
And Get current playable balance
Then Navigate to "Withdrawal" tab from my account screen
And Verify following fields are displayed on withdraw screen
|Total Balance|
|Available to Withdraw|
Then Enter Withdraw amount as "4"
And Click on " Withdraw" button
Then Verify 'The minimum amount you can withdraw is  £10.00 ' message should be display

@Desktop21
Scenario: Check whether system displays success message after performing withdrawal successfully
Given Invoke the Bella site on Desktop
When User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
And Get current playable balance
Then Navigate to "Withdrawal" tab from my account screen
And Verify following fields are displayed on withdraw screen
|Total Balance|
|Available to Withdraw|
Then Enter Withdraw amount as "10"
And Click on " Withdraw" button
Then Verify "Your withdrawal request for £10 was successful. Withdrawal reference" message is displayed with " Ok" button
Then Verify Menu screen get closed when click on " Ok" button

@Desktop21
Scenario: Check that system allows to withdraw the amount minimum 10
Given Invoke the Bella site on Desktop
When User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
And Get current playable balance
Then Navigate to "Withdrawal" tab from my account screen
And Verify following fields are displayed on withdraw screen
|Total Balance|
|Available to Withdraw|
Then Enter Withdraw amount as "10"
And Click on " Withdraw" button
Then Verify "Your withdrawal request for £10 was successful. Withdrawal reference" message is displayed with " Ok" button
Then Verify Menu screen get closed when click on " Ok" button

@Desktop21
Scenario: Check whether system displays transaction history is ordered by date with the latest date on the top of the list
Given Invoke the Bella site on Desktop
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Navigate to "Transaction History" tab from my account screen
Then Verify latest date transaction history is displayed on the top of the list

@Desktop21
Scenario: Transaction History_Check whether system can filter transaction by Date and display correctly
Given Invoke the Bella site on Desktop
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Navigate to "Transaction History" tab from my account screen
Then Select Filter Activity as "Deposits"
Then Set Date range and click on Filter button
Then Verify transaction history is displayed as per selected date
Then Set same date filter and verify Transcation history results

@Desktop21
Scenario: Transaction History_Check whether system can filter transaction by its type and display correctly
Given Invoke the Bella site on Desktop
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Navigate to "Transaction History" tab from my account screen
Then Verify following options are displayed under activity filter dropdown
|All Activity|
|Net Deposits|
|Deposits|
|Withdrawals|
|Bonuses|
|Wagers|
Then Select Filter Activity as "Deposits"
Then Verify filter results are displayed as per selected activity
Then Select Filter Activity as "Withdrawals"
Then Verify filter results are displayed as per selected activity

@Desktop21
Scenario: Check that customers must see 'Sort by period' with the following dropdown options:Last 24 Hours, Last 7 days, Last 30 Days,3 months
Given Invoke the Bella site on Desktop
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Navigate to "Transaction History" tab from my account screen
Then Select Filter Activity as "Net Deposits"
And Verify following period options are displayed under 'Sort by Period' dropdown
|Last 24 Hours|
|Last Week|
|Last 30 Days|
|3 Months|
Then Select period as "Last 24 Hours"
And Verify transaction is displayed within the period selected

@Desktop21
Scenario: Responsible Gambling > Check whether an option is always available for customer to set their deposit limit at all time
Given Invoke the Bella site on Desktop
When User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on my account button
Then Navigate to "Deposit Limits" tab from my account screen
Then User will get the "Setting Your Net Deposit Loss Limits" screen