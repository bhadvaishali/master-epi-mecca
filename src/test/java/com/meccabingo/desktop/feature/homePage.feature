Feature: Header 

#EMC-823
@DesktopReg @DesktopHiddenLive
Scenario: Check whether system displays Join Now button when user access site first time
Then Verify header displays join now button


@DesktopReg @DesktopHiddenLive
Scenario: Check whether no action triggered  when user clicks on Balance title  when balance is shown and not shown also when user click on 5 dots
When User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Click on user balance title from header
Then Click on balance toggle
Then Click on Balance title when balance not shown
Then Click on five dots when balance not shown

@DesktopReg @DesktopHiddenLive
Scenario: Header Logged out_Check whether system displays Mecca logo, Navigation Menu, Search icon, Login, Join now
Then Verify header displays following components :
|Headaer|
|Mecca Logo|
|Navigation menu|
|Search loupe icon|
|Login CTA|
|Join Now CTA|


@DesktopReg @DesktopHiddenLive
Scenario: Header Logged in_Check whether system displays Mecca logo, Navigation Menu, Search icon, Deposit CTA, balance block, My Account Icon in logged state at Header
When User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Verify header displays following components :
|Headaer|
|Mecca Logo|
|Navigation menu|
|Search loupe icon|
|Deposit CTA|
|Balance amount/dots|
|Balance text|
|Hide/Unhide balance switch|
|My account icon|


#EMC-1069
@DesktopReg @DesktopHiddenLive
Scenario: Check whether system displays 404 error page to user when user tries to access page which is not configured
When User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Verify user is successfully logged in
Then Navigate to invalid url
Then Verify error title

#EMC-1070
@DesktopReg77
Scenario: Check whether system displays cokkies banner when user access site without any stored cookies fro mecca site
Then Verify cookies text 
Then Verify continue button

@DesktopReg5555
Scenario: Navigation_Check whether system navigate user to respective pages when user clicks on Tab menu available on header
Then Verify following tab is displayed under "Bingo" secondary navigation bar
|Most Popular|
|New|
|Special|
|90 Ball|
|75 Ball|
And Verify 'Play Bingo' cta
Then Verify following tab is displayed under "Slots & Games" secondary navigation bar
|All|
|In Club|
|Originals|
|Jackpots|
And Verify 'Sea all'link under "Slots & Games" section 
Then Verify 'Sea all'link under "New Games" section 
Then Verify 'Sea all'link under "Slingo" section
Then Verify 'Sea all'link under "Table Games" section
And Verify Winner feed carousel on home screen
Then Verify 'Find your nearest Mecca Bingo Club' section on footer
And Verify below fields under 'Fun on the Run' block
|Description|
|Find out more cta|
|Google play|
|iOS play store|
Then Verify "Welcome to the Mecca of Online Bingo" text
And Verify below blocks under help
|Live Help|
|Contact|
|Community|