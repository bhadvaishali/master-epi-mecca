Feature: Bonuses

@DesktopReg @DesktopHiddenLive
Scenario: Check whether System displays Header and Body on My Account > Enter bonus code [Screen 1] page
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Bonuses" option
Then Verify following option on enter bonus code page:
|Close button|
|Back button|
|Enter code here text|
|Enter code field|
|Clear button|
|Submit button|


@DesktopReg @DesktopHiddenLive
Scenario: Check whether system displays an error message on the screen if the bonus code is incorrect
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Bonuses" option
Then Enter invalid bonus code
Then Click on submit
#Then Verify error message "Incorrect Bonus Code"
Then Verify text "Incorrect Bonus Code" displayed

@DesktopReg
Scenario: My Account Enter Bonus Success_Check whether system displays success message after claiming promotions
Then Invoke the Web portal "https://cogs_staging.dagacube.net/Default.aspx"
#And Enter username as "Aishwarya" and password as "AishwaryaB1234@"
And Enter username as "Satya" and password as "0o9i8u7y^T"
Then Click on Sign In button
Then Select "Search Players" submenu from "Players" menu
And Search players using username
Then Select "Promo Groups" submenu from "Promotions" menu
Then Add player id in "Mecca Promo" Promo group
Then Invoke Mecca site
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Bonuses" option
Then Enter bonus code as "mbtest123"
And Click on submit
Then Promotion details will display to user
Then Select the T & C option and click on Claim button
Then Bonus success screen will display to the user

@DesktopReg
Scenario: My Account Enter Bonus Code Page1_Check whether System navigate user to Screen 2 when enters correct code in field and click on Submit button
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Bonuses" option
Then Verify following option on enter bonus code page:
|Close button|
|Back button|
|Enter code here text|
|Enter code field|
|Clear button|
|Submit button|
Then Enter bonus code as "dfertg"
Then Click on submit
#Then Verify error message "Incorrect Bonus Code"
Then Verify text "Incorrect Bonus Code" displayed
Then Click on Clear button
Then Verify Entered code is removed
Then Enter bonus code as "mbtest123"
And Click on submit
Then Promotion details will display to user

@DesktopReg
Scenario: My Account Enter Bonus Instant Type_Check whether User able to claim Instant Bonus using promocode
Then Invoke the Web portal "https://cogs_staging.dagacube.net/Default.aspx"
#And Enter username as "Aishwarya" and password as "AishwaryaB1234@"
And Enter username as "Satya" and password as "0o9i8u7y^T"
Then Click on Sign In button
Then Select "Search Players" submenu from "Players" menu
And Search players using username
Then Select "Promo Groups" submenu from "Promotions" menu
Then Add player id in "Mecca Promo" Promo group
Then Invoke Mecca site
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Bonuses" option
Then Enter bonus code as "Meccatestbonus"
And Click on submit
Then Promotion details will display to user
Then Select the T & C option and click on Claim button
Then Bonus success screen will display to the user
Then Click on ActivePromotions button
Then Verify text "Active bonuses" displayed

@DesktopReg
Scenario: My Account Active Bonuses_Check whether system displays Active bonuses details on page when user having some bonuses which are in Bonus status = Active, Rewarded and PartQualified
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Bonuses" option
Then Click menu option "Active bonuses"
Then Verify following details should be displayed on unExpanded view
|Type|
|Bonus status|
|Amount|
Then Click on + button of any claimed bonus
Then Verify following details should be displayed on Expanded view
|Type|
|Bonus status|
|Amount|
|Wagering target|
|Progress|
|Activation|
|Bonus status|
|Expiry|
|Opt out link|
|T&Cs link|

@DesktopReg
Scenario: My Account Bonus Transaction Opt Out_Check whether user able to view opt out for the promotion from Bonus Histroy page
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Bonuses" option
Then Click menu option "Bonus History"
Then Select bonus type as "Instant" from filter
Then Verify filter results are displayed as per selected bonus type
Then Click on + button of any claimed bonus
Then Verify following details should be displayed on Expanded view
|Type|
|Bonus status|
|Amount|
|Wagering target|
|Progress|
|Activation|
|Bonus status|
|Expiry|
|Opt out link|
|View T&Cs link|
Then Click on Opt Out link
Then Accept confirmation pop up
Then Verify that Bonus status displayed as "Cancelled"

@DesktopReg
Scenario: My Account Active Bonuses Opt Out_Check whether user able to opt out from Active promotion page
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Bonuses" option
Then Click menu option "Active bonuses"
Then Click on + button of any claimed bonus
Then Verify following details should be displayed on Expanded view
|Type|
|Bonus status|
|Amount|
|Wagering target|
|Progress|
|Activation|
|Bonus status|
|Expiry|
|Opt out link|
|View T&Cs link|
Then Click on Opt Out link
Then Accept confirmation pop up

@DesktopReg
Scenario: Bonus History_Check that the customer can filter the Bonus history based on date range
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Bonuses" option
Then Click menu option "Bonus History"
Then Change the bonus Date and click on Filter button
Then Verify Bonus history is displayed as per selected bonus date

@DesktopReg
Scenario: My Account Bonus Transactions_Check whether system displays Bonus hisotry transactions as per type bonus type
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Bonuses" option
Then Click menu option "Bonus History"
Then Verify following options are displayed under bonus type filter dropdown
|All types|
|First Deposit|
|Deposit|
|Instant|
|Player registration|
|Card registration|
|Deposit over bonus|
|Cashback on Total Stake| 
|Cashback On Net losses|
|Opt in|
|Content|
Then Select bonus type as "Instant" from filter
Then Verify filter results are displayed as per selected bonus type
Then Select bonus type as "Opt in" from filter
Then Verify filter results are displayed as per selected bonus type
Then Select bonus type as "First Deposit" from filter
Then Verify filter results are displayed as per selected bonus type