Feature: Game and bingo container

#EMC-11
#bingo hero tile
@Desktop
Scenario: Check the actions on bingo game tile when user clicks on below sections:-
|Image|
|hover over "i" flag|
|title of a hero tile|
|click the "i" flag| 
|click the top flag or jackpot flag|
|click the price/prize/type of bingo|
Then Navigate through hamburger to "bingo" menu
Then Click on image of the game
Then Verify system displays login window
Then Click close 'X' icon
Then Navigate through hamburger to "bingo" menu
Then Click on title of the game
Then user should remain on bingo container page
Then Navigate through hamburger to "bingo" menu
Then click on top flag of bingo game
Then Verify system displays login window

#EMC-100
#games hero tile
@Desktop
Scenario: Check the actions on game hero tile when user clicks on below sections:
|Headaer|
|image of the game hero tile|
|title of a hero tile|
|description of a hero tile|
|user hover over "i" flag of a hero tile|
|click "i" flag of a hero tile|
Then Click on image of the hero tile "Fluffy Favourites"
Then Click on title of the hero tile "Fluffy Favourites"
Then Click on description of the hero tile "Fluffy Favourites"
Then Hover on i of the hero tile "Fluffy Favourites"
Then User clicks on i button of hero tile "Fluffy Favourites"

#EMC-88
#bingo tile
@Desktop
Scenario: Check the actions on bingo tile when user clicks on below sections:
|Headaer|
|Image of the tile|
|Hover over "i" flag|
|Title of the tile|
| "i" flag |
|price/prize/type of bingo|
Then Navigate through hamburger to "slot" menu
Then Click on image of the bingo tile "Best Odds Bingo"
Then Click on title of the bingo tile "Best Odds Bingo"
Then Hover on i of the bingo tile "Best Odds Bingo"
Then User clicks on i button of bingo tile "Best Odds Bingo"
Then Click on prize of the bingo tile "Best Odds Bingo"

#EMC-350
#bingo hero tile
@Desktop
Scenario: Check the actions on bingo hero tile when user clicks on below sections:
|image of the bingo hero tile|
|title of a hero tile|
|description of a hero tile|
|user hover over "i" flag of a hero tile|
|"i" flag of a hero tile|
Then Navigate through hamburger to "bingo" menu 
Then Click on image of the hero tile "Best Odds Bingo" 
Then Click on title of the hero tile "Best Odds Bingo"
Then Click on description of the hero tile "Best Odds Bingo"
Then Hover on i of the hero tile "Best Odds Bingo"
Then User clicks on i button of hero tile "Best Odds Bingo"

#EMC-991
@Desktop
Scenario: Check whether opticity layer gets apllied for background image on Game/Bingo details page accroding to opticity value set in CMS
Then Navigate through hamburger to "bingo" menu
Then User clicks on i button of hero tile "Best Odds Bingo"
Then Verify background image of "Best Odds Bingo" has the opacity layer applied in game details page