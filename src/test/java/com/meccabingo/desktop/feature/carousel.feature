Feature: Carousel

#EMC-294
@DesktopReg @DesktopHiddenLive
Scenario: Carousel Linked Slides_Check whether user able to navigate to respective link when user click on Slide
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click anywhere on slider/carousel
Then Verify user navigate to respective page

#EMC-95
@DesktopReg @DesktopHiddenLive
Scenario: Check whether user can see navigation arrows and pagination dots for Carousel when more than one slide configure
Then Verify navigation arrows on carousel
Then Verify pagination dots on carousel

#@DesktopReg
Scenario: Check whether slider moves automatically one slide to left after after X seconds
Then Verify slider moves automatically after time inerval

@DesktopReg @DesktopHiddenLive
Scenario: Check whether slider moves one slide to left when user clicks on Left Arrow
Then Click on left arrow on carousel

@DesktopReg @DesktopHiddenLive
Scenario: Check whether slider moves one slide to left when user clicks on Right Arrow
Then Click on right arrow on carousel
 
#EMC-303
@Desktop @DesktopHiddenLive
Scenario: Check whether system displays feefo carousel as configured 
Then Verify feefo carousel slide
Then Verify left button on feefo carousel
Then Verfi right button on feefo carousel