Feature: Promotions

@DesktopReg
Scenario: Check whether system displays appropriate informative message when user selects 'Active' filter and he / she doesn’t opted-in to any of the promotions available on the site
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
When User click on Promotion tab
Then Verify following tab is displayed under secondary navigation bar
|All|
|Bingo|
|Slots & Games|
|Club|
|My Active Promotions|
Then Select "My Active Promotions" tab from navigation tab
Then Verify 'Dont have any active promotions' message is displayed

@DesktopReg
Scenario: Promotion Claim Promotion Tile_Check whether user able to claim instant Type promotion from Promotion Tile on Promotion Listing Page
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
When User click on Promotion tab
Then Verify "Instant bonus" display following components Before claim or OptIn :
|Options|
|Claim|
|Learn more|
|T&Cs apply|
Then User Click on "Claim" button
Then Verify "Instant bonus" dispaly following components After claim or optIn:
|Options|
|Claimed|
|Learn more|
|T&Cs apply|

@DesktopReg
Scenario: Promotion Opt-In Promotion Tile_Check whether user able to Opt-In for Opt-In Type promotion from Promotion Tile on Promotion Listing Page
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
When User click on Promotion tab
Then Verify "Opt in bonus" display following components Before claim or OptIn :
|Options|
|Opt in|
|Learn more|
|T&Cs apply|
Then User Click on "OptIn" button
Then Verify "Opt in bonus" dispaly following components After claim or optIn:
|Options|
|Opted in|
|Learn more|
|T&Cs apply|

@DesktopReg
Scenario: Promotion Opt-In Promotion Details_Check whether user able to Opt-In for Opt-In Type promotion from Promotion details page using Opt-In CTA
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
When User click on Promotion tab
Then User Click on Learn More button from "Opt in bonus" promotion
Then User Click on "OptIn" button
Then Verify Optedin and Opt out button is displayed on page
#Then Verify featured games section dispalyed on page


@DesktopReg
Scenario: Promotion Instant Promotion Details_Check whether user able to claim instant Type promotion from Promotion details page using Claim CTA
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
When User click on Promotion tab
Then User Click on Learn More button from "Instant bonus" promotion
Then User Click on "Claim" button
Then Verify "Claimed" status on promotion details page
#Then Verify featured games section dispalyed on page

@DesktopReg
Scenario: Check whether system does not allows user to opt-in to the promotion in logged OUT state
When User click on Promotion tab
Then User Click on Learn More button from "Opt in bonus" promotion
Then User Click on "OptIn" button
Then Verify Login Page displayed

@DesktopReg
Scenario: Promotion Active Tab_Check whether system displays Active OR PartQualified promotions under active promotion tab which are claimed by users
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
When User click on Promotion tab
Then Select "My Active Promotions" tab from navigation tab
Then Verify Active promotions are displayed under active promotion tab

@DesktopReg
Scenario: Promotion Active Tab_Check whether system displays details for no of days left on promotion tile avaibale under active promotion Tab
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
When User click on Promotion tab
Then Select "My Active Promotions" tab from navigation tab
Then Verify Active promotions are displayed under active promotion tab
And Verify Countdown clock is displayed with no of days left in the bottom right with one of follwoing form
|If more than 3 days color is green for text|
|If more than 24hrs and less than 72 hrs text color is Orange|
|If less than 25hrs and not yet expired text color is red|