Feature: User Registration
 
@MultiUser
Scenario Outline: Registration journey 
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Enter the password in valid format
Then Enter the firstname in valid format
Then Enter the surname in valid format
Then Enter Random Date of birth
Then Enter valid mobile number
Then Enter valid postcode "ls251az"
Then Select one address from dopdown
Then Click on select all in marketing preference section
Then Click on Register CTA
Then Verify Deposit Now header
#Then Verify "Why not set a deposit limit?" link

Examples:
| Counter |
|	1  		  |
|	1  		  |
|	1  		  |
|	1  		  |
|	1  		  |
|	1  		  |
|	1  		  |
|	1  		  |
|	1  		  |
|	1  		  |
|	1  		  |
|	1  		  |
|	1  		  |
|	1  		  |
|	1  		  |
|	1  		  |
|	1  		  |
|	1  		  |
|	1  		  |
|	1  		  |
|	1  		  |
|	1  		  |
|	1  		  |
|	1  		  |
|	1  		  |
|	1  		  |
|	1  		  |
|	1  		  |
|	1  		  |
|	1  		  |
|	1  		  |
|	1  		  |
|	1  		  |
|	1  		  |
|	1  		  |