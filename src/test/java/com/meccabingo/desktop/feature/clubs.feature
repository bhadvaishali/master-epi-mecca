Feature: Clubs

@DesktopReg @DesktopHiddenLive
Scenario: Club Favourite_Check whether System displays confirmation message pop up when user clicks on “Favourite“ CTA from Club details page
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Navigate through hamburger to "clubs" menu
Then Search club "Mecca Rochadale"
Then Select first club that appears in list
Then Click on more Info
Then Mark it as favorite
Then Verify "Next time you click on the club section, Mecca Rochdale will appear as your homepage." confirmation message
#Then Verify text "Next time you click on the club section, Mecca Rochdale will appear as your homepage. Happy to go ahead and favourite it?" displayed

@DesktopReg @DesktopHiddenLive
Scenario: Club Favourite_Check whether System displays confirmation message pop up after successful login when user clicks on “Favourite“ CTA in logged out mode
Then Navigate through hamburger to "clubs" menu
Then Search club "Mecca Rochadale"
Then Select first club that appears in list
Then Click on more Info
Then Mark it as favorite
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Verify "Next time you click on the club section, Mecca Rochdale will appear as your homepage." confirmation message
#Then Verify text "Next time you click on the club section, Mecca Rochdale will appear as your homepage. Happy to go ahead and favourite it?" displayed
Then click button having label as "Yes"
Then Verify club marked as favorite
Then Navigate through hamburger to "clubs" menu
And Verify Favourited club section is displayed on screen

@DesktopReg @DesktopHiddenLive
Scenario: Club Favourite_Check whether System displays unfavourite“ the club message pop up when user clicks on “Favourite“ CTA from Club details page which is already added in favourities
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Navigate through hamburger to "clubs" menu
Then Search club "Mecca Rochadale"
Then Select first club that appears in list
Then Click on more Info
Then Unfavorite it
Then Verify "This club will be removed from your favourites, and will no longer appear as your club homepage." confirmation message
#Then Verify text "This club will be removed from your favourites and will no longer appear as your club homepage. Want to carry on?" displayed
Then click button having label as "Yes"
Then Verify club favorite tag is removed

@DesktopReg @DesktopHiddenLive
Scenario: Join Club_Check whether User able to join favourite club using Join Club Cta available on club details page
#Then User clicks on Login Button from header of the Page
#Then User enters username
#And User enters password
#Then User clicks on login button from login window
#Then Verify balance section is displayed
Then Navigate through hamburger to "clubs" menu
Then Search club "Mecca Oldham"
Then Select first club that appears in list
Then Click on more Info
Then Click link having text as "Join Club"
Then Enter the firstname in valid format
Then Enter the surname in valid format
Then Enter Date of birth "11" "08" "1996"
Then Enter valid postcode "wr53da"
Then Select one address from dopdown
Then Enter join club email "MBxRocdz@mailinator.com"
Then Enter Home phone "023446789"
Then Enter valid mobile number
Then Click on select all in marketing preference section
Then Click on age checkbox
Then click button having label as "Join your local club"
Then Verify user is joined club successfully

@DesktopReg @DesktopHiddenLive
Scenario: Venue Finder_Check whether system displays maximum of 5 search results when user enters 3 and more than 3 characters in field
Then Verify following sections are displayed under Venue finder
|Image|
|Find your nearest Mecca Bingo Club text|
|Description|
|Field Postcode or town|
|Find Nearest CTA|
Then Enter 3 or more than 2 chacaters in search fields
And Verify maximum of 5 results are shown below the search field with 'Name' and 'Distance'
Then Verify text 'These are the five nearest clubs to your location' at the top of the results
Then Select first club that appears in list
Then Map area is opened with following club info
|Club name|
|Club phone number|
|Club address|
|Todays Opening time|
|More Info cta|
|Directions cta|
Then Click on "Directions " cta
Then Google maps will open in a new tab
Then Back to Mecca site
And Click on "More Info" cta
Then User navigate to the club details page
