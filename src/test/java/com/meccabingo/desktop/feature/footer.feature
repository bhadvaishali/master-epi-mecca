Feature: Footer - Social Media, Useful links, Payments, Partener section etc

#EMC-22 Social media TCs
@DesktopReg @DesktopHiddenLive
Scenario: Footer Social Media_Check whether system navigate user to respective URL when user clicks on any block available for social mediaCheck whether system displays following Social media components:
-Facebook
-Twitter
-Instagram
-Youtube
Then Verify system displays following Social media components:
|Header|
|Twitter|
|Facebook|
|Youtube|
|Instagram|
And Verify system displays Icon, Text and Name for each social media block
Then Click on Facebook block from footer
Then Switch to child window
Then Verify system navigate user to "https://www.facebook.com/MeccaBingo" url
Then Switch to parent window
Then Click on Instagram block from footer
Then Switch to child window
Then Verify system navigate user to "https://www.instagram.com/meccabingo/" url
Then Switch to parent window
Then Click on twitter block from footer
Then Switch to child window
Then Verify system navigate user to "https://twitter.com/MeccaBingo" url
Then Switch to parent window
Then Click on youtube block from footer
Then Switch to child window
Then Verify system navigate user to "https://www.youtube.com/user/MeccaBingoClubs" url

#EMC-23 : Footer - Trusted Partners - Payment methods
@DesktopReg @DesktopHiddenLive
Scenario: Footer Trusted Partners_Check whether system displays following section for Payment Providers components:-Title-Description -LogosCheck whether system displays following section for Trusted partners components:
-Title
-Description 
-Logos
Then Verify system displays following section for payment providers components:
|Title|
|Description|
|Logos|

#EMC-27 test Cases for useful links
@DesktopReg @DesktopHiddenLive
Scenario: Check whether system displays folllowing URL under Useful Links block:
-Privacy Policy
-Terms and Conditions
-Affiliates
-Mecca Club Terms
-Play Online Casino
-Mecca Blog

Then Verify following URLs are displaying under Useful Links block:
|Headaer|
|Privacy Policy|
|Terms and Conditions|
|Affiliates|
|Mecca Club Terms|
|Play Online Casino|
|Mecca Blog|

@DesktopReg
Scenario: Check whether system navigate to appropriate URL when user clicks on any Useful Links
Then Click on Privacy Policy Link
Then Switch to child window
Then Verify system navigates user to "https://tst01-mecc-cms4.rankgrouptech.net/privacy-policy"  
Then Switch to parent window
Then Click on Terms and Conditions Link
Then Switch to child window
Then Verify system navigates user to "https://tst01-mecc-cms4.rankgrouptech.net/terms-and-conditions"
Then Switch to parent window
Then Click on Affiliates Link
Then Switch to child window
Then Verify system navigates user to "https://www.rankaffiliates.com/"
Then Switch to parent window
Then Click on Mecca Club Terms Link
Then Switch to child window
Then Verify system navigates user to "https://tst01-mecc-cms4.rankgrouptech.net/club-terms-and-conditions"
Then Switch to parent window
Then Click on Play Online Casino Link
Then Switch to child window
Then Verify system navigates user to "https://www.grosvenorcasinos.com/"
Then Switch to parent window
Then Click on Mecca Blog Link
Then Switch to child window
Then Verify device navigate user to containing url "https://blog.meccabingo.com/"


@DesktopHiddenLive
Scenario: Check whether system navigate to appropriate URL when user clicks on any Useful Links
Then Click on Privacy Policy Link
Then Switch to child window
Then Verify system navigates user to "https://hidden.meccabingo.com/privacy-policy"  
Then Switch to parent window
Then Click on Terms and Conditions Link
Then Switch to child window
Then Verify system navigates user to "https://hidden.meccabingo.com/terms-and-conditions"
Then Switch to parent window
Then Click on Affiliates Link
Then Switch to child window
Then Verify system navigates user to "https://www.rankaffiliates.com/"
Then Switch to parent window
Then Click on Mecca Club Terms Link
Then Switch to child window
Then Verify system navigates user to "https://hidden.meccabingo.com/club-terms-and-conditions"
Then Switch to parent window
Then Click on Play Online Casino Link
Then Switch to child window
Then Verify system navigates user to "https://www.grosvenorcasinos.com/"
Then Switch to parent window
Then Click on Mecca Blog Link
Then Switch to child window
Then Verify device navigate user to containing url "https://blog.meccabingo.com/"

#Sprint 7
#EMC-36 : Footer - GamCare, 18, GamStop, Gambling commision, Gambling control Logos
@DesktopReg @DesktopHiddenLive
Scenario: Check whether user able to view following logos at Footer section:
-GamCare
-18
-GamStop
-Gambling commision
-Gambling control
-IBAS
-ESSA
Then Verify user able to view following logos at Footer in partners block:
|ESSA|
|IBAS|
|18|
|Gambling Control|
|GamCare|
|GamStop|
|Gambling Commision|

@DesktopReg
Scenario: Check whether user able to navigate to configured URL when user clicks on any of the Logo
Then User clicks on IBSA logo
Then Switch to child window
Then Verify system navigates user to "https://www.ibas-uk.com/"
Then Switch to parent window
Then User clicks on Gambling Control logo
Then Switch to child window
Then Verify system navigates user to "https://www.gamblingcontrol.org/"
Then Switch to parent window
Then User clicks on GamCare logo
Then Switch to child window
Then Verify system navigates user to "https://www.gamcare.org.uk/"
Then Switch to parent window
Then User clicks on GamStop logo
Then Switch to child window
Then Verify system navigates user to "https://www.gamstop.co.uk/"
Then Switch to parent window
Then User clicks on Gambling Commission logo
Then Switch to child window
Then Verify system navigates user to "https://www.gamblingcommission.gov.uk/"
Then Switch to parent window
Then User clicks on essa logo
Then Switch to child window
Then Verify system navigates user to "https://www.essa.uk.com/"
Then Verify essa logo

@DesktopHiddenLive
Scenario: Check whether user able to navigate to configured URL when user clicks on any of the Logo
Then User clicks on IBSA logo
Then Switch to child window
Then Verify system navigates user to "https://www.ibas-uk.com/"
Then Switch to parent window
Then User clicks on Gambling Control logo
Then Switch to child window
Then Verify system navigates user to "https://www.gamblingcontrol.org/"
Then Switch to parent window
Then User clicks on GamCare logo
Then Switch to child window
Then Verify system navigates user to "https://www.gamcare.org.uk/"
Then Switch to parent window
Then User clicks on GamStop logo
Then Switch to child window
Then Verify system navigates user to "https://www.gamstop.co.uk/"
Then Switch to parent window
Then User clicks on Gambling Commission logo
Then Switch to child window
Then Verify system navigates user to "https://www.gamblingcommission.gov.uk/"


#Sprint 7
#EMC-272 : Footer - Verisign Secured
@DesktopReg1111
Scenario: Check whether user able to view Verisign Secured logo at Footer section
Then Verify user should be able to view Verisign Secured logo at Footer section


@DesktopReg @DesktopHiddenLive
Scenario: Footer Main Area_Check whether system displays following section footer Main area:
- Mecca Logo
- Awards Section
- verisigned information
- Keep it fun information
- Licensed Information
Then Verify user is able to view following sections in footer
|Mecca Logo|
|Awards Section|
|verisigned information|
|Keep it fun information|
|Licensed Information|


#EMC-10 : Footer - Rank group
@DesktopReg @DesktopHiddenLive
Scenario: Check whether user navigate to Configured link on click of hyperlinked text
Then User clicks on Alderney Gambling Control Commission link
Then Switch to child window
Then Verify system navigates user to "https://www.gamblingcontrol.org/"
Then Switch to parent window
Then User clicks on UK Gambling Commission link
Then Switch to child window
Then Verify device navigate user to containing url "gamblingcommission.gov.uk"
Then Switch to parent window
Then User click on BeGambleAware link
Then Switch to child window
Then Verify system navigates user to "https://www.begambleaware.org/"
Then Switch to parent window
Then User clicks on Rank Group link
Then Switch to child window
Then Verify system navigates user to "https://www.rank.com/en/index.html"

@DesktopReg @DesktopHiddenLive
Scenario: Page Layout > Check whether footer contains 18+ logo and it is NOT a hyperlink (It should be just a logo)
Then Verify "18+" logo is displayed in footer section
Then Verify 18+ logo should not be a hyperlink

@DesktopReg @DesktopHiddenLive
Scenario: Page Layout > Check whether footer contains a Keep it fun logo and it should be hyperlinked to correct landing page
Then Verify "Keep it fun" logo is displayed in footer section
Then Verify "Keep it fun" logo hyperlinked to the correct page

@DesktopReg @DesktopHiddenLive
Scenario: Page Layout > Check whether footers identify the regulators. A link should be present which should take the customer to the Gambling Commission page, which displays the relevant company license status
Then Verify Regulators (UK and Alderney) and associated license statements is displayed under License section of the footer
And Click and verify 'UK Gambling Commission' link redirect to the correct page

@DesktopReg @DesktopHiddenLive
Scenario: Limiting collusion/cheating > Ensure that information is available to the customer regarding cheating, recovered player funds and how to complain
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Verify user is successfully logged in
Then Verify "Terms and Conditions" link is displayed on under Usefullinks footer section
Then Access Terms & Conditions link and Verify that information is available to the customer regarding cheating, recovered player funds and how to complain

@DesktopReg @DesktopHiddenLive
Scenario: Peer to Peer customers - Third party software > Ensure that information is available to the customer regarding the rules around the use of third-party software, and how to report suspected use that is against the operators T&Cs
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Verify user is successfully logged in
Then Verify "Terms and Conditions" link is displayed on under Usefullinks footer section
Then Access Terms & Conditions link and Verify that information is available to the customer regarding third party software