Feature: Deposit limit

@DesktopReg44 @DesktopHiddenLive55
Scenario: Responsible Gambling > Check whether system offers customers the opportunity to make use of Daily, Weekly and Monthly parameters while setting deposit limits
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
When User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Deposit Limits"
Then Verify "Deposit Limits" as header displayed
Then Daily,Weekly and yearly options displayed to user
Then Click on edit button of "Daily"
Then Set daily deposit limit as "191"
Then Verify updated "Daily" deposit limit is displayed
Then Click on edit button of "Weekly"
And Set Weekly deposit limit as "400"
Then Verify updated "Weekly" deposit limit is displayed
Then Click on edit button of "Monthly"
And Set Monthly deposit limit as "800"
Then Verify updated "Monthly" deposit limit is displayed

@DesktopReg77 @DesktopHiddenLive55
Scenario: Check whether system displays error message when user enters limit equal or smaller than next higher limits
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Deposit Limits"
Then Verify "Deposit Limits" as header displayed
Then Daily,Weekly and yearly options displayed to user
Then Click on edit button of "daily"
Then Verify deposit limit edit and Net deposit loss limit info is displayed on screen
Then Set daily deposit limit as "1"
Then Verify " Your limit is lower than the minimum deposit. " error message on deposit limit screen
#Then Set daily deposit limit as "0"
#Then Verify "Enter a valid amount" error message on deposit limit screen
Then User set daily limit greater than weekly
Then Verify "daily limit cannot be higher than weekly limit" error message on deposit limit screen
Then User set daily limit greater than monthly
Then Verify "daily limit cannot be higher than monthly limit" error message on deposit limit screen

@DesktopReg33 @DesktopHiddenLive55
Scenario: Check whether system changes deposit limits immediately when user decrease limits
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
When Click menu option "Deposit Limits"
Then Verify "Deposit Limits" as header displayed
Then Daily,Weekly and yearly options displayed to user
Then Set daily limit less than previously set limit
Then Verify updated deposit limit is displayed

@DesktopReg33 @DesktopHiddenLive55
Scenario: Check whether system do not apply changes deposit limits when user increases limits
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
When Click menu option "Deposit Limits"
Then Verify "Deposit Limits" as header displayed
Then Daily,Weekly and yearly options displayed to user
Then Increase the previously set deposit limit
And Verify deposit limit is displayed in Pending state

@DesktopReg33old @DesktopHiddenLiveold
Scenario: Check whether system displays error message when user enters  limit equal or smaller than next higher limits
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Deposit Limits"
Then Verify "Deposit Limits" as header displayed
Then Verify button having label as "£" displayed 
Then Verify button having label as "Set Limit" displayed 
Then Verify button having label as "Monthly" displayed 
Then Verify button having label as "Weekly" displayed 
Then Verify button having label as "Daily" displayed 
Then Click deposit limit button "Daily"
Then Click button having text "Reset limit"
Then Enter limit "1"
Then click button having label as "Set Limit"
Then Verify error message "Please enter an amount greater than 5"
Then Click button having text "Reset limit"
Then Enter limit "30"
Then Click deposit limit button "Weekly"
Then Enter limit "20"
Then Click deposit limit button "Monthly"
Then Click button having text "Reset limit"
Then Enter limit "10"
Then click button having label as "Set Limit"
Then Verify error message "Your weekly limit must be greater than your daily limit"
Then Verify error message "Your monthly limit must be greater than your daily limit"
Then Verify error message "Your monthly limit must be greater than your weekly limit"


@DesktopReg3301Old
Scenario: Responsible Gambling > Check whether system offers customers the opportunity to make use of Daily, Weekly and Monthly parameters while setting deposit limits
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Deposit Limits"
Then Verify "Deposit Limits" as header displayed
Then Verify button having label as "£" displayed 
Then Verify button having label as "Set Limit" displayed 
Then Verify button having label as "Monthly" displayed 
Then Verify button having label as "Weekly" displayed 
Then Verify button having label as "Daily" displayed 
Then Click deposit limit button "Daily"
Then Click button having text "Reset limit"
Then Enter limit "200"
Then Click deposit limit button "Weekly"
Then Click button having text "Reset limit"
Then Enter limit "300"
Then Click deposit limit button "Monthly"
Then Click button having text "Reset limit"
Then Enter limit "400"
Then click button having label as "Set Limit"
Then Verify message "Your limits have been updated successfully"


@DesktopReg3301Old @DesktopReg3301Oldression
Scenario: Check whether system changes deposit limits immediately when user decrese limits
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Deposit Limits"
Then Verify "Deposit Limits" as header displayed
Then Click deposit limit button "Monthly"
Then Reduce deposit limit by one
Then click button having label as "Set Limit"
Then Verify message "Your limits have been updated successfully"
Then click button having label as "Close"

@DesktopReg3301Old @DesktopReg3301Oldression
Scenario: Check whether system do not apply changes deposit limits when user increases limits
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Deposit Limits"
Then Verify "Deposit Limits" as header displayed
Then Click deposit limit button "Weekly"
Then Increase deposit limit by one
Then click button having label as "Set Limit"
Then Verify message "After 24 hours you will be able to activate your new limit."
Then click button having label as "Close"