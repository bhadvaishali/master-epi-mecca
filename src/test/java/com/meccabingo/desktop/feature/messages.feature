Feature: Messages

@DesktopReg
Scenario: Check whether system displays notification on the ‘My Account’ start screen if there is any new message in the inbox
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
#Then Click menu option "Messages"
Then Get current message count from my account screen
Then Navigate to new tab
Then Invoke the Web portal "https://cogs_staging.dagacube.net/Default.aspx"
#And Enter username as "Aishwarya" and password as "AishwaryaB1234@"
And Enter username as "Satya" and password as "1q2w3e4r%T"
Then Click on Sign In button
Then Select "Search Players" submenu from "Players" menu
#And Search players by username as "ZqEl80C1"
And Search players using username
Then Select "Message Template List" submenu from "Player Messages" menu
And Search Message "expleo mecca test message"
And Schedule Message
Then Back to Mecca site
Then Verify system displays notification on the My Account screen for new message in the inbox

@DesktopReg
Scenario: Check whether user can see a clear visual cue on the ‘Messages’ list screen if a message is unread
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on my account
Then Click menu option "Messages"
Then verify unread message icon is display for unread message

@DesktopReg
Scenario: Check whether unread message changes to read, when user reads the message
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on my account
Then Click menu option "Messages"
Then Click on unread Message from list of message
Then Verify message subject,sender name and message content is display on screen
Then Click on back button
Then verify unread message icon change into read icon

@DesktopReg
Scenario: Check whether system displays Body section for message single view
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Messages"
Then Verify delete icon displayed in disabled state
Then Open first message
Then Verify Live help link
Then Verify delete icon displayed
Then Verify message in detail
Then Verify message subject,sender name and message content is display on screen
And Verify close 'X' icon


@DesktopReg
Scenario: Check whether user able to delete message using Delete button from Message overlay
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Messages"
Then select message to delete
Then Click delete button
Then verify selected message gets delete


@DesktopReg
Scenario: Check whether system updates message count from Title section, Message sub menu icon and My account icon from header when user read / delete unread message
Then Navigate to new tab
Then Invoke the Web portal "https://cogs_staging.dagacube.net/Default.aspx"
#And Enter username as "Aishwarya" and password as "AishwaryaB1234@"
And Enter username as "Satya" and password as "1q2w3e4r%T"
Then Click on Sign In button
Then Select "Search Players" submenu from "Players" menu
#And Search players by username as "ZqEl80C1"
And Search players using username
Then Select "Message Template List" submenu from "Player Messages" menu
And Search Message "expleo mecca test message"
And Schedule Message
Then Back to Mecca site
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Get message count from header 
Then Click on my account
Then Get current message count from my account screen
Then Click menu option "Messages"
Then Get message count from Message header
Then Open first message
Then Verify that message count is reduced from my account icon ,Title section and Message sub menu