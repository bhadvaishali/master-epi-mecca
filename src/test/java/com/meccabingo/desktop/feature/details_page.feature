Feature: Details Page

@DesktopReg @DesktopHiddenLive
Scenario: Bingo Details page_Check whether system displays All fields on Bingo details page
Then Navigate through hamburger to "bingo" menu
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on info button of first game of "Bingo" section
Then Verify below fields on top section
|Title|
|Brief description|
|Pre-buy/Join Room CTA|
Then Verify backround image of game
Then Verify main image of game
Then Verify below fields on Bottom section
|Description|
|How To Play|
|Next game starts at|
|Others also played game section|

@DesktopReg @DesktopHiddenLive
Scenario: Game Details page_Check whether system displays All fields on Game details page
Then Navigate through hamburger to "slot" menu
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on info button of first game of "Slots & Games" section
Then Verify below fields on top section
|Title|
|Brief description|
|Play Now CTA|
Then Verify backround image of game
Then Verify main image of game
Then Verify below fields on Bottom section
|Description|
|How To Play|
|Game Information|
|Others also played game section|

@DesktopReg @DesktopHiddenLive
Scenario: Live casino Details page_Check whether system displays All fields on Live casino details page
Then Navigate through hamburger to "casino" menu
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on info button of first game of "Online Casino Games" section
Then Verify below fields on top section
|Title|
|Brief description|
|Play Now CTA|
Then Verify backround image of game
Then Verify main image of game
Then Verify below fields on Bottom section
|Description|
|How To Play|
|Game Information|
|Others also played game section|

@DesktopReg555
Scenario: Promotion Details page_Check whether system displays All fields on promotion details page
Then Navigate through hamburger to "casino" menu
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window