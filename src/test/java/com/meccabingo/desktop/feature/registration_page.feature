Feature: Registration Page

#EMC-59
@Desktop23
Scenario: Check whether all marketing preference checkboxes (Email, SMS, Phone and Post) are checked when user select 'Select All' CTA from marketing preference section
Given Invoke Mecca Site
Then Click on Join Now button
Then Enter email address 
Then Click on age checkbox
Then Click on Next button
Then Click on select all in marketing preference section
Then Verify following checkboxes are selected
|Email|
|SMS|
|Phone|
|Post|

@Desktop
Scenario: Check whether user able to select single marketing preference checkboxes (Email, SMS, Phone and Post) from marketing preference section
Given Invoke Mecca Site
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Click on email checkbox
Then Click on SMS checkbox
Then Click on phone checkbox
Then Click on post checkbox

#EMC-871
@Desktop
Scenario: Check whether Marketing preference checkboxes are displayed in red color and displays err below fields when user do not select any of the option from Marketing preference section and click on Register button
Given Invoke Mecca Site
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Click on Register CTA
Then Verify error message "Please select contact preference" is displayed in red color 

#EMC-863
@Desktop
Scenario: Check whether system displays green line below the username field when user enters username in valid format
Given Invoke Mecca Site
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Verify green line displays below username field

@Desktop
Scenario: Check whether system displays error message when user enters the existing username in username field
Given Invoke Mecca Site
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the existing username "automecca2020" in username field
Then Verify error message "The supplied username is already associated with a different user"

#EMC-61
@Desktop
Scenario: Check whether below fields are displayed on second step of registration journey:
- Title
- First name
- Surname
- Date of birth
Given Invoke Mecca Site
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Verify fields are displayed on page
|Title|
|First name|
|Surname|
|Date of birth| 
|Username|
|Password|

@Desktop1
Scenario: Check whether all titles are displayed on second step of registration journey:
Given Invoke Mecca Site
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Verify all titles are displayed on page
|Ms|
|Mr|
|Miss|
|Mrs|
|Dr|
|Mx|

@Desktop
Scenario: Check whether user able to select any option from Title section
Given Invoke Mecca Site
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Select any option from title section
Then Verify selected option gets highlighted

@Desktop
Scenario: Check whether green line is displayed when user enter the first name in correct format
Given Invoke Mecca Site
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the firstname in valid format
Then Verify green line displays below firstname field

@Desktop
Scenario: Check whether green line is displayed when user enter the surname in correct format
Given Invoke Mecca Site
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the surname in valid format
Then Verify green line displays below surname field

@Desktop
Scenario: Check whether green line is displayed when user enter the DOB in correct format
Given Invoke Mecca Site
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter Date of birth "11" "08" "1996"
Then Verify green line displays below DOB field


@Desktop
Scenario: Check whether system displays green line below the password field when user enters the password in correct format
Given Invoke Mecca Site
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the password in valid format
Then Verify green line displays below password field

#EMC-63
@Desktop
Scenario: Check whether Yes option is selected by default on first step registration page
Given Invoke Mecca Site
Then Click on Join Now button
Then Verify Yes option is selected

@Desktop
Scenario: Check whether green line is displayed below email address field when user enter the email address in correct format
Given Invoke Mecca Site
Then Click on Join Now button
Then Enter email address
Then Verify green line displays below email field

@Desktop
Scenario: Check whether system displays error message and displays Login screen when user enters existing Email address and click on Next button
Given Invoke Mecca Site
Then Click on Join Now button
Then Enter registered email address "emc685test@mailinator.com"
Then Click on disabled Next button
Then Verify error message "You must confirm you are 18 years or over and agree with the T&Cs"

@Desktop
Scenario: Check whether system displays Registration Page 2 when user enters valid email address n tick TnC check box and click on Next button
Given Invoke Mecca Site
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Verify select all button

#EMC-810
@Desktop
Scenario: Check whether error message displayed under respective fields when user clicks on next button on first step registration page
Given Invoke Mecca Site
Then Click on Join Now button
Then Click on disabled Next button
Then Verify error message "You must enter your Email Address"  
Then Verify error message "Please confirm you are at least 18 years old" 

@Desktop
Scenario: Check whether help text disappear and error message display when user enters invalid email address on first step registration screen and leave field
Given Invoke Mecca Site
Then Click on Join Now button
Then Enter invalid email address
Then Click on disabled Next button
Then Verify help text dissappears 
Then Verify error message "You must enter a valid email address"

#EMC-45
@Desktop
Scenario: Check whether system displays “Membership number sign up” screen with “Card membership number”, “Date of Birth” fields, "Terms & Conditions" checkbox, "Next" CTA on click of “No (I have a membership card)” CTA from First step of Registration page 
Given Invoke Mecca Site
Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Verify following option displays on “Membership number sign up” screen 
|Card membership number” field|
|Date of Birth fields|
|Terms & Conditions checkbox|
|Next button|

@Desktop
Scenario: Check whether Next Cta gets active when user enters membership number and DOB in correct format and selected “Terms & Conditions”  checkbox on “Membership number sign up” screen 
Given Invoke Mecca Site
Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Enter membership number "907276932"
Then Enter Date of birth "11" "08" "1996"
Then Click on age checkbox
Then Verify Next button gets enabled

#EMC-771
@Desktop
Scenario: Check whether system displays error message along with Login link when user enters existing membership number while registration
Given Invoke Mecca Site
Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Enter membership number "907276932"
Then Enter Date of birth "11" "08" "1996"
Then Click on age checkbox
Then Click on Next button
Then Verify error message "You already have an Account. Please enter your username and password to continue login"

#EMC-801 
@Desktop
Scenario: if membership user with membership details are not found with backend then  the system should display an error message below the “Card membership number” field
Given Invoke Mecca Site
Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Enter membership number "123456"
Then Enter Date of birth "11" "08" "2010" 
Then Click on age checkbox
Then Click on Next button
Then Verify error message "Unable to get customer details"

#EMC-770
@Desktop
Scenario: Check whether system displays Accoutn details and Registration Form for entered membership user when Number and DOB entered correctly
Given Invoke Mecca Site
Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Enter membership number "907276970"
Then Enter Date of birth "04" "04" "1995" 
Then Click on age checkbox
Then Click on Next button
#Then Verify 'Here are your account details' text
Then Verify title is displayed
Then Verify firstname is displayed
Then Verify surname is displayed
Then Verify date of birth is displayed
Then Verify "Not you?" link
Then Verify username field is displayed
Then Verify password field is displayed
Then Verify "Select All" button
Then Verify following checkboxes:
|Email|
|SMS|
|Phone|
|Post| 
Then Verify "I’d rather not receive offers and communications" checkbox
Then Verify "Would you like to set a deposit limit now?" section

@Desktop
Scenario: Check whether system navigate back to Registration page 1 when user click on “Not you?“ link from account detaisl section from Registration page 2
Given Invoke Mecca Site
Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Enter membership number "907276970"
Then Enter Date of birth "04" "04" "1995" 
Then Click on Next button
Then Click on Not you? link
Then Verify link navigate to 'Sign Up' page

@Desktop
Scenario: Check whether system correct validations for Email Address fields on Membership Registration page 2
Given Invoke Mecca Site
Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Enter membership number "907276970"
Then Enter Date of birth "04" "04" "1995" 
Then Click on Next button
Then Enter invalid email address
Then Verify help text ""
Then Verify grey line is displayed below email field
Then Enter valid email on second page
Then Verify green line is displayed below email field

@Desktop
Scenario: Check whether system correct validations for Mobile fields on Membership Registration page 2
Given Invoke Mecca Site
Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Enter membership number "907276970"
Then Enter Date of birth "04" "04" "1995" 
Then Click on Next button
Then Enter invalid mobile number
Then Verify help text "Mobile number"
Then Verify grey line is displayed below mobile number field
Then Enter valid mobile number
Then Verify green line is displayed below mobile number field

@Desktop
Scenario: Check whether system correct validations for Username fields on Membership Registration page 2
Given Invoke Mecca Site
Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Enter membership number "907276970"
Then Enter Date of birth "04" "04" "1995" 
Then Click on Next button
Then Enter invalid username
Then Verify help text "Please enter your username. Must be between 6-15 characters"
Then Verify grey line is displayed below username field
Then Enter valid username
Then Verify green line is displayed below username field

@Desktop
Scenario: Check whether system correct validations for Password fields on Membership Registration page 2
Given Invoke Mecca Site
Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Enter membership number "907276970"
Then Enter Date of birth "04" "04" "1995" 
Then Click on Next button
Then Enter invalid password
Then Verify help text "Please enter password"
Then Verify grey line is displayed below password field
Then Enter valid password
Then Verify green line is displayed below password field
