package com.meccabingo.desktop.stepDefinition;

import com.generic.utils.Utilities;
import com.meccabingo.desktop.page.Mecca.PayPalPage;

import io.cucumber.java.en.Then;

public class PayPalPageStep {

	private PayPalPage objPayPalPage;
	Utilities utilities;

	public PayPalPageStep(PayPalPage payPalPage) {

		this.objPayPalPage = payPalPage;
	}

	@Then("^Enter paypal id \"([^\"]*)\"$")
	public void enter_paypal_id(String strID) {
		objPayPalPage.enterPayPalID(strID);
	}

	@Then("^Click paypal next button$")
	public void click_paypal_next_button() {
		objPayPalPage.clickPayPalNext();
	}

	@Then("^Enter paypal password \"([^\"]*)\"$")
	public void enter_paypal_password(String strPassword) {
		objPayPalPage.enterPayPalPassword(strPassword);
	}

	@Then("^Click paypal login button$")
	public void click_paypal_login_button() {
		objPayPalPage.clickPayPalLogin();
	}

	@Then("^Click paypal confirm and pay button$")
	public void click_paypal_confirm_and_pay_button() {
		objPayPalPage.clickPayPalConfirm();
	}
}
