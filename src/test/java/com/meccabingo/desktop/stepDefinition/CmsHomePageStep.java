/**
 * 
 */
package com.meccabingo.desktop.stepDefinition;



import com.meccabingo.desktop.page.Mecca.CmsHomePage;

import io.cucumber.java.en.Then;


/**
 * @author Harshvardhan Yadav, Namrata Donikar (Expleo)
 *
 */
public class CmsHomePageStep {
	

	private CmsHomePage objCmsHomePage;
	
	public CmsHomePageStep(CmsHomePage cmsHomePage)
	{				
		this.objCmsHomePage = cmsHomePage;				
	}	
	
	@Then("^Verify login Successful$")
	public void verify_login_Successful() {
	    objCmsHomePage.verifyUserLoggedIn();
	}
	
	@Then("^Click on Toggle Navigation Panel$")
	public void Click_on_Toggle_Navigation_Panel() {
		objCmsHomePage.clickToggleNavigationPanel();
	}
	
	@Then("^Click on Content Configuration tab in Mecca Redesign$")
	public void click_on_Content_Configuration_tab_in_Mecca_Redesign(){
	    objCmsHomePage.clickOnContentConfiguration();
	}
	
	
}
