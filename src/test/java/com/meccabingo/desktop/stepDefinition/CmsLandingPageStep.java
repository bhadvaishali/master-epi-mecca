/**
 * 
 */
package com.meccabingo.desktop.stepDefinition;



import com.meccabingo.desktop.page.Mecca.CmsLandingPage;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;

/**
 * @author Harshvardhan Yadav, Namrata Donikar (Expleo)
 *
 */
public class CmsLandingPageStep {
	
	
	private CmsLandingPage objCmsLandingPage;
	
	public CmsLandingPageStep(CmsLandingPage cmsLandingPage)
	{	
		this.objCmsLandingPage = cmsLandingPage;		
				
	}
	
	@Given("^CMS site should be accessed$")
	public void cms_site_should_be_accessed() {
		//objDriverProvider.getWebDriver().get(objConfiguration.getConfig("web.cms.url"));
	    objCmsLandingPage.verifyCMSSite();
	}
	
	@When("^Login with valid userName \"([^\"]*)\" and valid password \"([^\"]*)\"$")
	public void login_with_valid_userName_and_valid_password(String userName, String password){
		objCmsLandingPage.enterUserName(userName);
		objCmsLandingPage.enterPassword(password);
		objCmsLandingPage.clickLogInButton();
	}

		
}
