package com.meccabingo.desktop.stepDefinition;

import java.util.List;

import com.generic.utils.Utilities;
import com.meccabingo.desktop.page.Mecca.DepositPage;
import com.meccabingo.desktop.page.Mecca.HomePage;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;

public class DepositPageStep {

	private DepositPage objDepositPage;
	private HomePage objHomePage;
	Utilities utilities;
	String balance,currentBalance,depAmt;
	public DepositPageStep(DepositPage depositPage,HomePage HomePage) {

		this.objDepositPage = depositPage;
		this.objHomePage = HomePage ;
	}

	@Then("^Click on Deposit button besides myaccount$")
	public void click_on_Deposit_button_besides_myaccount() {
		objDepositPage.clickOnDepositBesidesMyAcct();
	}


	@Then("^Select \"([^\"]*)\" payment method$")
	public void select_payment_method(String arg1){
		if(objDepositPage.verifyPOPFCheckbox())
		{
			objDepositPage.acceptPOPFChecbox();
			objDepositPage.clickOnNext();
		}
		objDepositPage.switchToDepositIframe();
		objDepositPage.selectPaymentMethod(arg1);
	}

	@Then("^Verify following errors are displayed when fields are empty$")
	public void verify_following_errors_are_displayed_when_fields_are_empty(DataTable dt){	
		objDepositPage.switchToCardIframe();
		objDepositPage.clickOnSubmit();
		List<String> list = dt.asList(String.class);
		for (int i = 0; i < list.size(); i++) {
			objDepositPage.verifyErrormessage(list.get(i));}
	}
	
	@Then("^Enter Card number \"([^\"]*)\"$")
	public void enter_Card_number(String strCNo) {
		objDepositPage.enterCardNumber(strCNo);
	}

	@Then("^Enter expiry month year as \"([^\"]*)\"$")
	public void enter_Expiry_moth_year(String strExpiry) {
		objDepositPage.enterExpiryMonthYear(strExpiry);
	}

	@Then("^Enter CVN as \"([^\"]*)\"$")
	public void enter_CVV(String strCVV) {
		objDepositPage.enterCVV(strCVV);
	}
	
	@Then("Again enter CVN as {string} and hit deposit button")
	public void again_enter_cvn_as_and_hit_deposit_button(String strCVV) {
		objDepositPage.switchToCVVFrame();
		objDepositPage.enterCVV(strCVV);
		objDepositPage.clickDepositButton();
	}
	
	
	@Then("Enter Deposit Amount as {string}")
	public void enter_deposit_amount_as(String strAmt) {
		depAmt = strAmt ;
		objDepositPage.enterDepositAmt(strAmt);
	}

	@Then("^Click on Deposit button from Deposit page$")
	public void click_on_Deposit_button_from_Deposit_page() {
		objDepositPage.clickDepositButton();
	}

	@Then("^Verify Deposit Successful confirmation message$")
	public void verify_Deposit_Successful_confirmation_message() {
		objDepositPage.verifyDepositSuccessMessages("Deposited Successfully");
		objDepositPage.verifyButton("Play Now");
		objDepositPage.clickOnButton("Play Now");
	}
	@Then("{string} message displayed on screen")
	public void message_displayed_on_screen(String msg) {
		objDepositPage.verifyDepositSuccessMessages(msg);
	}

	/*	@Then("^Click close button from deposit successful popup$")
	public void click_close_button_from_deposit_successful_popup() {
		objDepositPage.clickCloseFromDepositSuccessfulPopup();
	}

	@Then("^Click withdraw$")
	public void click_withdraw() {
		objDepositPage.clickWithdraw();
	}

	@Then("^Verify withdraw is successful$")
	public void verify_withdraw_is_successful() {
		objDepositPage.verifyWithdrawSuccessful();
	}

	@Then("^Click saved card \"([^\"]*)\"$")
	public void click_saved_card(String arg1) {
		objDepositPage.selectSavedCard(arg1);
	}

	@Then("^Click PayPal$")
	public void click_PayPal() {
		objDepositPage.clickPayPal();
	}

	@Then("^Click PaySafe$")
	public void click_PaySafe() {
		objDepositPage.clickPaySafe();
	}*/

	@And("^Verify POPF message on screen$")
	public void verify_POPF_message_on_screen() {
		objDepositPage.verifyPOPFCheckbox();}

	@And("^Get current balance from Deposit screen$")
	public void get_current_balance_from_Deposit_screen() {
		balance =  objHomePage.getBalancefromHeader();
		System.out.println("***** balance " +balance);
	}
	@And("^Click on Submit$")
	public void click_on_submit() {
		objDepositPage.clickOnSubmit();}
	
	@Then("^Click on \"([^\"]*)\" button from deposit screen$")
	public void click_on_button_from_deposit_screen(String btnName) {
		objDepositPage.clickOnButton(btnName);
	}
	@Then("^Verify Deposit amount is credited to account$")
	public void verify_Deposit_amount_is_credited_to_account() {
		currentBalance =  objHomePage.getBalancefromHeader();
	//	objDepositPage.validateBalanceDisplayed(balance);
		objDepositPage.validateBalanceAfterDeposit(balance,currentBalance,depAmt);
	}
}
