package com.meccabingo.desktop.stepDefinition;


import com.generic.WebActions;
import com.generic.utils.Configuration;
import com.generic.utils.Utilities;
import com.meccabingo.desktop.page.Mailinator.MailinatorPage;
import com.meccabingo.desktop.page.Mecca.LogInPage;

import io.cucumber.java.en.Then;


public class MailinatorPageSteps {

	private MailinatorPage objMailinatorPage;
	private Utilities objUtilities;
	private WebActions objWebActions;
	private LogInPage objLogInPage;
	private Configuration configuration;
	
	public MailinatorPageSteps(MailinatorPage mailinatorPage,Utilities objUtilities,WebActions objWebActions,LogInPage objLogInPage,Configuration objconfiguration) {		
		this.objMailinatorPage = mailinatorPage;
		this.objUtilities = objUtilities;
		this.objWebActions = objWebActions;
		this.objLogInPage = objLogInPage;
		this.configuration = objconfiguration;
	}

	@Then("^Invoke Mailinator site$")
	public void invoke_Mailinator_site(){
		//objMailinatorPage.invokeMailinatorSite();
	}


	@Then("^Sreach the mailbox of the \"([^\"]*)\" email address which associated with the username$")
	public void sreach_the_mailbox_of_the_email_address_which_associated_with_the_username(String arg1) {
		objMailinatorPage.verifyMailinatorLogoDisplayed();
		objMailinatorPage.setInboxName(arg1);
		objMailinatorPage.clickGO();
		//objMailinatorPage.verifyInboxDisplayed();
	}

	@Then("^Search email address which associated with the username$")
	public void sreach_email_address_which_associated_with_the_username() {
		configuration.loadConfigProperties();
		String emailId = configuration.getConfig("web.emailID");
		System.out.println("emailId " +emailId);
		objMailinatorPage.verifyMailinatorLogoDisplayed();
		objMailinatorPage.setInboxName(emailId);
		objMailinatorPage.clickGO();
		//objMailinatorPage.verifyInboxDisplayed();
	}
	
	@Then("^Verify User receive Username Reminder mail$")
	public void verify_User_receive_Username_Reminder_mail() {
		objMailinatorPage.verifyForgotUserNameMailReceived();
	}
	
	@Then("^Verify User receive Reset Password mail$")
	public void verify_User_receive_Reset_Password_mail() {
		objMailinatorPage.verifyForgotPasswordMailReceived();
	}
	
	@Then("^Verify User receive Welcome mail after successful registration$")
	public void verify_User_receive_Reset_Welcome_mail_after_successful_registration() {
		objMailinatorPage.verifyWelcomeMailReceivedOrNot();
	}
	@Then("^Open Email and Click on Password Reset link$")
	public void open_Email_and_Click_on_Password_Reset_link(){
		objMailinatorPage.openForgotPasswordMail();
		objWebActions.switchToFrameUsingNameOrId("html_msg_body");
		objMailinatorPage.clickResetPasswordButtonFromMail();
		objWebActions.switchToChildWindow(); 
	}
	
	@Then("^Verify \"([^\"]*)\" username is displayed in received mail$")
	public void verify_username_displayed_in_receive_mail(String username){
		objMailinatorPage.openForgotUsernameMail();
		//objWebActions.switchToFrameUsingNameOrId("html_msg_body");
		objMailinatorPage.validateUsernameinEmail(username);
	}
	@Then("^Open Received Email$")
	public void open_Email(){
		objMailinatorPage.openWelcomeMail();
	}
	
	@Then("^Verify Username and Login cta in the email$")
	public void verify_Username_and_Login_cta_in_the_email()  {
		objMailinatorPage.verifyUsernameAndLoginLinkInSuccessfulRegistrationEmail(configuration.getConfig("web.userName"),configuration.getConfig("web.Url"));}
	
	@Then("^Verify Reset Password screen is displayed$")
	public void verify_Reset_Password_screen_is_displayed()  {
		objLogInPage.verifyResetPasswordHdrDisplayed();
		
	}

	@Then("^Enter invalid passwords and hit submit button$")
	public void enter_invalid_passwords_and_hit_submit_button() {
		objLogInPage.setNewPassword("sfd");
		//objLogInPage.clickSubmitForNewPassword();
	}
}
