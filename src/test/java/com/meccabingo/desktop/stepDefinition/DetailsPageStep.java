package com.meccabingo.desktop.stepDefinition;


import java.util.List;

import com.generic.utils.Utilities;
import com.meccabingo.desktop.page.Mecca.DetailsPage;

import io.cucumber.java8.En;

public class DetailsPageStep implements En {

	private DetailsPage detailsPage;
	private Utilities utilities;
	
	public DetailsPageStep(DetailsPage detailsPage, Utilities utilities) {
		this.detailsPage = detailsPage;
		this.utilities = utilities;
		Then("Navigate to Live Casino Page", () -> this.detailsPage.navigateToLiveCasinoPgae());

		Then("Verify login details page of game {string}",
				(String nameofgame) -> this.detailsPage.verifyLoginDetailsPage(nameofgame));

		Then("Verify title of the game of game {string}",
				(String nameofgame) -> this.detailsPage.verifyTitleOfGame(nameofgame));

		Then("Verify brief description", () -> this.detailsPage.verifyBriefDescription());

		Then("Verify backround image of game", () -> this.detailsPage.verifyBackroundImageOfGame());

		Then("Verify main image of game", () -> this.detailsPage.verifyMainImageOfGame());

		Then("Verify information box of game", () -> this.detailsPage.verifyInformationBoxOfGame());

		Then("Scroll up the box", () -> this.detailsPage.scrollUpTheBox());

		Then("Scroll down the box", () -> this.detailsPage.scrollDownTheBox());

		Then("Click on join now from details page", () -> this.detailsPage.clickOnJoinNowfromDetailsPage());

		Then("Verify game launches in new window", () -> this.detailsPage.verifyGameLaunchesInNewWindow());

		Then("Verify {string} in information box", (String text) -> this.detailsPage.verifyTextInInformationBox(text));

		Then("Verify join now button in information box", () -> this.detailsPage.verifyJoinNowButtonInInformationBox());

		Then("Verify Next game starts at in information box", () -> this.detailsPage.verifyNextGameStartsAtText());

		Then("Verify AVAILABLE ON in information box", () -> this.detailsPage.verifyAVAILABLEONText());

		Then("Hove on i button of {string}", (String type) -> this.detailsPage.hoverOnIButton(type));

		Then("Verify help text {string} for {string}", (String helptext,String type) -> this.detailsPage.verifyHelpText(helptext, type));

		Then("Verify below fields on top section", (io.cucumber.datatable.DataTable dt) -> {
			System.out.println("*******************  "+utilities.getListDataFromDataTable(dt).size());
			List<String> list = dt.asList(String.class);
			for (int i = 0; i < list.size(); i++) {
				detailsPage.verifyFieldsOnDetailsPage(list.get(i));
			}
		});

		Then("Verify below fields on Bottom section", (io.cucumber.datatable.DataTable dt) -> { 
			System.out.println("*******************  "+utilities.getListDataFromDataTable(dt).size());
			List<String> list = dt.asList(String.class);
			for (int i = 0; i < list.size(); i++) {
				detailsPage.verifyFieldsOnDetailsPage(list.get(i));
			}
		});

	}
}
