package com.meccabingo.desktop.stepDefinition;

import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.meccabingo.desktop.page.Mecca.GameWindow;
import com.meccabingo.desktop.page.Mecca.Games;

import io.cucumber.java.en.Then;
import io.cucumber.java8.En;

public class GamesStep implements En {

	private Games games;
	private GameWindow gameWindow;

	public GamesStep(Games games, GameWindow gameWindow) {
		this.games = games;
		this.gameWindow = gameWindow;

		Then("Click on image of the hero tile {string}", (String nameofthegame) -> {
			gameWindow.clickOnImageOfHerotile(nameofthegame);
		});

		Then("Hover on i of the hero tile {string}", (String nameofthegame) -> {
            games.hoverOnIbtnOfHerotile(nameofthegame);
		});

		Then("User clicks on i button of hero tile {string}", (String nameofthegame) -> {
            gameWindow.clickOnibtnOfHeroTile(nameofthegame);
		});

		Then("Click on jackpot flag of the hero tile {string}", (String nameofthegame) -> {
		
		});

		Then("Click on price of the hero tile {string}", (String nameofthegame) -> {
			gameWindow.clickOnPrizeofHerotile();
		});

		Then("Click on title of the hero tile {string}", (String nameofthegame) -> {
			gameWindow.clickOnTitleOfHerotile(nameofthegame);
		});

		Then("Click on description of the hero tile {string}", (String nameofthegame) -> {
			gameWindow.clickOnDescriptionOfHerotile(nameofthegame);
		});

		Then("Click on image of the bingo tile {string}", (String nameofthegame) -> {
	
		});

		Then("Click on title of the bingo tile {string}", (String nameofthegame) -> {
			
		});

		Then("Hover on i of the bingo tile {string}", (String nameofthegame) -> {
			gameWindow.clickOnibtnOfGameTile(nameofthegame);
		});

		Then("User clicks on i button of bingo tile {string}", (String nameofthegame) -> {
			gameWindow.clickOnibtnOfGameTile(nameofthegame);
		});

		Then("Verify background image of {string} has the opacity layer applied in game details page",
				(String nameofthegame) -> {
					games.verifyOpacityOfImageOnGameDetailsPage(nameofthegame);
				});
	}
	@Then("^Verify device navigate user to containing url \"([^\"]*)\"$")
	public void verify_device_navigate_user_to_containing_url(String url) {
		games.verifyContainsUrl(url);
	}
	
	@Then("^Click on Join Now button of first game of bingo section$")
	public void click_on_Join_Now_button_of_first_game_of_bingo_section() {
		games.clickJoinNowOfFirstBingoGame();
	}
	
	@Then("^Click on info button of first game of bingo section$")
	public void click_on_info_button_of_first_game_of_bingo_section() {
		games.clickInfoOfFirstBingoGame();
	}
	
	/**@Then("^Click on image of the game \"([^\"]*)\"$")
	public void click_on_image_of_the_game(String nameofthegame) {
		games.clickOnImageOfTile(nameofthegame);
	}
	
	@Then("^Click on title of the game \"([^\"]*)\"$")
	public void click_on_title_of_the_game(String nameofthegame) {
		games.clickOnTitleoftheGame(nameofthegame);
	}*/
	
	@Then("^user should remain on bingo container page$")
	public void user_should_remain_on_bingo_container_page() {
		games.bingoContainerSectionDisplayed();
	}
	
	@Then("^click on top flag of bingo game$")
	public void click_on_top_flag_of_bingo_game() {
		games.clickTopFlagOfBingoGame();
	}
}
