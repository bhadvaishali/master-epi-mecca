/**
 * 
 */
package com.meccabingo.desktop.stepDefinition;

import java.util.List;

import com.generic.utils.Utilities;
import com.meccabingo.desktop.page.Mecca.HomePage;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

/**
 * @author Harshvardhan Yadav, Namrata Donikar (Expleo)
 *
 */
public class HomePageStep {

	private HomePage objHomePage;
	private Utilities objUtilities;

	public HomePageStep(HomePage objHomePage, Utilities objUtilities) {
		this.objHomePage = objHomePage;
		this.objUtilities = objUtilities;
	}

	@Given("^Invoke the Mecca site on Desktop$")
	public void invoke_the_Mecca_site_on_Desktop() {
		//objHomePage.verifyHeaderLogo();
	}

	@Then("^Navigate to Social media Block in footer$")
	public void navigate_to_Social_media_Block_in_footer() {
		objHomePage.scrollToFooterSocial();
	}

	@When("^Resize browser window$")
	public void resize_browser_window() {
		objHomePage.setBrowserSize("600", "950");
	}

	@Then("^Verify social media block resizes to fit the screen$")
	public void verify_social_media_block_resizes_to_fit_the_screen() {
		objHomePage.verifySocialMediaBlockSize();
	}

	@When("^Resize browser window to reach mobile breakpoint$")
	public void resize_browser_window_to_reach_mobile_breakpoint() {
		objHomePage.setBrowserSize("375", "812");
	}

//	@Then("^Navigate to useful link block in footer$")
//	public void navigate_to_useful_link_block_in_footer() {
//	    //objHomePage.scrollToFooterUsefullLink();
//	}
//	
//	@Then("^Navigate to partners logo section in footer$")
//	public void navigate_to_partners_logo_section_in_footer() {
//	   //objHomePage.scrollToPartnersLogo();
//	}
//	
//	@Then("^Navigate to privacy and security block in footer$")
//	public void navigate_to_privacy_and_security_block_in_footer(){
//		 //objHomePage.scrollToprivacyAndSecurityBlock();
//	}
//	
//	@When("^Navigate to secure payments block in footer$")
//	public void navigate_to_secure_payments_block_in_footer()  {
//		//objHomePage.scrollToPaymentProvidersBlock();
//	}
//	
//	@When("^Navigate to rank group block in footer$")
//	public void navigate_to_rank_group_block_in_footer() {
//	   // this.navigate_to_privacy_and_security_block_in_footer();
//	}

	@When("^User clicks anywhere on the slider/carousel$")
	public void user_clicks_anywhere_on_the_slider_carousel() {
		objHomePage.clickOnSlider();
	}

	@When("^User clicks on Login Button from header of the Page$")
	public void user_clicks_on_Login_Button_from_header_of_the_Page() {
		objHomePage.clickOnLogInButton();
	}

	@Then("^Verify user is successfully logged in$")
	public void Verify_user_is_successfully_logged_in() {
		objHomePage.verifyMyAccount();
	}

	@Then("^Refresh the window$")
	public void Refresh_the_window() {
		objHomePage.windowRefresh();
	}

	@Then("^Verify header displays join now button$")
	public void verify_header_displays_join_now_button() {
		objHomePage.verifyHeaderJoinNowbtn();

	}
	

	@Then("^Verify card is displayed in battenberg block$")
	public void verify_card_is_displayed_in_battenberg_block() {
		objHomePage.verifybattenberg();
	}

	@Then("^Verify blocks contain one row$")
	public void verify_blocks_contain_one_row() {
		objHomePage.verifyBlockInOneRow();
	}

	@Then("^Verify all three blocks$")
	public void verify_all_three_blocks() {
		objHomePage.verifyThreeBlock();
	}

	@Then("^Verify header displays Not a member yet\\? Register here link$")
	public void verify_header_displays_Not_a_member_yet_Register_here_link() {
		objHomePage.verifyRegisterNowLink();

	}

	@Then("^Verify system displays user balance section at header$")
	public void verify_system_displays_user_balance_section_at_header() {
		objHomePage.verifyUserBalanceSection();
	}

	@Then("^Verify system displays balance Title and correct amount under Balance section$")
	public void verify_system_displays_balance_Title_and_correct_amount_under_Balance_section() {
		objHomePage.verifyUserBalanceTitle();
		objHomePage.verifyUserBalanceAmount();
	}

	@Then("^Click on user balance title from header$")
	public void click_on_user_balance_title_from_header() {
		objHomePage.clickOnUserBalanceTitle();
	}

	@Then("^Click on balance toggle$")
	public void click_on_balance_toggle() {
		objHomePage.clickOnUserBalanceToggle();
	}

	@Then("^Click on Balance title when balance not shown$")
	public void click_on_Balance_title_when_balance_not_shown() {
		objHomePage.clickOnUserBalanceTitle();

	}

	@Then("^Click on five dots when balance not shown$")
	public void click_on_five_dots_when_balance_not_shown() {
		objHomePage.clickOnUserBalanceDots();
	}

	@Then("^Click on play now button from searched list$")
	public void click_on_play_now_button_from_searched_list() {
		objHomePage.clickOnPlayNowButton();
	}

	@Then("^Verify game launches successfully$")
	public void verify_game_launches_successfully() {
		objHomePage.verifyBackroungimageInGame();
	}

	@Then("^Verify header displays following components :$")
	public void verify_header_displays_following_components(DataTable dt) {
		for (int i = 0; i < objUtilities.getListDataFromDataTable(dt).size(); i++) {
			objHomePage.verifyHeaderComponents(objUtilities.getListDataFromDataTable(dt).get(i));
		}
	}

	@Then("^Navigate to invalid url$")
	public void navigate_to_invalid_url() {
		objHomePage.NavigateToInvalidUrl();
	}

	@Then("^Verify error title$")
	public void verify_error_title() {
		objHomePage.verifyErrorTitle();
	}

	@Then("^Verify cookies text$")
	public void verify_cookies_text() {
		objHomePage.verifyCookiesText();
	}

	@Then("^Verify continue button$")
	public void verify_continue_button() {
		objHomePage.verifyCookiesButton();
	}
	@When("^Navigate to footer section$")
	public void navigate_to_footer_section()  {
		objHomePage.scrollToprivacyAndSecurityBlock();
	}

	@Then("^Click on top arrow anchor$")
	public void click_on_top_arrow_anchor()  {
	   objHomePage.clickOnTopArrowAnchor();
	}
	@Then("^Verify following tab is displayed under \"([^\"]*)\" secondary navigation bar$")
	public void verify_following_tab_is_displayed_under_secondary_navigation_bar(String sectionName,DataTable dt) {
		List<String> list = dt.asList(String.class);
		for (int i = 0; i < list.size(); i++) {
			objHomePage.verifySubHeaderTabs(sectionName,list.get(i));}
	}
	
	@Then("^Click on Deposit button from Header$")
	public void click_on_Deposit_button_from_Header()  {
	   objHomePage.clickOnDepositLinkFromHeader();
	}
	
}
