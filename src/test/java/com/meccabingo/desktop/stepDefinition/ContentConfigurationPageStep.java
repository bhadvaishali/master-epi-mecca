/**
 * 
 */
package com.meccabingo.desktop.stepDefinition;

import com.generic.utils.Utilities;
import com.meccabingo.desktop.page.Mecca.ContentConfigurationPage;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;

/**
 * @author Harshvardhan Yadav, Namrata Donikar (Expleo)
 *
 */
public class ContentConfigurationPageStep {

	private Utilities objUtilities;	
	private ContentConfigurationPage objContentConfigurationPage;

	public ContentConfigurationPageStep(Utilities utilities, ContentConfigurationPage contentConfigurationPage) {
		this.objUtilities = utilities;		
		this.objContentConfigurationPage = contentConfigurationPage;
	}

	@Then("^Click on Configuration$")
	public void click_on_Configuration() {
		objContentConfigurationPage.clickConfigurationTab();
	}

	@Then("^Navigate to social media$")
	public void navigate_to_social_media() {
		objContentConfigurationPage.editSocialMediaFooterContent();
	}

	@Then("^Verify following are configured$")
	public void verify_following_are_configured(DataTable dt) {
		
		for (int i = 0; i < objUtilities.getListDataFromDataTable(dt).size(); i++) {
			
			System.out.println(objUtilities.getListDataFromDataTable(dt).get(i));			
			objContentConfigurationPage.verifySocialMediaOptionConfigured(objUtilities.getListDataFromDataTable(dt).get(i));
			
		}
	}

	@Then("^Verify following things are availble in configuration for social media block:$")
	public void verify_following_things_are_availble_in_configuration_for_social_media_block(DataTable dt) {

		for (int i = 0; i < objUtilities.getListDataFromDataTable(dt).size(); i++) {			
						
			objContentConfigurationPage.verifySocialMediaConfigurationBlocks(objUtilities.getListDataFromDataTable(dt).get(i));
			
		}
	}

	@Then("^Navigate to Payment providers$")
	public void navigate_to_Payment_providers() {
		objContentConfigurationPage.editPaymentProvidersFooterContent();
	}

	@Then("^Add logos in Payment providers$")
	public void add_logos_in_Payment_providers() {

	}

	@Then("^Verify user able to add logos for Payment providers$")
	public void verify_user_able_to_add_logos_for_Payment_providers() {

	}
}
