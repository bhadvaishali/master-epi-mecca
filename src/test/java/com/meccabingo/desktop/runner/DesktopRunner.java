
package com.meccabingo.desktop.runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
//import io.cucumber.junit.CucumberOptions;

@CucumberOptions(features = "src/test/java/com/meccabingo/desktop/feature/responsibleGambling.feature", strict = true, plugin = {"summary","json:target/JSON/Desktop.json",
		"rerun:target/SyncFails.txt", /*"com.generic.cucumberReporting.CustomFormatter"*/ }, 
		tags = "@DesktopReg55", 
		dryRun = false, monochrome = true,
		glue = { "com.hooks", "com.meccabingo.desktop.stepDefinition" }
)

public class DesktopRunner extends AbstractTestNGCucumberTests {
/*	@Override
    @DataProvider(parallel = true)
    public Object[][] scenarios() {
        return super.scenarios();
    }*/
}	
