
package com.meccabingo.desktop.runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(features = "@target/SyncFails.txt", 
				strict = true, 
				plugin = { "json:target/JSON/DesktopRerun.json", /*"com.generic.cucumberReporting.CustomFormatter"*/}, 		
				monochrome = true,
				glue = { "com.hooks",	"com.meccabingo.desktop.stepDefinition" }
		)

public class DesktopRunner_Rerun extends AbstractTestNGCucumberTests {
}