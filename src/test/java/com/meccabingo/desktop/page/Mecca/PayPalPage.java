package com.meccabingo.desktop.page.Mecca;

import org.openqa.selenium.By;

import com.generic.WebActions;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.logger.LogReporter;
import com.generic.webdriver.DriverProvider;

public class PayPalPage {
	WebActions objWebActions;
	private DriverProvider objDriverProvider;
	private LogReporter logReporter;

	public PayPalPage(DriverProvider driverProvider, WebActions webActions,
			LogReporter logReporter) {
		this.objDriverProvider = driverProvider;
		this.objWebActions = webActions;
		this.logReporter = logReporter;
	}

	public void enterPayPalID(String strID) {
		By locator = By.id("email");
		if (objWebActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("enter paypal id> >", objWebActions.setText(locator, strID));
	}

	public void clickPayPalNext() {
		By locator = By.id("btnNext");
		if (objWebActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("Click paypal next button> >", objWebActions.click(locator));
	}

	public void enterPayPalPassword(String strPassword) {
		By locator = By.id("password");
		if (objWebActions.checkElementDisplayedWithMidWait(locator)) {
			objWebActions.invokeOnLocator(locator, "clearText");
			objWebActions.setText(locator, "");
			logReporter.log("enter paypal Password> >", objWebActions.setText(locator, strPassword));
		}
	}

	public void clickPayPalLogin() {
		By locator = By.id("btnLogin");
		if (objWebActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("Click paypal login button> >", objWebActions.click(locator));
	}

	public void clickPayPalConfirm() {
		By locator = By.id("confirmButtonTop");
		if (objWebActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("Click paypal confirmButtonTop button> >", objWebActions.click(locator));
	}
}
