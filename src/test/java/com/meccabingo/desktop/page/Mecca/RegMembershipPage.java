package com.meccabingo.desktop.page.Mecca;

import org.openqa.selenium.By;

import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.utils.Utilities;
import com.generic.utils.WaitMethods;
import com.mifmif.common.regex.Generex;

public class RegMembershipPage {
	private WebActions webActions;
	private LogReporter logReporter;
	private Utilities utilities;
	private WaitMethods wait;

	public RegMembershipPage(WebActions webActions, LogReporter logReporter, WaitMethods wait, Utilities utilities) {
		this.webActions = webActions;
		this.logReporter = logReporter;
		this.utilities = utilities;
		this.wait = wait;

	}
	
	
	String greencolor = "#009D7A";
	String greycolor = "#5ECCF3";
	String redcolor = "#B41D68";
	By locator = By.xpath("");
	
	
	By title = By.xpath("//li/span/p[contains(text(),'Mr')]");
	By titletext = By.xpath("//li/span/p[contains(text(),'Title')]");
	By name = By.xpath("//li/span/p[contains(text(),'John Ray')]");
	By nametext = By.xpath("//li/span/p[contains(text(),'Name')]");
	By DOB = By.xpath("//li/span/p[contains(text(),'5th June 1996')]");
	By dateofbirthtext = By.xpath("//li/span/p[contains(text(),'Date of Birth')]");
	
	By username = By.xpath("//input[contains(@id,'username')]");
	By password = By.xpath("//input[contains(@id,'password')]");
	By selectAll = By.xpath("//button[contains(@class,'colour-button-green')]");
	By email = By.xpath("//input[contains(@id,'gdpr-email')]");
	By sms = By.xpath("//input[contains(@id,'gdpr-sms')]");
	By phone = By.xpath("//input[contains(@id,'gdpr-phone')]");
	By post = By.xpath("//input[contains(@id,'gdpr-post')]");
	By offersandcommunication = By.xpath("//label[text()=\"I’d rather not receive offers and communications\"]");
	By depositlimit = By.xpath("");
	By MPC = By.xpath("//input[contains(@id,'gdpr-not-receive')]");
	By register = By.xpath("//button/span[contains(text(),'Register')]");
	By successmessage = By.xpath("");
	By EmailAdrress = By.xpath("//input[contains(@type,'email')]");
	
	By errorofemail = By.xpath("//ul[@class='errors']/li[text()='You must enter a valid email address']");
	By errorofmobile = By.xpath("//ul[@class='errors']/li[text()='Please enter a valid phone number between 8 and 11 digits long']");
	By errorofusername = By.xpath("//ul[@class='errors']/li[text()='Your username must be between 6-15 characters long.']");
	By errorofpassword = By.xpath("");
	By errorofmarketingpreferencefield = By.xpath("//ul[@class='errors']/li[text()='Please select your contact preferences ']");
	By errorofpostcode = By.xpath("//ul[@class='errors']/li[text()='Please enter your postcode']");
	By countryname = By.xpath("");
	By postcodename = By.xpath("");
	By gibcountry = By.xpath("");
	By agecheckboxofmembership = By.xpath("//*[@id=\"id-agreed\"]");
	
	
	public void verify() {
		logReporter.log("", webActions.checkElementDisplayed(locator));
	}  
	public void clickOn() {
		logReporter.log("", webActions.click(locator));
	}	
	public void verifyTitle() {
		logReporter.log("", webActions.checkElementDisplayed(title));
		logReporter.log("", webActions.checkElementDisplayed(titletext));
	}
	public void verifyName() {
		logReporter.log("", webActions.checkElementDisplayed(name));
		logReporter.log("", webActions.checkElementDisplayed(nametext));
	}
	
	public void verifyDateofBirth() {
		logReporter.log("", webActions.checkElementDisplayed(DOB));
		logReporter.log("", webActions.checkElementDisplayed(dateofbirthtext));
	}
	
	public void verifyUsernameTextbox() {
		logReporter.log("", webActions.checkElementDisplayed(username));
	}
	public void verifyPasswordTextbox() {
		logReporter.log("", webActions.checkElementDisplayed(password));
	}
	public void verifySelectAllButton() {
		logReporter.log("", webActions.checkElementDisplayed(selectAll));	
	}
	public void verifyCheckboxes(String options) {
		switch (options) {
		case "Email":
			logReporter.log("", webActions.checkElementDisplayed(email));
			break;
		case "SMS":
			logReporter.log("", webActions.checkElementDisplayed(sms));
			break;
		case "Phone":
			logReporter.log("", webActions.checkElementDisplayed(phone));
			break;
		case "Post":
			logReporter.log("", webActions.checkElementDisplayed(post));
			break;

		}

	}
	public void verifyOffersCheckbox() {
		logReporter.log("", webActions.checkElementDisplayed(offersandcommunication));
	} 
	public void verifyDepositLimitSection() {
		logReporter.log("", webActions.checkElementDisplayed(depositlimit));
	}  
	public void clickOnSelectAllCTA() {
		logReporter.log("", webActions.click(selectAll));
	}
	public void clickOnMarketingPreferenceCheckbox() {
		logReporter.log("", webActions.click(MPC));
	}	
	public void clickOnRegisterButton() {
		logReporter.log("", webActions.click(register));
	}
	public void verifySuccessMessage() {
		logReporter.log("", webActions.checkElementDisplayed(successmessage));
	}
	public void verifyCheckboxIsNotSelected() {
		if (!webActions.selectCheckbox(email, false)) {
			logReporter.log("Check element", true);
		}
	}  
	public void verifyRedColorBelowEmailAdrress() {
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(EmailAdrress, "border-bottom-color", redcolor));
	}
	public void enterInvalidEmailAdrress() {
		logReporter.log("enter invalid email", webActions.setText(EmailAdrress, "20ehc.mailianator.com"));
	}
	public void verifyErrorMessageBelowEmailAdrress() {
		logReporter.log("", webActions.checkElementDisplayed(errorofemail));
	}
	public void verifyErrorMessageBelowEmailAdrressInRedColor() {
		logReporter.log("", webActions.checkCssValue(errorofemail, "color", redcolor));
	}
	public void verifyErrorMessageBelowMobilenumber() {
		logReporter.log("", webActions.checkElementDisplayed(errorofmobile));
	}
	
	public void verifyErrorMessageBelowUsername() {
		logReporter.log("", webActions.checkElementDisplayed(errorofusername));
	}
	public void verifyErrorMessageBelowPassword() {
		logReporter.log("", webActions.checkElementDisplayed(errorofpassword));
	}
	public void enterExistingUsername(String name) {
		logReporter.log("", webActions.setTextWithClear(username, name));
	}
	public void verifyErrorMessageBelowMarketingPreferenceField() {
		logReporter.log("", webActions.checkElementDisplayed(errorofmarketingpreferencefield));
	}
	public void verifyErrorMessageBelowPostcode() {
		logReporter.log("", webActions.checkElementDisplayed(errorofpostcode));
	}
	public void verifyCountryNameIsDisplayed() {
		logReporter.log("", webActions.checkElementDisplayed(countryname));
	}
	public void verifyPostcodeIsDisplayed() {
		logReporter.log("", webActions.checkElementDisplayed(postcodename));
	}
	public void clickOnGibRoiCountry() {
		By option = By.xpath("//option[text()='Gibraltar']");
		logReporter.log("Select country", webActions.click(option));
	}
	public void clickOnAgeCheckboxofMembership() {
		logReporter.log("", webActions.clickUsingJS(agecheckboxofmembership));
	}
	public void clickOnNotYouLink() {
		By notyou = By.xpath("//a[contains(text(),'Not You')]");
		logReporter.log("click on Not You link > >", webActions.click(notyou));
	}
	
	public void clickOnDisableRegisterCTA() {
		By register = By.xpath("//button[contains(.,'Register')]");
		logReporter.log("click on post checkbox > >", webActions.clickOnDisabledElement(register));
	}
	
	public void verifyPostcodeField() {
		By postcode = By.xpath("//input[contains(@id,'address-lookup-search')]"); 
		logReporter.log("verify countrys field", webActions.checkElementDisplayed(postcode));
	}
	
	public void verifyLink(String text) {
		By locator = By.xpath("//a[contains(text(),'"+text+"')]");
		logReporter.log("Verify link", webActions.checkElementDisplayed(locator));
	}
	
	public void ClickOnLink(String text) {
		By locator = By.xpath("//a[contains(text(),'"+text+"')]");
		logReporter.log("Verify link", webActions.click(locator));
	}
	public void verifyRedColorBelowMobileNumber() {
		By mobilenumber = By.xpath("//input[contains(@id,'mobile')]");
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(mobilenumber, "border-bottom-color", "#E22C2C"));
	}
	
	public void enterInvalidMobileNumber() {
		By mobilenumber = By.xpath("//input[contains(@id,'mobile')]");
		String regex = "[a-zA-Z]{8}";
		String randomnumber = new Generex(regex).random();
		logReporter.log("enter mobile number", webActions.setText(mobilenumber, randomnumber));
	}
	
	public void verifyRedColorBelowUsername() {
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(username, "border-bottom-color", redcolor));
	}
	
	public void verifyFieldUnderYourPersonalDetailsSection(String fieldNm)
	{
		By locator = By.xpath("//h5[contains(.,'Your personal details')]//following-sibling::li//span//p[text()='"+fieldNm+"']");
		logReporter.log("Verify ' "+fieldNm+ " ' is displayed under Your Personal Details Section", webActions.checkElementDisplayed(locator));
	}
}
