package com.meccabingo.desktop.page.Mecca;

import org.openqa.selenium.By;

import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.webdriver.DriverProvider;

public class PaySafePage {
	WebActions objWebActions;
	private DriverProvider objDriverProvider;
	private LogReporter logReporter;

	public PaySafePage(DriverProvider driverProvider, WebActions webActions,
			LogReporter logReporter) {
		this.objDriverProvider = driverProvider;
		this.objWebActions = webActions;
		this.logReporter = logReporter;
	}

	public void enterPaySafeAccount(String strAcc) {
		By locator = By.xpath("//input[contains(@class,'input-pin')]");
		if (objWebActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("enter acc> >", objWebActions.setText(locator, strAcc));
	}

	public void clickPaySafeAgree() {
		By locator = By.xpath("//div[contains(@id,'acceptTerms')]//input");
		By addPin = By.xpath("//*[@id='toggleTopUpInput']");
		if (objWebActions.checkElementDisplayedWithMidWait(addPin))
		logReporter.log("click agree > >", objWebActions.clickUsingJS(locator));
	}
	
	public void clickPaySafePay() {
		By locator = By.xpath("//span[text()='Pay']");
		logReporter.log("click spaysafe > >", objWebActions.click(locator));
		//objWebActions.switchToParentWindow();
	}

}
