package com.meccabingo.desktop.page.Mecca;

import org.openqa.selenium.By;
import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.utils.WaitMethods;

public class GameWindow {

	By exitarrowbtn = By.xpath("//div[contains(@class,'game-window-header')]/ul/li/a");
	By sessiontime = By.xpath("//div[contains(@class,'game-window-header')]/ul/span/li[2]/i");
	By playforrealbtn = By.xpath("//button[contains(.,'Free Play')]");
	By titleofthegame = By.xpath("//div[contains(@class,'game-window-header')]/ul/span/li[3]");
	By expandbtn = By.xpath("//div[contains(@class,'game-window-header')]/ul/span/li[4]/a/i");
	By reducebtn = By.xpath("//div[contains(@class,'game-window-header')]/ul/span/li[4]/a/i");
	By depositbtn = By.xpath("//div[contains(@class,'game-window-header')]/ul/span/li[5]");
	By myaccountbtn = By.xpath("//div[contains(@class,'game-window-header')]/ul/li[2]");
	By sessiontimewithmmss = By.xpath("//div[contains(@class,'game-window-header')]/ul/span/li[2]/span");
	By plyNowbtnOnGamedetailsPage = By.xpath("(//button[text()='Play Now'])[1]");
	private WebActions webActions;
	private LogReporter logReporter;
	private WaitMethods wait;

	private By getPlayNowBtn(String nameofthegame) {
		return By.xpath("//p[contains(.,'" + nameofthegame
				+ "')]/parent::div/div[contains(@class,'game-panel-buttons')]/apollo-play-game-cta/button[contains(.,'Play Now')]");
	}

	public GameWindow(WebActions webActions, LogReporter logReporter,WaitMethods wait) {
		this.webActions = webActions;
		this.logReporter = logReporter;
		this.wait = wait;
	}

	public void clickOnPlayNow(String nameofthegame) {
		By playNowbtn = getPlayNowBtn(nameofthegame);
		webActions.checkElementDisplayedWithMidWait(playNowbtn);
		logReporter.log("check element > >", webActions.click(playNowbtn));

	}

	public void clickOnibtnOfHeroTile(String nameofthegame) {
		By ibtn = By.xpath("//apollo-bingo-tile[contains(@game-type,'"+nameofthegame+"')]//i[contains(@class,'info colour')]");
		logReporter.log("click on 'i' > >", webActions.click(ibtn)); 
	}

	//	public void clickOnibtnOfSlotHeroTile(String nameofthegame) {
	//		By ibtn = By.xpath("//p[contains(text(),'" + nameofthegame +"')]/parent::div/preceding-sibling::div/a/i");
	//		logReporter.log("click on 'i' > >", webActions.click(ibtn)); 
	//	}

	public void clickOnibtnOfGameTile(String nameofthegame) {
		By ibtn = By.xpath("//apollo-bingo-tile[contains(@game-type,'"+nameofthegame+"')]//i[contains(@class,'info colour')]");
		logReporter.log("click on 'i' > >", webActions.click(ibtn)); // change apollo-slot-hero-tile for hero tile
	}

	public void clickOnibtnOfBingoTile(String nameofthegame) {
		By ibtn = By.xpath("//apollo-bingo-tile[contains(@game-type,'"+nameofthegame+"')]//i[contains(@class,'info colour')]");
		logReporter.log("click on 'i' > >", webActions.click(ibtn)); // change apollo-slot-hero-tile for hero tile
	}

	public void clickExpandbutton() {
		logReporter.log("click on expand button > >", webActions.click(expandbtn));

	}

	public void clickReducebutton() {
		logReporter.log("click on reduce button > >", webActions.click(reducebtn));
	}

	public void clickOnFreePlayBtn() {
		By freeplaybtn = By.xpath("//button[contains(.,'Free Play')]");
		logReporter.log("click on 'Free play button' > >", webActions.click(freeplaybtn));
	}

	public void verifyExitArrowBtn() {
		logReporter.log("check element > >", webActions.checkElementDisplayed(exitarrowbtn));
	}

	public void verifySessionTime() {
		logReporter.log("check element > >", webActions.checkElementDisplayed(sessiontime));
	}

	public void verifyNonSessionTime() {
		if (!webActions.checkElementDisplayed(sessiontime))
			logReporter.log("check element > >", true);
	}

	public void verifyPlayforRealBtn() {
		logReporter.log("check element > >", webActions.checkElementDisplayed(playforrealbtn));
	}

	public void verifyTitleoftheGame() {
		logReporter.log("check element > >", webActions.checkElementDisplayed(titleofthegame));
	}

	public void verifyExpandBtn() {
		logReporter.log("check element > >", webActions.checkElementDisplayed(expandbtn));
	}

	public void verifyDepositBtn() {
		logReporter.log("check element > >", webActions.checkElementDisplayed(depositbtn));
	}

	public void verifyMyaccountBtn() {
		logReporter.log("check element > >", webActions.checkElementDisplayed(myaccountbtn));
	}

	public void verifyDemoheadercomponents(String options) {

		switch (options) {

		case "“Exit game arrow": {
			logReporter.log("check element > >", webActions.checkElementDisplayed(exitarrowbtn));
			break;
		}

		case "Play for Real CTA": {
			logReporter.log("check element > >", webActions.checkElementDisplayed(playforrealbtn));
			break;
		}

		case "Title of the game": {
			logReporter.log("check element > >", webActions.checkElementDisplayed(titleofthegame));
			break;
		}

		case "Expand CTA": {
			logReporter.log("check element > >", webActions.checkElementDisplayed(expandbtn));
			break;
		}
		case "Deposit CTA": {
			logReporter.log("check element > >", webActions.checkElementDisplayed(depositbtn));
			break;
		}
		case "My account CTA": {
			logReporter.log("check element > >", webActions.checkElementDisplayed(myaccountbtn));
			break;
		}
		}
	}

	public void imageAtExpandbtn() {
		Boolean isFullScreen = false;
		Object result = webActions.executeJavascript("return document.fullscreen");
		if (result instanceof Boolean) {
			isFullScreen = (Boolean) result;
		}
		logReporter.log("check element > >", isFullScreen);

	}

	public void imageAtReducebtn() {
		Boolean isFullScreen = true;
		Object result = webActions.executeJavascript("return document.fullscreen");
		if (result instanceof Boolean) {
			isFullScreen = (Boolean) result;
		}
		logReporter.log("check element > >", !isFullScreen);
	}

	public void verifyHeaderInGame() {
		By header = By.xpath("//div[contains(@class,'game-window-header')]");
		logReporter.log("check element > >", webActions.checkElementDisplayed(header));

	}

	public void verifyBackroungimageInGame() {

		By bagroundimage = By.xpath("//*[contains(@class,'game-window-image')]"); // *[contains(@id,'Desktop')]/body/canvas
		logReporter.log("check element > >", webActions.checkElementDisplayed(bagroundimage));
	}

	public void clickOnImageOfHerotile(String nameofthegame) {
		By image = By.xpath("//apollo-bingo-tile[contains(@game-type,'"+nameofthegame+"')]//img");
		if (!webActions.click(image)) {
			logReporter.log("Check element", true);
		}
	}

	public void clickOnTitleOfHerotile(String nameofthegame) {
		By title = By.xpath("//apollo-bingo-tile[contains(@game-type,'"+nameofthegame+"')]//p[contains(text(),'" + nameofthegame + "')]");
		if (!webActions.click(title)) {
			logReporter.log("Check element", true);
		}
	}

	public void clickOnDescriptionOfHerotile(String nameofthegame) {
		By description = By.xpath("//apollo-bingo-tile[contains(@game-type,'"+nameofthegame+"')]////p[contains(text(),'" + nameofthegame + "')]/parent::div/p[2]");
		if (!webActions.click(description)) {
			logReporter.log("Check element", true);
		}
	}

	public void clickOnPrizeofHerotile() {
		By prizebtn = By.xpath("//apollo-bingo-tile[contains(@game-type,'Best Odds Bingo')]//i[contains(@class,'money-bag')]");
		if (!webActions.click(prizebtn)) {
			logReporter.log("Check element", true);
		}
	}

	public void clickOnPlayNowOnGameDetailsPage() {
		logReporter.log("play now button on games detail page", webActions.click(plyNowbtnOnGamedetailsPage));
	}

	public void clickJoinNowOfFirstBingoGame() {

	}

	public void clickExitGameCTA() {
		webActions.processElement(exitarrowbtn);
		if (webActions.checkElementDisplayedWithMidWait(exitarrowbtn))
			logReporter.log("Click Exit Game> >", webActions.click(exitarrowbtn));
	}

	public void clickMyAccFromGame() {
		By locator = By.xpath("//span[text()='Deposit']/following::i[contains(@class,'my-account')]");
		webActions.processElement(locator);
		if (webActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("Click my acc from game> >", webActions.click(locator));
	}

	public void navigateThroughHamburgerMenu(String strMenuName) {
		//By hamburgerMenu = By.xpath("//apollo-top-navigation");
		//if (webActions.checkElementDisplayedWithMidWait(hamburgerMenu))
		//	logReporter.log("Click hamburger menu> >", webActions.click(hamburgerMenu));
		
		By locator = By.xpath("//a[contains(@class,'top-navigation-link') and contains(@href,'" + strMenuName + "')]");
			logReporter.log("Click menu> >", webActions.click(locator));
	}

	public void clickOnPlayNow() {
		By playNowbtn = By
				.xpath("//h2[text()='Slots & Games']/following::apollo-slot-tile[1]//button[text()='Play Now']");
		webActions.processElement(playNowbtn);
		//webActions.androidScrollToElement(playNowbtn);
		if (webActions.checkElementDisplayed(playNowbtn))
			logReporter.log("click element play now > >", webActions.click(playNowbtn));
	}




	public void verifySessionTimeInmmssformat() {

		logReporter.log("check element > >", webActions.checkElementDisplayedWithMidWait(sessiontimewithmmss));
	}


	public void clickOnPlayforRealBtn() {
		if (webActions.checkElementDisplayedWithMidWait(playforrealbtn))
			logReporter.log("check element > >", webActions.click(playforrealbtn));
	}

	public void verifyNonExpandBtn() {
		if (!webActions.checkElementDisplayedWithMidWait(expandbtn))
			logReporter.log("check element > >", true);
	}

	public void usernameDisplayed(String strUName) {
		strUName = strUName.toUpperCase();
		By locator = By.xpath("//h4[text()='" + strUName + "']");
		logReporter.log("Check username in header : " + strUName + "> >",
				webActions.checkElementDisplayedWithMidWait(locator));
	}

	public void clickInfoOfFirstGame(String sectionName) {
		/*By locator = By.xpath("(//h2[text()='Slots & Games']//following::a/i)[1]");
		logReporter.log("Click i> >", webActions.click(locator));*/
		wait.sleep(8);
		By locator = By.xpath("//h2[text()='"+sectionName+"']//following::a[contains(@class,'game-panel-info text-decoration-none')]");
		webActions.checkElementToBeClickablWithMidWait(locator);
		logReporter.log("Click info icon > >", webActions.clickUsingJS(locator));
	}

	public void clickOnPlayNowFromGameDetails() {
		By playNowbtn = By.xpath("(//button[text()='Play Now'])[1]");
		logReporter.log("click element play now > >", webActions.click(playNowbtn));

	}

	public void clickOnImageOfTile(String nameofthegame) {
		By image = By.xpath("//p[contains(text(),'" + nameofthegame + "')]/parent::div/preceding-sibling::div/img");
		logReporter.log("click on 'image' > >", webActions.click(image));
	}

	public void clickOnTitleoftheGame(String nameofthegame) {
		By titleofthegame = By.xpath("//p[contains(text(),'" + nameofthegame + "')]");
		if (!webActions.click(titleofthegame)) {
			logReporter.log("Check element", true);
		}
	}

}
