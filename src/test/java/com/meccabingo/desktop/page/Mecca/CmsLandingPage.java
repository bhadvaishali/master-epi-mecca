/**
 * 
 */
package com.meccabingo.desktop.page.Mecca;

import org.openqa.selenium.By;

import com.generic.WebActions;
import com.generic.logger.LogReporter;

/**
 * @author Harshvardhan Yadav, Namrata Donikar (Expleo)
 *
 */
public class CmsLandingPage {
	String user = "";
	private WebActions objWebActions;
	private LogReporter logReporter;

	public CmsLandingPage(WebActions webActions, LogReporter logReporter) {
		this.objWebActions = webActions;
		this.logReporter = logReporter;
	}

	public void verifyCMSSite() {
		By txtEpiLogo = By.xpath("(//img[@class='logo'])[1]");
		logReporter.log("check element > >", objWebActions.checkElementDisplayed(txtEpiLogo));
	}

	public void enterUserName(String userName) {
		By tbUserName = By.xpath("//input[@id='LoginControl_UserName']");
		user = userName;
		logReporter.log("Enter Value in userName > >", objWebActions.setText(tbUserName, userName));
	}

	public void enterPassword(String password) {
		By tbPassword = By.xpath("//input[@id='LoginControl_Password']");
		logReporter.log("Enter Value in password > >", objWebActions.setText(tbPassword, password));
	}

	public void clickLogInButton() {
		By btnLogIn = By.xpath("//input[@id='LoginControl_Button1']");
		logReporter.log("click 'LogIn button' > >", objWebActions.click(btnLogIn));
	}
}
