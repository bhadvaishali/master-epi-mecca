/**
 * 
 */
package com.meccabingo.desktop.page.Mecca;

import org.openqa.selenium.By;

import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.Utilities;
import com.generic.utils.WaitMethods;

/**
 * @author Harshvardhan Yadav, Namrata Donikar (Expleo)
 *
 */
public class HomePage {

	// By logoBingoHeader = By.xpath("//div[contains(@id,'header')]/header/a/img");
	// ////a[@class='logo' and @title='Mecca Bingo']/img[@alt='Mecca Bingo']
	// //*[@id=\"header\"]/header/a/img
	// By footer_UsefulLink = By.xpath("//ul[@class='useful-links']");
	// By footer_partnersLogo = By.xpath("//ul[@class='partners']");
	// By footer_privacyAndSecurityBlock = By.xpath("//div[@class='footer-row'] ");
	// By footer_PaymentProvidersBlock = By.xpath("//*[contains(text(),'Secure
	// Payments')]"); //div[@class='footer-payments']
	// By slider = By.xpath("(//div[@class='hero-carousel-slide-text'])[1]");
	// //(//div[@class='hero-carousel-slide-text'])[1]//span[contains(text(), 'Play
	// Book of Ra and Win2')]");
	// By loginButton = By.xpath("//button[contains(text(),'Login')]");
	// By myAccounticon = By.xpath("//i[@class='my-account']");

	By logoBingoHeader = By.xpath("//header[@id='header']//a//img[@alt='Mecca Bingo']");
	By footer_Social = By.xpath("//ul[@class='footer-social']"); // div[@class='footer-social']
	By MyAccountButton = By.xpath("//a[contains(@class,'open-myaccount')]");
	By BalanceText = By.xpath("//div/apollo-balance-block/span[contains(@class,'balance-label')]");
	By BalanceAmount = By.xpath("//div/apollo-balance-block/strong");
	By BalanceToggle = By.xpath("//div[contains(@class,'balance-toggle')]");
	By BalanceDots = By.xpath("//div/apollo-balance-block/strong");
	By searchedGame_PlayNow_btn = By.xpath("//*[@id=\"overlay-level-1\"]/section/div/section/div/div/div/div/button");
	By bagroundimage = By.xpath("//*[contains(@class,'game-window-image')]"); // *[contains(@id,'Desktop')]/body/canvas
	By search_loupe = By.xpath("//button[contains(@class,'search-open-btn')]");
	By DepositButton = By.xpath("//button[contains(text(),'Deposit')]");
	By Message = By.xpath("//span[contains(@class,'messages')]");
	By NavigationMenu = By.xpath("//ul[contains(@class,'top-navigation')]");
	By CookiesText = By.xpath("//p[contains(text(),'cookie')]");
	By CookiesButton = By.xpath("//button[contains(.,'Continue')]");
	By TopArrowAnchor = By.xpath("//apollo-scroll-top");

	private WebActions objWebActions;
	private LogReporter logReporter;
	private WaitMethods waitMethods;
	private Configuration configuration;
	
	public HomePage(WebActions webActions, LogReporter logReporter,WaitMethods waitMethods,Configuration configuration) {
		this.objWebActions = webActions;
		this.logReporter = logReporter;
		this.waitMethods = waitMethods;
		this.configuration= configuration;
	}

	public void verifyHeaderLogo() {

		logReporter.log("Check mecca logo displayed", objWebActions.checkElementDisplayed(logoBingoHeader));

	}

	public void scrollToFooterSocial() {
		objWebActions.scrollToElement(footer_Social);
	}

	public void setBrowserSize(String width, String height) {
		getElementSizeBeforeResize();
		objWebActions.setBrowserWindowSize(width, height);
		// logReporter.log("Set browser window > >",
		// objWebActions.setBrowserWindowSize(width, height));
		// objUtilities.assertEquals("Set browser window > >", true,
		// objWebActions.setBrowserWindowSize(width, height));
	}

	public void verifySocialMediaBlockSize() {

		if (this.socialMediaBlock_width > objWebActions.getWidth(footer_Social)) {
			logReporter.log("Check social media block size after window resize: ", true);
		} else
			logReporter.log("Check social media block size after window resize: ", false);
	}

	private int socialMediaBlock_width = 0;

	private void getElementSizeBeforeResize() {
		socialMediaBlock_width = objWebActions.getWidth(footer_Social);
	}

	public void scrollToFooterUsefullLink() {
		By footer_UsefulLink = By.xpath("//ul[@class='useful-links']");
		objWebActions.scrollToElement(footer_UsefulLink);
	}

	public void scrollToPartnersLogo() {
		By footer_partnersLogo = By.xpath("//ul[contains(@class,'partners')]");
		objWebActions.scrollToElement(footer_partnersLogo);
	}

	public void scrollToprivacyAndSecurityBlock() {
		By footer_privacyAndSecurityBlock = By.xpath("//div[@class='footer-row'] ");
		objWebActions.scrollToElement(footer_privacyAndSecurityBlock);
	}

	public void scrollToPaymentProvidersBlock() {
		By footer_PaymentProvidersBlock = By.xpath("//*[contains(text(),'Secure Payments')]");
		objWebActions.scrollToElement(footer_PaymentProvidersBlock);
	}

	public void clickOnSlider() {
		By slider = By.xpath("(//div[@class='hero-carousel-slide-text'])[1]");
		logReporter.log("click on 'slider' > >", objWebActions.click(slider));
	}

	public void clickOnLogInButton() {
		By loginButton = By.xpath("//button[contains(text(),'Login')]");
		logReporter.log("click on 'LogIn button' > >", objWebActions.click(loginButton));
	}

	public void verifyLoginbtnInHeader() {
		By locator = By.xpath("//button[contains(text(),'Login')]");
		logReporter.log("Verify Login button", objWebActions.checkElementDisplayed(locator));
	}
	public void verifyMyAccount() {

		logReporter.log("Verify my acccount button displayed' > >",
				objWebActions.checkElementDisplayed(MyAccountButton));
	}

	public void windowRefresh() {
		objWebActions.pageRefresh();

	}

	public void verifyRegisterNowLink() {
		By registernowlink = By.xpath("//a[contains(text(),'Not a member')]");
		logReporter.log("Verify register now link", objWebActions.checkElementDisplayed(registernowlink));
	}

	public void verifyHeaderJoinNowbtn() {
		//By joinnowbtn = By.xpath("//a[contains(text(),'Join Now')]");
		By joinnowbtn = By.xpath("//a[contains(text(),'Join')]");
		logReporter.log("Verify join now button", objWebActions.checkElementDisplayed(joinnowbtn));
	}

	public void verifybattenberg() {
		By battenberg = By.xpath("//apollo-battenberg");
		logReporter.log("verify batternberg block", objWebActions.checkElementDisplayed(battenberg));
	}

	public void verifyBlockInOneRow() {
		By onerowblock = By.xpath("/html/body/main/section[4]");
		logReporter.log("Verify block", objWebActions.checkElementDisplayed(onerowblock));
	}

	public void verifyThreeBlock() {
		By blockone = By.xpath(
				"//section[contains(@class,'three-columns')]/apollo-card-block[contains(@class,'boxed card-block')][1]");
		By blocktwo = By.xpath(
				"//section[contains(@class,'three-columns')]/apollo-card-block[contains(@class,'boxed card-block')][2]");
		By blockthree = By.xpath(
				"//section[contains(@class,'three-columns')]/apollo-card-block[contains(@class,'boxed card-block')][3]");
		logReporter.log("Verify block one", objWebActions.checkElementDisplayed(blockone));
		logReporter.log("Verify block two", objWebActions.checkElementDisplayed(blocktwo));
		logReporter.log("Verify block three", objWebActions.checkElementDisplayed(blockthree));

	}

	public void verifyUserBalanceSection() {
		logReporter.log("verify my account button", objWebActions.checkElementDisplayed(MyAccountButton));
	}

	public void verifyUserBalanceTitle() {
		logReporter.log("verify balance text", objWebActions.checkElementDisplayed(BalanceText));
	}

	public void verifyUserBalanceAmount() {
		logReporter.log("verify balance amount", objWebActions.checkElementDisplayed(BalanceAmount));
	}

	public void clickOnUserBalanceTitle() {
		if (!objWebActions.click(BalanceText))
			logReporter.log("verify balance text", true);
	}

	public void clickOnUserBalanceToggle() {
		logReporter.log("verify balance toggle", objWebActions.click(BalanceToggle));
	}

	public void clickOnUserBalanceDots() {
		logReporter.log("verify balance toggle", objWebActions.click(BalanceDots));
	}

	public void clickOnPlayNowButton() {
		logReporter.log("click on play now button", objWebActions.click(searchedGame_PlayNow_btn));
	}

	public void verifyBackroungimageInGame() {

		logReporter.log("check element > >", objWebActions.checkElementDisplayed(bagroundimage));
	}

	public void verifyHeaderComponents(String text) {
		switch (text) {

		case "Mecca Logo": {
			logReporter.log("check element > >", objWebActions.checkElementDisplayed(logoBingoHeader));
			break;
		}

		case "Navigation menu": {
			logReporter.log("check element > >", objWebActions.checkElementDisplayed(NavigationMenu));
			break;
		}

		case "Search loupe icon": {
			logReporter.log("check element > >", objWebActions.checkElementDisplayed(search_loupe));
			break;
		}

		case "Deposit button": {
			logReporter.log("check element > >", objWebActions.checkElementDisplayed(DepositButton));
			break;
		}
		case "Balance amount/dots": {
			logReporter.log("check element > >", objWebActions.checkElementDisplayed(BalanceAmount));
			break;
		}
		case "Balance text": {
			logReporter.log("check element > >", objWebActions.checkElementDisplayed(BalanceText));
			break;
		}
		case "Hide/Unhide balance toggle": {
			logReporter.log("check element > >", objWebActions.checkElementDisplayed(BalanceToggle));
			break;
		}
		case "My account icon": {
			logReporter.log("check element > >", objWebActions.checkElementDisplayed(MyAccountButton));
			break;
		}
		case "number of unread messages": {
			logReporter.log("check element > >", objWebActions.checkElementDisplayed(Message));
			break;
		}
		case "Login CTA": {
			verifyLoginbtnInHeader();
			break;
		}
		case "Join Now CTA": {
			verifyHeaderJoinNowbtn();
			break;
		}
		}
		
	}

	public void NavigateToInvalidUrl() {
		logReporter.log("navigate to invalid url", objWebActions.navigateToInvalidUrl());
	}

	public void verifyErrorTitle() {
		By error = By.xpath("//h1[contains(text(),'404 Error')]");
		logReporter.log("verify 404 error title", objWebActions.checkElementDisplayed(error));
	}

	public void verifyCookiesText() {
		logReporter.log("verify cookies text", objWebActions.checkElementDisplayed(CookiesText));
	}

	public void verifyCookiesButton() {
		logReporter.log("verify cookies text", objWebActions.checkElementDisplayed(CookiesButton));
	}

	public void clickOnTopArrowAnchor() {
		logReporter.log("click on top arrow link", objWebActions.click(TopArrowAnchor));
	}
	
	public void verifySubHeaderTabs(String sectionName,String tabName)
	{

		if(tabName.contains("~"))
		{
			String[] arr1 = tabName.split("~");
			for (String pref1 : arr1) 
			{
				
				//By locator = By.xpath("//apollo-filter-heading//div//ul//li//a[contains(.,'"+pref1+"')]");
				By locator = By.xpath("//h2[contains(.,'"+sectionName+"')]//following-sibling::div//ul//li//span[contains(.,'"+pref1+"')]");
				logReporter.log("Verify '" +pref1 + " ' option is displayed on under " +sectionName+ " promotions sub tabs",  
						objWebActions.checkElementDisplayed(locator));
			}
		}
		else
		{
			//By locator = By.xpath("//apollo-filter-heading//div//ul//li//a[contains(.,'"+tabName+"')]");
			By locator = By.xpath("//h2[contains(.,'"+sectionName+"')]//following-sibling::div//ul//li//span[contains(.,'"+tabName+"')]");
			logReporter.log("Verify ' "+tabName+  " tab is displayed under " +sectionName+ " promotions sub tabs",objWebActions.checkElementDisplayed(locator));}
	}
	
	public void clickOnDepositLinkFromHeader()
	{
		logReporter.log("click Deposit link from header", objWebActions.click(DepositButton));
	}
	
	public String getBalancefromHeader()
	{
		waitMethods.sleep(10);
		return objWebActions.getText(BalanceAmount);
		
	}
	//h2[contains(.,'Bingo')]//following-sibling::div[2]//.//button[contains(.,'Play Bingo')]

	//h2[contains(.,'Slots & Games')]//following-sibling::div//a[contains(.,'See all')]
	//New Games
	//Slingo
	//Table Games

	//winners feed
	//apollo-winners-carousel

	//apollo-card-block//div[@class='card-block-image']//following-sibling::div[contains(@class,'card-block-content')]//./h2[contains(.,'Fun on the Run')]

	//apollo-card-block//div[@class='card-block-image']//following-sibling::div[contains(@class,'card-block-content')]//div//p[contains(.,'t leave us behind, you can play bingo anywhere and any time with our apps!')]

	//apollo-card-block//div[@class='card-block-image']//following-sibling::div[contains(@class,'card-block-content')]//div//p[contains(.,'You can download Mecca apps on your iPhone, iPad or Android phone for huge jackpots and prizes.')]

	//apollo-card-block//div[@class='card-block-image']//following-sibling::div[contains(@class,'card-block-content')]//div//a[contains(.,' Find Out More ')]

	//apollo-card-block//div[@class='card-block-image']//following-sibling::div[contains(@class,'card-block-content')]//div//a//img[contains(@src,'google.png')]

	//apollo-card-block//div[@class='card-block-image']//following-sibling::div[contains(@class,'card-block-content')]//div//a//img[contains(@src,'ios.png')]

	//h1[contains(.,'Welcome to the Mecca of Online Bingo')]

	//h5[contains(.,'Frequently asked questions')]

	//section[contains(@class,'three-columns content-wrapper three-columns-alternate')]//apollo-card-block[@cb-cta-text='Go to Live Help ']

	//section[contains(@class,'three-columns content-wrapper three-columns-alternate')]//apollo-card-block[@cb-cta-text='Contact Us']

	//section[contains(@class,'three-columns content-wrapper three-columns-alternate')]//apollo-card-block[@cb-cta-text=' View Our Community']
	//bingo...
	//h1[contains(.,'Mecca Bingo – fun, friends and the best bingo games!')]
	//slots..
	//h1[contains(.,'Play the best online slots and table games at Mecca Bingo')]
	//
	//Online casino games
	
	//Play jackpot slots at Mecca Bingo
	//h1[contains(.,'Slots & Bingo promotions are at Mecca Bingo!')]
}
