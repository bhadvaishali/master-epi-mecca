package com.meccabingo.desktop.page.Mecca;

import org.openqa.selenium.By;

import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.utils.WaitMethods;
import com.generic.webdriver.DriverProvider;

public class Games {
	private WebActions webActions;
	private DriverProvider objDriverProvider;
	private LogReporter logReporter;
	private WaitMethods waitMethods;
	
	public Games(WebActions webActions, DriverProvider driverProvider, LogReporter logReporter,WaitMethods waitMethods) {
		this.webActions = webActions;
		this.objDriverProvider = driverProvider;
		this.logReporter = logReporter;
		this.waitMethods = waitMethods;
	}

	public void clickOnAutoInBingo() {
		By autobtn = By.xpath("//apollo-bingo-container/div/div/ul/li[contains(text(),'Auto')]");
		logReporter.log("click on 'Auto button in bingo section' > >", webActions.click(autobtn));
	}

	public void clickOnAutoInGames() {
		By autobtn = By.xpath("//apollo-game-container/div/div/ul/li[contains(text(),'Auto')]");
		logReporter.log("click on 'Auto button in games section' > >", webActions.click(autobtn));
	}

	public void hoverOnIbtnOfHerotile(String nameofthegame) {
		By ibtn = By.xpath("//apollo-bingo-tile[contains(@game-type,'"+nameofthegame+"')]//i[contains(@class,'info colour')]");
		if (!webActions.mouseHover(ibtn)) {
			logReporter.log("Check element", true);
		}
	}

	public void hoverOnIbtnOfGametile(String nameofthegame) {
		By ibtn = By.xpath("//p[contains(text(),'" + nameofthegame + "')]/parent::div/preceding-sibling::div/a/i");
		logReporter.log("Hover on i of normal tile", webActions.mouseHover(ibtn));

	}

	public void clickOnPrizeofGametile(String nameofthegame) {
		By prizebtn = By.xpath("//p[contains(text(),'" + nameofthegame + "')]/parent::div/div/div[1]");
		if (!webActions.click(prizebtn)) {
			logReporter.log("Check element", true);
		}

	}

	public void verifyOpacityOfImageOnGameDetailsPage(String nameofthegame) {
		String opacity = "0.4";
		By image = By.xpath("//img[contains(@class,'background')]/following::div[contains(@class,'overlay opacity')]");
		logReporter.log("verify opacity of image", webActions.checkElementDisplayed(image));

	}

	public void verifyContainsUrl(String url) {
		String currentURL = objDriverProvider.getWebDriver().getCurrentUrl();
		logReporter.log("verify URL > >" + url, currentURL.contains(url));
	}

	public void clickJoinNowOfFirstBingoGame() {
		By locator = By.xpath("(//h2[text()='Bingo']/following::button[contains(@class,'-green')])[1]");
		webActions.processElement(locator);
		if (webActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("Click Join Now> >", webActions.click(locator));
	}

	public void clickInfoOfFirstBingoGame() {
		waitMethods.sleep(8);
		By locator = By.xpath("(//apollo-play-bingo-cta//following::a)[1]");
	//	webActions.checkElementDisplayedWithMidWait(locator);
		logReporter.log("Click info icon of First bingo game> >", webActions.clickUsingJS(locator));
	}

	public void clickOnImageOfTile(String nameofthegame) {
		By image = By.xpath("//p[contains(text(),'" + nameofthegame + "')]/parent::div/preceding-sibling::div/img");
		logReporter.log("click on 'image' > >", webActions.click(image));
	}

	public void clickOnTitleoftheGame(String nameofthegame) {
		By titleofthegame = By.xpath("//p[contains(text(),'" + nameofthegame + "')]");
		if (!webActions.click(titleofthegame)) {
			logReporter.log("Check element", true);
		}
	}

	public void bingoContainerSectionDisplayed() {
		By bingoContainerSection = By.xpath("//apollo-filter-heading[@title='Bingo']");
		logReporter.log("bingo container displayed > >", webActions.checkElementDisplayed(bingoContainerSection));
	}

	public void clickTopFlagOfBingoGame() {
		By topFlagOfBingoGame = By.xpath("(//apollo-bingo-tile//div[contains(@class,'pot-flag')])[1]");
		webActions.scrollToElement(topFlagOfBingoGame);
		webActions.click(topFlagOfBingoGame);
	}
}
