package com.meccabingo.desktop.page.Mecca;

import org.openqa.selenium.By;

import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.WaitMethods;
import com.generic.webdriver.DriverProvider;

/**
 * @author Harshvardhan Yadav, Namrata Donikar (Expleo)
 *
 */
public class FooterPage {

	By linkTwitter = By.xpath("//a[@rel='Twitter']"); // a[@title='Twitter']
	By linkFacebook = By.xpath("//a[@rel='Facebook']");
	By linkYouTube = By.xpath("//a[@rel='YouTube']");
	By linkInstagram = By.xpath("//a[@rel='Instagram']");

	By txtTwitter = By.xpath("//span[@class='footer-social-text']/span[contains(text(),'Twitter')]"); // p[@class='footer-social-text']/span[contains(text(),'Twitter')]
	By txtFacebook = By.xpath("//span[@class='footer-social-text']/span[contains(text(),'Facebook')]");
	By txtYouTube = By.xpath("//span[@class='footer-social-text']/span[contains(text(),'YouTube')]");
	By txtInstagram = By.xpath("//span[@class='footer-social-text']/span[contains(text(),'Instagram')]");

	By imageTwitter = By.xpath("//img[@alt='Twitter']");
	By imageFacebook = By.xpath("//img[@alt='Facebook']");
	By imageYouTube = By.xpath("//img[@alt='YouTube']");
	By imageInstagram = By.xpath("//img[@alt='Instagram']");

	By descriptionTwitter = By
			.xpath("//span[@class='footer-social-text']/span[contains(text(),'Twitter')]//preceding-sibling::span"); // p[@class='footer-social-text']/span[contains(text(),'Twitter')]//preceding-sibling::span
	By descriptionFacebook = By
			.xpath("//span[@class='footer-social-text']/span[contains(text(),'Facebook')]//preceding-sibling::span");
	By descriptionYouTube = By
			.xpath("//span[@class='footer-social-text']/span[contains(text(),'YouTube')]//preceding-sibling::span");
	By descriptionInstagram = By
			.xpath("//span[@class='footer-social-text']/span[contains(text(),'Instagram')]//preceding-sibling::span");

	By linkPrivacyPolicy = By.xpath("//a[contains(text(),'Privacy Policy')]"); // a[contains(text(),'Privacy Policy')]
	By linkTermsAOndConditions = By.xpath("//a[contains(text(),'Terms and Conditions')]");
	By linkOurMeccaPromise = By.xpath("//a[contains(text(),'Our Mecca Promise')]");
	By linkAffiliates = By.xpath("//a[contains(text(),'Affiliates')]");
	By linkMeccaClubTerms = By.xpath("//a[contains(text(),'Mecca Club Terms')]");
	By linkPlayOnlineCasino = By.xpath("//a[contains(text(),'Play Online Casino')]");
	By linkMeccaBlog = By.xpath("//a[contains(text(),'Mecca Blog')]");

	By logo_Essa = By.xpath("//a[@title='ESSA']//img");
	By logo_Ibsa = By.xpath("//a[@title='IBAS']//img");
	By logo_18 = By.xpath("//img[@alt='Over 18s only']");
	By logo_GamblingControl = By.xpath("//a[@title='Gambling Control']//img");
	By logo_GamCare = By.xpath("//a[@title='GamCare']//img");
	By logo_GamStop = By.xpath("//a[@title='GamStop']//img");
	By logo_GamblingCommission = By.xpath("//a[@title='Gambling Commission']//img");
	By logo_saferGamblingStandard = By.xpath("//a[@title='Safer Gambling Standard']//img");

	By logo_VeriSignSecured = By.xpath("//img[contains(@title,'Verisign')]"); // a[@title='Verisign Title']/img

	By paymentTitleBlock = By.xpath("//section[contains(@class,'footer-payments')]//h2[contains(.,'Secure Payments')]"); // div[@class='footer-payments-header']
	By paymentDescriptionBlock = By.xpath("//section[contains(@class,'footer-payments')]//h5[contains(.,'We use 128bit encryption and accept all major card providers.')]"); // div[@class='footer-payments-subheader']
	By paymentLogosBlock = By.xpath("//li[contains(@class,'footer-payment')]"); // ul[@class='footer-payments-list']

	By link_Alderney_Gambling_Control_Commission = By
			.xpath("//a[contains(text(),'Alderney Gambling Control Commission')]");
	By link_UK_Gambling_Commission = By.xpath("//a[contains(@title,'Gambling Commission')]");
	By link_BeGambleAware = By.xpath("//a[contains(@href,'begambleaware')]");
	By link_Rank_Group = By.xpath("//ul//li//a[contains(@title,'Rank Group')]//img");

	private WebActions objWebActions;
	private DriverProvider objDriverProvider;
	private LogReporter logReporter;
	private WaitMethods waitMethods;
	private Configuration configuration;
	
	public FooterPage(WebActions webActions, DriverProvider driverProvider, LogReporter logReporter,
			WaitMethods waitMethods,Configuration configuration) {
		this.objWebActions = webActions;
		this.objDriverProvider = driverProvider;
		this.logReporter = logReporter;
		this.waitMethods = waitMethods;
		this.configuration= configuration;
	}

	public void verifySocialMediaComponents(String text) {

		switch (text) {

		case "Twitter": {
			logReporter.log("check element > >", objWebActions.checkElementDisplayed(linkTwitter));
			break;
		}

		case "Facebook": {
			logReporter.log("check element > >", objWebActions.checkElementDisplayed(linkFacebook));
			break;
		}

		case "YouTube": {
			logReporter.log("check element > >", objWebActions.checkElementDisplayed(linkYouTube));
			break;
		}

		case "Instagram": {
			logReporter.log("check element > >", objWebActions.checkElementDisplayed(linkInstagram));
			break;
		}

		}
	}

	public void verifyImageAndTextOfSocial() {

		logReporter.log("check element > >", objWebActions.checkElementDisplayed(imageTwitter));
		logReporter.log("check element > >", objWebActions.checkElementDisplayed(imageFacebook));
		logReporter.log("check element > >", objWebActions.checkElementDisplayed(imageYouTube));
		logReporter.log("check element > >", objWebActions.checkElementDisplayed(imageInstagram));

		logReporter.log("check element > >", objWebActions.checkElementDisplayed(txtTwitter));
		logReporter.log("check element > >", objWebActions.checkElementDisplayed(txtFacebook));
		logReporter.log("check element > >", objWebActions.checkElementDisplayed(txtYouTube));
		logReporter.log("check element > >", objWebActions.checkElementDisplayed(txtInstagram));

		logReporter.log("check element > >", objWebActions.checkElementDisplayed(descriptionTwitter));
		logReporter.log("check element > >", objWebActions.checkElementDisplayed(descriptionFacebook));
		logReporter.log("check element > >", objWebActions.checkElementDisplayed(descriptionYouTube));
		logReporter.log("check element > >", objWebActions.checkElementDisplayed(descriptionInstagram));
	}

	public void clickFacebookBlock() {
		logReporter.log("click 'Facebook' > >", objWebActions.click(linkFacebook));
	}

	public void clickTwitterBlock() {
		logReporter.log("click 'Twitter' > >", objWebActions.click(linkTwitter));
	}

	public void clickYouTubeBlock() {
		logReporter.log("click 'YouTube' > >", objWebActions.click(linkYouTube));
	}

	public void clickInstagramBlock() {
		logReporter.log("click 'Instagram' > >", objWebActions.click(linkInstagram));
	}

	public boolean verifyUrl(String url) {
		if (objDriverProvider.getWebDriver().getCurrentUrl().equals(url))
			return true;
		else
			return false;
	}

	public void mouseHoverOnFacebook() {
		objWebActions.mouseHover(linkFacebook);
	}

	public void verifyColorCode(String area, String colorCode) {

		switch (area) {

		case "Twitter": {
			logReporter.log("check CSS property > >", objWebActions.checkCssValue(linkTwitter, "color", colorCode));
			break;
		}

		case "Facebook": {
			logReporter.log("check CSS property > >", objWebActions.checkCssValue(linkFacebook, "color", colorCode));
			break;
		}

		case "YouTube": {
			logReporter.log("check CSS property > >", objWebActions.checkCssValue(linkYouTube, "color", colorCode));
			break;
		}

		case "Instagram": {
			logReporter.log("check CSS property > >", objWebActions.checkCssValue(linkInstagram, "color", colorCode));
			break;
		}
		}

	}

	public void mouseHoverOnTwitter() {
		objWebActions.mouseHover(linkTwitter);
	}

	public void mouseHoverOnInstagram() {
		objWebActions.mouseHover(linkInstagram);
	}

	public void mouseHoverOnYouTube() {
		objWebActions.mouseHover(linkYouTube);
	}

	public void setBrowserSize(String width, String height) {
		getElementSizeBeforeResize();
		objWebActions.setBrowserWindowSize(width, height);
		// logReporter.log("Set browser window > >",
		// objWebActions.setBrowserWindowSize(width, height));
		// objUtilities.assertEquals("Set browser window > >", true,
		// objWebActions.setBrowserWindowSize(width, height));
	}

	private int getFontSize(By locator) {
		return Integer.parseInt(objWebActions.getCssValue(locator, "font-size").split("px")[0]);
	}

	public void verifyFontSizeOfElements(String text) {

		switch (text) {

		case "Twitter": {
			this.checkFontSizeOfTwitter("name");
			this.checkFontSizeOfTwitter("description");
			this.checkFontSizeOfTwitter("logo");
			break;
		}

		case "Facebook": {
			this.checkFontSizeOfFacebook("name");
			this.checkFontSizeOfFacebook("description");
			this.checkFontSizeOfFacebook("logo");
			break;
		}

		case "YouTube": {
			this.checkFontSizeOfYouTube("name");
			this.checkFontSizeOfYouTube("description");
			this.checkFontSizeOfYouTube("logo");
			break;
		}

		case "Instagram": {
			this.checkFontSizeOfInstagram("name");
			this.checkFontSizeOfInstagram("description");
			this.checkFontSizeOfInstagram("logo");
			break;
		}

		}
	}

	private int font_TwitterText = 0;
	private int font_TwitterDescription = 0;
	private int font_TwitterLogo = 0;

	private int font_FacebookText = 0;
	private int font_FacebookDescription = 0;
	private int font_FacebookLogo = 0;

	private int font_YouTubeText = 0;
	private int font_YouTubeDescription = 0;
	private int font_YouTubeLogo = 0;

	private int font_InstagramText = 0;
	private int font_InstagramDescription = 0;
	private int font_InstagramLogo = 0;

	private void getElementSizeBeforeResize() {

		font_TwitterText = this.getFontSize(txtTwitter);
		font_TwitterDescription = this.getFontSize(descriptionTwitter);
		font_TwitterLogo = objWebActions.getWidth(imageTwitter);

		font_FacebookText = this.getFontSize(txtFacebook);
		font_FacebookDescription = this.getFontSize(descriptionFacebook);
		font_FacebookLogo = objWebActions.getWidth(imageFacebook);

		font_YouTubeText = this.getFontSize(txtYouTube);
		font_YouTubeDescription = this.getFontSize(descriptionYouTube);
		font_YouTubeLogo = objWebActions.getWidth(imageYouTube);

		font_InstagramText = this.getFontSize(txtInstagram);
		font_InstagramDescription = this.getFontSize(descriptionInstagram);
		font_InstagramLogo = objWebActions.getWidth(imageInstagram);
	}

	private void checkFontSizeOfTwitter(String text) {
		switch (text) {
		case "name": {
			if (this.font_TwitterText > this.getFontSize(txtTwitter)) {

				logReporter.log("Check Twitter text font size after window resize: ", true);
			} else
				logReporter.log("Check Twitter text font size after window resize: ", false);
			break;
		}

		case "description": {
			if (this.font_TwitterDescription > this.getFontSize(descriptionTwitter)) {
				logReporter.log("Check Twitter description font size after window resize: ", true);
			} else
				logReporter.log("Check Twitter description font size after window resize: ", false);
			break;
		}

		case "logo": {
			if (this.font_TwitterLogo > objWebActions.getWidth(imageTwitter)) {
				logReporter.log("Check Twitter logo size after window resize: ", true);
			} else
				logReporter.log("Check Twitter logo size after window resize: ", false);
			break;
		}

		}

	}

	private void checkFontSizeOfFacebook(String text) {
		switch (text) {
		case "name": {
			if (this.font_FacebookText > this.getFontSize(txtFacebook)) {
				logReporter.log("Check Facebook text font size after window resize: ", true);
			} else
				logReporter.log("Check Facebook text font size after window resize: ", false);
			break;
		}

		case "description": {
			if (this.font_FacebookDescription > this.getFontSize(descriptionFacebook)) {
				logReporter.log("Check Facebook description font size after window resize: ", true);
			} else
				logReporter.log("Check Facebook description font size after window resize: ", false);
			break;
		}

		case "logo": {
			if (this.font_FacebookLogo > this.getFontSize(imageFacebook)) {
				logReporter.log("Check Facebook logo size after window resize: ", true);
			} else
				logReporter.log("Check Facebook logo size after window resize: ", false);
			break;
		}

		}

	}

	private void checkFontSizeOfYouTube(String text) {
		switch (text) {
		case "name": {
			if (this.font_YouTubeText > this.getFontSize(txtYouTube)) {

				logReporter.log("Check YouTube text font size after window resize: ", true);
			} else
				logReporter.log("Check YouTube text font size after window resize: ", false);
			break;
		}

		case "description": {
			if (this.font_YouTubeDescription > this.getFontSize(descriptionYouTube)) {
				logReporter.log("Check YouTube description font  size after window resize: ", true);
			} else
				logReporter.log("Check YouTube description font size after window resize: ", false);
			break;
		}

		case "logo": {
			if (this.font_YouTubeLogo > this.getFontSize(imageYouTube)) {
				logReporter.log("Check YouTube logo size after window resize: ", true);
			} else
				logReporter.log("Check YouTube logo size after window resize: ", false);
			break;
		}

		}

	}

	private void checkFontSizeOfInstagram(String text) {
		switch (text) {
		case "name": {
			if (this.font_InstagramText > this.getFontSize(txtInstagram)) {

				logReporter.log("Check Instagram Text font size after window resize: ", true);
			} else
				logReporter.log("Check Instagram Text font size after window resize: ", false);
			break;
		}

		case "description": {
			if (this.font_InstagramDescription > this.getFontSize(descriptionInstagram)) {
				logReporter.log("Check Instagram Description font block size after window resize: ", true);
			} else
				logReporter.log("Check Instagram Description font size after window resize: ", false);
			break;
		}

		case "logo": {
			if (this.font_InstagramLogo > this.getFontSize(imageInstagram)) {
				logReporter.log("Check Instagram Logo size after window resize: ", true);
			} else
				logReporter.log("Check Instagram Logo size after window resize: ", false);
			break;
		}

		}

	}

	public void clickOnUsefulLinks(String lnk)
	{
		By lnks = By.xpath("//ul[contains(@class,'useful-links')]//li//a[text()='"+lnk+"']");
		logReporter.log("Click on  " +lnk+ "under userful link section ", 
				objWebActions.click(lnks));
	}
	public void verifyUsefulLinksInFooter(String lnk) {
		
		By lnks = By.xpath("//ul[contains(@class,'useful-links')]//li//a[text()='"+lnk+"']");
		logReporter.log("verify  " +lnk+ "under userful link section ", 
				objWebActions.checkElementDisplayed(lnks));
	/*	switch (text) {

		case "Privacy Policy": {
			logReporter.log("Check Privacy Policy Link > >", objWebActions.checkElementDisplayed(linkPrivacyPolicy));
			break;
		}

		case "Terms and Conditions": {
			logReporter.log("Check Privacy Policy Link > >",
					objWebActions.checkElementDisplayed(linkTermsAOndConditions));
			break;
		}

		case "Our Mecca Promise": {
			logReporter.log("Check Privacy Policy Link > >", objWebActions.checkElementDisplayed(linkOurMeccaPromise));
			break;
		}

		case "Affiliates": {
			logReporter.log("Check Privacy Policy Link > >", objWebActions.checkElementDisplayed(linkAffiliates));
			break;
		}

		case "Mecca Club Terms": {
			logReporter.log("Check Privacy Policy Link > >", objWebActions.checkElementDisplayed(linkMeccaClubTerms));
			break;
		}
		case "Play Online Casino": {
			logReporter.log("Check Privacy Policy Link > >", objWebActions.checkElementDisplayed(linkPlayOnlineCasino));
			break;
		}
		case "Mecca Blog": {
			logReporter.log("Check Privacy Policy Link > >", objWebActions.checkElementDisplayed(linkMeccaBlog));
			break;
		}
		}*/
	}

	public void clickPrivacyPolicyLink() {
		logReporter.log("click 'Privacy Policy Link' > >", objWebActions.click(linkPrivacyPolicy));
	}

	public void clickTermsAndConditionsLink() {
		logReporter.log("click 'Terms And Conditions Link' > >", objWebActions.click(linkTermsAOndConditions));
	}

	public void clickOurMeccaPromiseLink() {

		if (objWebActions.checkElementDisplayed(linkOurMeccaPromise)) {
			logReporter.log("click 'Our Mecca Promise Link' > >", objWebActions.click(linkOurMeccaPromise));
		}
	}

	public void clickAffiliatesLink() {

		if (objWebActions.checkElementDisplayed(linkAffiliates)) {
			logReporter.log("click 'Affiliates Link' > >", objWebActions.click(linkAffiliates));
		}
	}

	public void clickMeccaClubTermsLink() {
		if (objWebActions.checkElementDisplayed(linkMeccaClubTerms)) {
			logReporter.log("click 'Club Terms Link' > >", objWebActions.click(linkMeccaClubTerms));
		}

	}

	public void clickPlayOnlineCasinoLink() {
		if (objWebActions.checkElementDisplayed(linkPlayOnlineCasino)) {
			logReporter.log("click 'Privacy Policy Link' > >", objWebActions.click(linkPlayOnlineCasino));
		}
	}

	public void clickmeccaBlogLink() {
		if (objWebActions.checkElementDisplayed(linkMeccaBlog)) {
			logReporter.log("click 'Play Mecca Blog Link' > >", objWebActions.click(linkMeccaBlog));
		}
	}

	public void verifyUsefulLink(String url) {
		// System.out.println(url);
		// System.out.println(objWebActions.getUrl());
		waitMethods.sleep(15);
		logReporter.log("check redirected url > >", url, objWebActions.getUrl());
	}

	public void verifyPartnersLogoInFooter(String text) {

		switch (text) {

		case "ESSA": {
			logReporter.log("Check ESSA Logo > >", objWebActions.checkElementDisplayed(logo_Essa));
			break;
		}

		case "IBAS": {
			logReporter.log("Check IBSA logo > >", objWebActions.checkElementDisplayed(logo_Ibsa));
			break;
		}

		case "18": {
			verify18LogoDisplayed();
			break;
		}

		case "Gambling Control": {
			logReporter.log("Check Privacy Policy Link > >", objWebActions.checkElementDisplayed(logo_GamblingControl));
			break;
		}

		case "GamCare": {
			logReporter.log("Check Privacy Policy Link > >", objWebActions.checkElementDisplayed(logo_GamCare));
			break;
		}
		case "GamStop": {
			logReporter.log("Check Privacy Policy Link > >", objWebActions.checkElementDisplayed(logo_GamStop));
			break;
		}
		case "Keep it fun": {
			verifyKeepItFunDisplayed();
			break;
		}
		case "Gambling Commision": {
			logReporter.log("Check Gambling Commision LOGO> >",
					objWebActions.checkElementDisplayed(logo_GamblingCommission));
			break;
		}
		case "Safer Gambling Standard": 
		{
			logReporter.log("Check Safer Gambling Standard LOGO > >",
					objWebActions.checkElementDisplayed(logo_saferGamblingStandard));
			break;}
		}
	}
	public void verify18LogoDisplayed() {
		logReporter.log("Verify '18+ Logo' displayed", 
				objWebActions.checkElementDisplayed(logo_18));
	}
	public void clickLogoEssa() {
		if (objWebActions.checkElementDisplayed(logo_Essa)) {
			logReporter.log("click 'Essa partner logo' > >", objWebActions.click(logo_Essa));
		}
	}

	public void clickLogoIbsa() {
		if (objWebActions.checkElementDisplayed(logo_Ibsa)) {
			logReporter.log("click 'Ibsa partner logo' > >", objWebActions.click(logo_Ibsa));
		}
	}

	public void clickLogoGamblingControl() {
		if (objWebActions.checkElementDisplayed(logo_GamblingControl)) {
			objWebActions.scrollToElement(logo_GamblingControl);
			logReporter.log("click 'Gambling Control partner logo' > >", objWebActions.click(logo_GamblingControl));
		}
	}

	public void clickLogoGamCare() {
		if (objWebActions.checkElementDisplayed(logo_GamCare)) {
			logReporter.log("click 'GamCare partner logo' > >", objWebActions.click(logo_GamCare));
		}

	}

	public void clickLogoGamStop() {
		if (objWebActions.checkElementDisplayed(logo_GamStop)) {
			logReporter.log("click 'GamStop partner logo' > >", objWebActions.click(logo_GamStop));
		}

	}

	public void clickLogoGamblingCommission() {
		if (objWebActions.checkElementDisplayed(logo_GamblingCommission)) {
			logReporter.log("click 'Gambling Commission partner logo' > >",
					objWebActions.click(logo_GamblingCommission));
		}
	}

	public void verifyVeriSignLogoNinformation() {
		By locator = By.xpath("//img[@title='Verisign Logo']//following-sibling::div//p[contains(.,'Your privacy and security is our number one priority here at Mecca Bingo. We protect your account with market-leading security technology so we')]");
		logReporter.log("Check VeriSign Secured Logo > >", objWebActions.checkElementDisplayed(logo_VeriSignSecured));
		logReporter.log("Check VeriSign Secured information > >", objWebActions.checkElementDisplayed(locator));
	}

	public void verifyPaymentProvidersBlockInFooter(String text) {

		switch (text) {

		case "Title": {
			logReporter.log("Check Title block in payment providers > >",
					objWebActions.checkElementDisplayed(paymentTitleBlock));
			break;
		}

		case "Description": {
			logReporter.log("Check Description block in payment providers > >",
					objWebActions.checkElementDisplayed(paymentDescriptionBlock));
			break;
		}

		case "Logos": {
			verifyPaymentMethodLogos("MasterCard~PayPal~Visa~paysafecard");
			break;
		}
		}
	}

	public void verifyPaymentMethodLogos(String paymentMethodNm)
	{
		if(paymentMethodNm.contains("~"))
		{
			String[] arr1 = paymentMethodNm.split("~");

			for (String labels2 : arr1) 
			{
				By locator = By.xpath("//li[contains(@class,'footer-payment')]//img[@title='"+labels2+"']");
				logReporter.log(" Verify ' "+labels2+ " ' paymentmethod logo is displayed under footer",  
						objWebActions.checkElementDisplayed(locator));
			}
		}
		else
		{
			By locator = By.xpath("//li[contains(@class,'footer-payment')]//img[@title='"+paymentMethodNm+"']");
			logReporter.log(" Verify ' "+paymentMethodNm+ " ' paymentmethod logo is displayed under footer",  
					objWebActions.checkElementDisplayed(locator));
		}
	}
	public void switchToChild() {
		objWebActions.switchToChildWindow();
	}

	public void click_Alderney_Gambling_Control_Commission_link() {

		logReporter.log("click 'Aderney Gambling Control Commision Link' > >",
				objWebActions.click(link_Alderney_Gambling_Control_Commission));
	}

	public void click_UK_Gambling_Commission_link() {

		logReporter.log("click 'UK Gambling Commission Link' > >", objWebActions.click(link_UK_Gambling_Commission));
	}

	public void click_BeGambleAware_link() {
		logReporter.log("click 'BeGambleAware Link' > >", objWebActions.click(link_BeGambleAware));
	}

	public void click_Rank_Group_link() {
		logReporter.log("click 'Rank_Group Link' > >", objWebActions.click(link_Rank_Group));

	}

	public void Verifyessalogo() {
		By essalogo = By.xpath("//*[@id=\"rstpl-main-menu-position\"]/div/div/div/nav/div/div/div[1]/a/img");
		logReporter.log("Verify essa logo", objWebActions.checkElementDisplayed(essalogo));
	}

	public void verifyKeepItFunDisplayed() {
		By logoKeepItFun = By.xpath("//a[contains(@href,'https://keepitfun.rank.com/')]//img[contains(@src,'keep-it-fun-logo_mecca_violet_.png')]");
		logReporter.log("Verify 'Keep It Fun Logo' displayed", 
				objWebActions.checkElementDisplayed(logoKeepItFun));

	}

	public void clickKeepItFunLogo() {
		By logoKeepItFun = By.xpath("//a[contains(@href,'https://keepitfun.rank.com/')]//img[contains(@src,'keep-it-fun-logo_mecca_violet_.png')]");
		String url = "https://keepitfun.rank.com/";
		logReporter.log("Click 'Keep It Fun Logo'", 
				objWebActions.click(logoKeepItFun));
		objWebActions.switchToChildWindow();
		String currentURL =objDriverProvider.getWebDriver().getCurrentUrl();
		logReporter.log("Redirect to the correct url ", currentURL.equalsIgnoreCase(url));
	}

	public void verifyLogoLinkRedirectToTheCorrectPage(String logoName)
	{
		switch(logoName)
		{

		case "Keep it fun":
			this.clickKeepItFunLogo();
			break;

		}
	}

	public void verifyAssociatedLicenseStatement()
	{
		
		//By locator = By.xpath("//p[contains(.,'Licensed and regulated by The Gambling Commission under licence ') and contains(.,' for GB customers playing on our online site. Licensed and regulated by ')and contains(.,' (licences ') and contains(.,') for non-GB customers playing on our online site. Licensed and regulated by The Gambling Commission under licence') and contains(.,'for customer playing in our land-based bingo clubs. Mecca Bingo is part of the ') and contains(.,'and the MECCA logos are registered trade marks of Rank Leisure Holdings Ltd.')]");
		By locator = By.xpath("//p[contains(.,'Mecca Bingo is Licensed and regulated by The Gambling Commission under licence') and contains(.,'GB customers playing on our online site. Licensed and regulated by ') and contains(.,'(licences') and contains(.,') for non-GB customers playing on our online site. Licensed and regulated by The Gambling Commission under licence') and contains(.,'customers playing in our land-based bingo clubs.')]");
		logReporter.log("verify Associated license statements ", 
				objWebActions.checkElementDisplayed(locator));
	}

	public void verifyLinksUnderAssociatedLicenseStatement(String lnk)
	{

		//a[text()='38750'][@href='https://registers.gamblingcommission.gov.uk/38750']

		//a[text()='The Alderney Gambling Control Commission'][@href='https://www.gamblingcontrol.org/']
		//a[text()='13 C1 & C2'][@href='https://www.gamblingcontrol.org/licensees/egambling-licensees/']
		//a[text()='2396'][@href='https://registers.gamblingcommission.gov.uk/2396']
		//a[text()='Rank Group'][@href='https://www.rank.com/']

		String[] arrSingleEntry = lnk.split("~"); //
		for(String singleEntry : arrSingleEntry) 
		{
			String[] entry = singleEntry.split(",");

			By locator = By.xpath("//a[text()='"+entry[0]+"'][@href='"+entry[1]+"']");
			logReporter.log(" Verify "+entry[1]+" section is displayed with "+entry[0]+" icon",
					objWebActions.checkElementDisplayed(locator));

		}
	}

	public void clickUKGamblingCommissionLink() {
		By loctor = By.xpath("//a[text()='38750'][@href='https://registers.gamblingcommission.gov.uk/38750']");
		//String url = "https://secure.gamblingcommission.gov.uk/PublicRegister/Search/Detail/38750";
		String url = "https://www.gamblingcommission.gov.uk/public-register/business/detail/38750";
		
		logReporter.log("Click '38750' Link", 
				objWebActions.click(loctor));
		objWebActions.switchToChildWindow();
		String currentURL = objDriverProvider.getWebDriver().getCurrentUrl();
		logReporter.log("Redirect to the correct url ", currentURL.equalsIgnoreCase(url));

	}

	public void verifyMeccaLogoInFooter()
	{
		By locator = By.xpath("//a[@title='Mecca Bingo']//img[contains(@alt,'Mecca Bingo Logo')]");
		logReporter.log(" Verify Mecca Bingo logo in footer",
				objWebActions.checkElementDisplayed(locator));
	}

	public void verifyAwardsListInFooter(String awdNm)
	{
		if(awdNm.contains("~")){
			String[] arr1 = awdNm.split("~");
			for (String links : arr1  ) {
				By locator = By.xpath("//ul[contains(@class,'list-of-awards')]//li//span//img[@title='"+links+"']");
				logReporter.log(links+" award displayed  ",
						objWebActions.checkElementDisplayed(locator));}}
		else{
			By locator = By.xpath("//ul[contains(@class,'list-of-awards')]//li//span//img[@title='"+awdNm+"']");
			logReporter.log(awdNm+" award displayed   ",
					objWebActions.checkElementDisplayed(locator));}

	}

	public void verifyKeepItFuninformation() {
		By locator = By.xpath("//a[@title='Keep it fun']//following-sibling::div//p[contains(.,'At Mecca Bingo, we want you to enjoy every second that you play with us. Find tips on how to play safely, discover places to get support and get information on all the tools we have in place at: ') and contains(.,'keepitfun.rank.com')]");
	//	By locator = By.xpath("//a[@title=\"Keep it fun\"]//following-sibling::div//p[contains(.,'Whether you play with Mecca Bingo or Grosvenor Casinos, and whether you play online, via mobile or in one of our land-based bingo clubs or casinos, we believe that gambling is something') and contains(.,'keepitfun.rank.com')]");
		logReporter.log("Check Keep it fun  information > >", objWebActions.checkElementDisplayed(locator));
	}

	
	public void verifyFooterMainSections(String section)
	{
		switch (section) {
		case "Mecca Logo": 
			this.verifyMeccaLogoInFooter();
			break;
		case "Awards Section": 
			//this.verifyAwardsListInFooter("Bingo Marketing Campaign~Innovation in Mobile and Tablet~Best Marketing Campaign 2020~Best Mobile Bingo Site 2020~Best Customer Service Mecca Bingo");
			this.verifyAwardsListInFooter("Best Marketing Campaign 2020~Best Mobile Bingo Site 2020~Best Customer Service Mecca Bingo");
			break;
		case "verisigned information": 
			verifyVeriSignLogoNinformation();
			break;
		case "Keep it fun information": 
			verifyKeepItFuninformation();
			break;
		case "Licensed Information":
			verifyAssociatedLicenseStatement();
			break;
		}
	}
	public void verifyArticleBlockHeaders(String hdrs)
	{
		String[] arrSingleEntry = hdrs.split("~"); //
		for(String singleEntry : arrSingleEntry) {
			String[] entry = singleEntry.split(";");
			By locator = By.xpath("//"+entry[0]+"[contains(.,'"+entry[1]+"')]");	
			logReporter.log(" Verify '"+entry[1]+"' ",
					objWebActions.checkElementDisplayed(locator));	}
	}
	public void verifyURLRedirection(String expectedurl) {

		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
		String currentURL =objDriverProvider.getWebDriver().getCurrentUrl();
		System.out.println("currentURL "+currentURL);
		System.out.println("currentURL "+currentURL+" exp "+expectedurl);
		logReporter.log("Redirect to the correct url ", currentURL.equalsIgnoreCase(expectedurl));
	}
}
