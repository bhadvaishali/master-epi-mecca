/**
 * 
 */
package com.meccabingo.desktop.page.Mecca;

import org.openqa.selenium.By;
import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.Utilities;
import com.generic.utils.WaitMethods;
import com.mifmif.common.regex.Generex;

public class RegistrationPage {
	private WebActions webActions;
	private LogReporter logReporter;
	private Utilities utilities;
	private WaitMethods wait;
	private Configuration configuration;

	public RegistrationPage(WebActions webActions, LogReporter logReporter, WaitMethods wait, Utilities utilities,Configuration configuration) {
		this.webActions = webActions;
		this.logReporter = logReporter;
		this.utilities = utilities;
		this.wait = wait;
		this.configuration = configuration;
	}

	String randomEmail;

	/*String greencolor = "#5ea85a";
	String greycolor = "#B3B3B3";//"#e6dae6";
	String redcolor = "#e22c2c";*/
	String greencolor = "#009D7A";
	String greycolor = "#5ECCF3";
	String redcolor = "#B41D68";

	By emailcheckbox = By.xpath("//input[contains(@id,'gdpr-email')]");
	By yesbutton = By.xpath("//button[contains(.,'Yes')]");
	By selectall = By.xpath("//button[contains(@class,'colour-button-green')]//span[contains(.,'Select All')]");
	By SMScheckbox = By.xpath("//input[contains(@id,'gdpr-sms')]");
	By phonecheckbox = By.xpath("//input[contains(@id,'gdpr-phone')]");
	By postcheckbox = By.xpath("//input[contains(@id,'gdpr-post')]");
	By username = By.xpath("//input[contains(@id,'username')]");
	By password = By.xpath("//input[contains(@id,'password')]");

	By title = By.xpath("//h4[contains(text(),'Title')]");
	By firstname = By.xpath("//input[contains(@id,'firstname')]");
	By surname = By.xpath("//input[contains(@id,'surname')]");
	By dateofbirth = By.xpath("//div[contains(@class,'ip-dob-inner')]");
	By day = By.xpath("//input[contains(@id,'dob-day')]");
	By month = By.xpath("//input[contains(@id,'dob-month')]");
	By year = By.xpath("//input[contains(@id,'dob-year')]");
	By notyou = By.xpath("//a[contains(text(),'Not You')]");

	By Ms = By.xpath("//button[contains(.,'Ms')]");
	By Mr = By.xpath("//button[contains(.,'Mr')]");
	By Miss = By.xpath("//button[contains(.,'Miss')]");
	By Mrs = By.xpath("//button[contains(.,'Mrs')]");
	By Dr = By.xpath("//button[contains(.,'Dr')]");
	By Mx = By.xpath("//button[contains(.,'Mx')]");

	By emailtextbox = By.xpath("//input[@placeholder='Email address']");
	By agecheckbox = By.xpath("//input[contains(@id,'id-agreed')]");
	By next = By.xpath("//div[contains(@class,'join-now')]/form/fieldset[3]/button");
	By membership = By.xpath("//input[contains(@type,'number')]");
	By secondemail = By.xpath("//input[contains(@type,'email')]");
	By mobilenumber = By.xpath("//input[contains(@id,'mobile')]");

	By EnterAddressManuallyBtn = By.xpath("//form[contains(@class,'join-now')]/fieldset[3]/button");
	By firstAddress = By.xpath("//input[contains(@id,'input-address-line1')]");
	By secondAddress = By.xpath("//input[contains(@id,'input-address-line2')]");
	By Town_City = By.xpath("//input[contains(@id,'town-city')]");
	By country = By.xpath("//input[contains(@id,'county')]");
	By postcode = By.xpath("//input[contains(@id,'address-lookup-search')]"); // input[contains(@id,'postcode')]
	By uk = By.xpath("//select[contains(@id,'input-country')]/option[contains(text(),'United Kingdom')]");
	By countrydropdown = By.xpath("//select[contains(@id,'input-country')]");
	By addressFromPostcodeSearch = By.xpath("//ul[contains(@class,'postcode-search-results')]/li[1]");

	public void clickJoindNowbtn() {
	//	By JoinNow = By.xpath("//a[contains(text(),'Join Now')]");
		By JoinNow = By.xpath("//a[contains(text(),'Join')]");
		logReporter.log("click on join now button > >", webActions.click(JoinNow));
	}

	public String enterEmailaddress() {
		//wait.sleep(5);
		String randomemail = utilities.getEmail();
		System.out.println("*******   randomemail  "+randomemail);
		webActions.click(emailtextbox);
		logReporter.log("enter email address", webActions.setTextWithClear(emailtextbox, randomemail));
		return randomemail;
	}
	//	public String getEmail() {
	//		String regex = "[a-zA-Z0-9]{5}\\@mailinator\\.com";
	//		this.randomEmail = new Generex(regex).random();
	//		return randomEmail;
	//	}

	public void enterEmailaddressOnSecondPage() {
		logReporter.log("enter email address", webActions.setTextWithClear(secondemail, "6abd32@gmail.com"));
	}

	public void verifyEmailaddressOnSecondPageTextboxColor() {

		wait.sleep(3);
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(secondemail, "border-bottom-color", greencolor));
	}

	public void enterRegisteredEmailaddress(String email) {
		logReporter.log("enter email address", webActions.setTextWithClear(emailtextbox, email));
	}

	public void clickAgecheckbox() {

		logReporter.log("click on age checkbox > >", webActions.clickUsingJS(agecheckbox));
	}

	public void clickOnNextbtn() {

		logReporter.log("click on next button > >", webActions.click(next));
	}

	public void clickOnEmailCheckbox() {

		logReporter.log("click on email checkbox > >", webActions.click(emailcheckbox));
		logReporter.log("email checkbox is selected > >", webActions.isCheckBoxSelected(emailcheckbox));
	}

	public void clickOnSMSCheckbox() {

		logReporter.log("click on SMS checkbox > >", webActions.click(SMScheckbox));
		logReporter.log("sms checkbox is selected > >", webActions.isCheckBoxSelected(SMScheckbox));
	}

	public void clickOnPhoneCheckbox() {
		logReporter.log("click on Phone checkbox > >", webActions.click(phonecheckbox));
		logReporter.log("phone checkbox is selected > >", webActions.isCheckBoxSelected(phonecheckbox));
	}

	public void clickOnPostCheckbox() {

		logReporter.log("click on post checkbox > >", webActions.click(postcheckbox));
		logReporter.log("post checkbox is selected > >", webActions.isCheckBoxSelected(postcheckbox));
	}

	public void clickOnDisableRegisterCTA() {
		By register = By.xpath("//button[contains(.,'Register')]");
		logReporter.log("click on post checkbox > >", webActions.clickOnDisabledElement(register));
	}

	public void clickRegisterCTA() {
		By register = By.xpath("//button[@type='submit']/span[text()='Register']");
		logReporter.log("click on Register button > >", webActions.clickUsingJS(register));
	}
	public void clickOnSelectAllbtn() {
		//	webActions.scrollToElement(selectall);

		//webActions.checkElementToBeClickablWithMidWait(selectall);
		logReporter.log("click on SelectAll checkbox > >", webActions.clickUsingJS(selectall));
		wait.sleep(5);
	}

	public void clickNoIhaveaMembershipCardbtn() {
		wait.sleep(5);
		System.out.println("*****  ddddd");
		
	//	By membershipbtn = By.xpath("//button[contains(.,'NO (I have a membership card)')]");
		By membershipbtn = By.xpath("//button[contains(.,'NO')]");
		//webActions.checkElementDisplayedWithMidWait(membershipbtn);
		logReporter.log("click on No (I have a membership card) button > >", webActions.clickUsingJS(membershipbtn));
		wait.sleep(5);
	}

	public void clickOnNotYouLink() {
		logReporter.log("click on Not You link > >", webActions.click(notyou));
	}

	public void verifyNotYouLink() {
		logReporter.log("", webActions.checkElementDisplayed(notyou));
	}

	public void verifyAllSelectedCheckboxes(String checkboxes) {
		switch (checkboxes) {
		case "Email":
			logReporter.log("email checkbox is selected > >", webActions.isCheckBoxSelected(emailcheckbox));
			break;
		case "SMS":
			logReporter.log("sms checkbox is selected > >", webActions.isCheckBoxSelected(SMScheckbox));
			break;
		case "Phone":
			logReporter.log("phone checkbox is selected > >", webActions.isCheckBoxSelected(phonecheckbox));
			break;
		case "post":
			logReporter.log("post checkbox is selected > >", webActions.isCheckBoxSelected(postcheckbox));
			break;
		}

	}

	public void verifyMembershipScreenOptions(String options) {
		switch (options) {
		case "membershipnumberbox":
			logReporter.log(" > >", webActions.checkElementDisplayed(membership));
			break;
		case "DOBbox":
			logReporter.log(" > >", webActions.isCheckBoxSelected(dateofbirth));
			break;
		case "agecheckbox":
			logReporter.log("p > >", webActions.isCheckBoxSelected(agecheckbox));
			break;
		case "nextbtn":
			logReporter.log(" > >", webActions.isCheckBoxSelected(next));
			break;
		}

	}

	public void verifyColorOfErrorMessage() {
		By errormessage = By.xpath("//li[contains(.,'contact preference')]");
		String errorcolor = "#e22c2c";// "rgba(226, 44, 44, 1)";
		logReporter.log("Check color of error message > >",
				webActions.checkCssValue(errormessage, "color", errorcolor));
	}

	public String enterUsername() {
		String randomUserName = "MB" + utilities.getUsername();
		//String randomUserName = "MB" + utilities.getUsername()+"OPST";
		logReporter.log("enter email address", webActions.setTextWithClear(username, randomUserName));
		return randomUserName;
	}

	public void enterInvalidUsername() {
		String regex = "[a-zA-Z]{4}";
		String randomusername = new Generex(regex).random();
		logReporter.log("enter email address", webActions.setTextWithClear(username, randomusername));
	}

	public void verifyUsernameTextboxColor() {

		logReporter.log("Check color of error message > >",
				webActions.checkCssValue(username, "border-bottom-color", greencolor));
	}

	public void enterExistingUsername(String existingusername) {
		logReporter.log("enter username", webActions.setTextWithClear(username, existingusername));
	}

	public void verifyErrorMessages(String errormessage) {
		By error = By.xpath("//li[contains(.,'" + errormessage + "')]");
		logReporter.log("Check error message '" +errormessage+ " '", webActions.checkElementDisplayed(error));
	}

	public void verifyAllFieldsOnPage(String textboxes) {
		switch (textboxes.toLowerCase()) {
		case "title":
			logReporter.log("Check title text ", webActions.checkElementDisplayed(title));
			break;
		case "first name":
			logReporter.log("Check firstname field", webActions.checkElementDisplayed(firstname));
			break;
		case "surname":
			logReporter.log(" Check surname field", webActions.checkElementDisplayed(surname));
			break;
		case "date of birth":
			logReporter.log("Check dateofbirth field", webActions.checkElementDisplayed(dateofbirth));
			break;
		case "username":
			logReporter.log(" Check username field", webActions.checkElementDisplayed(username));
			break;
		case "password":
			logReporter.log(" Check password field", webActions.checkElementDisplayed(password));
			break;
		}
	}

	public void verifyAllTitlesOnPage(String titles) {
		switch (titles) {
		case "Ms":
			logReporter.log("Check title 'Ms' is displayed", webActions.checkElementDisplayed(Ms));
			break;
		case "Mr":
			logReporter.log("Check title 'Mr' is displayed", webActions.checkElementDisplayed(Mr));
			break;
		case "Miss":
			logReporter.log("Check title 'Miss' is displayed", webActions.checkElementDisplayed(Miss));
			break;
		case "Mrs":
			logReporter.log("Check title 'Mrs' is displayed", webActions.checkElementDisplayed(Mrs));
			break;
		case "Mx":
			logReporter.log("Check title 'Mx' is displayed", webActions.checkElementDisplayed(Mx));
			break;
		}

	}

	public void enterFirstName() {
		String regex = "[a-zA-Z]{4}";
		String randomfirstname = new Generex(regex).random();
		logReporter.log("enter firstname", webActions.setTextWithClear(firstname, randomfirstname));
		webActions.pressKeybordKeys(firstname, "tab");
	}

	public void enterFirstName(String fname) {
		logReporter.log("enter firstname as "+fname, webActions.setTextWithClear(firstname, fname));
		webActions.pressKeybordKeys(firstname, "tab");
	}
	public void verifyFirstNameTextboxColor() {

		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(firstname, "border-bottom-color", greencolor));
	}

	public void enterSurName() {
		String regex = "[a-zA-Z]{4}";
		String randomsurname = new Generex(regex).random();
		logReporter.log("enter surname ", webActions.setTextWithClear(surname, randomsurname));
	}

	public void enterSurName(String sname) {
		logReporter.log("enter surname as "+sname, webActions.setTextWithClear(surname, sname));
		webActions.pressKeybordKeys(surname, "tab");
	}
	public void verifySurNameTextboxColor() {

		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(surname, "border-bottom-color", greencolor));
	}

	public void enterDateOfBirth(String Day, String Month, String Year) {
		logReporter.log("Enetr day", webActions.setTextWithClear(day, Day));
		logReporter.log("Enetr day", webActions.setTextWithClear(month, Month));
		logReporter.log("Enetr day", webActions.setTextWithClear(year, Year));
		webActions.pressKeybordKeys(year, "tab");
	}

	public void verifyDateOfBirthTextboxColor() {

		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(dateofbirth, "border-bottom-color", greencolor));
	}

	public String enterPassword() {
		String randomPassword = utilities.genarateRandomPassword();
		//String randomPassword = "Test@1234";
		logReporter.log("enter password", webActions.setTextWithClear(password, randomPassword));
		return randomPassword;
	}

	public void setPassword(String pwd) {
		logReporter.log("enter password", webActions.setTextWithClear(password, pwd));
	}


	public void enterInvalidPassword() {
		String regex = "[a-zA-Z]{8}";
		String randompassword = new Generex(regex).random();
		logReporter.log("enter password", webActions.setTextWithClear(password, randompassword));
	}

	public void verifyPasswordTextboxColor() {

		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(password, "border-bottom-color", greencolor));
	}

	public void verifyPasswordTextboxGreyColor() {
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(password, "border-bottom-color", greycolor));
	}
	public void verifyRedColorBelowPassword() {
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(password, "border-bottom-color", redcolor));
	}
	public void verifyUsernameTextboxGreyColor() {
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(username, "border-bottom-color", greycolor));
	}
	public void verifyRedColorBelowUsername() {
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(username, "border-bottom-color", redcolor));
	}
	public void verifyEmailTextboxGreyColor() {
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(secondemail, "border-bottom-color", greycolor));
	}

	public void verifyMobileNumberTextboxGreyColor() {
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(mobilenumber, "border-bottom-color", greycolor));
	}

	public void verifyHelpTextBelowInput(String hinttext) {
		By helptextbelowinput = By.xpath("//span[contains(text(),'" + hinttext + "')]");
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkElementDisplayed(helptextbelowinput));
	}

	public void verifyYesBtnIsSelected() {
		String color = "#e12482";
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(yesbutton, "background-color", color));

	}

	public void verifyEmailTextboxColorOnFirstPage() {

		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(emailtextbox, "border-bottom-color", greencolor));
	}

	public void verifySelectAllbtn() {
		logReporter.log("Verify select all button", webActions.checkElementDisplayed(selectall));
	}

	public void clickOnDisabledNextbtn() {
		By next = By.xpath("//div[contains(@class,'join-now')]/form/fieldset[3]/button");
		logReporter.log("click on next button > >", webActions.clickOnDisabledElement(next));
	}

	public void verifyNextbtnIsEnabled() {

		logReporter.log("Verify next button is enabled", webActions.click(next));
	}

	//	public void verifyHelpbtnDisappear() {
	//		By helpbtn = By.xpath("//a[contains(@class,'livechat')]");
	//		if (!webActions.checkElementDisplayed(helpbtn)) {
	//			logReporter.log("check help button dissappears", true);
	//		}
	//
	//	}

	public void verifyErrorOnFirstRegPage(String message) {
		By errormessage = By.xpath("//ul[contains(@class,'errors')]/li[contains(text()," + message + ")]");
		logReporter.log("click on next button > >", webActions.checkElementDisplayed(errormessage));
	}

	public void enterInvalidEmailaddress() {
		String regex = "[a-zA-Z0-9]{5}\\@gmail\\com";
		String randomInvalidEmail = new Generex(regex).random();
		By locator = By.xpath("//div[contains(@class,'join-now')]/form/fieldset[2]/div/div/input");
		logReporter.log("enter email address", webActions.setTextWithClear(locator, "sdsds@xx.d"));
		webActions.pressKeybordKeys(locator, "tab");
	}

	public void enterMembershipNumber(String number) {
		logReporter.log("enter Membership Number", webActions.setTextWithClear(membership, number));
	}

	public void clickOnDrtitle() {
		logReporter.log("click on title mrs", webActions.click(Mrs));
	}

	public void verifyMrstitleIsSelected() {
		//	String color = "#A94976";//"#d35b94";
		wait.sleep(30);
		By locator = By.xpath("//button[contains(@class,'button-pink')]//span[contains(.,'Mrs')]");
		logReporter.log("Mrs highlighted",
				webActions.checkElementDisplayed(locator));

	}

	public void verifyFirstRegPage() {
		logReporter.log("verify first registration page", webActions.checkElementDisplayed(next));
	}

	public void enterMobileNumber() {
		String regex = "[1-9]{9}";
		String randomnumber = new Generex(regex).random();
		logReporter.log("enter mobile number", webActions.setTextWithClear(mobilenumber, randomnumber));
		webActions.pressKeybordKeys(mobilenumber, "tab");
	}

	public void enterInvalidMobileNumber() {
		String regex = "[a-zA-Z][1-9]{6}";
		String randomnumber = new Generex(regex).random();
		logReporter.log("enter mobile number", webActions.setTextWithClear(mobilenumber, randomnumber));
		webActions.pressKeybordKeys(mobilenumber, "tab");
		wait.sleep(5);
	}

	public void setMobileNumber(String no) {
		logReporter.log("enter mobile number", webActions.setTextWithClear(mobilenumber, no));
		webActions.pressKeybordKeys(mobilenumber, "tab");
		wait.sleep(5);
	}
	public void verifyMobileNumberTextboxColor() {
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(mobilenumber, "border-bottom-color", greencolor));
	}

	public void verifyUKasDefaultValue() {

		logReporter.log("verify default value as UK", webActions.checkElementDisplayed(uk));
	}

	public void selecyUKInDropdown() {
		By locator = By.xpath("//option[text()='United Kingdom']");
		logReporter.log("select second option from dropdown", webActions.click(locator));
	}

	public void selectRepublicOptionInCountry() {
		By locator = By.xpath("//option[text()='Republic Of Ireland']");
		logReporter.log("select second option from dropdown", webActions.click(locator));
	}

	public void verifyEnterAddressManuallyBtn() {
		logReporter.log("check Enter Manually button", webActions.checkElementDisplayed(EnterAddressManuallyBtn));
	}

	public void clickOnEnterAddressManuallyBtn() {
		logReporter.log("click on enter address manually button", webActions.click(EnterAddressManuallyBtn));
	}

	public void verifyEnterAddressManuallyBtnDisappears() {
		if (!webActions.checkElementExists(EnterAddressManuallyBtn)) {
			logReporter.log("check Enter Manually button dissappears", true);
		}
	}

	public void verifyTownCityField() {
		logReporter.log("verify town and city field", webActions.checkElementDisplayed(Town_City));
	}

	public void enterValidInputInTownCityField() {
		logReporter.log("enter valid input in town/city field", webActions.setTextWithClear(Town_City, "United Kingdom"));
	}

	public void enterinvalidInputInTownCityField() {
		logReporter.log("enter invalid input in town/city field", webActions.setTextWithClear(Town_City, "United Kingdom"));
	}

	public void verifyTownCityFieldDisappears() {
		if (!webActions.checkElementExists(Town_City)) {
			logReporter.log("check Enter Manually button dissappears", true);
		}
	}

	public void verifyGreenColorBelowTownCityField() {
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(Town_City, "border-bottom-color", greencolor));
	}

	public void verifyCountryField() {
		logReporter.log("verify countrys field", webActions.checkElementDisplayed(country));
	}

	public void verifyCountryFieldDisappears() {
		if (!webActions.checkElementExists(country)) {
			logReporter.log("check Enter Manually button dissappears", true);
		}
	}

	public void verifyPostcodeField() {
		logReporter.log("verify countrys field", webActions.checkElementDisplayed(postcode));
	}

	public void clickOnPostcodeField() {
		logReporter.log("click on postcode field", webActions.click(postcode));
	}

	public void enterValidPostcode(String Postcode) {
		logReporter.log("enter valid postcode", webActions.setTextWithClear(postcode, Postcode));
		wait.sleep(10);
	}

	public void enterInvalidPostcode() {
		logReporter.log("enter invalid postcode", webActions.setTextWithClear(postcode, "t6776767@#$5fghg"));
		try {
			wait.sleep(30);
		}

		catch(Exception e) {
			e.printStackTrace();
		}
	}

	public void verifyRedColorBelowPostcode() {
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(postcode, "border-bottom-color", redcolor));
	}

	public void verifyGreyColorBelowPostcode() {
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(postcode, "border-bottom-color", greycolor));
	}

	public void verifyGreenColorBelowPostcode() {
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(postcode, "border-bottom-color", greencolor));
	}

	public void verifyHelpTextDisappearsBelowPostcode() {
		By helptext = By.xpath("//li[contains(text(),'Please enter a postcode')]");
		if (!webActions.checkElementExists(helptext)) {
			logReporter.log("help text below postcode disappear", true);
		}
	}

	public void clickOnMobileNumberField() {
		logReporter.log("click on mobile number field", webActions.click(mobilenumber));
	}

	public void verifyGreyColorBelowMobileNumber() {
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(mobilenumber, "border-bottom-color", greycolor));
	}

	public void verifyGreenColorBelowMobileNumber() {
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(mobilenumber, "border-bottom-color", greencolor));
	}

	public void verifyRedColorBelowMobileNumber() {
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(mobilenumber, "border-bottom-color",redcolor));
	}

	public void verifyFirstAddressField() {
		logReporter.log("verify address line 1", webActions.checkElementDisplayed(firstAddress));
	}

	public void enterValidInputInFirstAddress() {
		logReporter.log("enter first address", webActions.setTextWithClear(firstAddress, "washington"));
		webActions.pressKeybordKeys(firstAddress, "tab");
	}

	public void verifyFirstAddressFieldDissappears() {
		if (!webActions.checkElementExists(firstAddress))
			logReporter.log("", true);
	}

	public void verifyGreenColorBelowFirstAddress() {
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(firstAddress, "border-bottom-color", greencolor));
	}

	public void verifyRedColorBelowFirstAddress() {
		wait.sleep(15);
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(firstAddress, "border-bottom-color", redcolor));
	}

	public void verifySecondAddressField() {
		logReporter.log("verify address line 1", webActions.checkElementDisplayed(secondAddress));
	}

	public void enterValidInputInSecondAddress() {
		logReporter.log("enter second address", webActions.setTextWithClear(secondAddress, "DC"));
	}

	public void verifySecondAddressFieldDissappears() {
		if (!webActions.checkElementExists(secondAddress))
			logReporter.log("", true);
	}

	public void verifyGreenColorBelowSecondAddress() {
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(secondAddress, "border-bottom-color", greencolor));
	}

	public void verifyRedColorBelowSecondAddress() {
		logReporter.log("Check color of textbox bottom border > >",
				webActions.checkCssValue(secondAddress, "border-bottom-color",redcolor));
	}

	public void selectAddressFromPostcodeSearch() {
		//if(webActions.checkElementDisplayedWithMidWait(addressFromPostcodeSearch))
		logReporter.log("select address from postcode search", webActions.click(addressFromPostcodeSearch));
	}

	public void EnterInvalidInputInFirstAddress() {
		logReporter.log("click on fiest address", webActions.click(firstAddress));
		logReporter.log("click tab", webActions.pressKeybordKeys(firstAddress, "tab"));
	}

	public void EnterInvalidInputInSecondAddress() {
		logReporter.log("click on second address ", webActions.click(secondAddress));
		logReporter.log("click tab", webActions.pressKeybordKeys(secondAddress, "tab"));
	}
	public void clearUsername() {
		logReporter.log("", webActions.clearText(username));
	}

	public void verifyH4HeadingDisplayed(String strText) {
		System.out.println("*********************************msgggg");
		By locator = By.xpath("//h4[contains(text(),'" + strText + "')]");
		if (webActions.checkElementExists(locator))
			logReporter.log("text displayed'" + strText + "' displayed >>", true);
		else
			logReporter.log( strText + "'\"text is not displayed'\" +", false);
	}

	public void clickCheckbox(String chkboxName) {
		By locator = By.xpath("//input[@type='checkbox' and @id='gdpr-" + chkboxName + "']");
		logReporter.log("select checkbox > >"+chkboxName, webActions.click(locator));
	}

	public void verifyCheckboxIsChecked(String chkboxName) {
		By locator = By.xpath("//input[@id='gdpr-" + chkboxName + "']");
		String checkValue = webActions.getAttribute(locator, "value");
		if (checkValue.equals("true"))
			logReporter.log(chkboxName + "checkbox is checked :", true);
		else
			logReporter.log(chkboxName + "checkbox is checked :", false);

	}

	public void verifyCheckboxIsUnChecked(String chkboxName) {
		By locator = By.xpath("//input[@id='gdpr-" + chkboxName + "']");
		String checkValue = webActions.getAttribute(locator, "value");
		if (checkValue.equals("false"))
			logReporter.log(chkboxName + "checkbox is unchecked :", true);
		else
			logReporter.log(chkboxName + "checkbox is unchecked :", false);

	}

	public void verifyAllCheckboxesChecked() {
		By chkboxEmail = By.xpath("//input[@id='gdpr-email']");
		By chkboxSMS = By.xpath("//input[@id='gdpr-sms']");
		By chkboxPhone = By.xpath("//input[@id='gdpr-phone']");
		By chkboxPost = By.xpath("//input[@id='gdpr-post']");

		String valueEmail = webActions.getAttribute(chkboxEmail, "value");
		String valueSMS = webActions.getAttribute(chkboxSMS, "value");
		String valuePhone = webActions.getAttribute(chkboxPhone, "value");
		String valuePost = webActions.getAttribute(chkboxPost, "value");
		if (valueEmail.equals("true") == true && valueSMS.equals("true") == true && valuePhone.equals("true") == true
				&& valuePost.equals("true") == true)
			logReporter.log("All the checkboxes are checked", true);
		else
			logReporter.log("All the checkboxes are checked", false);

	}

	public void verifyAllCheckboxesUnChecked() {
		By chkboxEmail = By.xpath("//input[@id='gdpr-email']");
		By chkboxSMS = By.xpath("//input[@id='gdpr-sms']");
		By chkboxPhone = By.xpath("//input[@id='gdpr-phone']");
		By chkboxPost = By.xpath("//input[@id='gdpr-post']");

		String valueEmail = webActions.getAttribute(chkboxEmail, "value");
		String valueSMS = webActions.getAttribute(chkboxSMS, "value");
		String valuePhone = webActions.getAttribute(chkboxPhone, "value");
		String valuePost = webActions.getAttribute(chkboxPost, "value");
		if (valueEmail.equals("true") == false && valueSMS.equals("true") == false && valuePhone.equals("true") == false
				&& valuePost.equals("true") == false)
			logReporter.log("All the checkboxes are unchecked", true);
		else
			logReporter.log("All the checkboxes are unchecked", false);

	}

	public void verifyValidPasswordInfoText(String txt)
	{
		By locator = By.xpath("//ul[contains(@class,'valid-password')]//li[contains(@class,'p-v-2 flex-d-f flex-ai-c')][contains(.,'"+txt+"')]");
		logReporter.log("Verify ' "+txt+" '", webActions.checkElementDisplayed(locator));
	}

	public boolean verifyCheckboxIsCheckedOrNot(String chkboxName) {
		boolean flag =false;
		By locator = By.xpath("//input[@id='gdpr-" + chkboxName + "']");
		String checkValue = webActions.getAttribute(locator, "value");
		if (checkValue.equals("true"))
			flag=true;
		else
			flag=false;

		return flag;
	}

	public void setmanualAddress(String addressLine1, String addressLine2, 
			String townCity, String country1, String postCode) 
	{
		logReporter.log("Set address line1", addressLine1, 
				webActions.setTextWithClear(firstAddress, addressLine1));
		logReporter.log("Set address line2", addressLine2, 
				webActions.setTextWithClear(secondAddress, addressLine2));
		logReporter.log("Set Town/City", townCity, 
				webActions.setTextWithClear(Town_City, townCity));
		logReporter.log("Set country", country1, 
				webActions.setTextWithClear(country, country1));
		setPostCode(postCode);
	}
	public String getAddress()
	{
		String address = webActions.getAttribute(firstAddress, "value") +" "+webActions.getAttribute(secondAddress, "value") + " "+webActions.getAttribute(Town_City, "value") + " "+webActions.getAttribute(country, "value");
		System.out.println("adress :::  "+address);
		return address;
	}
	public void setPostCode(String postCode)
	{
		By locator = By.id("input-postcode");
		logReporter.log("Set post code", postCode, 
				webActions.setTextWithClear(locator, postCode));
	}
	public void setUserName(String usname) {
		logReporter.log("enter email address", webActions.setTextWithClear(username, usname));
		webActions.pressKeybordKeys(username, "tab");
	}
	public void verifyErrorMessageDoesNotDisplay(String err)
	{
		if(err.contains("~")){
			String[] arr1 = err.split("~");
			for (String links : arr1  ) {
				By errMsg = By.xpath("//ul[@class='errors']//li[text()='"+links+"']");
				logReporter.log(links+"  not displayed  ",
						webActions.checkElementNotDisplayed(errMsg));}}
		else{
			By errMsg = By.xpath("//ul[@class='errors']//li[text()='"+err+"']");
			logReporter.log(err+" not displayed  ",
					webActions.checkElementNotDisplayed(errMsg));}

	}
	public void verifyDepositNowHeader()
	{
		wait.sleep(configuration.getConfigIntegerValue("maxwait"));
		By hdrDeposit = By.xpath("//h4[contains(.,'Deposit')]");
		By DepositInfoText = By.xpath("//p[contains(.,'By depositing, you understand we hold your funds securely in a designated bank account with a medium level of protection.')]//following-sibling::a[contains(.,'See details')]");
		//By hdrDeposit = By.xpath("//form[@class=\"safecharge\"]");
		verifyStepCount("Create Account", "3");
		/*logReporter.log(" verify 'Deposit' header is displayed on screen" ,
				webActions.checkElementDisplayed(hdrDeposit));*/
	//	logReporter.log(" verify 'By depositing, you understand we hold your funds securely in a designated bank account with a medium level of protection.' text is displayed on screen" ,
			//	webActions.checkElementDisplayed(DepositInfoText));
	}

	public void verifyStepCount(String headerTxt,String stepNo)
	{
		By locator = By.xpath("//h4[contains(.,'"+headerTxt+"')]//following-sibling::p[contains(.,'Step') and contains(.,'"+stepNo+"') and contains(.,'of') and contains(.,'3')]");
		logReporter.log(" verify ' "+headerTxt+ " ' header is displayed with step ' " +stepNo+ " 'of 3 ",
				webActions.checkElementDisplayed(locator));
				
	}
	public void verifyEmailFieldonScreen()
	{
		logReporter.log(" verify 'Email' field on regisration screen" ,webActions.checkElementDisplayed(secondemail));
	}

	public void verifyMobileNumberFieldOnScreen()
	{
		logReporter.log(" verify 'Mobile number' field on regisration screen" ,webActions.checkElementDisplayed(mobilenumber));
	}
	public void setMembershipNumberFromConfig() {
		String memNumber = configuration.getConfig("web.Retail.membershipNumber");
		logReporter.log("Enter Value in userName > >", webActions.setTextWithClear(membership, memNumber));
	}

	public void enterDateOfBirthFromConfig() {
		String dob = configuration.getConfig("web.Retail.DOB");
		String date[] = dob.split("/");
		logReporter.log("Enetr day", webActions.setTextWithClear(day, date[0]));
		logReporter.log("Enetr Month", webActions.setTextWithClear(month, date[1]));
		logReporter.log("Enetr year", webActions.setTextWithClear(year, date[2]));
		webActions.pressKeybordKeys(year, "tab");
	}
}