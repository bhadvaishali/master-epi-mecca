/**
 * 
 */
package com.meccabingo.desktop.page.Mecca;

import org.openqa.selenium.By;

import com.generic.WebActions;
import com.generic.logger.LogReporter;

/**
 * @author Harshvardhan Yadav, Namrata Donikar (Expleo)
 *
 */
public class CmsHomePage {

	private WebActions objWebActions;
	private CmsLandingPage ObjCmsLandingPage;
	private LogReporter logReporter;

	public CmsHomePage(WebActions webActions, CmsLandingPage cmsLandingPage, LogReporter logReporter) {
		this.objWebActions = webActions;
		this.ObjCmsLandingPage = cmsLandingPage;
		this.logReporter = logReporter;
	}

	public void verifyGrosvenorHomePageLogo() {
		By header_logo = By.xpath("//a[@class='logo']");
		logReporter.log("check element > >", objWebActions.checkElementDisplayed(header_logo));
	}

	public void clickLoginButton() {
		By login_button = By.xpath("//button[@class='open-login' and contains(text(),'Login')]");
		logReporter.log("click 'login button' > >", objWebActions.click(login_button));
	}

	public void verifyUserLoggedIn() {
		String beforeUserButton = "//button[@title='";
		String afterUserButton = "']";
		By userLoggedIn = By.xpath(beforeUserButton + ObjCmsLandingPage.user + afterUserButton);
		logReporter.log("check element > >", objWebActions.checkElementDisplayed(userLoggedIn));
	}

	public void clickToggleNavigationPanel() {
		By toggleNavigationPanel = By.xpath("//span[@title='Toggle navigation pane']");
		logReporter.log("click 'toggle panel' > >", objWebActions.click(toggleNavigationPanel));
	}

	public void clickOnContentConfiguration() {
		By contentConfiguration = By.xpath("//span[contains(text(),'Content Configuration')]");
		objWebActions.mouseHover(contentConfiguration);
		logReporter.log("click 'content configuration' > >", objWebActions.click(contentConfiguration));

		By pin = By.xpath("(//span[@title='Pin'])[1]");
		objWebActions.click(pin);
	}

}
