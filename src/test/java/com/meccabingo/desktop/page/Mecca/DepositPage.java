package com.meccabingo.desktop.page.Mecca;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.WaitMethods;
import com.generic.webdriver.DriverProvider;



public class DepositPage {
	WebActions objWebActions;
	private DriverProvider objDriverProvider;
	private LogReporter logReporter;
	private Configuration configuration;
	private WaitMethods wait;

	private String iFrameId = "payment-process";
	//payment-process
	//
	public DepositPage(DriverProvider driverProvider, WebActions webActions,LogReporter logReporter,Configuration configuration,WaitMethods wait) {
		this.objDriverProvider = driverProvider;
		this.objWebActions = webActions;
		this.logReporter = logReporter;
		this.configuration= configuration;
		this.wait = wait;
	}

	public void clickOnDepositBesidesMyAcct() {
		By btnDeposit = By.xpath("//*[@id='logged-in-bar']/button");
		if (objWebActions.checkElementDisplayedWithMidWait(btnDeposit))
			logReporter.log("Click deposit besides my acct button> >", objWebActions.click(btnDeposit));
	}

	public void switchToDepositIframe()
	{
		objWebActions.switchToFrameUsingNameOrId(iFrameId);
	}
	public void selectPaymentMethod(String strPaymtMethod) {
		By locator;
		switch (strPaymtMethod) {
		case "Card":
			selectPaymentMethodFromPaymentDropdown();
			locator = By.xpath("//app-list-item[@id='add-account-option-debit']");
			logReporter.log("Select Card paymentmethod> >", objWebActions.click(locator));
			break;
		case "Paypal":
			selectPaymentMethodFromPaymentDropdown();
			locator = By.xpath("//app-list-item[@id='add-account-option-paypal']");
			logReporter.log("Select Paypal paymentmethod> >", objWebActions.click(locator));
			break;
		case "Paysafe":
			selectPaymentMethodFromPaymentDropdown();
			locator = By.xpath("//app-list-item[@id='add-account-option-paysafe']");
			logReporter.log("Select Paysafe paymentmethod> >", objWebActions.click(locator));
			break;
		}
	}
	
	public void selectPaymentMethodFromPaymentDropdown()
	{
		By locator = By.xpath("//app-cashier-button[@id='show-list-button']//button//span[contains(.,'Change')]");
		By lnkManagePaymentMethods = By.xpath("//div[@id=\"dropdown\"]//a[contains(.,'Manage payment methods')]");
		if(objWebActions.checkElementDisplayedWithMidWait(locator))
		{
			logReporter.log(" Click on change> >", objWebActions.click(locator));
			if(objWebActions.checkElementDisplayedWithMidWait(lnkManagePaymentMethods))
			{
				logReporter.log(" Click on change> >", objWebActions.click(lnkManagePaymentMethods));
				clickOnButton("Add new payment method");
			}
		}
	}
	
	public void verifyErrormessage(String err)
	{
		if(err.contains("~")){
			String[] arr1 = err.split("~");
			for (String links : arr1  ) {

				//By errMsg = By.xpath("//p[@class='error-text'][contains(.,'"+links+"')]");
				By errMsg = By.xpath("//div[@class='error validation'][contains(.,'"+links+"')]");
				logReporter.log(links+" displayed  ",
						objWebActions.checkElementDisplayed(errMsg));}}
		else{
			By errMsg = By.xpath("//div[@class='error validation'][contains(.,'"+err+"')]");
			logReporter.log(err+" displayed  ",
					objWebActions.checkElementDisplayed(errMsg));}
	}

	public void switchToCardIframe()
	{
		wait.sleep(15);
		By locator = By.xpath("//iframe[@id='riptIframe']");
		java.util.List<WebElement> webEle = objDriverProvider.getWebDriver().findElements(locator);
		System.out.println(webEle.get(0));
		System.out.println("****** "+webEle.size());
		//objWebActions.switchToFrameUsingNameOrId("riptIframe");

		objDriverProvider.getWebDriver().switchTo().frame(objWebActions.processElement(locator));
		wait.sleep(3);
	}
	public void enterCardNumber(String strNo) {
		By cardNo = By.id("CardNumber");
		logReporter.log("enter card no> >", objWebActions.setText(cardNo, strNo));
	}

	public void enterExpiryMonthYear(String strExpiry) {
		By expiry = By.id("expiryMonthYear");
		logReporter.log("enter exp date> >", objWebActions.setText(expiry, strExpiry));
	}

	public void enterCVV(String strCVV) {
		By objCVV = By.id("cvn");
		logReporter.log("enter card no> >", objWebActions.setText(objCVV, strCVV));
	}
	public void switchToCVVFrame()
	{
		wait.sleep(10);
		By locator = By.xpath("//iframe[@name='cvvIframe']");
		java.util.List<WebElement> webEle = objDriverProvider.getWebDriver().findElements(locator);
		System.out.println(webEle.get(0));
		System.out.println("***ss*** "+webEle.size());
		//objWebActions.switchToFrameUsingNameOrId("riptIframe");

		objDriverProvider.getWebDriver().switchTo().frame(objWebActions.processElement(locator));
		wait.sleep(3);
	}
	public void clickOnSubmit()
	{
		By locator = By.id("submitbtn");
		logReporter.log("Click on submit >", objWebActions.click(locator));

	}
	public void enterDepositAmt(String strAmt) {
		By amt = By.xpath("//input[contains(@placeholder,'Other (Min £5.00)')]");
		logReporter.log("enter amount> >", objWebActions.setText(amt, strAmt));
	}

	public void clickDepositButton() {
		wait.sleep(10);
		By locator = By.xpath("//input[@id='submitbtn'][contains(@value,'Deposit £')]");
		
		//objWebActions.switchToFrameUsingNameOrId(iFrameId);
			logReporter.log("Click Deposit button> >", objWebActions.clickUsingJS(locator));
		
	}

	public void verifyDepositSuccessMessages(String msg) {
		objWebActions.switchToDefaultContent();
		this.switchToDepositIframe();
		//*[@class='symbol success-mark medium']//following::div//div//h6[contains(.,'Payment Method Added')]
		//*[@class='symbol success-mark medium']//following::div//div//h6[contains(.,'Deposited Successfully')]
		By locator = By.xpath("//*[@class='symbol success-mark medium']//following::div//div//h6[contains(.,'"+msg+"')]");
		logReporter.log("Verify "+msg+ " on screen> >", objWebActions.checkElementDisplayedWithMidWait(locator));

	}


	public void verifyDepositAmountIsDisplayedCorrectlyOrNot(String amt)
	{
		By depositAmt = By.xpath("//div[@id=\"deposit-summary\"]//div//div//div[contains(.,'Deposit amount')]//following::div[contains(@class,'value')]");
		String currentAmount = objWebActions.getText(depositAmt);
		System.out.println(currentAmount);
		System.out.println(amt);
		logReporter.log("Verify 'Deposit Amount is displayed as  "+currentAmount, 
				currentAmount.contains(amt));
	}
	public void clickOnButton(String btnName)
	{
		//app-cashier-button//button[contains(.,'Add new payment method')]
		//app-cashier-button//button[contains(.,'Deposit')]
		//app-cashier-button//button[contains(.,'Play Now')]
		wait.sleep(configuration.getConfigIntegerValue("minwait"));
		By locator = By.xpath("//app-cashier-button//button[contains(.,'"+btnName+"')]");
		logReporter.log(" Click on ' "+btnName+" ' button ",  
				objWebActions.clickUsingJS(locator));
		wait.sleep(configuration.getConfigIntegerValue("midwait"));
	}
	public void verifyButton(String btnName)
	{
		By locator = By.xpath("//app-cashier-button//button[contains(.,'"+btnName+"')]");
		logReporter.log(" verify ' "+btnName+" ' button ",  
				objWebActions.checkElementDisplayed(locator));
		wait.sleep(configuration.getConfigIntegerValue("midwait"));
	}


	public boolean verifyPOPFCheckbox()
	{
		By locator = By.xpath("//div[contains(@class,'checkbox')]//label[@for='id-agreed']//p[contains(.,'“We hold your balance in a designated bank account so that, in the event of insolvency, sufficient funds are always available for you to withdraw at any time. This represents the medium level of protection, based on the categories provided by the UK Gambling Commission. ')]");

		return objWebActions.checkElementDisplayed(locator);
	}

	public void clickOnNext()
	{
		By btnNext = By.xpath("//button//span[contains(.,'Next')]");
		logReporter.log(" Click on 'Next' button ",  
				objWebActions.clickUsingJS(btnNext));
	}
	public void acceptPOPFChecbox()
	{
		By locator = By.xpath("//div[contains(@class,'checkbox')]//label[@for='id-agreed']//p[contains(.,'“We hold your balance in a designated bank account so that, in the event of insolvency, sufficient funds are always available for you to withdraw at any time. This represents the medium level of protection, based on the categories provided by the UK Gambling Commission. ')]");
		logReporter.log("Verify POPF Checkbox > >", objWebActions.click(locator));
	}
	public void validateBalanceAfterDeposit(String initialBal ,String currentBal, String depAmt)
	{
		System.out.println("SumOfinitialBalDepAmt : "+initialBal+ " currentBal  "+currentBal);
		System.out.println("****** initial depAmt"+depAmt);
		wait.sleep(5);
		initialBal = initialBal.replace("£","");
		currentBal = currentBal.replace("£","");
		System.out.println("****** initial baalll"+initialBal);
		System.out.println("****** Float.parseFloat(depAmt) "+Float.parseFloat(depAmt) );
		float SumOfinitialBalDepAmt =Float.parseFloat(initialBal) + Float.parseFloat(depAmt) ;
		System.out.println("SumOfinitialBalDepAmt : "+SumOfinitialBalDepAmt);

		logReporter.log(" Balance after deposit is  : "+initialBal ,
				String.valueOf(Float.parseFloat(currentBal)).equals(String.valueOf(SumOfinitialBalDepAmt)) );
	}
	
	
}
