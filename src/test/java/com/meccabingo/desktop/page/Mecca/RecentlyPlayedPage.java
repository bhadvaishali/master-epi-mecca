/**
 * 
 */
package com.meccabingo.desktop.page.Mecca;

import org.openqa.selenium.By;

import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.utils.WaitMethods;

public class RecentlyPlayedPage { 

	private WebActions objWebActions;
	private LogReporter logReporter;
	private WaitMethods waitMethods;
	public RecentlyPlayedPage(WebActions webActions, LogReporter logReporter,WaitMethods waitMethods) {
		this.objWebActions = webActions;
		this.logReporter = logReporter;
		this.waitMethods = waitMethods;
	
	}
	
	public void verifyTabDisplayedOnHomePage(String header) {
		waitMethods.sleep(10);
		By locator = By.xpath("//h2[contains(.,'" + header + "')]");
		System.out.println("************** locator "+locator);
		logReporter.log("Verify '" + header + "' tab displayed on home page", objWebActions.checkElementDisplayed(locator));
	}
 	
	public void verifyYouDontHaveAnyRecentlyPlayedGamesMessageDisplayed() {
		By locator = By.xpath("//div[contains(@class,'game-recently-played game-panel swiper')]//div[contains(.,'You have no Recently Played Games to view')]");
		logReporter.log("Verify 'You dont have any recently played games' message displayed", objWebActions.checkElementDisplayed(locator));
	}
	
	public void selectNavigationMenu(String navigationMenu) {
		By locator = By.xpath("//ul[contains(@class,'top-navigation')]/apollo-top-navigation-item//a[text()='" + navigationMenu + "']");
		logReporter.log("Select navigation menu '" + navigationMenu + "'", objWebActions.click(locator));
	}
}
