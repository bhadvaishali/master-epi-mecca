/**
 * 
 */
package com.meccabingo.desktop.page.Mecca;

import org.openqa.selenium.By;

import com.generic.WebActions;
import com.generic.logger.LogReporter;

/**
 * @author Harshvardhan Yadav, Namrata Donikar (Expleo)
 *
 */
public class ContentConfigurationPage {
	private WebActions objWebActions;
	private LogReporter logReporter;

	public ContentConfigurationPage(WebActions webActions, LogReporter logReporter) {
		this.objWebActions = webActions;
		this.logReporter = logReporter;
	}

	public void clickConfigurationTab() {
		By tabConfiguration = By.xpath("//span[contains(text(),'Configuration') and @class='tabLabel']");
		logReporter.log("click 'configuration' > >", objWebActions.click(tabConfiguration));
	}

	public void editSocialMediaFooterContent() {
		By socialMediaFooterContent = By.xpath("//span[contains(text(),'Social Media')]");
		objWebActions.mouseHover(socialMediaFooterContent);
		By socialMediaEditOptions = By
				.xpath("(//span[@class='epi-extraIcon epi-pt-contextMenu epi-iconContextMenu'])[2]");
		objWebActions.click(socialMediaEditOptions);
		By socialMediaEdit = By.xpath(
				"//div[@class='dijitPopup dijitMenuPopup']/table/tbody/tr[1]//child::td[contains(text(),'Edit')]");
		logReporter.log("click 'Edit' > >", objWebActions.click(socialMediaEdit));

	}

	public void editPaymentProvidersFooterContent() {
		By paymentProvidersContent = By.xpath("//span[contains(text(),'Payment Providers')]");
		objWebActions.mouseHover(paymentProvidersContent);
		By paymentProvidersEditOptions = By
				.xpath("(//span[@class='epi-extraIcon epi-pt-contextMenu epi-iconContextMenu'])[2]");
		objWebActions.click(paymentProvidersEditOptions);
		By paymentProvidersEdit = By.xpath(
				"//div[@class='dijitPopup dijitMenuPopup']/table/tbody/tr[1]//child::td[contains(text(),'Edit')]");
		logReporter.log("click 'Edit' > >", objWebActions.click(paymentProvidersEdit));
	}

	public void verifySocialMediaOptionConfigured(String text) {

		switch (text) {

		case "Twitter": {
			By txtSocialTwitterName = By.xpath("//div[@id='dgrid_14-row-0']/table/tr/td[contains(text(),'Twitter')]");
			logReporter.log("check element > >", objWebActions.checkElementDisplayed(txtSocialTwitterName));
			break;
		}

		case "Facebook": {
			By txtSocialFacebookName = By.xpath("//div[@id='dgrid_14-row-1']/table/tr/td[contains(text(),'Facebook')]");
			logReporter.log("check element > >", objWebActions.checkElementDisplayed(txtSocialFacebookName));
			break;
		}

		case "YouTube": {
			By txtSocialYoutubeName = By.xpath("//div[@id='dgrid_14-row-2']/table/tr/td[contains(text(),'YouTube')]");
			logReporter.log("check element > >", objWebActions.checkElementDisplayed(txtSocialYoutubeName));
			break;
		}

		case "Instagram": {
			By txtSocialInstagramName = By
					.xpath("//div[@id='dgrid_14-row-3']/table/tr/td[contains(text(),'Instagram')]");
			logReporter.log("check element > >", objWebActions.checkElementDisplayed(txtSocialInstagramName));
			break;
		}
		default:
			System.out.println(
					"Kindly configure Social Media options for footer i.e. Twitter, Facebook, YouTube, Instagram..");
			break;
		}

	}

	public void verifySocialMediaConfigurationBlocks(String text) {

		switch (text) {

		case "Name": {
			By socialHeadingName = By
					.xpath("//div[@role='grid' and @id='dgrid_16']/div/table/tr/th/div[contains(text(),'Name')]");
			logReporter.log("check element > >", objWebActions.checkElementDisplayed(socialHeadingName));
			break;
		}

		case "Background colour": {
			/*
			 * By socialHeadingColour = By
			 * .xpath("//div[@role='grid' and @id='dgrid_16']/div/table/tr/th/div[contains(text(),'Colour')]"
			 * );
			 */ By socialHeadingName = By
					.xpath("//div[@role='grid' and @id='dgrid_16']/div/table/tr/th/div[contains(text(),'Name')]");
			logReporter.log("check element > >", objWebActions.checkElementDisplayed(socialHeadingName));
			break;
		}

		case "Image": {
			By socialHeadingImage = By
					.xpath("//div[@role='grid' and @id='dgrid_16']/div/table/tr/th/div[contains(text(),'Image')]");
			logReporter.log("check element > >", objWebActions.checkElementDisplayed(socialHeadingImage));
			break;
		}

		case "Title": {
			By socialHeadingTitle = By
					.xpath("//div[@role='grid' and @id='dgrid_16']/div/table/tr/th/div[contains(text(),'Title')]");
			logReporter.log("check element > >", objWebActions.checkElementDisplayed(socialHeadingTitle));
			break;
		}

		case "Alt text": {
			By socialHeadingAltTextAlt = By
					.xpath("//div[@role='grid' and @id='dgrid_16']/div/table/tr/th/div[contains(text(),'Alt')]");
			logReporter.log("check element > >", objWebActions.checkElementDisplayed(socialHeadingAltTextAlt));
			break;
		}

		case "Header": {
			By socialHeadingHeader = By
					.xpath("//div[@role='grid' and @id='dgrid_16']/div/table/tr/th/div[contains(text(),'Header')]");
			logReporter.log("check element > >", objWebActions.checkElementDisplayed(socialHeadingHeader));
			break;
		}

		case "Subheader": {
			By socialHeadingSubheader = By
					.xpath("//div[@role='grid' and @id='dgrid_16']/div/table/tr/th/div[contains(text(),'Subheader')]");
			logReporter.log("check element > >", objWebActions.checkElementDisplayed(socialHeadingSubheader));
			break;
		}

		case "Link URL": {
			By socialHeadingLink = By
					.xpath("//div[@role='grid' and @id='dgrid_16']/div/table/tr/th/div[contains(text(),'Link')]");
			logReporter.log("check element > >", objWebActions.checkElementDisplayed(socialHeadingLink));
			break;
		}

		case "Link target": {
			By socialHeadingLinkTarget = By.xpath(
					"//div[@role='grid' and @id='dgrid_16']/div/table/tr/th/div[contains(text(),'Link Target')]");
			logReporter.log("check element > >", objWebActions.checkElementDisplayed(socialHeadingLinkTarget));
			break;
		}
		default:
			System.out.println("Kindly check configuration of Social Media blocks i.e. Name, Colour, Image, link..");
			break;
		}

	}
}
