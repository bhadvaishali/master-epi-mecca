package com.meccabingo.desktop.page.Mecca;

import org.openqa.selenium.By;

import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.utils.WaitMethods;
import com.generic.webdriver.DriverProvider;

public class PreBuyPage {
	WebActions objWebActions;
	private DriverProvider objDriverProvider;
	private LogReporter logReporter;
	private WaitMethods waitMethods;
	
	public PreBuyPage(DriverProvider driverProvider, WebActions webActions,
			LogReporter logReporter,WaitMethods waitMethods) {
		this.objDriverProvider = driverProvider;
		this.objWebActions = webActions;
		this.waitMethods = waitMethods;
		this.logReporter = logReporter;
	}

	public void selectTicket(String str) {
		By locator = By.xpath("//div[contains(@class,'pre-buy-popup-buttons')]/button[text()='"+str+"']");
		if (objWebActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("click tickets> >", objWebActions.click(locator));
	}

	public void clickPreBuyButton() {
		//By locator = By.xpath("(//apollo-bingo-pre-buy-cta//button[contains(.,'Pre-Buy')]");
		By locator = By.xpath("(//button[text()='Pre-Buy'])[1]");
		logReporter.log("click pre-buy button > >", objWebActions.clickUsingJS(locator));
	}
	
	public void verifyPreBuyPopupDisplayed() {
		waitMethods.sleep(8);
		By locator = By.xpath("//section[contains(@class,'pre-buy-popup')]");
		logReporter.log("check pre-buy popup displayed > >", objWebActions.checkElementDisplayedWithMidWait(locator));
		waitMethods.sleep(8);
		verifyInfoFromThePreBuyPopUp("Starts~Prize~Tickets Purchased");
		verifySelectTicketSectionOnThePreBuyPopUp();
	}

	public void verifyInfoFromThePreBuyPopUp(String infotext)
	{
		if(infotext.contains("~")){
			String[] arr1 = infotext.split("~");
			for (String links : arr1  ) {
				By locator = By.xpath("//section[contains(@class,'pre-buy-popup')]//div[contains(@class,'pre-buy-popup-info')]//p//span[contains(.,'"+links+"')]");
				
				logReporter.log(links+"' displayed on pop pup ",
						objWebActions.checkElementDisplayed(locator));}}
		else{
			By locator = By.xpath("//section[contains(@class,'pre-buy-popup')]//div[contains(@class,'pre-buy-popup-info')]//p//span[contains(.,'"+infotext+"')]");
			logReporter.log(infotext+"' displayed on pop pup ",
					objWebActions.checkElementDisplayed(locator));}
	}
	public void verifySelectTicketSectionOnThePreBuyPopUp()
	{
		By locator = By.xpath("//section[contains(@class,'pre-buy-popup')]//div[contains(@class,'pre-buy-popup-tickets')]");
		logReporter.log("Verify select ticket section on pop pup ",
					objWebActions.checkElementDisplayed(locator));
	}
}
