package com.meccabingo.desktop.page.cogs;

import org.openqa.selenium.By;

import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.Utilities;
import com.generic.utils.WaitMethods;
import com.generic.webdriver.DriverProvider;

public class Cogs_PromoGroup {
	private WebActions objWebActions;
	private DriverProvider objDriverProvider;
	private LogReporter logReporter;
	private WaitMethods waitMethods;
	private Configuration configuration;
	private Utilities objUtilities;

	public Cogs_PromoGroup(WebActions webActions,  DriverProvider driverProvider,  LogReporter logReporter,WaitMethods waitMethods,Configuration configuration,Utilities Utilities ) {
		this.objWebActions = webActions;
		this.objDriverProvider = driverProvider;
		this.logReporter = logReporter;
		this.waitMethods = waitMethods;
		this.configuration= configuration;
		this.objUtilities = Utilities ;
	}

	public void searchPromoGroupByName(String arg1)
	{
		By locator = By.xpath("//input[contains(@id,'_PromoGroupName')]");
		logReporter.log("Enter Promo Group name as "+arg1,objWebActions.setText(locator, arg1));
	}
	
	public void clickOnSaveButton()
	{
		By locator = By.xpath("//div[contains(@id,'UpdateButton')]//span[text()='Save']");
		logReporter.log("Click on Save",objWebActions.click(locator));
	}
	
	public void clickOnEdit()
	{
		By locator = By.xpath("//div[contains(@id,'_EditButton')]//span[text()='Edit']");
		logReporter.log("Click on Edit",objWebActions.click(locator));
	}
	
	public void enterPlayerID(String arg1)
	{
		By locator = By.xpath("//textarea[contains(@id,'_CommaSeperatedPlayers')]");
		//objWebActions.click(locator);
		objWebActions.setText(locator,"");
		objWebActions.pressKeybordKeys(locator,"enter");
		logReporter.log("Enter Player ID ' "+arg1,objWebActions.setText(locator, arg1));
	}

	/*public void verifyPromoStatusIsDisplayedAsEnabled()
	{
		By locator = By.xpath("//td[@class='FieldHeader']//span[text()='Enabled']");
		logReporter.log("Verify Promo Status is displayed as Enabled  ",
				objWebActions.checkElementDisplayed(locator));
	}*/
	public void verifyPromoStatusIsDisplayedAsEnabled()
	{
		By locator = By.xpath("//td[@class='FieldHeader']//span[text()='Enabled']");
		if(objWebActions.checkElementDisplayedWithMidWait(locator))
		{	
		logReporter.log("Verify Promo Status is displayed as Enabled  ",
				objWebActions.checkElementDisplayed(locator));}
		//else
		{
			//acceptAlert();
		}
	}
	
	public void acceptAlert()
	{
		waitMethods.sleep(15);
		String text = objDriverProvider.getWebDriver().switchTo().alert().getText();
		System.out.println(text);
		waitMethods.sleep(configuration.getConfigIntegerValue("minwait"));
		if(text.contains("Multiple Records found for below player ID"))
		{
			objDriverProvider.getWebDriver().switchTo().alert().accept();
			System.out.println("******* accept ");
			waitMethods.sleep(configuration.getConfigIntegerValue("maxwait"));
		}
	}
}
