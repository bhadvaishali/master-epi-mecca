package com.meccabingo.desktop.page.cogs;

import org.openqa.selenium.By;

import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.WaitMethods;
import com.generic.webdriver.DriverProvider;

public class Cogs_PlayerDetailsPage {
	private WebActions objWebActions;
	private DriverProvider objDriverProvider;
	private LogReporter logReporter;
	private WaitMethods waitMethods;
	private Configuration configuration;

	public Cogs_PlayerDetailsPage(WebActions webActions,  DriverProvider driverProvider,  LogReporter logReporter,WaitMethods waitMethods,Configuration configuration ) {
		this.objWebActions = webActions;
		this.objDriverProvider = driverProvider;
		this.logReporter = logReporter;
		this.waitMethods = waitMethods;
		this.configuration= configuration;
	}

	public void setUserName(String userName)
	{
		By inpUserName = By.xpath("//input[@id='DefaultLoginView_CogsLoginControl_UserName']");
		logReporter.log("Set username ", userName,
				objWebActions.setTextWithClear(inpUserName, userName));
	}
	
	
	
	//td[@class="FieldHeader"]//span[text()='Take A Break:']//following::td//span
	//td[@class="FieldHeader"]//span[text()='Take A Break:']//following::td//span[contains(@id,'_TakeABreakLabel')]
	
	
	//td[contains(.,'Taken Break Until:')]//following::span[@id='TakeABreakEndDate']
	

	public void verifyPlayerCurrentStatus(String status)
	{
		//span[contains(@id,'_CurrentPlayerStatusLabel')][text()='Player Status: Take a Break']
		By lblPlayerStatus = By.xpath("//span[contains(@id,'_CurrentPlayerStatusLabel')][text()='Player Status: "+status+"']");
		logReporter.log("Verify Player Current Status is displayed as  ", status,
				objWebActions.checkElementDisplayed(lblPlayerStatus));
	}
	
	public void verifyPlayerStatus(String status)
	{
		//span[contains(@id,'_PlayerStatus')][text()='Disabled']
		//Enabled
		
		By locator = By.xpath("//span[contains(@id,'_PlayerStatus')][text()='"+status+"']");
		logReporter.log("Verify Player Status is displayed as  ", status,
				objWebActions.checkElementDisplayed(locator));
	}

	public String getEndDate()
	{
		By locator = By.xpath("//td[@class=\"FieldHeader\"]//span[text()='Take A Break:']//following::td//span[contains(@id,'_TakeABreakLabel')]");
		return objWebActions.getText(locator);
	}
	
	public void ClickOnCancelBreak()
	{
		By locator = By.xpath("//input[@type='submit'][contains(@id,'_TakeABreakButton')]//following::span[contains(text(),'Cancel Break')]");
		logReporter.log("Click on 'Cancel Break' ",
				objWebActions.click(locator));
	}
	
	public void ClickOnAccountStatusEditButton()
	{
		By locator = By.xpath("//input[@type='submit'][contains(@id,'_EditStatusButton')]");
		logReporter.log("Click on 'Edit' ",
				objWebActions.clickUsingJS(locator));
	}
	public void verifyPlayerTakeABreakPopup()
	{
		By locator = By.xpath("//div[contains(@id,'_TakeABreakPopup')]//span[text()='Player Take a Break']");
		logReporter.log("Verify  'Player Take a Break' pop up",
				objWebActions.checkElementDisplayed(locator));
	}
	
	public String getTakeABreakEndDateFromPopUP()
	{
		By locator = By.xpath("//td[contains(.,'Taken Break Until:')]//following::span[@id='TakeABreakEndDate']");
		return objWebActions.getText(locator);
	}
	
	public void SetReason(String reason)
	{
		By locator = By.xpath("//textarea[@id='ReasonCancel_I']");
		logReporter.log("Set reason ", reason,
				objWebActions.setTextWithClear(locator, reason));
	}
	public void ClickOnSubmit()
	{
		By locator = By.xpath("//div[contains(@id,'_TakeABreakPopup_btnUpdateText')]//span[contains(.,'Submit')]");
		logReporter.log("Click on 'Submit' ",
				objWebActions.click(locator));
	}
	public void acceptAlert()
	{
		String text = objDriverProvider.getWebDriver().switchTo().alert().getText();
		System.out.println(text);
		waitMethods.sleep(configuration.getConfigIntegerValue("minwait"));
		if(text.contains("Are you sure you want to update"))
		{
			objDriverProvider.getWebDriver().switchTo().alert().accept();
			System.out.println("******* accept ");
			waitMethods.sleep(configuration.getConfigIntegerValue("maxwait"));
		}
	}
	
	public void verifyEditPlayerStatusPopUp()
	{
		By iframe = By.xpath("//iframe[contains(@src,'EditPlayerStatus')]");
		objWebActions.switchToFrameUsingIframe_Element(iframe);
		By locator = By.xpath("//div[@class='pageTitle'][contains(.,'Edit Player Status')]");
		logReporter.log("Verify  'Edit Player Status",
				objWebActions.checkElementDisplayed(locator));
	}
	public void SetPlayerStatusOption(String option)
	{
	
		By locator = By.xpath("//input[contains(@id,'PlayerStatusOptions')]//following::label[text()='"+option+"']");
		logReporter.log("Select Player Status Option as  ", option,
				objWebActions.click(locator));
	}
	

	public void SetChangeReason(String reason)
	{
		By locator = By.xpath("//textarea[@id='ChangeReason']");
		logReporter.log("Set change reason ", reason,
				objWebActions.setTextWithClear(locator, reason));
	}
	
	public void ClickOnUpdate()
	{
		By locator = By.xpath("//textarea[@id='ChangeReason']//following::input[@value='Update']");
		logReporter.log("Click on 'Update' ",
				objWebActions.click(locator));
	}
	
	public void VerifyUpdateSuccesfulMessage()
	{
		By locator = By.xpath("//span[@id='PlayerStatusUpdateLabel'][contains(.,'Update was successful')]");
		logReporter.log("Verify 'Update was successful' message ",
				objWebActions.checkElementDisplayed(locator));
	}
	
	public void closePopup()
	{
		objWebActions.switchToDefaultContent();
		By locator = By.xpath("//span[@id='ui-dialog-title-popup'][contains(.,'Player Status')]//following-sibling::a[contains(@class,'close')]");
		logReporter.log("Close pop up' ",
				objWebActions.click(locator));
		waitMethods.sleep(5);
	}
	public void verifyPlayerOnlineFlag(String flag)
	{
		waitMethods.sleep(3);
		switch (flag) 
		{
		case "Green":
			By locator = By.xpath("//img[contains(@id,'_PlayerOnlineIcon_CallbackDetailViewPanel_ImgOnlineStatus')]");
			logReporter.log(" Verify 'Player online flag is displayed in Green'",  
					objWebActions.checkElementDisplayed(locator));
			break;
		case "Gray":
			By locator1 = By.xpath("//img[contains(@id,'_PlayerOnlineIcon_CallbackDetailViewPanel_ImgOfflineStatus')]");
			logReporter.log(" Verify 'Player online flag is displayed in Gray'",  
					objWebActions.checkElementDisplayed(locator1));
			break;
		}
	}
	
	public void verifyMarketingPreferencesCheckboxesIsSelectedOrNot(String pref) 
	{
		if(pref.contains("~")){
			String[] arr1 = pref.split("~");
			for (String pref1 : arr1) {
				By locator = By.xpath("//tr[contains(@id,'MarketingOffersAndNewsSectionRow')]//span[@class='dxWeb_edtCheckBoxChecked dxICheckBox dxichSys']//input[contains(@id,'"+pref1+"')]");
				logReporter.log("Verify '" +pref1 + " ' option is displayed on marketing preferences screen",  
						objWebActions.checkElementDisplayed(locator));}}
		else{
			By locator = By.xpath("//tr[contains(@id,'MarketingOffersAndNewsSectionRow')]//span[@class='dxWeb_edtCheckBoxChecked dxICheckBox dxichSys']//input[contains(@id,'"+pref+"')]");
			logReporter.log("Verify '" +pref + " ' option is displayed on marketing preferences screen",  
					objWebActions.checkElementDisplayed(locator));}
	}
	
	public void verifyUpdatedMobilenumberIsDisplayedOrNot(String mno)
	{
		By locator = By.xpath("//td[@class='FieldHeader'][contains(.,'Mobile Phone:')]//following-sibling::td//table//tbody//tr//td//input");
		String mobileNum = objWebActions.getAttribute(locator, "value");
		System.out.println("*****************   mobileNum  "+ mobileNum+ "  *****   "+mno);
		logReporter.log("Updated number is displayed in Cogs",  
				mno.equalsIgnoreCase(mobileNum));
	}
}
