package com.meccabingo.desktop.page.cogs;

import org.openqa.selenium.By;

import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.WaitMethods;
import com.generic.webdriver.DriverProvider;

public class Cogs_PlayerSearchPage {
	
	private WebActions objWebActions;
	private DriverProvider objDriverProvider;
	private LogReporter logReporter;
	private WaitMethods waitMethods;
	private Configuration configuration;

	public Cogs_PlayerSearchPage(WebActions webActions,  DriverProvider driverProvider,  LogReporter logReporter,WaitMethods waitMethods,Configuration configuration ) {
		this.objWebActions = webActions;
		this.objDriverProvider = driverProvider;
		this.logReporter = logReporter;
		this.waitMethods = waitMethods;
		this.configuration= configuration;
	}


	public void verifyPageHeader(String hdr)
	{
		By locator = By.xpath("//div[@class='pageTitle'][text()='"+hdr+"']");
		logReporter.log(" Verify ' "+hdr+" ' is displayed as page title",  
				objWebActions.checkElementDisplayed(locator));
	}
	
	
	public String getUserNameFromConfig() {
		String userName = configuration.getConfig("web.userName");
		return userName;
		}

	
	public void setSearchString(String txt)
	{
		By locator = By.xpath("//input[@id='SearchString']");
			logReporter.log("Set " +txt + " as search string ",
					objWebActions.setTextWithClear(locator, txt));
	}
	
	
	public void selectSearchCheckBox(String label)
	{
		By locator = By.xpath("//label[text()='"+label+"']//preceding::input[@type='checkbox'][1]");
		if(!objWebActions.isCheckBoxSelected(locator))
		{logReporter.log("Select checkbox as " +label,
				objWebActions.selectCheckbox(locator,true));}
	}
	
	public void searchPlayerByStatus(String label)
	{
		By locator = By.xpath("//span[text()='"+label+"']//preceding::input[@type='checkbox'][1]");
		if(!objWebActions.isCheckBoxSelected(locator))
		{logReporter.log("Select checkbox as " +label,
				objWebActions.selectCheckbox(locator,true));}
	}
	
	public void clickOnSearchPlayers()
	{
		By locator = By.xpath("//input[@value='Search Players']");
		logReporter.log("Click on Serach Player ",
				objWebActions.click(locator));
	}
	public void clickOnPlayerDetails(String unm)
	{
		By locator = By.xpath("//a[@class='dxeHyperlink'][text()='"+unm.toLowerCase()+"']");
				logReporter.log("Click on Serach Player ",
						objWebActions.click(locator));	
	}
	public String getPlayerID()
	{
		//By locator = By.xpath("//td[@class='FieldValue']//span[contains(@id,'_FV_ID')]");
		By locator = By.xpath("//table[contains(@id,'ctl00_ContentPlaceHolder1_PlayerGrid_DXMainTable')]//tr[contains(@id,'_PlayerGrid_DXDataRow0')]//td[1]");
		
		return objWebActions.getText(locator);
	}
	public void verifyPlayerOnlineFlag(String flag)
	{
		switch (flag) 
		{
		case "Green":
			By locator = By.xpath("//img[contains(@id,'_PlayerOnlineIcon_CallbackDetailViewPanel_ImgOnlineStatus')]");
			logReporter.log(" Verify 'Player online flag is displayed in Green'",  
					objWebActions.checkElementDisplayed(locator));
			break;
		case "Gray":
			By locator1 = By.xpath("//img[contains(@id,'_PlayerOnlineIcon_CallbackDetailViewPanel_ImgOfflineStatus')]");
			logReporter.log(" Verify 'Player online flag is displayed in Gray'",  
					objWebActions.checkElementDisplayed(locator1));
			break;
		}
	}
}
