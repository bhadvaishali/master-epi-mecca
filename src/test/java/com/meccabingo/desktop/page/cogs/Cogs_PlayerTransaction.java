package com.meccabingo.desktop.page.cogs;

import java.util.Hashtable;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.WaitMethods;
import com.generic.webdriver.DriverProvider;

public class Cogs_PlayerTransaction {
	private WebActions objWebActions;
	private DriverProvider objDriverProvider;
	private LogReporter logReporter;
	private WaitMethods waitMethods;
	private Configuration configuration;

	public Cogs_PlayerTransaction(WebActions webActions,DriverProvider driverProvider,LogReporter logReporter,WaitMethods waitMethods,Configuration configuration ) {
		this.objWebActions = webActions;
		this.objDriverProvider = driverProvider;
		this.logReporter = logReporter;
		this.waitMethods = waitMethods;
		this.configuration= configuration;
	}


	By msgNoDataToDisplay = By.xpath("//tr[contains(@class,'dxgvEmptyDataRow dxgvLVR')]//td//div[contains(.,'No data to display')]");
	//*[@id="ctl00_ContentPlaceHolder1_TopMenu_ButtonMenu_DXI0_T"]





	//td[contains(@id,'_TransactionDates_QuickDateCombo_')]

	//td[@class='FilterLabel'][text()='Quick Date:']


	//Transaction Type:
	//	Device Category:


	public void verifyDropdownFields(String fieldNm)
	{
		By locator = By.xpath("//td[@class='FilterLabel'][contains(.,'"+fieldNm+":')]//following-sibling::td//table[@class='dxeButtonEditSys dxeButtonEdit FilterControl']");	
		logReporter.log(" Verify "+fieldNm ,
				objWebActions.checkElementDisplayed(locator));
	}

	public void clickOnDropdown(String fieldNm)
	{
		By locator = By.xpath("//td[@class='FilterLabel'][contains(.,'"+fieldNm+":')]//following-sibling::td//table[@class='dxeButtonEditSys dxeButtonEdit FilterControl']//tr//td[contains(@class,'dxeButton dxeButtonEditButton')]");	
		logReporter.log(" Click "+fieldNm ,
				objWebActions.click(locator));
	}


	public void verifyPageTitle(String title)
	{
		//div[@class="pageTitle"]//table//tbody//tr//td[contains(.,'Player Transactions -')]
		By locator = By.xpath("//div[@class='pageTitle']//table//tbody//tr//td[contains(.,'Player "+title+" -')]");	
		logReporter.log(" Verify page title is displayed as " +title,  
				objWebActions.checkElementDisplayed(locator));
	}

	public void selectTopMenu(String menu)
	{	
		//	Transactions
		By locator = By.xpath("//div[@id='topButtonMenu']//li//a[contains(@id,'ContentPlaceHolder1_TopMenu_ButtonMenu_')]//span[text()='"+menu+"']");	
		logReporter.log("select "+menu,  
				objWebActions.click(locator));
	}

	public void selectQuickDate(String dateFilter)
	{
		/*Yesterday
		Specific Date
		Today*/
		clickOnDropdown("Quick Date");
		By locator = By.xpath("//table[contains(@id,'_TransactionDates_QuickDateCombo_DDD_L_LBT')]//tbody//tr//td[text()='"+dateFilter+"']");	
		logReporter.log("click on sumbit",  
				objWebActions.click(locator));
	}

	public void clickOnSumbit()
	{
		By locator = By.xpath("//div[contains(@id,'SubmitButton')]//span[text()='Submit']");	
		logReporter.log("click on sumbit",  
				objWebActions.clickUsingJS(locator));
		waitMethods.sleep(5);
	}

	public void verifyTransactionTableHeader(String hdrs)
	{
		/*Transaction
		Real Amount
		Bonus Amount
		Bonus Wins Amount
		Points Amount
		Balance Real
		Balance Bonus
		Balance Bonus Wins
		Total Balance
		Balance Points
		Device Category*/

		if(hdrs.contains("~")){
			String[] arr1 = hdrs.split("~");
			for (String hdrs1 : arr1) {
				By locator = By.xpath("//table[contains(@id,'PlayerTransactionsGrid_DXMainTable')]//tbody//tr//td[contains(@id,'PlayerTransactionsGrid')]//td[text()='"+hdrs1+"']");
				logReporter.log("Verify '" +hdrs1 + " ' option is displayed on marketing preferences screen",  
						objWebActions.checkElementDisplayed(locator));}}
		else{
			By locator = By.xpath("//table[contains(@id,'PlayerTransactionsGrid_DXMainTable')]//tbody//tr//td[contains(@id,'PlayerTransactionsGrid')]//td[text()='"+hdrs+"']");
			logReporter.log("Verify '" +hdrs + " ' header is displayed",  
					objWebActions.checkElementDisplayed(locator));}
	}

	public void verifyNodataDisplayMsg()	{
		logReporter.log(" Verify 'No data to display ' message",  
				objWebActions.checkElementDisplayed(msgNoDataToDisplay));}

	public void verifyTransactionHistory(String header,String value){
		By locator = By.xpath("//table[contains(@id,'PlayerTransactionsGrid_DXMainTable')]");
		if(objWebActions.checkElementDisplayed(msgNoDataToDisplay)){
			this.verifyNodataDisplayMsg();
		}
		else {
			logReporter.log("Verify Transaction search result",  
					this.verifyTableContent(locator,header,value));}
	}


	public boolean verifyTableContent(By locator, String columnHeader, String ContentToVerify) {
		Hashtable<String, String> dataColumnHeader = new Hashtable<String, String>();
		int intColumnNumber = 1;
		boolean blnverify = false;
		try {
			WebElement weResultTable = objWebActions.processElement(locator);
			List<WebElement> weColumnsHeaders = weResultTable.findElements(By.xpath(".//tbody//tr//td[contains(@id,'PlayerTransactionsGrid')]//td"));
			for (WebElement weColumnHeader : weColumnsHeaders) {
				String strHeader = weColumnHeader.getText().trim();
				if (!strHeader.equals(""))
				{dataColumnHeader.put(strHeader, String.valueOf(intColumnNumber));
				intColumnNumber++;}}

			List<WebElement> weRows = weResultTable.findElements(By.xpath(".//tbody//tr[contains(@id,'PlayerTransactionsGrid_DXDataRow')]"));
			for (WebElement weRow : weRows) {

				System.out.println("?**************** value :: "+(".//td[" + dataColumnHeader.get(columnHeader) + "]"));
				WebElement weExceptedClm = weRow.findElement(By.xpath
						(".//td[" + dataColumnHeader.get(columnHeader) + "]"));

				System.out.println("?**************** value :: "+weExceptedClm.getText());

				if (weExceptedClm.getText().trim().contains(ContentToVerify)) {
					blnverify = true;
					return blnverify;
				}
			}
			return blnverify;
		} catch (Exception exception) {
			exception.printStackTrace();
			return false;
		}
	}

	public void ClearTransactionType()
	{
		By locator = By.xpath("//div[contains(@id,'TransactionTypeList_DDD_gv_Title_ClearProvider_')]//input[contains(@value,'Clear')]");
		logReporter.log("click on 'Clear' ",  
				objWebActions.clickUsingJS(locator));
		waitMethods.sleep(5);
	}
	
	public void selectTransactionType(String type)
	{
		By locator = By.xpath("//table[contains(@id,'TransactionTypeList_DDD_gv_DXMainTable')]//tr[contains(@id,'TransactionTypeList_DDD_gv_DXDataRow')]//td[text()='"+type+"']//preceding::td[1]//span[contains(@id,'TransactionTypeList_DDD_gv_DXSelBtn')]");
		logReporter.log("Select transaction type as  "+type, objWebActions.click(locator));
	}
	
	public void selectDeviceCategory(String category)
	{
		
		clickOnDropdown("Device Category");
		By locator = By.xpath("//table[contains(@id,'_DeviceCategoryComboBox')]//tbody//tr//td[text()='"+category+"']");	
		logReporter.log("Select Device Category",  
				objWebActions.click(locator));
	}

}

