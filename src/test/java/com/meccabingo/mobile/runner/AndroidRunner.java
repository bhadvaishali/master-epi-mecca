
package com.meccabingo.mobile.runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(features = "src/test/java/com/meccabingo/mobile/feature", strict = true, plugin = {"summary", "json:target/JSON/Android.json",
		"rerun:target/SyncFails.txt", /*"com.generic.cucumberReporting.CustomFormatter"*/ }, 
		tags = "@Android", 
		dryRun = false, monochrome = true,
		glue = { "com.hooks", "com.meccabingo.mobile.stepDefinition"}
)

public class AndroidRunner extends AbstractTestNGCucumberTests {
}
