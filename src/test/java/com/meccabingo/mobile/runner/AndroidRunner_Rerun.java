
package com.meccabingo.mobile.runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(features = "@target/SyncFails.txt", 
		strict = true, plugin = { "json:target/JSON/Mobile_Rerun.json", /*"com.generic.cucumberReporting.CustomFormatter"*/}, 		
		monochrome = true,
		glue = { "com.hooks", "com.meccabingo.mobile.stepDefinition" }
)

public class AndroidRunner_Rerun extends AbstractTestNGCucumberTests {
}
