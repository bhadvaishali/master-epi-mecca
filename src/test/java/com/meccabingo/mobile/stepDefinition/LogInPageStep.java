/**
 * 
 */
package com.meccabingo.mobile.stepDefinition;

import io.cucumber.java.en.Then;

import com.generic.utils.Configuration;
import com.generic.utils.Utilities;
import com.meccabingo.mobile.page.Mecca.HomePage;
import com.meccabingo.mobile.page.Mecca.LogInPage;

import io.cucumber.java.en.And;
import io.cucumber.java8.En;

/**
 * @author Harshvardhan Yadav, vaishali bhad
 *
 */
public class LogInPageStep implements En {

	private LogInPage objLogInPage;
	private HomePage objHomePage;
	private Utilities objUtilities;
	private Configuration configuration;
	String newPwd;
	
	public LogInPageStep(LogInPage logInPage, HomePage homePage, Utilities objUtilities,Configuration configuration) {

		this.objLogInPage = logInPage;
		this.objHomePage = homePage;
		this.objUtilities = objUtilities;
		this.configuration = configuration;
		Then("Verify success message", () -> {
		});
		Then("Enter email id {string}", (String string) -> {

		});

		Then("Click on go from search", () -> {

		});

		Then("Verify email in inbox", () -> {

		});

		Then("Verify reset link inside mail", () -> {

		});

		Then("Enter generated random email", () -> {

		});

		Then("Click on reset password link inside mail", () -> {

		});

		Then("User entes new password", () -> {

		});

		Then("User retype the password", () -> {

		});

		Then("Click on reset password button", () -> {

		});

		Then("Login with new password", () -> {

		});
	}

	@Then("^Verify Login Page displayed$")
	public void verify_Login_Page_displayed() {
		objLogInPage.verifyLoginPage();
	}

	@Then("^Verify system displays login Title$")
	public void verify_system_displays_login_Title() {
		objLogInPage.verifyLoginHeaderTitle();
	}

	@Then("^Verify close 'X' icon$")
	public void verify_close_X_icon() {
		objLogInPage.verifyCloseXicon();
	}

	@Then("^Verify 'Username' field$")
	public void verify_Username_field() {
		objLogInPage.verifyTBUserNameDisplayed();
	}

	@Then("^Verify 'Password' field$")
	public void verify_Password_field() {
		objLogInPage.verifyTBPasswordDisplayed();
	}

	@Then("^Verify Toggle within password field$")
	public void verify_Toggle_within_password_field() {
		objLogInPage.verifyPasswordToggle();
	}

	@Then("^Verify Remember me checkbox$")
	public void verify_Remember_me_checkbox() {
		objLogInPage.verifyRememberMeCheckbox();
	}

	@Then("^Verify 'Log In' CTA$")
	public void verify_Log_In_CTA() {
		objLogInPage.verifyLoginCTA();
	}

	@Then("^Verify Forgot username link$")
	public void verify_Forgot_username_link() {
		objLogInPage.veryForgotUserNameLink();
	}

	@Then("^Verify Forgot password link$")
	public void verify_Forgot_password_link() {
		objLogInPage.veryForgotPasswordLink();
	}

	@Then("^Verify 'New to Mecca Bingo\\?' text$")
	public void verify_New_to_Mecca_Bingo() {
		objLogInPage.verifyNewToMeccaText();
	}

	@Then("^Verify Sign up link$")
	public void verify_Sign_up_link() {
		objLogInPage.verifySignUpLinkDisplayed();
	}

	@Then("^Verify Help contact information$")
	public void verify_Help_contact_information() {
		objLogInPage.verifyHelpContactDetails();
	}

	@Then("^Verify Live Chat link$")
	public void verify_Live_support_link() {
		objLogInPage.verifyLiveChat();
	}

	@Then("^User enters username \"([^\"]*)\"$")
	public void user_enters_username(String userName) {
		objLogInPage.enterUserName(userName);
	}

	@Then("^User enters password \"([^\"]*)\"$")
	public void user_enters_password(String password) {
		objLogInPage.enterPassword(password);
	}

	@Then("^User enters username$")
	public void user_enters_username() {
		objLogInPage.setUserNameFromConfig();
	}

	@And("^User enters password$")
	public void user_enters_password() {
		objLogInPage.setPasswordFromConfig();
	}

	@Then("^Verify Login button gets enabled$")
	public void verify_Login_button_gets_enabled() {
		objLogInPage.verifyLogInEnabled();
	}

	@Then("^User clicks on login button$")
	public void user_clicks_on_login_button() {
		objLogInPage.clickLogin();
	}

	@Then("^User clicks on 'Remember Me' tick$")
	public void user_clicks_on_Remember_Me_tick() {
		objLogInPage.clickOnRememberMe();
	}

	@Then("^Verify Username auto pupulated with \"([^\"]*)\"$")
	public void verify_Username_auto_pupulated_with(String userName) {
		objLogInPage.VerifyTextFromUserName(userName);
	}

	@Then("^User clicks on forgot password link$")
	public void user_clicks_on_forgot_password_link() {
		objLogInPage.clickOnForgotPasswordLink();
	}

	@Then("^Verify system displays Forgotten your password page with header as 'Forgot your password'$")
	public void verify_system_displays_Forgotten_your_password_page_with_header_as_Forgot_your_password() {
		objLogInPage.verifyForgottenPasswordHeader();
	}

	@Then("^Verify back arrow '<'$")
	public void verify_back_arrow() {
		objLogInPage.verifyBackArrow();
	}

	@Then("^Verify text \"([^\"]*)\"$")
	public void verify_text(String text) {
		objLogInPage.verifyHelperTextFromForgottenPasswordPage(text);
	}

	@Then("^Verify 'Send reset instructions' CTA$")
	public void verify_Send_reset_instructions_CTA() {
		objLogInPage.verifySendResetInstructionsCTA();
	}

	@Then("^User clicks on 'Send reset instructions'$")
	public void user_clicks_on_Send_reset_instructions() {
		objLogInPage.clickOnSendResetInstructions();
	}

	@Then("^Verify Success Message displayed to user$")
	public void verify_Success_Message_displayed_to_user() {
		objLogInPage.verifyPasswordResetSuccessMessage();
	}

	@Then("^Verify I didnot receive an email link$")
	public void Verify_I_didnot_receive_an_email_link() {
		objLogInPage.verifyIDidnotReceiveAnEmailLink();
	}

	@Then("^User clicks on forgot usename link$")
	public void user_clicks_on_forgot_usename_link() {
		objLogInPage.clickOnForgotUsernameLink();
	}

	@Then("^Verify system displays Forgotten your username page with header as 'Forgotten your Username'$")
	public void verify_system_displays_Forgotten_your_username_page_with_header_as_Forgotten_your_Username() {
		objLogInPage.verifyForgottenUsernameHeader();
	}

	@Then("^Verify 'Email' field$")
	public void verify_Email_field() {
		objLogInPage.verifyEmailFieldFromForgottenUsername();
	}

	@Then("^Verify 'Send usename reminder' CTA$")
	public void verify_Send_usename_reminder_CTA() {
		objLogInPage.verifySendUsernameReminderCTA();
	}

	@Then("^Verify helper text \"([^\"]*)\"$")
	public void verify_helper_text(String text) {
		objLogInPage.verifyHelperTextFromForgottenUsernamePage(text);
	}

	@Then("^User enters email \"([^\"]*)\"$")
	public void user_enters_email(String emailAddress) {
		objLogInPage.enterEmailAddressInForgotUsernameSection(emailAddress);
	}

	@Then("^User clicks on 'Send username reminder'$")
	public void user_clicks_on_Send_username_reminder() {
		objLogInPage.clickOnSendUsernameReminder();
	}

	@Then("^Verify login window shows error message \"([^\"]*)\"$")
	public void verify_login_window_shows_error_message(String errorMessage) {
		objLogInPage.verifyLogInErrorMessage(errorMessage);
	}

	@Then("^Clear username$")
	public void clear_username() {
		objLogInPage.clearUsernameText();
	}

	@Then("^Clear password$")
	public void clear_password() {
		objLogInPage.clearPasswordText();
	}

	@Then("^Click on my account button$")
	public void click_on_my_account_button() {
		objLogInPage.clickOnMyAccountButtonFromHeader();
	}

	@Then("^Click on logout button$")
	public void click_on_logout_button() {
		objLogInPage.clickOnLogoutbutton();
	}

	@Then("^Click close 'X' icon$")
	public void Click_close_X_icon_on_mobile() {
		objLogInPage.clickOnCloseXicon();
	}

	@Then("Verify {string} message")
	public void verify_message(String string) {
		objLogInPage.verifyPasswordResetSuccessMessageDisplayed();
	}


	@Then("Verify user can login with new password")
	public void verify_user_can_login_with_new_password() {
		objLogInPage.enterUserName(configuration.getConfig("web.FlogUser"));
		objLogInPage.enterPassword(newPwd);
		objLogInPage.clickLogin();
		objHomePage.verifyMyAccount();
		configuration.updateParameterDetailsInConfig("web.NewPassword",newPwd);
	}
	@Then("Enter new password and hit submit button")
	public void enter_new_password_and_hit_submit_button() {
		newPwd = objUtilities.genarateRandomPassword();
		objLogInPage.setNewPassword(newPwd);
		objLogInPage.clickSubmitForNewPassword();
	}
}
