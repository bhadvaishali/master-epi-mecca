/**
 * 
 */
package com.meccabingo.mobile.stepDefinition;
import java.util.List;

import com.generic.utils.Utilities;
import com.meccabingo.mobile.page.Mecca.PromotionPage;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java8.En;


public class PromotionPageStep implements En
{

	private Utilities objUtilities;
	private PromotionPage objPromotionPage;

	public PromotionPageStep(PromotionPage promotionPage,Utilities utilities) 
	{
		this.objPromotionPage = promotionPage;		
		this.objUtilities = utilities;
	
		
	When("^User click on Promotion tab$",() ->this.objPromotionPage.clickOnPramotionTab()); 
	
	//Then("^Verify \"([^\"]*)\" bonus card is available$",(String bonusCard)->this.objPromotionPage.VerifyBonusCardIsPresent(bonusCard));
	
//	Then("^User verify \"([^\"]*)\" is Not claimed$", (String bonusName) -> this.objPromotionPage.VerifyBonusIsNotClaimed(bonusName));
		
	Then ("^Verify \"([^\"]*)\" dispaly following components After claim or optIn:$",(String bonusName,DataTable table)->{
		for (int i = 0; i < objUtilities.getListDataFromDataTable(table).size(); i++)	{			
			this.objPromotionPage.verifyBonusCardComponents(bonusName,this.objUtilities.getListDataFromDataTable(table).get(i));
		}
	});
	
	
	Then ("^Verify \"([^\"]*)\" display following components Before claim or OptIn :$",(String bonusName,DataTable table)->{
		for (int i = 0; i < objUtilities.getListDataFromDataTable(table).size(); i++)	{			
			this.objPromotionPage.verifyBonusCardComponents(bonusName,this.objUtilities.getListDataFromDataTable(table).get(i));
		}
	});
	
	Then("Verify \"([^\"]*)\" status on promotion details page$", (String status) -> this.objPromotionPage.verifyClaimedOrOptedInButton(status));
	
	Then ("^Verify featured games section dispalyed on page$",()->this.objPromotionPage.VerifyFeaturedGameSectionIsDisplayed());
	
	Then ("^User Click on \"([^\"]*)\" button$",(String buttonName)->this.objPromotionPage.clickOnPromoTypeCTA(buttonName));
	
	 Then ("^User Click on Learn More button from \"([^\"]*)\" promotion$",(String bonustype)->this.objPromotionPage.clickOnLearnMoreCTA(bonustype));
	Then ("^Click on \"([^\"]*)\" button from promotion details page$",(String buttonName) ->this.objPromotionPage.ClickOnButtonsFromPromotionDetailPage(buttonName));
	}
	

	@Then("^Verify Optedin and Opt out button is displayed on page$")
	public void verify_Optedin_and_Opt_out_button_is_displayed_on_page() {
		objPromotionPage.verifyClaimedOrOptedInButton("Opted in");
		objPromotionPage.verifyOptOutBTNDisplayed();
	}
	@Then("^Verify following tab is displayed under secondary navigation bar$")
	public void verify_following_tab_is_displayed_under_secondary_navigation_bar(DataTable dt) {
		objPromotionPage.clickOnPromotionsGameFilterDropdown();
		List<String> list = dt.asList(String.class);
		for (int i = 0; i < list.size(); i++) {
			objPromotionPage.verifyPromotionsSubHeaderTabs(list.get(i));}
	}
	@Then("^Select \"([^\"]*)\" tab from navigation tab$")
	public void select_tab_from_navigation_tab(String arg1) {
		objPromotionPage.clickPromotionsSubHeaderTabs(arg1);
	}
	@Then("^Verify 'Dont have any active promotions' message is displayed$")
	public void verify_Dont_have_any_active_promotions_message_is_displayed() {
		objPromotionPage.verifyDonthaveanyactivepromotionsInformativeMessage();
	}
	 
	@Then("^Verify Active promotions are displayed under active promotion tab$")
	public void verify_Active_promotions_are_displayed_under_active_promotion_tab() {
		objPromotionPage.verifyActivepromotionsIsDisplayedUnderActiveTab();
	}

	@Then("^Verify Countdown clock is displayed with no of days left in the bottom right with one of follwoing form$")
	public void verify_Countdown_clock_is_displayed_with_no_of_days_left_in_the_bottom_right_with_one_of_follwoing_form(DataTable dt) {
		objPromotionPage.verifynoOfDayLeftText();
	}
}
