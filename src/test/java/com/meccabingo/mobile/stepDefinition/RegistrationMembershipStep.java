package com.meccabingo.mobile.stepDefinition;

import java.util.List;

import com.generic.utils.Utilities;
import com.meccabingo.mobile.page.Mecca.RegMembershipPage;
import com.meccabingo.mobile.page.Mecca.RegistrationPage;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.PendingException;
import io.cucumber.java.en.Then;
import io.cucumber.java8.En;

public class RegistrationMembershipStep implements En {

	private RegMembershipPage regMembershipPage;
	private RegistrationPage registrationPage;
	private Utilities utilities;
	
	public RegistrationMembershipStep(RegMembershipPage regMembershipPage,Utilities utilities) {
		this.regMembershipPage = regMembershipPage;
		this.utilities = utilities;
		

		Then("Verify title is displayed", () -> this.regMembershipPage.verifyTitle());

		Then("Verify firstname is displayed", () -> this.regMembershipPage.verifyName());

		Then("Verify surname is displayed", () -> this.regMembershipPage.verifyName());

		Then("Verify date of birth is displayed", () -> this.regMembershipPage.verifyDateofBirth());

		Then("Verify 'Not you?' link", () -> this.registrationPage.verifyNotYouLink());

		Then("Verify username field is displayed", () -> this.regMembershipPage.verifyUsernameTextbox());

		Then("Verify password field is displayed", () -> this.regMembershipPage.verifyPasswordTextbox());

		Then("Verify {string} button", (String string) -> this.regMembershipPage.verifySelectAllButton());

		Then("Verify following checkboxes:", (DataTable dt) -> {
				for (int i = 0; i < this.utilities.getListDataFromDataTable(dt).size(); i++) {
					this.regMembershipPage.verifyCheckboxes(this.utilities.getListDataFromDataTable(dt).get(i));
				}
			});
		

		Then("Verify {string} checkbox", (String string) -> this.regMembershipPage.verifyOffersCheckbox());

		Then("Verify {string} section", (String string) -> this.regMembershipPage.verifyDepositLimitSection());

		Then("Click on Not you? link", () -> this.regMembershipPage.clickOnNotYouLink());

		Then("Click on select all button", () -> this.regMembershipPage.clickOnSelectAllCTA());

		Then("Click on marketing preference checkbox", () -> this.regMembershipPage.clickOnMarketingPreferenceCheckbox());

		Then("Verify selected checkbox gets deselect", () -> this.regMembershipPage.verifyCheckboxIsNotSelected());

		Then("Enter all values correctly", () -> {

		});

		Then("Click on register button", () -> this.regMembershipPage.clickOnRegisterButton());

		Then("Verify success message is displayed", () -> this.regMembershipPage.verifySuccessMessage());

		Then("Click on disabled register button", () -> this.regMembershipPage.clickOnDisableRegisterCTA());

		Then("Verify red line is displayed below email field", () -> this.regMembershipPage.verifyRedColorBelowEmailAdrress());

		Then("Enter incorrect email address", () -> this.regMembershipPage.enterInvalidEmailAdrress());

		Then("Verify error message below email field", () -> this.regMembershipPage.verifyErrorMessageBelowEmailAdrress());

		Then("Verify error message displayed in red color", () -> this.regMembershipPage.verifyErrorMessageBelowEmailAdrressInRedColor());

		Then("Verify red line is displayed below mobile number field", () -> this.regMembershipPage.verifyRedColorBelowMobileNumber());

		Then("Enter incorrect mobile number", () -> this.regMembershipPage.enterInvalidMobileNumber());

		Then("Verify error message below mobile number field", () -> this.regMembershipPage.verifyErrorMessageBelowMobilenumber());

		Then("Verify red line is displayed below username field", () -> this.regMembershipPage.verifyRedColorBelowUsername());

		Then("Enter incorrect username", () -> this.registrationPage.enterInvalidUsername());

		Then("Verify error message below username field", () -> this.regMembershipPage.verifyErrorMessageBelowUsername());

		Then("Clear username field", () -> this.registrationPage.clearUsername());

		Then("Enter existing username {string}", (String name) -> this.regMembershipPage.enterExistingUsername(name));

		Then("Verify red line is displayed below password field", () -> this.registrationPage.verifyRedColorBelowPassword());

		Then("Enter incorrect password", () -> this.registrationPage.enterInvalidPassword());

		Then("Verify error message below password field", () -> this.regMembershipPage.verifyErrorMessageBelowPassword());

		Then("Verify error message below marketing preference field field", () -> this.regMembershipPage.verifyErrorMessageBelowMarketingPreferenceField());

		Then("Verify red line is displayed below postcode field", () -> this.registrationPage.verifyRedColorBelowPostcode());

		Then("Verify error message below postcode field field", () -> this.regMembershipPage.verifyErrorMessageBelowPostcode());
			
		Then("Verify postcode field", () -> this.regMembershipPage.verifyPostcodeField());

		Then("Verify country name is displayed", () -> this.regMembershipPage.verifyCountryNameIsDisplayed());

		Then("Verify postcode is displayed", () -> this.regMembershipPage.verifyPostcodeIsDisplayed());

		Then("Click on GIB\\/ROI country", () -> this.regMembershipPage.clickOnGibRoiCountry());
		
		Then("Click on age checkbox on membership", () -> this.regMembershipPage.clickOnAgeCheckboxofMembership());

	}
	
	@Then("Verify {string} link")
	public void verify_link(String string) {
	    this.regMembershipPage.verifyLink(string);
	}
	@Then("Click on {string} link")
	public void click_link(String string) {
	    this.regMembershipPage.ClickOnLink(string);
	}
	@Then("Verify following fields are displayed under {string} section")
	public void verify_following_fields_are_displayed_under_section(String string, io.cucumber.datatable.DataTable dt) {
		
		System.out.println("*******************  "+utilities.getListDataFromDataTable(dt).size());
		List<String> list = dt.asList(String.class);
		for (int i = 0; i < list.size(); i++) {
			this.regMembershipPage.verifyFieldUnderYourPersonalDetailsSection(list.get(i));
		}
	}
}
