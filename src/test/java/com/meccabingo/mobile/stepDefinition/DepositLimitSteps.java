package com.meccabingo.mobile.stepDefinition;


import com.generic.MobileActions;
import com.generic.utils.Configuration;
import com.generic.utils.Utilities;
import com.generic.utils.WaitMethods;
import com.meccabingo.mobile.page.Mecca.DepositLimitPage;

import io.cucumber.java.en.Then;


public class DepositLimitSteps {

	private Utilities utilities;
	private Configuration configuration;
	private WaitMethods waitMethods;
	private MobileActions objMobileActions;
	private DepositLimitPage objDepositLimitPage;
	String dailyLmt,monthlyLmt,weeklyLmt;
	int lmt;

	public DepositLimitSteps(Utilities utilities,Configuration configuration,WaitMethods waitMethods,MobileActions objMobileActions,DepositLimitPage DepositLimitPage) {

		this.utilities = utilities;
		this.configuration= configuration;
		this.waitMethods = waitMethods ;
		this.objMobileActions = objMobileActions;
		this.objDepositLimitPage = DepositLimitPage ;
	}

	@Then("Daily,Weekly and yearly options displayed to user")
	public void daily_weekly_and_yearly_options_displayed_to_user() {
		objMobileActions.switchToFrameUsingNameOrId("deposit-limits-iframe");
		objDepositLimitPage.verifyDepositLimitOptions("24-hour limit~7-day limit~30-day limit");
		dailyLmt =objDepositLimitPage.getDepositLimitValue("24-hour limit");
		weeklyLmt =objDepositLimitPage.getDepositLimitValue("7-day limit");
		monthlyLmt =objDepositLimitPage.getDepositLimitValue("30-day limit");
	}

	@Then("Set daily limit less than previously set limit")
	public void set_daily_limit_less_than_previously_set_limit() {

		objDepositLimitPage.ClickOnedit("24-hour limit");
		dailyLmt = objDepositLimitPage.getDepositLimitInputPlaceholderValue();
		System.out.println("****** dep limit ::::: "+dailyLmt);
		lmt = Integer.valueOf(dailyLmt) - 1;
		System.out.println("****** lmt ::::: "+lmt);
		objDepositLimitPage.setDepositLimit("24-hour", String.valueOf(lmt));
		objDepositLimitPage.clickOnButton(" Submit ");

	}



	@Then("Verify updated deposit limit is displayed")
	public void verify_updated_deposit_limit_is_displayed() {
		objDepositLimitPage.verifyUpdatedLimitDisplayedOrNot("24-hour",String.valueOf(lmt));;
	}

	@Then("Increase the previously set deposit limit")
	public void increase_the_previously_set_deposit_limit() {
		objDepositLimitPage.ClickOnedit("24-hour limit");
		dailyLmt = objDepositLimitPage.getDepositLimitInputPlaceholderValue();
		System.out.println("****** dep limit ::::: "+dailyLmt);
		lmt = Integer.valueOf(dailyLmt) + 1;
		System.out.println("****** lmt ::::: "+lmt);
		objDepositLimitPage.setDepositLimit("24-hour", String.valueOf(lmt));
		objDepositLimitPage.clickOnButton(" Submit ");
	}
	@Then("Verify deposit limit is displayed in Pending state")
	public void verify_deposit_limit_is_displayed_in_pending_state() {
		objDepositLimitPage.verifyLimitIsDisplayedInPendingStateOrNot("24-hour",String.valueOf(lmt));
	}

	@Then("Verify updated {string} deposit limit is displayed")
	public void verify_updated_deposit_limit_is_displayed(String string) {
		if(string.equalsIgnoreCase("daily"))
		{objDepositLimitPage.verifyUpdatedLimitDisplayedOrNot("24-hour",String.valueOf(lmt));;}
		else if(string.equalsIgnoreCase("weekly"))
		{objDepositLimitPage.verifyUpdatedLimitDisplayedOrNot("7-day",String.valueOf(lmt));}
		else {	objDepositLimitPage.verifyUpdatedLimitDisplayedOrNot("30-day",String.valueOf(lmt));;
		}
	}

	@Then("Set daily deposit limit as {string}")
	public void set_daily_deposit_limit_as(String limit) {

		lmt = Integer.valueOf(limit);
		System.out.println("limit "+lmt);
		objDepositLimitPage.setDepositLimit("24-hour", String.valueOf(lmt));
		objDepositLimitPage.clickOnButton(" Submit ");
	}

	@Then("Set Weekly deposit limit as {string}")
	public void set_weekly_deposit_limit_as(String limit) {

		lmt = Integer.valueOf(limit);
		System.out.println("limit "+lmt);
		objDepositLimitPage.setDepositLimit("7-day", String.valueOf(lmt));
		objDepositLimitPage.clickOnButton(" Submit ");
	}


	@Then("Set Monthly deposit limit as {string}")
	public void set_monthly_deposit_limit_as(String limit) {

		lmt = Integer.valueOf(limit);
		System.out.println("limit "+lmt);
		objDepositLimitPage.setDepositLimit("30-day", String.valueOf(lmt));
		objDepositLimitPage.clickOnButton(" Submit ");
	}

	@Then("Click on edit button of {string}")
	public void click_on_edit_button_of(String string) {
		if(string.equalsIgnoreCase("daily"))
		{objDepositLimitPage.ClickOnedit("24-hour limit");}
		else if(string.equalsIgnoreCase("weekly"))
		{objDepositLimitPage.ClickOnedit("7-day limit");}
		else {	objDepositLimitPage.ClickOnedit("30-day limit");
		}
	}

	@Then("Verify deposit limit edit and Net deposit loss limit info is displayed on screen")
	public void verify_deposit_limit_edit_and_net_deposit_loss_limit_info_is_displayed_on_screen() {
		objDepositLimitPage.verifyDepositLimitEditInfo();
		objDepositLimitPage.verifyNetDepositLossLimitInfo();
	}

	@Then("Verify {string} error message on deposit limit screen")
	public void verify_error_message_on_deposit_limit_screen(String errmsg) {
		objDepositLimitPage.verifyDepositLimitErrorMsg(errmsg);
	}
	
	@Then("User set daily limit greater than weekly")
	public void user_set_daily_limit_greater_than_weekly() {
		weeklyLmt = weeklyLmt.replace("£", "");
		weeklyLmt = weeklyLmt.replace(".00", "");
		lmt = Integer.valueOf(weeklyLmt) + 1;
		System.out.println("********************weeklyLmt::  "+lmt);
		objDepositLimitPage.setDepositLimit("24-hour", String.valueOf(lmt));
		objDepositLimitPage.clickOnButton(" Submit ");
	}
	
	@Then("User set daily limit greater than monthly")
	public void user_set_daily_limit_greater_than_monthly() {
		monthlyLmt=monthlyLmt.replace("£", "");
		monthlyLmt = monthlyLmt.replace(".00", "");
		lmt = Integer.valueOf(monthlyLmt) + 1;
		System.out.println("********************monthhh::  "+lmt);
		objDepositLimitPage.setDepositLimit("24-hour", String.valueOf(lmt));
		objDepositLimitPage.clickOnButton(" Submit ");
	}



}

