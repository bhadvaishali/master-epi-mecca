
package com.meccabingo.mobile.stepDefinition;

import java.util.List;

import com.generic.MobileActions;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.utils.Configuration;
import com.generic.utils.Utilities;
import com.generic.utils.WaitMethods;
import com.meccabingo.mobile.page.Mecca.FooterPage;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;


public class FooterPageStep {

	private Utilities objUtilities;
	private AppiumDriverProvider objDriverProvider;
	private FooterPage objFooterPage;
	private MobileActions objMobileActions;
	private Configuration configuration;
	private WaitMethods waitMethods;
	String url;
	public FooterPageStep(Utilities utilities, AppiumDriverProvider driverProvider, FooterPage footerPage,MobileActions objMobileActions,Configuration configuration,WaitMethods waitMethods) {
		this.objUtilities = utilities;
		this.objDriverProvider = driverProvider;	
		this.objFooterPage = footerPage;
		this.objMobileActions = objMobileActions ;
		this.configuration = configuration;
		this.waitMethods = waitMethods ;
	}

	
	@Then("^Verify system displays following Social media components:$")
	public void verify_system_displays_following_Social_media_components(DataTable dt) {

		for (int i = 0; i < objUtilities.getListDataFromDataTable(dt).size(); i++) {			
			objFooterPage.verifySocialMediaComponents(objUtilities.getListDataFromDataTable(dt).get(i));

		}
	}
	
	@Then("^Verify system displays Icon, Text and Name for each social media block$")
	public void verify_system_displays_Icon_Text_and_Name_for_each_social_media_block() {
		objFooterPage.verifyImageAndTextOfSocial();
	}
	
	@Then("^Click on Facebook block from footer$")
	public void click_on_Facebook_block_from_footer(){
	    objFooterPage.clickFacebookBlock();
	    
	}
	
	@Then("^Switch to child window$")
	public void Switch_to_child_window(){
		
		if(System.getProperty("os.name").trim().toLowerCase().contains("mac"))
		{objMobileActions.acceptAlertiOS();
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));}
		else
		{	objFooterPage.switchToChild();}
	}
	
	@Then("^Click on Instagram block from footer$")
	public void click_on_Instagram_block_from_footer() {
		objFooterPage.clickInstagramBlock();
	}

	@Then("^Click on twitter block from footer$")
	public void click_on_twitter_block_from_footer() {
		objFooterPage.clickTwitterBlock();
	}

	@Then("^Click on youtube block from footer$")
	public void click_on_youtube_block_from_footer() {
		objFooterPage.clickYouTubeBlock();
	}

	@Then("^Verify system navigate user to \"([^\"]*)\" url$")
	public void verify_system_navigate_user_to_url(String url){
		objFooterPage.verifyUrl(url);
	}
	
	@Then("^User mouse hover on Facebook block from footer$")
	public void user_mouse_hover_on_Facebook_block_from_footer() {
	   
		objFooterPage.mouseHoverOnFacebook();
	}

	@Then("^Verify system highlight \"([^\"]*)\" block in \"([^\"]*)\" color$")
	public void verify_system_highlight_block_in_color(String area, String colorCode) {
		objFooterPage.verifyColorCode(area, colorCode);
	}
	
	@Then("^User mouse hover on Instagram block from footer$")
	public void user_mouse_hover_on_Instagram_block_from_footer() {
		objFooterPage.mouseHoverOnInstagram();
	}

	@Then("^User mouse hover on Twitter block from footer$")
	public void user_mouse_hover_on_Twitter_block_from_footer() {
		objFooterPage.mouseHoverOnTwitter();
	}

	@Then("^User mouse hover on Youtube block from footer$")
	public void user_mouse_hover_on_Youtube_block_from_footer() {
		objFooterPage.mouseHoverOnYouTube();
	}
	
	
	@Then("^Verify following fonts are diminish for name, description, logo$")
	public void verify_following_fonts_are_diminish_for_name_description_logo(DataTable dt){
	    
		for (int i = 0; i < objUtilities.getListDataFromDataTable(dt).size(); i++) {			
			objFooterPage.verifyFontSizeOfElements(objUtilities.getListDataFromDataTable(dt).get(i));
		}
	}
	

	@Then("^Verify following URLs are displaying under Useful Links block:$")
	public void verify_following_URLs_are_displaying_under_Useful_Links_block(DataTable dt){	    	    
	    for (int i = 0; i < objUtilities.getListDataFromDataTable(dt).size(); i++) {			
			objFooterPage.verifyUsefulLinksInFooter(objUtilities.getListDataFromDataTable(dt).get(i));
		}
	}
	
	@Then("^Click on Privacy Policy Link$")
	public void click_on_Privacy_Policy_Link() {
	    objFooterPage.clickPrivacyPolicyLink();
	}

	@Then("^Verify system navigates user to \"([^\"]*)\"$")
	public void verify_system_navigates_user_to(String url) {
	    objFooterPage.verifyUsefulLink(url);
	}
	
	
	@Then("^Click on Terms and Conditions Link$")
	public void click_on_Terms_and_Conditions_Link(){
	   objFooterPage.clickTermsAndConditionsLink();
	}

	@Then("^Click on Our Mecca promise Link$")
	public void click_on_Our_Mecca_promise_Link() {
		objFooterPage.clickOurMeccaPromiseLink();
	}

	@Then("^Click on Affiliates Link$")
	public void click_on_Affiliates_Link() {
	    objFooterPage.clickAffiliatesLink();
	}
	
	@Then("^Click on Mecca Club Terms Link$")
	public void click_on_Mecca_Club_Terms_Link() {
	    objFooterPage.clickMeccaClubTermsLink();
	}

	@Then("^Click on Play Online Casino Link$")
	public void click_on_Play_Online_Casino_Link(){
	    objFooterPage.clickPlayOnlineCasinoLink();
	}

	@Then("^Click on Mecca Blog Link$")
	public void click_on_Mecca_Blog_Link() throws Throwable {
	    objFooterPage.clickmeccaBlogLink();
	}
	
	@Then("^Verify user able to view following logos at Footer in partners block:$")
	public void verify_user_able_to_view_following_logos_at_Footer_in_partners_block(DataTable dt) {
		for (int i = 0; i < objUtilities.getListDataFromDataTable(dt).size(); i++) {			
			objFooterPage.verifyPartnersLogoInFooter(objUtilities.getListDataFromDataTable(dt).get(i));
		}
	}
	
	@Then("^User clicks on essa logo$")
	public void user_clicks_on_essa_logo() {
	   objFooterPage.clickLogoEssa();
	}

	@Then("^User clicks on IBSA logo$")
	public void user_clicks_on_IBSA_logo() {
		objFooterPage.clickLogoIbsa();
	}

	@Then("^User clicks on Gambling Control logo$")
	public void user_clicks_on_Gambling_Control_logo(){
		objFooterPage.clickLogoGamblingControl();
	}

	@Then("^User clicks on GamCare logo$")
	public void user_clicks_on_GamCare_logo(){
		objFooterPage.clickLogoGamCare();
	}

	@Then("^User clicks on GamStop logo$")
	public void user_clicks_on_GamStop_logo() {
		objFooterPage.clickLogoGamStop();
	}

	@Then("^User clicks on Gambling Commission logo$")
	public void user_clicks_on_Gambling_Commission_logo(){
		objFooterPage.clickLogoGamblingCommission();
	}
	
	@Then("^Verify user should be able to view Verisign Secured logo at Footer section$")
	public void verify_user_should_be_able_to_view_Verisign_Secured_logo_at_Footer_section() {
		objFooterPage.verifyVeriSignLogoNinformation();
	}
	
	@Then("^Verify system displays following section for payment providers components:$")
	public void verify_system_displays_following_section_for_payment_providers_components(DataTable dt){
		for (int i = 0; i < objUtilities.getListDataFromDataTable(dt).size(); i++) {			
			objFooterPage.verifyPaymentProvidersBlockInFooter(objUtilities.getListDataFromDataTable(dt).get(i));
		}
	}
	
	@Then("^User clicks on Alderney Gambling Control Commission link$")
	public void user_clicks_on_Alderney_Gambling_Control_Commission_link(){
	    objFooterPage.click_Alderney_Gambling_Control_Commission_link();
	}
	
	@Then("^User clicks on UK Gambling Commission link$")
	public void user_clicks_on_UK_Gambling_Commission_link() {
		objFooterPage.click_UK_Gambling_Commission_link();
	}
	
	@Then("^User click on BeGambleAware link$")
	public void user_click_on_BeGambleAware_link() {
	    objFooterPage.click_BeGambleAware_link();
	}
	
	@Then("^User clicks on Rank Group link$")
	public void user_clicks_on_Rank_Group_link()  {
	    objFooterPage.click_Rank_Group_link();
	}
	
	@Then("^Navigate back to window$")
	public void navigate_back_to_window() {
	    objDriverProvider.getAppiumDriver().navigate().back();
	}
	@Then("Verify essa logo$")
	public void Verify_essa_logo() {
		objFooterPage.Verifyessalogo();
	}
	@Then("^Back to Mecca site$")
	public void back_to_Bell_site() {
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("************** window");
	    objMobileActions.switchToWindowUsingTitle("Play Online Bingo with Mecca Bingo | Spend £10 and Get a £60 Bonus - MeccaBingo");
	}
	
	/*@Then("^Invoke the Web portal \"([^\"]*)\"$")
	public void invoke_the_web_portal(String arg1){
	    // Write code here that turns the phrase above into concrete actions
	//	objDriverProvider.initialize();
		objDriverProvider.getAppiumDriver().get(arg1);;
	}
	
	@Then("^Invoke Mecca site$")
	public void invoke_Bell_site() {
		invoke_the_web_portal(configuration.getConfig("web.Url"));
	}*/

	
	@Then("^Verify \"([^\"]*)\" logo is displayed in footer section$")
	public void verify_logo_is_displayed_in_footer_section(String arg1){
		objFooterPage.verifyPartnersLogoInFooter(arg1);
	}
	
	@Then("^Verify (\\d+)\\+ logo should not be a hyperlink$")
	public void verify_logo_should_not_be_a_hyperlink(int arg1){
		objFooterPage.verify18LogoDisplayed();
	}
	@Then("^Verify \"([^\"]*)\" logo hyperlinked to the correct page$")
	public void verify_logo_hyperlinked_to_the_correct_page(String arg1) {
		objFooterPage.verifyLogoLinkRedirectToTheCorrectPage(arg1);
	}
	@Then("^Verify Regulators \\(UK and Alderney\\) and associated license statements is displayed under License section of the footer$")
	public void verify_Regulators_UK_and_Alderney_and_associated_license_statements_is_displayed_under_License_section_of_the_footer() {
		objFooterPage.verifyAssociatedLicenseStatement();
		objFooterPage.verifyLinksUnderAssociatedLicenseStatement("38750,https://registers.gamblingcommission.gov.uk/38750~The Alderney Gambling Control Commission,https://www.gamblingcontrol.org/~13 C1 & C2,https://www.gamblingcontrol.org/licensees/egambling-licensees/~2396,https://beta.gamblingcommission.gov.uk/public-register/business/detail/2396");
	}
	@Then("^Click and verify 'UK Gambling Commission' link redirect to the correct page$")
	public void click_and_verify_UK_Gambling_Commission_link_redirect_to_the_correct_page(){
		objFooterPage.clickUKGamblingCommissionLink();
	}
	
	@Then("^Verify user is able to view following sections in footer$")
	public void verify_user_is_able_to_view_following_sections_in_footer(DataTable dt) {
		System.out.println("*******************  "+objUtilities.getListDataFromDataTable(dt).size());
		List<String> list = dt.asList(String.class);
		for (int i = 0; i < list.size(); i++) {
			objFooterPage.verifyFooterMainSections(list.get(i));
		}
	}
	
	@Then("^Verify \"([^\"]*)\" link is displayed on under Usefullinks footer section$")
	public void verify_link_is_displayed_on_under_footer_section(String arg1) {
		objFooterPage.verifyUsefulLinksInFooter(arg1);
	}
	
	@Then("^Access Terms & Conditions link and Verify that information is available to the customer regarding cheating, recovered player funds and how to complain$")
	public void access_Terms_Conditions_link_and_Verify_that_information_is_available_to_the_customer_regarding_cheating_recovered_player_funds_and_how_to_complain(){
		
		url = configuration.getConfig("web.Url");
		objFooterPage.clickOnUsefulLinks("Terms and Conditions");
		objFooterPage.verifyURLRedirection(url+"terms-and-conditions");
		objFooterPage.verifyArticleBlockHeaders("h4;Prohibited Conduct~p;We reserve the right to seek criminal and other sanctions against you if you have, or we suspect you have, engaged in any form of criminal activity, collusion (including in relation to chargebacks), cheating (including obtaining any unfair advantage), fraudulent practice or unlawful, improper or dishonest activity (together ‘Prohibited Conduct’). You agree that we may disclose any information relating to Prohibited Conduct (including your personal information) to the relevant authorities and other relevant parties (such as other gambling operators).~p;17.2 We reserve the right to terminate your account, retain the balance of your account and recover from you the amount of any affected pay-outs, bonuses or winnings if:~li;17.2.1 you are found to be participating in any form of Prohibited Conduct with us or with any other in-club, online or mobile provider of gambling services;~li;17.2.2 we become aware that you have requested a charge-back or have denied any of the transactions made on your account;~li;17.2.3 you become bankrupt or subject to bankruptcy proceedings (or its equivalent in any other jurisdiction);~li;17.2.4 you have more than one account with us at any one time; or~li;17.2.5 you are using our software and/or system in a manner which we (acting reasonably) deem inappropriate;~p;and we may at any time suspend your account while we investigate whether you have engaged in any of the conduct described above.~p;17.3 You shall repay to us all costs, charges or losses sustained or incurred by us arising directly or indirectly from any of the matters referred to in sections 17.1 or 17.2 above.");
	}

	@Then("^Access Terms & Conditions link and Verify that information is available to the customer regarding third party software$")
	public void access_Terms_Conditions_link_and_Verify_that_information_is_available_to_the_customer_regarding_third_party_software() {
		url = configuration.getConfig("web.Url");
		System.out.println("****url "+url);
		objFooterPage.clickOnUsefulLinks("Terms and Conditions");
		objFooterPage.verifyURLRedirection(url+"terms-and-conditions");
		objFooterPage.verifyArticleBlockHeaders("h4;Software~p;19.1 For certain services we offer, you may need to download or use certain additional software, including software provided by third parties.~p; You may be required to enter into end user terms and conditions of use in order to make use of such software. You agree to be bound by the terms of any such agreement.~p;19.3 You shall not use (other than for its specified purpose), interfere with, copy, modify, decode, reverse engineer, disassemble, decompile, translate, convert or create derivative works from any software provided to you by us and/or any third party or attempt to do so.");
	}

}
