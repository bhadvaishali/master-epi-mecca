package com.meccabingo.mobile.stepDefinition;

import com.generic.utils.Utilities;
import com.meccabingo.mobile.page.Mecca.GameWindow;
import com.meccabingo.mobile.page.Mecca.HomePage;
import com.meccabingo.mobile.page.Mecca.LogInPage;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.PendingException;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class GameWindowStep {

	private GameWindow gameWindow;
	private HomePage homePage;
	private LogInPage logInPage;
	private Utilities utilities;

	public GameWindowStep(GameWindow gamewindow, HomePage homePage, LogInPage logInPage,Utilities utilities) {
		this.gameWindow = gamewindow;
		this.homePage = homePage;
		this.logInPage = logInPage;
		this.utilities = utilities;
	}

	@Given("^Invoke Mecca Site$")
	public void invoke_Mecca_Site() {
		homePage.verifyHeaderLogo();
	}

	@Then("^Click on Play Now button from Game \"([^\"]*)\"$")
	public void click_on_Play_Now_button_from_Game(String nameofthegame)  {
		gameWindow.clickOnPlayNow(nameofthegame);

	}

	@Then("^Verify system displays login window$")
	public void verify_system_displays_login_window() {
		logInPage.verifyLoginHeaderTitle();
	}

		
	@Then("^Click on Free play button$")
	public void click_on_Free_play_button() {
		gameWindow.clickOnFreePlayBtn();
	}
	
	@Then("^User clicks on login button from login window$")
	public void user_clicks_on_login_button_from_login_window() {
		logInPage.clickOnLoginCTAbutton();
	}

	@Then("^Verify system loads game window for selected game in real mode$")
	public void verify_system_loads_game_window_for_selected_game_in_real_mode() {
		gameWindow.verifyTitleoftheGame();
	}
	
	@Then("^Verify Game window top bar displays “Exit game“ arrow button$")
	public void verify_Game_window_top_bar_displays_Exit_game_arrow_button() {
		gameWindow.verifyExitArrowBtn();
	}

	@Then("^Verify Game window top bar displays Session time$") 
	public void verify_Game_window_top_bar_displays_Session_time() {
		gameWindow.verifySessionTime();
	}

	@Then("^Verify Game window top bar displays Play for Real CTA button$")
	public void verify_Game_window_top_bar_displays_Play_for_Real_CTA_button() {
		gameWindow.verifyPlayforRealBtn();
	}

	@Then("^Verify Game window top bar displays Title of the game$")
	public void verify_Game_window_top_bar_displays_Title_of_the_game() {
		gameWindow.verifyTitleoftheGame();
	}

	@Then("^Verify Game window top bar displays Expand CTA button$")
	public void verify_Game_window_top_bar_displays_Expand_CTA_button() {
		gameWindow.verifyExpandBtn();
	}

	@Then("^Verify Game window top bar displays Deposit CTA button$")
	public void verify_Game_window_top_bar_displays_Deposit_CTA_button() {
		gameWindow.verifyDepositBtn();
	}

	@Then("^Verify Game window top bar displays My account CTA button$")
	public void verify_Game_window_top_bar_displays_My_account_CTA_button() {
		gameWindow.verifyMyaccountBtn();
	}
	

	@Then("^User clicks on i button of game \"([^\"]*)\"$")
	public void user_clicks_on_i_button_of_game(String nameofthegame) {
		gameWindow.clickOnibtnOfBingoTile(nameofthegame);
		
	}
		
	@Then("^User clicks on i button of hero game \"([^\"]*)\"$")
	public void user_clicks_on_i_button_of_hero_game(String nameofthegame) {
		gameWindow.clickOnibtnOfHeroTile(nameofthegame);
		
	}
	
	@Then("^Verify Game window top bar displays session time in mm:ss format$")
	public void verify_Game_window_top_bar_displays_session_time_in_mm_ss_format() {
		gameWindow.verifySessionTime();
	}

	@Then("^Click on expand button$")
	public void click_on_expand_button() {
		gameWindow.clickExpandbutton();
	}

	@Then("^Verify game gets expanded$")
	public void verify_game_gets_expanded()  {
		gameWindow.imageAtExpandbtn();
	}

	@Then("^Click on reduce button$")
	public void click_on_reduce_button() {
		gameWindow.clickReducebutton();
	}

	@Then("^Verify game gets reduced$")
	public void verify_game_gets_reduced() {
		gameWindow.imageAtReducebtn();
	}
	

	@Then("^Verify Game window header displays following components :$")
	public void verify_Game_window_header_displays_following_components(DataTable dt)  {
	for (int i = 0; i < utilities.getListDataFromDataTable(dt).size(); i++) {
		gameWindow.verifyDemoheadercomponents(utilities.getListDataFromDataTable(dt).get(i));
		
	}
	}
	@Then("^Verify system do not display session time for Game window$")
	public void verify_system_do_not_display_session_time_for_Game_window()  {
		gameWindow.verifyNonSessionTime();
	    
	}
	@Then("^Verify header is displayed$")
	public void verify_header_is_displayed() {
		gameWindow.verifyHeaderInGame();
	}

	@Then("^Verify background image is displayed$")
	public void verify_background_image_is_displayed()  {
		gameWindow.verifyBackroungimageInGame();
	}
	
	@Then("^Click on image of the game \"([^\"]*)\"$")
	public void click_on_image_of_the_game(String nameofthegame) {
		gameWindow.clickOnImageOfHerotile(nameofthegame);
	}

	@Then("^Click on title of the game \"([^\"]*)\"$")
	public void click_on_title_of_the_game(String nameofthegame) {
		gameWindow.clickOnTitleOfHerotile(nameofthegame);
	}

	@Then("^Click on description of the game \"([^\"]*)\"$")
	public void click_on_description_of_the_game(String nameofthegame) {
		gameWindow.clickOnDescriptionOfHerotile(nameofthegame);
	}
	
		
	@Then("^Click on prize of the bingo tile \"([^\"]*)\"$")
	public void click_on_prize_of_the_bingo_tile(String arg1)  {
	    gameWindow.clickOnPrizeofHerotile();
	}
	
	@Then("^Click on free play button on game details page$")
	public void click_on_free_play_button_on_game_details_page()  {
	   gameWindow.clickOnFreePlayBtn();
	}
	
	@Then("^Click on play now button on game details page$")
	public void click_on_play_now_button_on_game_details_page()  {
	  gameWindow.clickOnPlayNowOnGameDetailsPage();
	   
	}
	
	@Then("Click on Exit Game CTA")
	public void Click_on_Exit_Game_CTA() {
		gameWindow.clickExitGameCTA();
	}
	
	@Then("Click on my account from game")
	public void Click_on_my_account_from_game() {
		gameWindow.clickMyAccFromGame();
	}
	
	
	@Then("^Click on Play Now button from Game$")
	public void click_on_Play_Now_button_from_Game() {
		gameWindow.clickOnPlayNow();
	}
	
	@Then("^Verify Game window bottom bar displays “Exit game“ arrow button$")
	public void verify_Game_window_bottom_bar_displays_Exit_game_arrow_button() {
		gameWindow.verifyExitArrowBtn();
	}
	
	@Then("^Verify Game window bottom bar displays Session time$")
	public void verify_Game_window_bottom_bar_displays_Session_time() {
		gameWindow.verifySessionTime();
	}

	@Then("^Verify Game window bottom bar displays Play for Real CTA button$")
	public void verify_Game_window_bottom_bar_displays_Play_for_Real_CTA_button() {
		gameWindow.verifyPlayforRealBtn();
	}

	@Then("^Verify Game window bottom bar displays Deposit CTA button$")
	public void verify_Game_window_bottom_bar_displays_Deposit_CTA_button() {
		gameWindow.verifyDepositBtn();
	}

	@Then("^Verify Game window bottom bar displays My account CTA button$")
	public void verify_Game_window_bottom_bar_displays_My_account_CTA_button() {
		gameWindow.verifyMyaccountBtn();
	}

	@Then("^Username \"([^\"]*)\" as header displayed$")
	public void username_as_header_displayed(String uname) {
		gameWindow.usernameDisplayed(uname);
	}

	@Then("^Click on info button of first game of \"([^\"]*)\" section$")
	public void click_on_info_button_of_first_game_of_slot_section(String sectionName) {
		gameWindow.clickInfoOfFirstGame(sectionName);
	}
	
	@Then("^Click on Play Now button from Game details$")
	public void click_on_Play_Now_button_from_Game_details() {
		gameWindow.clickOnPlayNowFromGameDetails();
	}

}

