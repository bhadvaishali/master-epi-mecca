package com.meccabingo.mobile.stepDefinition;

import java.util.List;

import org.openqa.selenium.By;

import com.generic.MobileActions;
import com.generic.utils.Configuration;
import com.generic.utils.Utilities;
import com.generic.utils.WaitMethods;
import com.meccabingo.mobile.page.Mecca.LogInPage;
import com.meccabingo.mobile.page.Mecca.MyAccount;
import com.meccabingo.mobile.page.Mecca.RegistrationPage;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java8.En;

public class MyAccountStep implements En {

	private MyAccount myAccount;
	private Utilities utilities;
	private Configuration configuration;
	private RegistrationPage objRegistrationPage;
	private LogInPage objLogInPage;
	private WaitMethods waitMethods;
	private MobileActions mobileActions;
	String	BonusName ,BonusType,fromDate,toDate,promoCode,newPassword, messageCount ,lastLoginTime,currentLogoutTime,msgCountHeader;
	int msgcount;
	public MyAccountStep(MyAccount myAccount, Utilities utilities,Configuration configuration,RegistrationPage objRegistrationPage,LogInPage objLogInPage,WaitMethods waitMethods,MobileActions mobileActions) {
		this.myAccount = myAccount;
		this.utilities = utilities;
		this.configuration= configuration;
		this.objRegistrationPage = objRegistrationPage ;
		this.objLogInPage = objLogInPage;
		this.waitMethods = waitMethods ;
		this.mobileActions = mobileActions;

		Then("^Verify following options are displayed on my account page:$", (DataTable dt) -> {
			for (int i = 0; i < this.utilities.getListDataFromDataTable(dt).size(); i++) {
				this.myAccount.verifyAllFieldsOnPage(this.utilities.getListDataFromDataTable(dt).get(i));
			}
		});

		Then("^Verify playable balance text$", () -> this.myAccount.verifyPlayableBalanceText());

		Then("^Verify playable balance amount$", () -> this.myAccount.verifyPlayableBalanceAmount());

		Then("^Verify playable balance icon$", () -> this.myAccount.verifyPlayableBalanceIcon());

		Then("^Click on playable balance icon$", () -> this.myAccount.clickOnPlayableBalanceIcon());

		Then("^Verify following options are displayed on playable balance section:$", (DataTable dt) -> {
			for (int i = 0; i < this.utilities.getListDataFromDataTable(dt).size(); i++) {
				this.myAccount
				.verifyAllFieldsOnPlayableBalanceSection(this.utilities.getListDataFromDataTable(dt).get(i));
			}
		});

		Then("^Verify playable balance minus icon$", () -> this.myAccount.verifyPlayableBalanceMinusIcon());

		Then("^Click on playable balance minus icon$", () -> this.myAccount.clickOnPlayableBalanceMinusIcon());

		Then("^Verify on playable balance icon$", () -> this.myAccount.verifyPlayableBalanceIcon());

		Then("^Click on following options:$", (DataTable dt) -> {
			for (int i = 0; i < this.utilities.getListDataFromDataTable(dt).size(); i++) {
				this.myAccount.clickFieldsOnPlayableBalanceSection(this.utilities.getListDataFromDataTable(dt).get(i));
			}
		});

		Then("^Click on detailed view button$", () -> this.myAccount.clickOnPlayableBalanceDetailedButton());

		Then("^Verify button navigates to balance page$", () -> this.myAccount.verifyBalancePage());

		Then("^Click on back button$", () -> this.myAccount.clickOnBackbutton());

		Then("^Click on \"([^\"]*)\" option$", (String option) -> this.myAccount.clickOnOption(option));

		Then("^Verify title \"([^\"]*)\"$", (String title) -> this.myAccount.verifyPageTitle(title));

		Then("^Verify message count on message text$", () -> this.myAccount.verifyMessageCountOnMessageText());

		Then("^Click on username from top$", () -> this.myAccount.clickUsernameOnMyAccountPage());

		Then("^Click on Live chat link$", () -> this.myAccount.clickOnLiveChatLink());

		Then("^Click on close button$", () -> this.myAccount.clickOnCloseButton());

		Then("^Verify my account window gets closed$", () -> this.myAccount.verifyMyAccountButton());

		Then("^Verify following option on cashier page:$", (DataTable dt) -> {
			for (int i = 0; i < this.utilities.getListDataFromDataTable(dt).size(); i++) {
				this.myAccount.verifyOptionsOnCashierPage(this.utilities.getListDataFromDataTable(dt).get(i));
			}

		});

		Then("^Click on withdrawal on cashier page$", () -> this.myAccount.clickOnCashierWithdrawalBtn());

		Then("^Click on change password link$", () -> this.myAccount.clickOnChangePassword());

		Then("^Verify update button is disabled$", () -> this.myAccount.verifyUpdateBtnIsDisabled());

		Then("^Enter valid input in current password$", () -> this.myAccount.enterValidCurrentPassword());

		Then("^Enter valid input in new password$", () -> this.myAccount.enterValidNewPassword());

		Then("^Verify update button is enabled$", () -> this.myAccount.verifyUpdateBtnIsEnabled());

		Then("^Verify following option on responsible gambling page:$", (DataTable dt) -> {
			for (int i = 0; i < this.utilities.getListDataFromDataTable(dt).size(); i++) {
				this.myAccount
				.verifyOptionsOnResponsibleGamblingPage(this.utilities.getListDataFromDataTable(dt).get(i));
			}

		});

		Then("^Verify following option on deposit page:$", (DataTable dt) -> {
			for (int i = 0; i < this.utilities.getListDataFromDataTable(dt).size(); i++) {
				this.myAccount.verifyOptionsOnDepositPage(this.utilities.getListDataFromDataTable(dt).get(i));
			}
		});

		Then("^Verify success message displays on screen$", () -> this.myAccount.verifySuccessMessageOnLoginPage());

		Then("^Close success message$", () -> this.myAccount.closeSuccessPopup());

		Then("^Verify login window gets closed$", () -> {
		});

		Then("^Verify following option on account details page:$", (DataTable dt) -> {
			for (int i = 0; i < this.utilities.getListDataFromDataTable(dt).size(); i++) {
				this.myAccount.verifyOptionsOnAccountDetailsPage(this.utilities.getListDataFromDataTable(dt).get(i));
			}

		});

		Then("^Verify following option on take a break page:$", (DataTable dt) -> {

			System.out.println("*******************  "+utilities.getListDataFromDataTable(dt).size());
			List<String> list = dt.asList(String.class);
			for (int i = 0; i < list.size(); i++) {
				myAccount.verifyOptionsOnTakeABreakPage(list.get(i));
			}
		});

		Then("^Click on break time button$", () -> this.myAccount.clickOnBreakTimeButton());

		Then("^Verify following options:$", (DataTable arg1) -> {
		});

		Then("^Verify break period text$", () -> this.myAccount.verifyBreakPeriodText());

		Then("^Verify information text$", () -> {
		});

		When("^User click on any one day break button$", () -> this.myAccount.clickOnBreakTimeButton());

		Then("^Verify selected button gets highlited$", () -> this.myAccount.verifyButtonGetsHighlighted());


		When("^User click on take a break button$", () -> this.myAccount.clickOnTakeABreakButton());

		Then("^Verify confirmation popup$", () -> this.myAccount.verifyConfirmationBox());

		Then("^Verify popup contians take a break, logout, cancel button$", () -> {
		});

		When("^Click on take a break on popup$", () -> this.myAccount.clickBreakOnPopup());

		When("^User click on logout button$", () -> this.myAccount.clickLogoutOnPopup());
		//reality check page

		Then("^Verify following option on  reality check page:$", (DataTable dt) -> {
			System.out.println("*******************  "+utilities.getListDataFromDataTable(dt).size());
			List<String> list = dt.asList(String.class);
			for (int i = 0; i < list.size(); i++) {
				myAccount.verifyOptionsOnRealityCheckPage(list.get(i));
			}
		});

		Then("^Verify text in text box$", () -> this.myAccount.verifyTextInTextboxOnRealityPage());

		Then("^Click on uncollapsible icon$", () -> this.myAccount.clickOnUncollapsibleIconOnRealityPage());

		Then("^Verify system displays text$", () -> this.myAccount.verifyTextOnRealityPage());

		When("^User select any one option$", () -> this.myAccount.selectOptionOnRealityPage());

		When("^Click on save changes button$", () -> this.myAccount.clickOnSaveChangesOnRealityPage());

		// When("^Click on back button$", () -> this.myAccount.clickOnBackbutton());

		Then("^Verify changes get highlighted in account$", () -> {
		});

		Then("^Verify following option on gamstop page:$", (DataTable dt) -> {
			for (int i = 0; i < this.utilities.getListDataFromDataTable(dt).size(); i++) {
				this.myAccount
				.verifyOptionsOnGamstopPage(this.utilities.getListDataFromDataTable(dt).get(i));
			}
		});

		Then("^Verify text box$", () -> {

		});

		Then("^Click on \"([^\"]*)\"$", (String arg1) -> {
		});

		Then("^Verify link opens in new window$", () -> {
		});

		Then("User enters current password {string}", (String string) -> {
			this.myAccount.enterPasswdForCurrentPassword(string);
		});

		Then("Verify green line displays below current password field", () -> this.myAccount.verifyGreenColorBelowCurrentPassword());

		Then("User enters new password {string}", (String string) -> {
			this.myAccount.enterPasswdForNewPassword(string);
		});

		Then("Verify green line displays below new password field", () -> this.myAccount.verifyGreenColorBelowNewPassword());

		Then("Switch to paypal child window", () -> {

		});

		Then("Click on pay now", () -> {

		});

		Then("Switch to deposit window", () -> {

		});

		Then("Switch to paysafe child window", () -> {

		});

		Then("Enter paysafe account no {string}", (String string) -> {

		});

		Then("Click paysafe agree checkbox", () -> {

		});

		Then("Click on pay button", () -> {

		});

		Then("Click on uncollaps icon", () -> {

		});

		Then("Verify my account window", () -> {

		});

		Then("Click outside the window", () -> {

		});

		Then("Enter generated password", () -> {

			this.myAccount.enterPasswordOnTakeABreakPage();
		});

		Then("Verify following option on enter bonus code page:", (io.cucumber.datatable.DataTable dt) -> {
			System.out.println("*******************  "+utilities.getListDataFromDataTable(dt).size());
			List<String> list = dt.asList(String.class);
			for (int i = 0; i < list.size(); i++) {
				myAccount.verifyOptionsOnBonuses(list.get(i));
			}
		});

		Then("Verify submit button is disabled", () -> {

		});

		When("User enter bonus code", () -> {

		});

		Then("Verify submit button is enabled", () -> {

		});

		Then("Click on edit button", () -> {

		});

		Then("Verify following option on edit details page:", (io.cucumber.datatable.DataTable dataTable) -> {

		});

		Then("Enter postcode {string}", (String string) -> {

		});

		Then("Select one address", () -> {

		});

		Then("Verify address is selected", () -> {

		});

		When("User enters invalid password", () -> {

		});

		When("Click on update button", () -> {

		});

		Then("Verify error message", () -> {

		});

		When("User enters valid password", () -> {

		});

		Then("Verify confirmation message", () -> {

		});

		Then("Verify following options are displayed on bonuses page:", (io.cucumber.datatable.DataTable dataTable) -> {

		});

		Then("Verify {string} option", (String string) -> {

		});

		Then("Verify information box", () -> {

		});

		Then("Verify button {string}", (String string) -> {

		});

		Then("Click on button {string}", (String string) -> {

		});

		Then("Verify date is visible", () -> {

		});

		Then("Verify password field", () -> {

		});

		Then("Click on uncollapse button", () -> {

		});

		Then("Click on self exclude button", () -> {

		});

		Then("Enter correct password", () -> {

		});

		Then("Verify following options are displayed on balance page:", (io.cucumber.datatable.DataTable dataTable) -> {

		});

		Then("Verify following options are displayed on self exclude screen", (io.cucumber.datatable.DataTable dt) -> {

			List<String> list = dt.asList(String.class);
			for (int i = 0; i < list.size(); i++) {
				myAccount.verifyOptionsOnSelfexcludeScreen(list.get(i));}
		});
	}

	@Then("^Click on \"([^\"]*)\" from the screen$")
	public void click_on_from_the_screen(String str) {
		this.myAccount.clickOnYesOrNoFromExcludeScreen(str);
	}
	@Then("^Verify balance section is displayed$")
	public void verify_balance_section_is_displayed() {
		this.myAccount.verifyBalanceSectionDisplayed();
	}

	@Then("^Click on my account$")
	public void click_on_my_account() {
		this.myAccount.clickOnMyAccount();
	}

	@Then("^Click menu option \"([^\"]*)\"$")
	public void click_menu_option(String arg1) {
		this.myAccount.clickMenuOption(arg1);
	}

	@Then("^Verify \"([^\"]*)\" as header displayed$")
	public void verify_as_header_displayed(String arg1) {
		this.myAccount.verifyMenuHeaderDisplayed(arg1);
	}

	@Then("^Enter withdrawal amount \"([^\"]*)\"$")
	public void enter_withdrawal_amount(String arg1) {
		this.myAccount.enterWithdrawalAmt(arg1);
	}

	@Then("^Enter withdrawal password \"([^\"]*)\"$")
	public void enter_withdrawal_password(String arg1) {
		this.myAccount.enterWithdrawalPassword(arg1);
	}

	@Then("^click button having label as \"([^\"]*)\"$")
	public void click_span(String spanName) {
		this.myAccount.clickSpan(spanName);
	}

	@Then("^Verify Live help link$")
	public void verify_Live_help_link_on_mobile() {
		this.myAccount.verifyLiveHelpDisplayed();
	}

	@Then("^Verify transaction filter box displayed$")
	public void Verify_transaction_filter_box_displayed() {
		this.myAccount.transactionFilterBoxDisplayed();
	}

	@Then("^Verify dates are populated with today date$")
	public void Verify_dates_are_populated_with_today_date() {
		this.myAccount.datesPopulatedWithTodayDate();
	}

	@Then("^Verify transaction history box$")
	public void Verify_transaction_history_box() {
		this.myAccount.transactionHistoryBoxDisplayed();
	}

	@Then("^Select \"([^\"]*)\" filter activity$")
	public void select_filter_activity(String strFilterActivity) {
		this.myAccount.selectFilterActivity(strFilterActivity);
	}

	@Then("^Verify records are found of \"([^\"]*)\"$")
	public void verifyTransactionHistoryRecords(String activity) {
		this.myAccount.verifyTransactionHistoryRecords(activity);
	}
	@Then("^Click button Filter$")
	public void Click_button_Filter() {
		this.myAccount.clickFilterButton();
	}

	@Then("^Verify option \"([^\"]*)\" displayed$")
	public void Verify_option_displayed(String strOption) {
		this.myAccount.optionDisplayed(strOption);
	}

	@Then("^Select SortBy \"([^\"]*)\"$")
	public void Select_SortBy(String strSortBy) {
		this.myAccount.selectSortBy(strSortBy);
	}

	@Then("^Collapse one record$")
	public void collapse_one_record() {
		this.myAccount.collapseRecord();
	}

	@Then("^Verify the collapsed record$")
	public void Verify_the_collapsed_record() {
		this.myAccount.verifyCollapsedRecord();
	}

	@Then("^Open first message$")
	public void Open_first_message() {
		this.myAccount.clickFirstMessage();
	}

	@Then("^Verify message in detail$")
	public void Verify_message_in_detail() {
		this.myAccount.verifyMessageInDetail();
	}

	@Then("^Click delete button$")
	public void Click_delete_button() {
		this.myAccount.clickMsgDeleteButton();
	}

	@Then("Verify delete icon displayed")
	public void verify_delete_icon_displayed() {
		this.myAccount.verifyDeleteIconDisplayed();
	}

	@Then("Verify messages table displayed")
	public void verify_messages_table_displayed() {
		this.myAccount.verifyMessagesTableDisplayed();
	}

	@Then("^Verify link \"([^\"]*)\" of sub menu page$")
	public void verify_link_of_sub_menu_page(String arg1) {
		this.myAccount.verifyLinkOfSubMenuPage(arg1);
	}

	@Then("^Verify bold text \"([^\"]*)\" displayed$")
	public void Verify_bold_text_displayed(String strText) {
		this.myAccount.verifyH5TextDisplayed(strText);
	}

	@Then("^Verify message \"([^\"]*)\"$")
	public void Verify_error_message_from_para_tag(String strErrorMsg) {
		this.myAccount.verifyMsgFromParaTag(strErrorMsg);
	}

	@Then("^Verify button having label as \"([^\"]*)\" displayed$")
	public void verify_span_displayed(String strName) {
		this.myAccount.verifySpanDisplayed(strName);
	}

	@Then("^Switch to parent window$")
	public void switch_to_parent_window() {
		this.myAccount.switchToParent();
	}

	@Then("Read the message count")
	public void read_the_message_count() {
		this.myAccount.setMessageCount();
	}

	@Then("Verify that message count is reduced")
	public void verify_that_message_count_is_reduced() {
		this.myAccount.verifyMsgCountReduced();
	}


	@Then("^Verify following my account menu options:$")
	public void verify_following_my_account_menu_options(DataTable dt) {
		for (int i = 0; i < utilities.getListDataFromDataTable(dt).size(); i++) {
			this.myAccount.verifyMenuOptions(utilities.getListDataFromDataTable(dt).get(i));
		}
	}

	@Then("^Enter postal code \"([^\"]*)\"$")
	public void enter_postal_code(String strPostalCode) {
		this.myAccount.enterPostalCode(strPostalCode);
	}

	@Then("^Enter address line1 \"([^\"]*)\"$")
	public void enter_address_line(String arg2) {
		this.myAccount.enterAddressLine1(arg2);
	}

	@Then("^Enter city \"([^\"]*)\"$")
	public void enter_city(String arg1) {
		this.myAccount.enterCity(arg1);
	}

	@Then("^Enter county \"([^\"]*)\"$")
	public void enter_county(String arg1) {
		this.myAccount.enterCounty(arg1);
	}

	@Then("^Verify \"([^\"]*)\" reminder button is highlighted$")
	public void verify_reminder_button_is_highlighted(String arg1) {
		this.myAccount.reminderButtonHighlighted(arg1);
	}

	@Then("^Enter limit \"([^\"]*)\"$")
	public void enter_limit(String strLimit) {
		this.myAccount.enterLimit(strLimit);
	}

	@Then("^Click deposit limit button \"([^\"]*)\"$")
	public void click_deposit_limit_button(String strName) {
		this.myAccount.clickDepositLimitButton(strName);
	}

	@Then("Click button having text {string}")
	public void click_button_having_text(String strName) {
		this.myAccount.clickButtonHavingGivenText(strName);
	}

	@Then("Reduce deposit limit by one")
	public void Reduce_deposit_limit_by_one() {
		this.myAccount.reduceDepositLimitByOne();
	}

	@Then("Increase deposit limit by one")
	public void Increase_deposit_limit_by_one() {
		this.myAccount.increaseDepositLimitByOne();
	}

	@Then("^Enter password \"([^\"]*)\" for take a break$")
	public void enter_password_for_take_a_break(String strPassword) {
		this.myAccount.enterPasswordForTakeBreak(strPassword);
	}

	@Then("^click link \"([^\"]*)\"$")
	public void click_link(String link) {
		this.myAccount.clickLink(link);
	}

	@Then("^Enter random email id with suffix \"([^\"]*)\"$")
	public void enter_random_email_id_with_suffix_on_mobile(String arg1) {
		this.myAccount.enterRandomEmailIdWithGivenSuffix(arg1);
	}

	@Then("Click close button from live chat")
	public void clickCloseButtonOfLiveChat() {
		this.myAccount.clickCloseButtonFromLiveChat();
	}

	@Then("^Click on plus sign of \"([^\"]*)\" page$")
	public void click_on_plus_sign_of_take_a_break_page(String arg) {
		if(arg.equalsIgnoreCase("take a break"))
		{
			this.myAccount.clickOnPlusSignOfTakeABreakPage("Why Take a Break");
		}
		else if(arg.equalsIgnoreCase("Self Exclude"))
		{this.myAccount.clickOnPlusSignOfTakeABreakPage("Why Self Exclude?");}
	}

	@Then("^Verify information text \"([^\"]*)\" displayed$")
	public void Verify_information_text_displayed(String strText) {
		this.myAccount.verifyDivTextDisplayed(strText);
	}

	//vaishali
	@Then("^Enter invalid bonus code$")
	public void enter_invalid_bonus_code(){
		myAccount.setPromoCode("dfgf");
	}
	@Then("^Click on submit$")
	public void Click_on_submit(){
		myAccount.clickSubmitcode();
	}
	@Then("^Enter bonus code as \"([^\"]*)\"$")
	public void enter_bonus_code_as(String promocode)  {
		promoCode = promocode;
		myAccount.setPromoCode(promocode);
	}
	@Then("^Promotion details will display to user$")
	public void promotion_details_will_display_to_user() {
		myAccount.verifyTermsnConditionsLinkDisplayed();
		BonusName = myAccount.getBonusName();
		System.out.println("Bonus Name : " +BonusName);
		myAccount.verifyAcceptCheckboxDisplayed();
		myAccount.verifyAcceptTnCsLabelDisplayed();
		myAccount.verifyClaimPromotionBTN();
		myAccount.verifyCancelLinkDisplayed();
	}

	@Then("^Select the T & C option and click on Claim button$")
	public void select_the_T_C_option_and_click_on_Claim_button() {
		myAccount.SelectAcceptCheckboxDisplayed();
		myAccount.clickClaimPromotionBTN();
	}

	@Then("^Bonus success screen will display to the user$")
	public void bonus_success_screen_will_display_to_the_user(){
		myAccount.verifySuccessHeaderDisplayed();

		//myAccount.verifyClaimedBonusNameDisplayed(BonusName);
		myAccount.verifyClaimedBonusNameDisplayed(promoCode);
		myAccount.verifyCloseBTNDisplayed();
		myAccount.verifyActivePromotionsBTNDisplayed();
		//	myAccount.ClickCloseBTNDisplayed();
	}
	@Then("^Click on ActivePromotions button$")
	public void click_on_ActivePromotions_button()  {
		myAccount.clickActivePromotionsBTNDisplayed();
	}

	@Then("^Verify following details should be displayed on unExpanded view$")
	public void verify_following_details_should_be_displayed_on_unExpanded_view(DataTable dt){
		List<String> list = dt.asList(String.class);
		for (int i = 0; i < list.size(); i++) {
			myAccount.verifyBonusDetailsAfterExpandingBonus(list.get(i));}
	}
	@Then("^Verify following details should be displayed on Expanded view$")
	public void verify_following_details_should_be_displayed_on_Expanded_view(DataTable dt){
		List<String> list = dt.asList(String.class);
		for (int i = 0; i < list.size(); i++) {
			myAccount.verifyBonusDetailsAfterExpandingBonus(list.get(i));}
	}
	@Then("^Click on \\+ button of any claimed bonus$")
	public void click_on_button_of_any_claimed_bonus()  {
		if(myAccount.verifyExpandButtonDisplayed())
			myAccount.clickExpandButton();
	}

	@Then("^Select bonus type as \"([^\"]*)\" from filter$")
	public void select_bonus_type_as_from_filter(String arg1) {
		BonusType = arg1;
		myAccount.selectBonusType(arg1);
	}
	@Then("^Verify filter results are displayed as per selected bonus type$")
	public void verify_filter_results_are_displayed_as_per_selected_bonus_type() {
		if(BonusType.equalsIgnoreCase("Opt in"))
		{myAccount.verifyTypeHDRDisplayed("OptIn");}
		else {myAccount.verifyTypeHDRDisplayed(BonusType);}
	}

	@Then("^Change the bonus Date and click on Filter button$")
	public void change_the_bonus_Date_and_click_on_Filter_button()  {
		fromDate = utilities.getRequiredDay("-1", "dd MMMM yyyy", "");
		toDate = utilities.getCurrentDate("dd/MM/YYYY");
		System.out.println(fromDate+ " *******************   "+toDate);
		myAccount.setFromDate(fromDate);
		myAccount.verifyToDate(toDate);
		myAccount.clickFilterButton();
	}

	@Then("^Verify Bonus history is displayed as per selected bonus date$")
	public void verify_Bonus_history_is_displayed_as_per_selected_date_range(){
		myAccount.verifyExpandButtonDisplayed();
		myAccount.clickExpandButton();
		myAccount.verifyActiveBonusesLabelsDisplayed("Type,Bonus status,Amount,Wagering target,Progress,Activation,Bonus status,Expiry");
		myAccount.verifyBonusHistoryIsDisplayedAsPerSelectedDateRange(fromDate, toDate);
		//myAccount.verifyOptOutLinkDisplayed();
		myAccount.verifyViewTCsLinkDisplayed();
	}

	@Then("^Verify Claimed bonus details are displayed on screen$")
	public void verify_Claimed_bonus_details_are_displayed_on_screen() {
		//myAccount.verifyBonusNameOnBonusHistoryDisplayed("test");
		myAccount.clickExpandButton();
		myAccount.verifyActiveBonusesLabelsDisplayed("Type,Bonus status,Amount,Wagering target,Progress,Activation,Bonus status,Expiry");
	}

	@Then("^Click on Opt Out link$")
	public void click_on_Opt_Out_link() {
		myAccount.verifyOptOutLinkDisplayed();
		myAccount.verifyViewTCsLinkDisplayed();
		myAccount.clickOptOutLink();		
	}

	@Then("^Accept confirmation pop up$")
	public void accept_confirmation_pop_up(){
		myAccount.verifyAreUSureTXTonPopupDisplayed();
		myAccount.verifyOptOutTXTonPopupDisplayed();
		myAccount.verifyYesBTNonPopupDisplayed();
		myAccount.verifyNoBTNonPopupDisplayed();
		myAccount.clickYesBTNonPopup();
	}

	@Then("^Verify that Bonus status displayed as \"([^\"]*)\"$")
	public void verify_that_Bonus_status_displayed_as(String arg1)  {
		myAccount.validateBonusStatus(arg1);
	}
	@Then("^Verify following options are displayed under bonus type filter dropdown$")
	public void verify_following_options_are_displayed_under_bonus_type_filter_dropdown(DataTable dt) {
		List<String> list = dt.asList(String.class);
		for (int i = 0; i < list.size(); i++) {
			myAccount.verifyBonusTypesDisplayed(list.get(i));}
	}
	//take a break
	@Then("^Enter valid password for take a break$")
	public void enter_valid_password_for_take_a_break() {
		String password = configuration.getConfig("web.password");
		myAccount.enterPasswordForTakeBreak(password);

	}
	@Then("^Verify following break periods$")
	public void verify_following_break_periods(DataTable dt)  {
		List<String> list = dt.asList(String.class);
		for (int i = 0; i < list.size(); i++) {
			myAccount.verifyBreakPeriodOptions((list.get(i)));
		}
	}
	//gamestop
	@Then("^Verify and Click on Gamestop link from gamestop screen$")
	public void verify_click_on_Gamestop_link_from_gamestop_screen()  {
		myAccount.verifyGamestopLink();
		myAccount.clickOnGamestopLink();
	}
	//marketing pref
	@Then("^User will get the option to select between email, phone, sms, post and Select all$")
	public void user_will_get_the_option_to_select_between_email_phone_sms_post_and_Select_all(){
		myAccount.verifyMarketingPreferencesOption("Email~SMS~Phone~Post~Select All");
	}

	@Then("^Uncheck all the options$")
	public void  Uncheck_all_the_options(){
		objRegistrationPage.clickCheckbox("all");
		if(objRegistrationPage.verifyCheckboxIsCheckedOrNot("all"))
		{objRegistrationPage.clickCheckbox("all");}
	}
	//my account
	@Then("^Verify Name and date of birth field is non editable$")
	public void Verify_Name_and_date_of_birth_field_is_non_editable(){
		myAccount.verifyPageTitle("Name,Date of birth");
		myAccount.verifyNonEditableTXTDisplayed();
	}
	@Then("^Edit email address,Phone number and address$")
	public void edit_email_address_Phone_number_and_address() {
		myAccount.EditEmail();
		myAccount.EditPhoneNumber(String.valueOf(utilities.getRandomNumeric(11)));
		//objRegistrationPage.setmanualAddress(utilities.getRandomAlphanumericString(5) ,utilities.getRandomAlphanumericString(5), utilities.getRandomAlphanumericString(5),  utilities.getRandomAlphanumericString(5), "ha01sg");
	}

	@Then("^User enters correct current password$")
	public void user_enters_correct_current_password(){
		myAccount.setCurrentPassword(configuration.getConfig("web.password"));
	}

	@Then("^User enters incorrect current password as \"([^\"]*)\"$")
	public void user_enters_incorrect_current_password_as(String arg1){
		myAccount.setCurrentPassword(arg1);
	}

	@Then("^Enter valid New password as \"([^\"]*)\"$")
	public void enter_valid_New_password_as(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		newPassword = arg1;
		myAccount.setNewPassword(newPassword);
	}
	@Then("^Password updated confirmation message will display$")
	public void password_updated_confirmation_message_will_display() {
		objRegistrationPage.verifyH4HeadingDisplayed("Password updated");
		System.out.println("********** newPassword "+newPassword);
		configuration.updateParameterDetailsInConfig("web.password",newPassword);
	}

	@Then("^Enter updated password$")
	public void enter_updated_password(){
		objLogInPage.enterPassword(newPassword);
	}
	@Then("^Get current message count from my account screen$")
	public void Get_current_message_count_from_my_account_screen()  {
		messageCount = myAccount.getMessageCountFromMyAccountScreen();
		System.out.println(" messageCount :::::   "+messageCount);
	}

	@Then("^Verify system displays notification on the My Account screen for new message in the inbox$")
	public void verify_system_displays_notification_on_the_My_Account_screen_for_new_message_in_the_inbox(){
		waitMethods.sleep(59);
		mobileActions.pageRefresh();
		String currentMsgCount = myAccount.getMessageCountFromMyAccountScreen();
		System.out.println(messageCount +  "     "+currentMsgCount);
		myAccount.verifySystemdisplaysUpdatedMessageCountorNot(messageCount,currentMsgCount);
	}

	@Then("Verify {string} displayed")
	public void verify_displayed(String string) {
		myAccount.verifyTakeAbReakMessage();
	}
	@Then("^Logout user and get logout time$")
	public void Logout_user_and_get_logout_time(){

		currentLogoutTime = utilities.getCurrentDate("dd MMMM yyyy hh:mm:ss a");
		System.out.println("******* currentLogoutTime ::  "+currentLogoutTime);
		objLogInPage.clickOnLogoutbutton();
	}

	@Then("^User can see Last Login time in date-time format$")
	public void user_can_see_Last_Login_time_in_date_time_format(){
		lastLoginTime = myAccount.getLastLoginFormat();
		System.out.println("***** lastLoginTime  "+lastLoginTime);
		myAccount.validateLastLoginTimeFormat(lastLoginTime);
	}

	@Then("^Verify that previous login date and time is displayed as Last login time$")
	public void verify_that_previous_login_date_and_time_is_displayed_as_Last_login_time() {
		myAccount.verifyThatLastLoginTimeIsDisplayedCorrectly(currentLogoutTime);
	}

	@Then("Verify Add membership number field is not displayed on screen")
	public void verify_add_membership_number_field_is_not_displayed_on_screen() {
		myAccount.AddMembershipFieldNotApperedForRetailUserAccount();
	}

	@Then("Verify {string} field is displayed on screen")
	public void verify_field_is_displayed_on_screen(String string) {
		myAccount.verifyFieldsOnAccountDetailsScreen(string);
	}
	@Then("Add the membership number {string} in Membership no Field")
	public void add_the_membership_number_in_membership_no_field(String number) {
		myAccount.setMembershipNumber(number);
	}

	@Then("Click on Submit link")
	public void click_on_submit_link() {
		myAccount.clickOnSubmitLink();
	}

	@Then("Membership number added successfully")
	public void membership_number_added_successfully() {

	}


	@Then("Verify membership number {string} is displayed on Account details page")
	public void verify_membership_number_is_displayed_on_account_details_page(String expValue) {
		myAccount.verifyExpectedValueIsDisplayedOnMyAccountscreen(expValue, "Club Card number");
	}

	@Then("Click on Clear button")
	public void click_on_clear_button() {
		myAccount.clearTheBonusCode();
	}

	@Then("Verify Entered code is removed")
	public void verify_entered_code_is_removed() {
		myAccount.verifyBonusCodeInputFieldIsBlank();
	}

	//msg
		@Then("Verify delete icon displayed in disabled state")
		public void verify_delete_icon_displayed_in_disabled_state() {
			this.myAccount.verifyDeleteIconDisplyedInDisableState();
		}
		@Then("^verify unread message icon is display for unread message$")
		public void verify_unread_message_icon_is_display_for_unread_message() {
			myAccount.verifyunreadMessage("Customer Services");
		}

		@Then("^Click on unread Message from list of message$")
		public void click_on_unread_Message_from_list_of_message() {
			myAccount.selectUnreadMessage("Customer Services");	
		}

		@Then("^Verify message subject,sender name and message content is display on screen$")
		public void verify_message_subject_sender_name_and_message_content_is_display_on_screen() {
			myAccount.verifyMessageBodyDetails("Customer Services", "expleo mecca test message", "test message");
		}

		@Then("^verify unread message icon change into read icon$")
		public void verify_unread_message_icon_change_into_read_icon() {
			myAccount.verifyRecivedMessageDetails("Customer Services", "expleo mecca test message");
		}
		@Then("^select message to delete$")
		public void select_message_to_delete() {
			myAccount.selectMessageCheckbox("expleo mecca test message");
		}

		@Then("^verify selected message gets delete$")
		public void verify_selected_message_gets_delete() {
			myAccount.verifyDeletedMessageNotDisplayedInList("Customer Services", "expleo mecca test message");
		}

		@Then("^Get message count from header$")
		public void get_message_count_from_header() {
			msgCountHeader = myAccount.getMessageCountFromHomeScreen();
			System.out.println("***** msgCountHeader  ::  "+msgCountHeader);
			waitMethods.sleep(5);
		}

		@Then("^Get message count from Message header$")
		public void get_message_count_from_Message_header() {
			myAccount.setMessageCount();
			msgcount = myAccount.getMessageCount();
			System.out.println("***** msgcount message header ::  "+msgcount);
			waitMethods.sleep(5);
		}

		@Then("Verify that message count is reduced from my account icon ,Title section and Message sub menu")
		public void verify_that_message_count_is_reduced_from_my_account_icon_title_section_and_message_sub_menu() {
			myAccount.setMessageCount();
			int currentHeaderCount = myAccount.getMessageCount();
			System.out.println("** cureent msg header count ::::: "+currentHeaderCount);
			//myAccount.verifyUpdatedCountisDisplayedOnAllScrrens(String.valueOf(msgcount), String.valueOf(currentHeaderCount));
			myAccount.clickOnBackbutton();
			myAccount.clickOnBackbutton();
			String myAccountMsgCount = myAccount.getMessageCountFromMyAccountScreen();
			System.out.println("** cureent my account msg count ::::: "+myAccountMsgCount);
			myAccount.verifyUpdatedCountisDisplayedOnAllScrrens(messageCount, myAccountMsgCount);
			myAccount.clickOnCloseButton();
			waitMethods.sleep(5);
			String currentHomeMsgCount = myAccount.getMessageCountFromHomeScreen();
			System.out.println("** cureent home screebn msg count ::::: "+myAccountMsgCount);
			myAccount.verifyUpdatedCountisDisplayedOnAllScrrens(msgCountHeader, currentHomeMsgCount);
			
		}
		@Then("Verify {string} button is displayed on screen")
		public void verify_field_button_is_displayed_on_screen(String string) {
			myAccount.verifyButtonOnScreen(string);
		}
		
		@Then("Click on {string} button from screen")
		public void click_on_button_from_screen(String string) {
			myAccount.clickOnButtonFromScreen(string);
		}
}

