package com.meccabingo.mobile.stepDefinition;

import com.generic.utils.Utilities;
import com.meccabingo.mobile.page.Mecca.RecentlyPlayedPage;

import io.cucumber.java8.En;


public class RecentlyPlayedStep implements En {

	private RecentlyPlayedPage recentlyPlayedPage; 

	public RecentlyPlayedStep(RecentlyPlayedPage recentlyPlayedPage, Utilities utilities) 
	{
		this.recentlyPlayedPage = recentlyPlayedPage;
 
		Then("^Verify \"([^\"]*)\" tab displayed on home page$", (String header) -> this.recentlyPlayedPage.verifyTabDisplayedOnHomePage(header));
		
		Then("^Verify You dont have any recently played games message displayed$", () -> this.recentlyPlayedPage.verifyYouDontHaveAnyRecentlyPlayedGamesMessageDisplayed());
		
		Then("^Select \"([^\\\"]*)\" from top navigation menu$", (String menu) -> this.recentlyPlayedPage.selectNavigationMenu(menu));
	}
}
