package com.meccabingo.mobile.stepDefinition;

import com.generic.utils.Utilities;
import com.meccabingo.mobile.page.Mecca.MobilePreBuyPage;

import io.cucumber.java.en.Then;

public class MobilePreBuyPageStep {

	private MobilePreBuyPage objMobilePreBuyPage;
	Utilities utilities;

	public MobilePreBuyPageStep(MobilePreBuyPage preBuyPage) {

		this.objMobilePreBuyPage = preBuyPage;
	}

	@Then("Verify pre-buy popup is displayed")
	public void verify_pre_buy_popup_is_displayed() {
		objMobilePreBuyPage.verifyPreBuyPopupDisplayed();
	}
	
	@Then("Click pre-buy from bingo tile")
	public void click_pre_buy_from_bingo_tile() {
		objMobilePreBuyPage.clickPreBuyButton();
	}
	
	@Then("Select ticket {string}")
	public void select_ticket(String string) {
		objMobilePreBuyPage.selectTicket(string);
	}
	
	
}
