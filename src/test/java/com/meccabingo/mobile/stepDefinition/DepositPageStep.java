package com.meccabingo.mobile.stepDefinition;

import com.generic.utils.Utilities;
import com.meccabingo.mobile.page.Mecca.DepositPage;

import io.cucumber.java.en.Then;

public class DepositPageStep {

	private DepositPage objDepositPage;
	Utilities utilities;

	public DepositPageStep(DepositPage depositPage) {

		this.objDepositPage = depositPage;
	}

	@Then("^Click on Deposit button besides myaccount$")
	public void click_on_Deposit_button_besides_myaccount() {
		objDepositPage.clickOnDepositBesidesMyAcct();
	}

	@Then("^Click show more$")
	public void click_show_more() {
		objDepositPage.clickShowMore();
	}

	@Then("^Click payment method as \"([^\"]*)\"$")
	public void click_payment_method_as(String strPayMethod) {
		objDepositPage.selectPaymentMethod(strPayMethod);
	}

	@Then("^Enter Card number \"([^\"]*)\"$")
	public void enter_Card_number(String strCNo) {
		objDepositPage.enterCardNumber(strCNo);
	}

	@Then("^Enter Expiry \"([^\"]*)\"$")
	public void enter_Expiry(String strExpiry) {
		objDepositPage.enterExpiry(strExpiry);
	}

	@Then("^Enter CVV \"([^\"]*)\"$")
	public void enter_CVV(String strCVV) {
		objDepositPage.enterCVV(strCVV);
	}

	@Then("^Enter amount to deposit \"([^\"]*)\"$")
	public void enter_amount_to_deposit(String strAmt) {
		objDepositPage.enterDepositAmt(strAmt);
	}

	@Then("^Click on Deposit button from Deposit page$")
	public void click_on_Deposit_button_from_Deposit_page() {
		objDepositPage.clickDepositButton();
	}

	@Then("^Verify deposit is successful$")
	public void verify_deposit_is_successful() {
		objDepositPage.verifyDepositSuccessful();
	}

	@Then("^Click close button from deposit successful popup$")
	public void click_close_button_from_deposit_successful_popup() {
		objDepositPage.clickCloseFromDepositSuccessfulPopup();
	}

	@Then("^Click withdraw$")
	public void click_withdraw() {
		objDepositPage.clickWithdraw();
	}

	@Then("^Verify withdraw is successful$")
	public void verify_withdraw_is_successful() {
		objDepositPage.verifyWithdrawSuccessful();
	}

	@Then("^Click saved card \"([^\"]*)\"$")
	public void click_saved_card(String arg1) {
		objDepositPage.selectSavedCard(arg1);
	}

	@Then("^Click PayPal$")
	public void click_PayPal() {
		objDepositPage.clickPayPal();
	}

	@Then("^Click PaySafe$")
	public void click_PaySafe() {
		objDepositPage.clickPaySafe();
	}
}
