package com.meccabingo.mobile.stepDefinition;

import com.meccabingo.mobile.page.Mecca.Carousel;
import io.cucumber.java8.En;

public class CarouselStep implements En {
	private Carousel carousel;

	public CarouselStep(Carousel carousel) {
		this.carousel = carousel;

		// using double colon operator
		// Then("Click anywhere on slider\\/carousel",carousel::clickOnSlide);
		
		Then("close the popup", () -> this.carousel.closePopup());
		
		Then("Click anywhere on slider\\/carousel", () -> this.carousel.clickOnSlide());

		Then("Verify user navigate to respective page", () -> this.carousel.verifyNavigateToPage());

		Then("Verify navigation arrows on carousel", () -> this.carousel.verifyNavigationArrow());

		Then("Verify pagination dots on carousel", () -> this.carousel.verifyPaginationDots());

		//Then("Verify slider moves automatically after time inerval",
				//() -> this.carousel.verifySliderMovesAutomatically());

		Then("Click on left arrow on carousel", () -> this.carousel.clickOnLeftArrow());

		//Then("Verify slider moves one slide to left", () -> this.carousel.verifySlideMovesToLeft());

		Then("Click on right arrow on carousel", () -> this.carousel.clickOnRightArrow());

		//Then("Verify slider moves one slide to right", () -> this.carousel.verifySlideMovesToRight());

		Then("Verify feefo carousel slide", () -> this.carousel.verifyFeefoCarousel());

		Then("Verify left button on feefo carousel", () -> this.carousel.verifyLeftButton());
		
		Then("Verfi right button on feefo carousel", () -> this.carousel.verifyRightButton());
		
	}
}
