package com.meccabingo.mobile.stepDefinition;

import java.util.List;

import com.generic.MobileActions;
import com.generic.WebActions;
import com.generic.utils.Utilities;
import com.meccabingo.mobile.page.Mecca.ClubsPage;

import io.cucumber.java.en.Then;

public class ClubsPageStep {

	private ClubsPage objClubsPage;
	Utilities utilities;
	private MobileActions webActions;
	
	public ClubsPageStep(ClubsPage clubPage,MobileActions webActions) {
		this.webActions = webActions;
		this.objClubsPage = clubPage;
	}

	@Then("Search club {string}")
	public void search_club(String string) {
		objClubsPage.searchClub(string);
	}

	@Then("Click on more Info")
	public void click_on_more_info() {
		objClubsPage.clickMoreInfo();
	}

	@Then("Select first club that appears in list")
	public void select_first_club_that_appears_in_list() {
		objClubsPage.selectFirstClubFromList();
	}

	@Then("Mark it as favorite")
	public void mark_it_as_favorite() {
		objClubsPage.clickFavorite();
	}

	@Then("Unfavorite it")
	public void unfavorite_it() {
		objClubsPage.clickFavorite();
	}

	@Then("Click link having text as {string}")
	public void click_link_having_text_as(String string) {
		objClubsPage.clickLinkText(string);
	}

	@Then("Verify club marked as favorite")
	public void verify_club_marked_as_favorite() {
		objClubsPage.verifyMarkedAsFavorite();
	}

	@Then("Verify club favorite tag is removed")
	public void verify_club_favorite_tag_is_removed() {
		objClubsPage.verifyNotMarkedAsFavorite();
	}

	@Then("Enter Home phone {string}")
	public void enter_home_phone(String string) {
		objClubsPage.enterHomePhone(string);
	}

	@Then("Enter join club email {string}")
	public void enter_join_club_email(String string){
		objClubsPage.enterJoinClubEmail(string);
	}

	@Then("Verify user is joined club successfully")
	public void verify_user_joined_club_successfully( ) {
		objClubsPage.userJoinedClub();
	}
	@Then("Verify following sections are displayed under Venue finder")
	public void verify_following_sections_are_displayed_under_venue_finder(io.cucumber.datatable.DataTable dt) {
		List<String> list = dt.asList(String.class);
		for (int i = 0; i < list.size(); i++) {
			objClubsPage.verifyFieldsUnderVenuFinder(list.get(i));}
	}

	@Then("Enter {int} or more than {int} chacaters in search fields")
	public void enter_or_more_than_chacaters_in_search_fields(Integer int1, Integer int2) {
		search_club("Mec");
	}


	@Then("Verify maximum of {int} results are shown below the search field with {string} and {string}")
	public void verify_maximum_of_results_are_shown_below_the_search_field_with_and(Integer int1, String string, String string2) {
		objClubsPage.verifyMaximumof5ResultsAreShownBelowTheSearchfield();
		objClubsPage.verifyClubNameNMilesDisplayedUnderSearchResult();
	}


	@Then("Verify text {string} at the top of the results")
	public void verify_text_at_the_top_of_the_results(String string) {
		objClubsPage.verifyThesearethenearestcasinostoyourlocationText();
	}

	@Then("Map area is opened with following club info")
	public void map_area_is_opened_with_following_club_info(io.cucumber.datatable.DataTable dt) {
		List<String> list = dt.asList(String.class);
		for (int i = 0; i < list.size(); i++) {
			objClubsPage.verifyClubInformation(list.get(i));}
	}

	@Then("Click on {string} cta")
	public void click_on_cta(String string) {
		objClubsPage.clickOnFindVenuButtons(string);
	}

	@Then("Google maps will open in a new tab")
	public void google_maps_will_open_in_a_new_tab() {
		webActions.switchToChildWindow();
		objClubsPage.verifyGoogleMapLinkIsOpend();
	}


	@Then("User navigate to the club details page")
	public void user_navigate_to_the_club_details_page() {
		objClubsPage.verifyClubDetailsPage();
	}
	
	@Then("^Verify \"([^\"]*)\" confirmation message")
	public void verify_confirmation_message(String text) {
		objClubsPage.verifyConfirmationMessage(text);
	}
	
	@Then("Verify Favourited club section is displayed on screen")
	public void verify_Favourited_club_section_is_displayed_on_screen() {
		objClubsPage.verifyFavouritedClubSection();
	}
}