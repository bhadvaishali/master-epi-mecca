package com.meccabingo.mobile.stepDefinition;

import org.openqa.selenium.By;

import com.generic.utils.Utilities;
import com.meccabingo.mobile.page.Mecca.PaySafePage;

import io.cucumber.java.en.Then;

public class PaySafePageStep {

	private PaySafePage objPaySafePage;
	Utilities utilities;

	public PaySafePageStep(PaySafePage paySafePage) {

		this.objPaySafePage = paySafePage;
	}

	@Then("^Enter paysafe account \"([^\"]*)\"$")
	public void enter_paysafe_account(String arg1) {
		objPaySafePage.enterPaySafeAccount(arg1);
	}

	@Then("^Click paysafe agree$")
	public void click_paysafe_agree() {
		objPaySafePage.clickPaySafeAgree();
	}

	@Then("^Click paysafe pay$")
	public void click_paysafe_pay() {
		objPaySafePage.clickPaySafePay();
	}
}
