package com.meccabingo.mobile.page.cogs;

import org.openqa.selenium.By;

import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.WaitMethods;
import com.generic.webdriver.DriverProvider;

public class Cogs_LoginPage {
	
	private WebActions objWebActions;
	private DriverProvider objDriverProvider;
	private LogReporter logReporter;
	private WaitMethods waitMethods;
	private Configuration configuration;

	public Cogs_LoginPage(WebActions webActions,  DriverProvider driverProvider,  LogReporter logReporter,WaitMethods waitMethods,Configuration configuration ) {
		this.objWebActions = webActions;
		this.objDriverProvider = driverProvider;
		this.logReporter = logReporter;
		this.waitMethods = waitMethods;
		this.configuration= configuration;
	}

	public void setUserName(String userName)
	{
		By inpUserName = By.xpath("//input[@id='DefaultLoginView_CogsLoginControl_UserName']");
		logReporter.log("Set username ", userName,
				objWebActions.setTextWithClear(inpUserName, userName));
	}
	
	public void setPassword(String password)
	{
		By inpPassword = By.xpath("//input[@id='DefaultLoginView_CogsLoginControl_Password']");
		logReporter.log("Set Password ", password,
				objWebActions.setTextWithClear(inpPassword, password));
	}
	
	public void clickLogin() { 
		By btnLogin = By.xpath("//input[@id='DefaultLoginView_CogsLoginControl_LoginButton']");
		logReporter.log("click 'Login' ", 
				objWebActions.click(btnLogin));
	}
	
	public void verifyPrivacyError()
	{
		By locator = By.xpath("//button[@id='details-button']");
		if(objWebActions.checkElementDisplayedWithMidWait(locator))
			{
			objWebActions.click(locator);
			locator = By.xpath("//a[@id='proceed-link']");
			objWebActions.click(locator);
			}
	}
	
}



