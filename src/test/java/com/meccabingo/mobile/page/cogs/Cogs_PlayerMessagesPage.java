package com.meccabingo.mobile.page.cogs;

import org.openqa.selenium.By;

import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.Utilities;
import com.generic.utils.WaitMethods;
import com.generic.webdriver.DriverProvider;

public class Cogs_PlayerMessagesPage {
	private WebActions objWebActions;
	private DriverProvider objDriverProvider;
	private LogReporter logReporter;
	private WaitMethods waitMethods;
	private Configuration configuration;
	private Utilities objUtilities;
	public Cogs_PlayerMessagesPage(WebActions webActions,  DriverProvider driverProvider,  LogReporter logReporter,WaitMethods waitMethods,Configuration configuration,Utilities Utilities ) {
		this.objWebActions = webActions;
		this.objDriverProvider = driverProvider;
		this.logReporter = logReporter;
		this.waitMethods = waitMethods;
		this.configuration= configuration;
		this.objUtilities = Utilities ;
	}


	public void selectMenu(String mainMenu,String...subMenu)
	{
		By lnkMainMenu = By.xpath("//table[@id='menuContainerTable']//tbody//tr//ul//li/div//span[text()='"+mainMenu+"']");
		logReporter.log("Select ' "+mainMenu+ " ' from left side menu ",objWebActions.click(lnkMainMenu));

		if(subMenu != null) 
		{
			By lnksubMenu = By.xpath("//a[contains(@id,'_LeftMenu_LeftMenuControl_')]/span[text()='"+subMenu[0]+"']");
			logReporter.log("Select ' "+subMenu+ " ' ",objWebActions.click(lnksubMenu));
			}
	}

	public void clickSerachMessageButton()
	{
		By locator = By.xpath("//div[contains(@id,'_Button')]//span[text()='Search']");
		logReporter.log("Click on search ' ",objWebActions.click(locator));
	}
	public void searchMessageBySubject(String arg1)
	{
		By locator = By.xpath("//input[contains(@id,'_MessageSubject_I')]");
		logReporter.log("Enter message subject as ' "+arg1,objWebActions.setText(locator, arg1));
	}
	public void selectExpleoMessage(String message)
	{
		By locator = By.xpath("//tr//td[text()='"+message+"']");
		logReporter.log("select message ' ",objWebActions.click(locator));
	}

	public void clickOnSchedule()
	{
		By btnschedule = By.xpath("//div[contains(@id,'_ScheduleButton')]//span[text()='Schedule']");
		logReporter.log("Click on Schedule ",objWebActions.click(btnschedule));
	}

	public void switchToMessageTempleIframe()
	{
		By iframe = By.xpath("//iframe[contains(@src,'MessageTemplateSchedule')]");
		objWebActions.switchToFrameUsingIframe_Element(iframe);	
	}

	public void selectUplodTypeAsTextArea()
	{
		By rdoTextArea = By.xpath("//label[text()='Text Area']//preceding::span[contains(@id,'UploadTypeList_RB1')]");
		logReporter.log("select Uplod Type As TextArea",objWebActions.click(rdoTextArea));
	}

	public void setSendDateAndTime()
	{
		String date = objUtilities.getRequiredTime("1","dd MMM yyyy HH:mm:ss","GMT");
		String senddate = date.substring(0,date.lastIndexOf(" "));
		System.out.println(date );
		System.out.println(senddate );
		String sendTime = date.substring(date.lastIndexOf(" "));
		sendTime = sendTime.replace(" ","");
		sendTime = sendTime.replace(":","");
		System.out.println(sendTime );
		
		By inputSendDate = By.xpath("//input[contains(@id,'_PopupContentPlaceHolder_FreeCardsPanel_FE_StartDate_I')]");
		By inputSendTime = By.xpath("//input[contains(@id,'PopupContentPlaceHolder_FreeCardsPanel_FE_StartTime_I')]");
		
		logReporter.log("Set send date ",objWebActions.setText(inputSendDate, senddate));
		
		objWebActions.click(inputSendTime);
		logReporter.log("Set send time ",objWebActions.setText(inputSendTime, sendTime));
	}
	
	public void setData(String playerID)
	{
		By locator = By.xpath("//textarea[contains(@id,'_PopupContentPlaceHolder_TextAliasPanel_UserNameMemo_I')]");
		logReporter.log("Set Data ",objWebActions.setText(locator, playerID));
	}
	
	public void clickOnScheduleSend()
	{
		By btnschedule = By.xpath("//div[contains(@id,'_btnUpdateText')]//span[text()='Schedule Send']");
		logReporter.log("Click on Schedule Send ",objWebActions.click(btnschedule));
	}
	
	public void setDays(String days)
	{
		By inputDays = By.xpath("//input[contains(@id,'PopupContentPlaceHolder_ExpiryDateTimePanel_ExpDays_I')]");
		logReporter.log("Set Expiry days ",objWebActions.setText(inputDays, days));
	}
	
	public void acceptAlert()
	{
		String text = objDriverProvider.getWebDriver().switchTo().alert().getText();
		System.out.println(text);
		waitMethods.sleep(configuration.getConfigIntegerValue("minwait"));
		if(text.contains("Are you sure you want to schedule"))
		{
			objDriverProvider.getWebDriver().switchTo().alert().accept();
			System.out.println("******* accept ");
			waitMethods.sleep(configuration.getConfigIntegerValue("maxwait"));
		}
	}
	
	public void verifyUpdateSuccessfulMessage()
	{
		By locator = By.xpath("//span[@id='StatusMessage'][text()='Update Successful']");
		logReporter.log(" verify 'Update Successful' ",objWebActions.checkElementDisplayed(locator));
		waitMethods.sleep(50);
	}
}
