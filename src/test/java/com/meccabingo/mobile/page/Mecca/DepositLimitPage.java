package com.meccabingo.mobile.page.Mecca;

import org.openqa.selenium.By;

import com.generic.MobileActions;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.Utilities;
import com.generic.utils.WaitMethods;


public class DepositLimitPage {
	private MobileActions objMobileActions;
	private AppiumDriverProvider objDriverProvider;
	private LogReporter logReporter;
	private WaitMethods wait;
	private Configuration configuration;
	private Utilities objUtilities;

	public DepositLimitPage(MobileActions objMobileActions, AppiumDriverProvider driverProvider,LogReporter logReporter,WaitMethods wait,Configuration configuration,Utilities objUtilities) {
		this.objMobileActions = objMobileActions;
		this.objDriverProvider = driverProvider;
		this.logReporter = logReporter;
		this.configuration= configuration;
		this.wait = wait;
		this.objUtilities = objUtilities ;
	}
	public void clickExpandButton()
	{
		By expandBTN = By.xpath("//a[contains(@class,'bonus-item-button')]");
		logReporter.log(" click 'expand + BTN' ", 
				objMobileActions.clickUsingJS(expandBTN));
	}
	public void VerifyCollapseButton()
	{
		By collapseBTN = By.xpath("//a[contains(@class,'bonus-item-button')]/i[@class=' icon-collapse']");
		logReporter.log(" Verify 'collapse - BTN' displayed", 
				objMobileActions.checkElementDisplayed(collapseBTN));
	}

	By depositIframe = By.id("deposit-limits-iframe");


	public void verifyDepositLimitOptions(String lbl)
	{
		if(lbl.contains("~")){
			String[] arr1 = lbl.split("~");
			for (String links : arr1  ) {
				By locator = By.xpath("//app-deposit-limits-overview//..//span[contains(.,'"+links+"')]");
				logReporter.log(links+" option displayed on screen ",
						objMobileActions.checkElementDisplayed(locator));}}
		else{
			By locator = By.xpath("//app-deposit-limits-overview//..//span[contains(.,'"+lbl+"')]");
			logReporter.log(lbl+" is displayed on screen ",
					objMobileActions.checkElementDisplayed(locator));}
		//app-deposit-limits-overview//..//span[contains(.,'24-hour limit')]
		//	7-day limit
		//30-day limit
	}

	public String getDepositLimitValue(String lbl)
	{
		//app-deposit-limits-overview//..//span[contains(.,'24-hour limit')]//following-sibling::h6
		By locator = By.xpath("//app-deposit-limits-overview//..//span[contains(.,'"+lbl+"')]//following-sibling::h6");
		return objMobileActions.getText(locator);
	}

	public void ClickOnedit(String lbl)
	{
		//app-deposit-limits-overview//..//span[contains(.,'30-day limit')]//following::div//button[contains(text(),'Edit')]
		By locator = By.xpath("//app-deposit-limits-overview//..//span[contains(.,'"+lbl+"')]//following::div//button[contains(text(),'Edit')]");
		logReporter.log(" Edit ' "+lbl, 
				objMobileActions.clickUsingJS(locator));
	}

	public void verifyDepositLimitEditHeader(String lbl)
	{
		//app-deposit-limits-edit//h6[contains(.,'Set 24-hour Net Deposit Loss Limit')]
		//Set 7-day Net Deposit Loss Limit
		//Set 30-day Net Deposit Loss Limit
		By locator = By.xpath("//app-deposit-limits-edit//h6[contains(.,'Set "+lbl+" Net Deposit Loss Limit')");
		logReporter.log(" verify ' "+lbl+ " ' ",
				objMobileActions.checkElementDisplayed(locator));
	}

	public void verifyDepositLimitEditInfo()
	{
		By locator = By.xpath("//app-deposit-limits-edit//app-info-panel//div//p[contains(.,'If you’re lowering your limit, this will take effect immediately. However, if you’re increasing your limit, this will need to be re-confirmed by you after a 24-hour cooling-off period')]");
		logReporter.log(" verify 'If you’re lowering your limit, this will take effect immediately. However, if you’re increasing your limit, this will need to be re-confirmed by you after a 24-hour cooling-off period '",
				objMobileActions.checkElementDisplayed(locator));
	}

	public String getDepositLimitInputPlaceholderValue()
	{
		By locator = By.xpath("//div[contains(@class,'section-title')]//following::app-form-field//div//input[@type='number']");
		return objMobileActions.getAttribute(locator, "placeholder");
		
	}
	public void setDepositLimit(String lbl,String lmt)
	{
		//div[contains(@class,'section-title')][contains(.,'24-hour limit')]//following::app-form-field//div//input[@type='number']
		//	7-day limit
		//	30-day limit
		By locator = By.xpath("//div[contains(@class,'section-title')][contains(.,'"+lbl+" limit')]//following::app-form-field//div//input[@type='number']");
		logReporter.log(" Set ' "+lbl+ " ' ",
				objMobileActions.setTextWithClear(locator,lmt));
	}

	public void clickOnButton(String btn)
	{
		//app-cashier-button//button[text()=' Submit ']
		//app-cashier-button//button[text()='Yes'],No
		By locator = By.xpath("//app-cashier-button//button[contains(.,'"+btn+"')]");
		logReporter.log(" click ' "+btn, 
				objMobileActions.clickUsingJS(locator));
	}

	public void verifyDepositLimitErrorMsg(String errmsg)
	{
		//span[contains(@slot,'validation-error-message')][contains(.,' monthly limit cannot be lower than weekly limit ')]
		//daily limit cannot be higher than weekly limit
		//daily limit cannot be higher than monthly limit
		// weekly limit cannot be lower than daily limit 
		//span[contains(@slot,'validation-error-message')][contains(.,'Enter in a valid amount')]
		//span[@slot='validation-error-message'][contains(.,'Enter a valid amount')]
		//By locator = By.xpath("//span[contains(@slot,'validation-error-message')][contains(.,'"+errmsg+"')]");
		By locator = By.xpath("//span[@slot='validation-error-message'][contains(.,'"+errmsg+"')]");
		logReporter.log(" verify ' "+errmsg+ " ' ",
				objMobileActions.checkElementDisplayed(locator));
	}



	//div//span[@cashiericon="clock"]//following::div//span[contains(.,'£500 pending')]
	//app-deposit-limits-overview//..//span[contains(.,'24-hour limit')]//following::div//button[contains(text(),'Cance')]

	//div//span[@cashiericon="check_circle"]//following::div//span[contains(.,'£300 approved')]


	public void verifyUpdatedLimitDisplayedOrNot(String lmt,String expectedLmt)
	{//app-deposit-limits-overview//..//span[contains(.,'24-hour limit')]//following-sibling::h6[contains(@class,'recently-updated-text')]
		By locator = By.xpath("//app-deposit-limits-overview//..//span[contains(.,'"+lmt+" limit')]//following-sibling::h6[contains(@class,'recently-updated-text')]");
		String actualLmt = objMobileActions.getText(locator);
		
		expectedLmt = "£"+expectedLmt+".00";
		System.out.println("****** expectedLmtfffff  "+expectedLmt);
		System.out.println(":*********actual limit :::"+actualLmt);
		logReporter.log(" verify updated limit is displayed or not ",
				actualLmt.equalsIgnoreCase(expectedLmt));
	}

	public void verifyLimitIsDisplayedInPendingStateOrNot(String lmt,String expectedLmt)
	{//app-deposit-limits-overview//..//span[contains(.,'24-hour limit')]//following-sibling::h6[contains(@class,'recently-updated-text')]
		
		By locator = By.xpath("//div//span[@cashiericon='clock']//following::div//span[contains(.,'£"+expectedLmt+" pending')]");
		System.out.println("****** expectedLmtfffff  "+expectedLmt);
		logReporter.log(" verify updated limit is displayed in pending state ",
				objMobileActions.checkElementDisplayed(locator));
	}

	public void verifyNetDepositLossLimitInfo()
	{
		this.verifyNetDepositLossLimitInfoText("Your Net Deposit Loss is the difference between your deposits and your withdrawals. For example, if you deposit £20 and then go on to withdraw £15, your Net Deposit loss would be £5.~Daily limits are refreshed at midnight, weekly Limits are refreshed at 1am on Mondays, and monthly Limits are refreshed on the 1st of the new month.~This means you could reach your limit as your net deposit loss is accumulated across multiple sites.");
		this.verifyIncludedBrands("KittyBingo.com~LuckyPantsBingo.com~SpinandWin.com~MagicalVegas.com~RegalWins.com~LuckyVIP.com~KingJackCasino.com~Aspers.com");
	}

	public void verifyNetDepositLossLimitInfoText(String txt)
	{
		if(txt.contains("~")){
			String[] arr1 = txt.split("~");
			for (String text : arr1  ) {
				By locator = By.xpath("//app-explain-net-deposit-loss-limit//div[contains(.,'What is a Net Deposit Loss Limit?')]//p[contains(.,'"+text+"')]");
				logReporter.log(text+"  displayed on screen ",
						objMobileActions.checkElementDisplayed(locator));}}
		else{
			By locator = By.xpath("//app-explain-net-deposit-loss-limit//div[contains(.,'What is a Net Deposit Loss Limit?')]//p[contains(.,'"+txt+"')]");
			logReporter.log(txt+" is displayed on screen ",
					objMobileActions.checkElementDisplayed(locator));}
	}


	public void verifyIncludedBrands(String brandName)
	{
		if(brandName.contains("~")){
			String[] arr1 = brandName.split("~");
			for (String bnm : arr1  ) {
				By locator = By.xpath("//app-explain-net-deposit-loss-limit//div//p[contains(.,'Our brands include:')]//following-sibling::ul//li//p[contains(.,'- "+bnm+"')]");
				logReporter.log(bnm+"  displayed on screen ",
						objMobileActions.checkElementDisplayed(locator));}}
		else{
			By locator = By.xpath("//app-explain-net-deposit-loss-limit//div//p[contains(.,'Our brands include:')]//following-sibling::ul//li//p[contains(.,'- "+brandName+"')]");
			logReporter.log(brandName+" is displayed on screen ",
					objMobileActions.checkElementDisplayed(locator));}
	}
}
