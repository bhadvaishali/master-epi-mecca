
package com.meccabingo.mobile.page.Mecca;

import org.openqa.selenium.By;

import com.generic.MobileActions;
import com.generic.WebActions;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.Utilities;
import com.generic.utils.WaitMethods;

import io.cucumber.datatable.DataTable;


public class PromotionPage
{
	private MobileActions objMobileActions;
	private LogReporter logReporter;
	private WaitMethods wait;
	private Configuration configuration;
	private Utilities objUtilities;

	public PromotionPage(MobileActions webActions, LogReporter logReporter,WaitMethods wait,Configuration configuration,Utilities objUtilities) 
	{
		this.objMobileActions = webActions;
		this.logReporter = logReporter;
		this.configuration= configuration;
		this.wait = wait;
		this.objUtilities = objUtilities ;
	}

	By PromotionTab = By.xpath("(//a[@href='/promotions'])[1]");
	By InstantBonusCard = By.xpath("//h3[contains(.,'INSTANT Bonus BAT Testing')]");
	By ButtonClaim = By.xpath("//apollo-activate-bonus-cta[@promotion-type='claim']/child::button[contains(.,'Claim')]");
	By ClaimedButton = By.xpath("//apollo-activate-bonus-cta[@promotion-type='claim']/child::button[contains(.,'Claimed')]");
	By ClaimButtonFromDetailPage = By.xpath("//apollo-activate-bonus-cta[@cta-active-text='Claim']");
	By LearnMoreButtonFromInstantBonusCard = By.xpath("//apollo-activate-bonus-cta[@promotion-type='claim']//preceding-sibling::a[contains(.,'Learn more')]");
	By TandCLinkFromInstantBonusCard = By.xpath("//a[@href='/promotions/instant-bonus#terms-and-conditions']");
	By PromotionHeading = By.xpath("//h2[contains(.,'Promotions')]");
	By ClaimedStatusOnPromotionDetailPage = By.xpath("//apollo-activate-bonus-cta[@cta-done-text='Claimed']");
	By FeaturedGameSectionIsDisplayed = By.xpath("(//apollo-slot-tile)[2]");
	By CashBackBonusOptInButton = By.xpath("//apollo-activate-bonus-cta[@title='CASHBACKONTOTALSTAKE Bonus BAT Testing']/child::button[contains(.,'Opt-in')]");
	By CashBackBonusLearnMoreButton = By.xpath("//apollo-activate-bonus-cta[@title='CASHBACKONTOTALSTAKE Bonus BAT Testing']/preceding-sibling::a[contains(.,'Learn more')]");
	By CashbackBonusTandCLink = By.xpath("//a[@href='/promotions/cashback-on-total-stake#terms-and-conditions']");
	By FeaturedGameSectionForCashbackOptIn = By.xpath("//div[@class='promotion-info-featured-games ']/child::div[@class='promotion-info-featured-games-inner']");
	By OptedInButtonForCashbackBonusDetailPage = By.xpath("//apollo-activate-bonus-cta[@cta-done-text='Opted In']");
	By OptInButtonForCashbackBonusDetailPage = By.xpath("//apollo-activate-bonus-cta[@cta-active-text='Opt-in']");
	By CashbackBonusOptedIn = By.xpath("//apollo-activate-bonus-cta[@title='CASHBACKONTOTALSTAKE Bonus BAT Testing']/child::button[contains(.,'Opted In')]");

	public void clickOnPramotionTab()
	{
		logReporter.log("Check Pramotion tab is displyed:", objMobileActions.checkElementDisplayed(PromotionTab));
		logReporter.log("Click on Pramotion tab :", objMobileActions.click(PromotionTab));
		wait.sleep(5);
	}



	public void verifyBonusCardComponents(String bonusName, String text )
	{	
		if(bonusName.contains("Opt in bonus"))
		{
			switch (text)
			{

			case "Opt in": 
				this.verifyPromoTypeCTA("OptIn");
				break;
			case "Learn more": 
				this.verifyLearnMoreCTA("OptIn");
				break;
			case "T&Cs apply": 
				this.verifyViewTnCLink("OptIn");
				break;
			case "Opted in": 
				this.verifyClaimedOrOptedInButton("Opted in");
				break;
			}
		}
		else if(bonusName.contains("Instant bonus"))
		{
			switch(text)
			{
			case "Claim": 
				this.verifyPromoTypeCTA("Claim");
				break;
			case "Claimed": 
				this.verifyClaimedOrOptedInButton("Claimed");
				break;
			case "Learn more": 
				this.verifyLearnMoreCTA("Claim");
				break;
			case "T&Cs apply": 
				this.verifyViewTnCLink("Claim");
				break;
			}
		}
	}


	public void VerifyFeaturedGameSectionIsDisplayed() 
	{
		logReporter.log("verify Featured game section on page", objMobileActions.checkElementDisplayed(FeaturedGameSectionIsDisplayed));
	}



	public void ClickOnButtonsFromPromotionDetailPage(String buttonName) 
	{
		switch(buttonName)
		{
		case"Claim":
			logReporter.log("Click on Claim button from Instant promotion detail page::",objMobileActions.click(ClaimButtonFromDetailPage));
		}
	}

	//All
	//Slots & Games
	//Club
	//My Active Promotions
	//apollo-filter-heading//div//ul//li//a[contains(.,'Bingo')]

	//apollo-activate-bonus-cta[@cta-active-text='Opt in']
	//apollo-activate-bonus-cta[@cta-active-text='Claim']

	//apollo-activate-bonus-cta[@cta-active-text='Opt in']/button[contains(.,'Opted in')]
	//h4 You don’t currently have any active promotions

	public void verifyLearnMoreCTA(String promoTypeCTA)
	{
		By locator = By.xpath("//apollo-activate-bonus-cta[@cta-active-text='"+promoTypeCTA+"']/preceding-sibling::a[contains(.,'Learn more')]");
		logReporter.log("Verify 'Learn more' cta ",objMobileActions.checkElementDisplayed(locator));
	}

	public void clickOnLearnMoreCTA(String bonusName)
	{
		if(bonusName.contains("Opt in bonus")){
			By locator = By.xpath("//apollo-activate-bonus-cta[@cta-active-text='OptIn']//button[not(contains(@class,'colour-white'))]//preceding::a[contains(.,'Learn more')][1]");
			logReporter.log("click on  'Learn more' cta ",objMobileActions.click(locator));
		}
		else if(bonusName.contains("Instant bonus"))
		{
			By locator = By.xpath("//apollo-activate-bonus-cta[@cta-active-text='Claim']//button[not(contains(@class,'colour-white'))]//preceding::a[contains(.,'Learn more')][1]");
			logReporter.log("click on  'Learn more' cta ",objMobileActions.click(locator));
		}
	}


	public void verifyPromoTypeCTA(String promoTypeCTA)
	{
		By locator = By.xpath("//apollo-activate-bonus-cta[@cta-active-text='"+promoTypeCTA+"']");
		logReporter.log("Verify ' "+promoTypeCTA+  " button on promotiona page",objMobileActions.checkElementDisplayed(locator));
	}

	public void clickOnPromoTypeCTA(String promoTypeCTA)
	{
		By locator = By.xpath("//apollo-activate-bonus-cta[@cta-active-text='"+promoTypeCTA+"']");
		logReporter.log("Verify ' "+promoTypeCTA+  " button",objMobileActions.click(locator));
	}

	public void verifyClaimedOrOptedInButton(String btnType)
	{
		if(btnType.equalsIgnoreCase("Opted in"))
		{
			By locator = By.xpath("//apollo-activate-bonus-cta[@cta-active-text='OptIn']/button[contains(.,'Opted In')]");
			logReporter.log("Verify ' "+btnType+  " button is displayed on promotion page",objMobileActions.checkElementDisplayed(locator));}
		else
		{
			By locator = By.xpath("//apollo-activate-bonus-cta[@cta-active-text='Claim']/button[contains(.,'Claimed')]");
			logReporter.log("Verify ' "+btnType+  " button is displayed on promotion page",objMobileActions.checkElementDisplayed(locator));
		}
	}

	public void clickOnPromotionsGameFilterDropdown()
	{
		By locator = By.xpath("//h2[contains(.,'Promotions')]//following-sibling::div[contains(@class,'filter-heading-select-games-with-dots')]//div");
		logReporter.log("Click on promotions filter option ",  
				objMobileActions.click(locator));
	}
	public void verifyPromotionsSubHeaderTabs(String tabName)
	{

		if(tabName.contains("~"))
		{
			String[] arr1 = tabName.split("~");
			for (String pref1 : arr1) 
			{

				By locator = By.xpath("//apollo-filter-heading//div//ul//li//a[contains(.,'"+pref1+"')]");
				//By locator = By.xpath("//h2[contains(.,'"+sectionName+"')]//following-sibling::div//ul//li//a[contains(.,'"+pref1+"')]");
				logReporter.log("Verify '" +pref1 + " ' option is displayed on under promotions sub tabs",  
						objMobileActions.checkElementDisplayed(locator));
			}
		}
		else
		{
			By locator = By.xpath("//apollo-filter-heading//div//ul//li//a[contains(.,'"+tabName+"')]");
			//By locator = By.xpath("//h2[contains(.,'"+sectionName+"')]//following-sibling::div//ul//li//a[contains(.,'"+tabName+"')]");
			logReporter.log("Verify ' "+tabName+  " tab is displayed under  promotions sub tabs",objMobileActions.checkElementDisplayed(locator));}
	}

	public void verifyMyActivePromotionTabIsNotDisplayedInLOgoutState()
	{
		By locator = By.xpath("//apollo-filter-heading//div//ul//li//a[contains(.,'My Active Promotions')]");
		logReporter.log("Verify 'My Active Promotions' tab is not displayed in logout state",objMobileActions.checkElementNotDisplayed(locator));
	}
	public void clickPromotionsSubHeaderTabs(String tabName)
	{
		By loctaor = By.xpath("//apollo-filter-heading//div//ul//li//a");
		if(!(objMobileActions.checkElementDisplayedWithMidWait(loctaor)))
		{	clickOnPromotionsGameFilterDropdown();
		wait.sleep(configuration.getConfigIntegerValue("midwait"));}
		if(tabName.contains("~"))
		{
			String[] arr1 = tabName.split("~");
			for (String pref1 : arr1) 
			{
				By locator = By.xpath("//apollo-filter-heading//div//ul//li//a[contains(.,'"+pref1+"')]");
				logReporter.log("Click on '" +pref1 + " ' option ",  
						objMobileActions.click(locator));
			}
		}
		else
		{
			By  locator = By.xpath("//apollo-filter-heading//div//ul//li//a[contains(.,'"+tabName+"')]");
			logReporter.log("click on ' "+tabName+  " tab ",objMobileActions.click(locator));}
	}

	public void verifyViewTnCLink(String promotype)
	{
		By locator = By.xpath("//apollo-promotion-tile[@cta-active-text='"+promotype+"']//following-sibling::article[contains(@class,'promotion-tile')]//div[contains(@class,'promotion-tile-footer')]//p//a[text()='View T&Cs']");
		logReporter.log("Verify 'View T&Cs' link is displayed against the ' "+promotype,objMobileActions.checkElementDisplayed(locator));
	}

	public void verifyOptOutBTNDisplayed()
	{
		By locator = By.xpath("//apollo-deactivate-bonus-cta//button[text()='Opt out']");

		logReporter.log(" Verify Opt out Button displayed when clicked on OptIn BTN",
				objMobileActions.checkElementDisplayed(locator));

	}
	public void verifyDonthaveanyactivepromotionsInformativeMessage()
	{
		By locator = By.xpath("//h4[contains(.,'You don’t currently have any active promotions')]");
		logReporter.log(" Verify 'You don’t currently have any active promotions' message ",
				objMobileActions.checkElementDisplayed(locator));
	}
	public void verifyActivepromotionsIsDisplayedUnderActiveTab()
	{
		By locator = By.xpath("//apollo-active-promotion-tile");
		logReporter.log(" Verify  Active promotions Is Displayed Under Active Tab",
				objMobileActions.checkElementDisplayed(locator));
	}
	public void verifynoOfDayLeftText()
	{
		By locator = By.xpath("//div[contains(@class,'promotion-tile-days-left')]//i[contains(@class,'icon-clock')]//following-sibling::p");
		int size = objMobileActions.processMobileElements(locator).size();
		System.out.println("size******  "+size);
		for(int i=0;i<size;i++) {
			String days = objMobileActions.processMobileElements(locator).get(i).getText();
			System.out.println("******** days ++ "+days);
			if(days.contains("DAYS LEFT"))
			{
				days = days.substring(0, days.indexOf(" "));
				System.out.println("******** no of days ++ "+days);
				if(Integer.valueOf(days)>3)
				{
					String txtColor = objMobileActions.getCssValue(locator, "color");
					System.out.println("**************** text color "+txtColor);

					logReporter.log("Text is displayed in green color if days are more than 3 days",
							objMobileActions.checkCssValue(locator, "color","#5ea85a"));}
			}
			else if(days.contains("HOURS LEFT"))
			{
				days = days.substring(0, days.indexOf(" "));
				System.out.println("******** no of days ++ "+days);
				if(Integer.valueOf(days)> 25 && Integer.valueOf(days) < 72)
				{
					String txtColor = objMobileActions.getCssValue(locator, "color");
					System.out.println("**************** text color "+txtColor);

					logReporter.log("Text is displayed in Orenge color if days ore than 24hrs and less than 72 hrs",
							objMobileActions.checkCssValue(locator, "color","#5ea85a"));}
				else 
				{
					String txtColor = objMobileActions.getCssValue(locator, "color");
					System.out.println("**************** text color "+txtColor);

					logReporter.log("Text is displayed in Red color if days less than 25hrs and not yet expired",
							objMobileActions.checkCssValue(locator, "color","#e22c2c"));
				}
			}}
	}
}
