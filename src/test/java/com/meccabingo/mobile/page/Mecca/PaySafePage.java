package com.meccabingo.mobile.page.Mecca;

import org.openqa.selenium.By;

import com.generic.MobileActions;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.logger.LogReporter;


public class PaySafePage {
	
	private MobileActions objMobileActions;
	private AppiumDriverProvider objDriverProvider;
	private LogReporter logReporter;

	public PaySafePage(AppiumDriverProvider driverProvider, MobileActions objMobileActions,
			LogReporter logReporter) {
		this.objDriverProvider = driverProvider;
		this.objMobileActions = objMobileActions;
		this.logReporter = logReporter;
	}

	public void enterPaySafeAccount(String strAcc) {
		By locator = By.xpath("//input[contains(@class,'input-pin')]");
		if (objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("enter acc> >", objMobileActions.setText(locator, strAcc));
	}

	public void clickPaySafeAgree() {
		By locator = By.xpath("//div[contains(@id,'acceptTerms')]//input");
		By addPin = By.xpath("//*[@id='toggleTopUpInput']");
		if (objMobileActions.checkElementDisplayedWithMidWait(addPin))
		logReporter.log("click agree > >", objMobileActions.clickUsingJS(locator));
	}
	
	public void clickPaySafePay() {
		By locator = By.xpath("//span[text()='Pay']");
		logReporter.log("click spaysafe > >", objMobileActions.click(locator));
		//objMobileActions.switchToParentWindow();
	}

}
