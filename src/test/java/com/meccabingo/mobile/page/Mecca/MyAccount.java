package com.meccabingo.mobile.page.Mecca;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.server.handler.FindElements;
import com.generic.MobileActions;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.Utilities;
import com.generic.utils.WaitMethods;


public class MyAccount {

	private MobileActions objMobileActions;
	private AppiumDriverProvider objDriverProvider;
	private LogReporter logReporter;
	private WaitMethods wait;
	private Configuration configuration;
	private Utilities objUtilities;

	public MyAccount(MobileActions objMobileActions, AppiumDriverProvider driverProvider,LogReporter logReporter,WaitMethods wait,Configuration configuration,Utilities objUtilities) {
		this.objMobileActions = objMobileActions;
		this.objDriverProvider = driverProvider;
		this.logReporter = logReporter;
		this.configuration= configuration;
		this.wait = wait;
		this.objUtilities = objUtilities ;
	}


	String greencolor = "#009D7A";

	By Username = By.xpath("//h4[contains(text(),'AUTOMECCA2020')]");
	By Closebutton = By.xpath("//a[contains(@class,'icon-close')]");
	By backbutton = By.xpath("//a[contains(@class,'icon-arrow')]");
	By PlayableBalance = By.xpath("//h5[contains(text(),'Playable Balance')]");
	By Cashier = By.xpath("//a[contains(@heading,'Cashier')]");
	By Bonuses = By.xpath("//a[contains(@heading,'Bonuses')]");
	By Messages = By.xpath("//a[contains(@heading,'Message')]");
	By Accountdetails = By.xpath("//a[contains(@heading,'Account Details')]");
	By Responsiblegambling = By.xpath("//a[contains(@heading,'Responsible Gambling')]");
	By Logouticon = By.xpath("//a[contains(text(),'Logout')]/i");
	By Logoutlink = By.xpath("//a[contains(text(),'Logout')]");
	By Recentlyplayedsection = By.xpath("//h4[contains(text(),'Recently Played')]");
	By Email = By.xpath("//a[contains(text(),'Support@meccabingo.com')]");
	By Phonenumber = By.xpath("//div[contains(@class,'contacts-wrapper')]/span/p[contains(text(),'Call')]");
	By Livechat = By.xpath("//i[contains(@class,'icon-live-chat')]");
	By LiveHelpLink = By.xpath("//a[contains(text(),'Live help')]");

	By PlayableBalanceAmount = By.xpath("//h5[contains(text(),'Playable Balance')]/span");
	By PlayableBalancePlusIcon = By.xpath("//div[contains(@class,'playable-balance')]//div//div//i[contains(@class,'icon-expand')]");
	By PlayableBalanceMinusIcon = By.xpath("//div[contains(@class,'playable-balance')]//div//div//i[contains(@class,'opened')]");
	By PlayableBalanceCashText = By
			.xpath("//div[contains(@class,'playable-balance-details')]/div/p[contains(text(),'Cash')]");
	By PlayableBalanceCashAmount = By
			.xpath("//div[contains(@class,'playable-balance-details')]/div/p[contains(text(),'Cash')]/span");
	By PlayableBalanceBonusesText = By
			.xpath("//div[contains(@class,'playable-balance-details')]/div/p[contains(text(),'Bonuses')]");
	By PlayableBalanceBonusesAmount = By
			.xpath("//div[contains(@class,'playable-balance-details')]/div/p[contains(text(),'Bonuses')]/span");
	By PlayableBalanceDetailedButton = By.xpath("//div[contains(@class,'playable-balance-details')]/div/button[1]");
	By PlayableBalanceDepositButton = By.xpath("//div[contains(@class,'playable-balance-details')]/div/button[2]");
	By balancetext = By.xpath("//h4[contains(text(),'Balance')]");

	// By getPageTitle(String pageTitle) {
	// return By.xpath("//h4[contains(text(),'" + pageTitle + "')]");
	// }

	By MyAccountButton = By.xpath("//a[contains(@class,'open-myaccount')]/i");
	// Cashier
	By CashierPageTitle = By.xpath("//h4[contains(text(),'Cashier')]");

	By DepositOnCashierPage = By.xpath("//a[contains(@heading,'Deposit')]");
	By WithdrawalOnCashierPage = By.xpath("//div[contains(@class,'my-account-menu')]/a[2]");
	By PendingWithdrawalsOnCashierPage = By.xpath("//a[contains(@heading,'Pending Withdrawals')]");
	By TransactionHistoryOnCashierPage = By.xpath("//a[contains(@heading,'Transaction History')]");
	By BalanceOnCashierPage = By.xpath("//a[contains(@heading,'Balance')]");

	// Cashier deposit
	By TitleOnCashierDepositPage = By.xpath("//h4[contains(text(),'Deposit')]");
	By DepositLimitLink = By.xpath("//a[contains(text(),'Why not set deposit limits?')]");
	By SafechargeFrame = By.xpath("//form[contains(@id,'PPPParametersForm')]/div");

	By TitleOnCashierWithdrawalPage = By.xpath("//h4[contains(text(),'Withdrawal')]");
	By AmountText = By.xpath("//h3[contains(text(),'Choose amount of withdraw')]");
	By AlertBox = By
			.xpath("//span[contains(text(),'Please note: once you withdraw the cash you can’t reverse withdrawals')]");
	By AmountTextbox = By.xpath("//input[contains(@id,'input-amount')]");
	By PasswordTextbox = By.xpath("//input[contains(@id,'input-password')]");
	By NextButton = By.xpath("//button/span[contains(.,'Next')]");

	By TitleOnCashierPendingWithdrawalsPage = By.xpath("//h4[contains(text(),'Pending Withdrawals')]");

	By TitleOnCashierTransactionHistoryPage = By.xpath("//h4[contains(text(),'Transaction History')]");
	By ActivityBox = By.xpath("//div[contains(@class,'dialog-box')]");
	By HistoryTable = By.xpath("//div[contains(@class,'my-account')]");

	By TitleOnCashierBalancePage = By.xpath("//h4[contains(text(),'Balance')]");
	By DepositLimitOnBalancePage = By.xpath("//a[contains(text(),'Set a Deposit limit')]");
	By BlanceBox = By.xpath("//div[contains(@class,'content-box')]");

	// Bonuses
	By BonusesPageTitle = By.xpath("//h4[contains(text(),'Bonuses')]");
	By EnterBonusCode = By.xpath("//a[contains(@heading,'Enter bonus code')]");
	By EnterBonusCodeTitle = By.xpath("//h4[contains(text(),'Enter promotional code')]");
	By EnterBonusCodeText = By.xpath("//input[@id='bonus-code'][@placeholder='Enter code here']");
	By EnterBonusCodeField = By.xpath("//input[contains(@id,'bonus-code')]");
	By SubmitButton = By.xpath("//button[contains(@type,'submit')]");
	By btnClear = By.xpath("//button[contains(.,'Clear')]");

	By ActiveBonus = By.xpath("//a[contains(@heading,'Active bonuses')]");
	By ActiveBonusTitle = By.xpath("//h4[contains(text(),'Active bonuses')]");
	By ActiveBonuses = By.xpath("//*[@id=\"overlay-level-3\"]/div/section/div[2]/div/div[1]/div");

	By BonusHistory = By.xpath("//a[contains(@heading,'Bonus History')]");
	By BonusHistoryTitle = By.xpath("//h4[contains(text(),'Bonus History')]");
	By HistoryBox = By.xpath("//div[contains(@class,'dialog-box')]");

	// Messages
	By MessagesPageTitle = By.xpath("//h4[contains(text(),'Message')]");

	// Account Details
	By AccountdetailsPageTitle = By.xpath("//h4[contains(text(),'Account Details')]");
	By DetailsBox = By.xpath("//div[contains(@class,'content-box-details')]");
	By EditButton = By.xpath("//button[@type='button']/span[text()='Edit']");
	By ChangePassword = By.xpath("//span[text()='Change Password']");
	By ChangePasswordTitle = By.xpath("//h4[contains(text(),'Change Password')]");
	By CurrentPassword = By.xpath("//input[contains(@id,'old-password')]");
	By NewPassword = By.xpath("//input[contains(@id,'new-password')]");
	By UpdateButton = By.xpath("//button/span[contains(.,'UPDATE')]");

	By MarketinPreferences = By.xpath("//a[contains(@heading,'Marketing Preferences')]");
	By MarketinPreferencesTitle = By.xpath("//h4[contains(text(),'Marketing Preferences')]");
	By EmailCheckbox = By.xpath("//input[contains(@id,'gdpr-email')]");
	By SMSCheckbox = By.xpath("//input[contains(@id,'gdpr-sms')]");
	By PhoneCheckbox = By.xpath("//input[contains(@id,'gdpr-phone')]");
	By PostCheckbox = By.xpath("//input[contains(@id,'gdpr-post')]");
	By SelectAllCheckbox = By.xpath("//input[contains(@id,'gdpr-all')]");

	// Responsible Gambling
	By ResponsiblegamblingPageTitle = By.xpath("//h4[contains(text(),'Responsible Gambling')]");

	//Reality check
	By RealityCheck = By.xpath("//a[contains(@heading,'Reality Check')]");
	By RealityCheckTitle = By.xpath("//h4[contains(text(),'Reality Check')]");
	By textboxtext = By.xpath("//h5[text()='Tell me more']");
	By uncollapsibleicon = By.xpath("//i[contains(@class,'icon-expand ')]");
	By textonpage = By.xpath("//p[contains(text(),'Reality Checks are a helpful way of keeping track of the time you have spent playing our games')]");
	By option = By.xpath("");
	By tellmemoreicon = By.xpath("//h5[text()='Tell me more']//following-sibling::i[contains(@class,'icon-expand')]");
	By setyourreminder = By.xpath("//p[contains(.,'Set your reminder')]");
	By fifteenminButton = By.xpath("//button//span[text()='1 mins']");
	By firstbutton = By.xpath("//button//span[text()='30 mins']");
	By secondbutton = By.xpath("//button//span[text()='1 hour']");
	By fifteenbutton = By.xpath("//button//span[text()='15 mins']");
	By savechangesbutton = By.xpath("//button//span[text()='Save changes']");


	By DepositLimits = By.xpath("//a[contains(@heading,'Deposit Limits')]");
	By DepositLimitsTitle = By.xpath("//h4[contains(text(),'Deposit Limits')]");
	//
	By Takeabreak = By.xpath("//a[contains(@heading,'Take a break')]");
	By TakeabreakTitle = By.xpath("//h4[contains(text(),'Take a break')]");
	By TextBox = By.xpath("//p[contains(text(),'break period')]");
	By BreakCollapsibleBox = By.xpath("//div[contains(@class,'dialog-box-collapsible-title')]");
	By BreakTimeQuestion = By.xpath("//h5[contains(text(),'How long would you like to take a break?')]");
	By BreakButton = By.xpath("//button[contains(@type,'button')]/span[contains(text(),'1 Day')]");

	By BreakButtons(String buttons) {
		return By.xpath("//button[contains(@type,'button')]/span[contains(text(),'" + buttons + "')]");
	}
	By BreakPeriodText = By.xpath("//p[contains(text(),'If you would like to take a temporary break from gambling, you can choose a break period from 1 day up to 6 weeks.')]");
	By BreakRules = By.xpath("");

	By PasswordInput = By.xpath("//input[contains(@placeholder,'Password')]");
	By TakeABreak = By.xpath("//button/span[contains(.,'Take A Break')]");

	By ConfirmationPopup = By.xpath("//div[contains(@class,'take-a-break-confirmation')]");
	By TakeaBreakLogOutButton = By.xpath("//button/span[contains(text(),'Take a break & Log Out')]");
	By CancelButton = By.xpath("//button[contains(@type,'button')]/span[contains(.,'Cancel')]");

	By SelfExclude = By.xpath("//a[contains(@heading,'Self Exclude')]");
	By SelfExcludeTitle = By.xpath("//h4[contains(text(),'Self Exclude')]");
	By doyoufeelyouhaveaproblemtxt = By.xpath("//div[contains(text(),'Do you feel you have a problem with gambling?')]");

	By GamStop = By.xpath("//a[contains(@heading,'GamStop')]");
	By GamStopTitle = By.xpath("//h4[contains(text(),'GamStop')]");
	By GamLink = By.xpath("//a[contains(@title,'http://www.gamstop.co.uk/')]");
	By GamStoptext = By.xpath("");
	By successmessage; 
	By closebutton;

	By city = By.id("input-town-city");
	By county = By.id("input-county");
	By addressLine1 = By.id("input-address-line1");

	private String iFrameId = "payment-process";

	private int messageCount;


	public void verifyPlayableBalanceText() {
		logReporter.log("Verify playable balance text", objMobileActions.checkElementDisplayed(PlayableBalance));
	}

	public void verifyPlayableBalanceAmount() {
		logReporter.log("Verify playable balance amount", objMobileActions.checkElementDisplayed(PlayableBalanceAmount));
	}

	public void verifyPlayableBalanceIcon() {
		logReporter.log("Verify playable balance plus icon", objMobileActions.checkElementDisplayed(PlayableBalancePlusIcon));
	}

	public void clickOnPlayableBalanceIcon() {
		logReporter.log("Click on playable balance plus icon", objMobileActions.click(PlayableBalancePlusIcon));
	}

	public void clickOnPlayableBalanceMinusIcon() {
		logReporter.log("Click on playable balance minus icon", objMobileActions.click(PlayableBalanceMinusIcon));
	}

	public void verifyPlayableBalanceMinusIcon() {
		logReporter.log("Click on playable balance minus icon",
				objMobileActions.checkElementDisplayed(PlayableBalanceMinusIcon));
	}

	public void clickOnPlayableBalanceDetailedButton() {
		logReporter.log("click on detailed view button", objMobileActions.click(PlayableBalanceDetailedButton));
	}

	public void clickOnPlayableBalanceDepositButton() {
		logReporter.log("click on deposit button", objMobileActions.click(PlayableBalanceDepositButton));
	}

	public void clickOnBackbutton() {
		logReporter.log("click on back button", objMobileActions.click(backbutton));
	}

	public void verifyBalancePage() {
		logReporter.log("Verify playable balance text", objMobileActions.checkElementDisplayed(balancetext));
	}

	public void clickOnLiveChatLink() {
		logReporter.log("click on live chat link", objMobileActions.click(Livechat));
	}

	public void verifyMessageCountOnMessageText() {
		By MessagesCount = By.xpath("//div[contains(@class,'messages-count')]");
		logReporter.log("message count on message text", objMobileActions.checkElementDisplayed(MessagesCount));
	}

	public void clickUsernameOnMyAccountPage() {
		if (objMobileActions.click(Username))
			logReporter.log("click on username on my account page", true);
	}

	public void clickOnCloseButton() {
		logReporter.log("click on close button", objMobileActions.click(Closebutton));
	}

	public void verifyMyAccountButton() {
		logReporter.log("verify my account window gets closed", objMobileActions.checkElementDisplayed(MyAccountButton));
	}

	public void clickOnOption(String option) {
		By options = By.xpath("//span[contains(text(),'" + option + "')]");
		logReporter.log("click on ' "+option + " '", objMobileActions.click(options));
	}

	public void verifyPageTitle(String title) {

		if(title.contains(","))
		{
			String labl[] = title.split(",");
			for (String labl1 : labl)
			{
				By locator = By.xpath("//span[contains(text(),'" + labl1 + "')]");
				//System.out.println(" Label  " +labl1);
				logReporter.log(" Verify "+title+"  is displayed",
						objMobileActions.checkElementDisplayed(locator));
			}
		}
		else
		{
			By titles = By.xpath("//span[contains(text(),'" + title + "')]");
			logReporter.log("verify titles on page", objMobileActions.checkElementDisplayed(titles));
		}
	}

	public void clickOnCashierDepositBtn() {
		logReporter.log("click on deposit button", objMobileActions.click(DepositOnCashierPage));
	}

	public void verifyCashierDepositPage() {
		logReporter.log("verify deposit page", objMobileActions.checkElementDisplayed(TitleOnCashierDepositPage));
	}

	public void clickOnCashierWithdrawalBtn() {
		logReporter.log("click on deposit button", objMobileActions.click(WithdrawalOnCashierPage));
	}

	public void verifyCashierWithdrawalPage() {
		logReporter.log("verify deposit page", objMobileActions.checkElementDisplayed(TitleOnCashierWithdrawalPage));
	}

	public void clickOnCashierPendingWithdrawalsBtn() {
		logReporter.log("click on deposit button", objMobileActions.click(PendingWithdrawalsOnCashierPage));
	}

	public void verifyCashierPendingWithdrawalsPage() {
		logReporter.log("verify deposit page", objMobileActions.checkElementDisplayed(TitleOnCashierPendingWithdrawalsPage));
	}

	public void clickOnCashierTransactionHistoryBtn() {
		logReporter.log("click on deposit button", objMobileActions.click(TransactionHistoryOnCashierPage));
	}

	public void verifyCashierTransactionHistoryPage() {
		logReporter.log("verify deposit page", objMobileActions.checkElementDisplayed(TitleOnCashierTransactionHistoryPage));
	}

	public void clickOnCashierBalanceBtn() {
		logReporter.log("click on deposit button", objMobileActions.click(BalanceOnCashierPage));
	}

	public void verifyCashierBalancePage() {
		logReporter.log("verify deposit page", objMobileActions.checkElementDisplayed(TitleOnCashierBalancePage));
	}

	public void clickOnChangePassword() {
		logReporter.log("click on change password link", objMobileActions.click(ChangePassword));
	}

	public void verifyUpdateBtnIsDisabled() {
		logReporter.log("verify update button id disabled", objMobileActions.checkElementDisplayed(UpdateButton));
	}

	public void verifyUpdateBtnIsEnabled() {
		logReporter.log("verify update button id disabled", objMobileActions.checkElementDisplayed(UpdateButton));
	}

	public void enterValidCurrentPassword() {
		logReporter.log("enter current password", objMobileActions.setText(CurrentPassword, "Password123"));
	}

	public void enterValidNewPassword() {
		logReporter.log("enter new password", objMobileActions.setText(NewPassword, "Password123"));
	}

	public void clickOnBreakTimeButton() {
		logReporter.log("click on Break Time Button", objMobileActions.click(BreakButton));
	}

	public void verifyBreakPeriodText() {
		logReporter.log("verify Break Period Text", objMobileActions.checkElementDisplayed(BreakPeriodText));
	}

	public void clickOnUncollapseIcon() {
		//logReporter.log("click on uncollapse icon on take a break page", objMobileActions.click());
	}

	public void verifyButtonGetsHighlighted() {
		String value = "#f8ab20";
		logReporter.log("verify One Break Time Button gets highlighted",
				objMobileActions.checkCssValue(BreakButton, "color", value));
	}

	public void clickOnTakeABreakButton() {
		logReporter.log("click on take a Break Button", objMobileActions.click(TakeABreak));
	}

	public void verifyConfirmationBox() {
		logReporter.log("verify confirmation box", objMobileActions.checkElementDisplayed(ConfirmationPopup));
	}

	public void clickBreakOnPopup() {
		logReporter.log("click on break button on popup", objMobileActions.click(TakeaBreakLogOutButton));

	}
	public void clickLogoutOnPopup() {
		logReporter.log("click on logout button on popup", objMobileActions.click(CancelButton));

	}
	//reality check page

	public void verifyTextInTextboxOnRealityPage() {
		logReporter.log("verify text in textbox", objMobileActions.checkElementDisplayed(textboxtext));
	}
	public void clickOnUncollapsibleIconOnRealityPage() {
		logReporter.log("click on Uncollapsible Icon on reality page ", objMobileActions.click(uncollapsibleicon));		
	}
	public void verifyTextOnRealityPage() {
		logReporter.log("verify text on reality page", objMobileActions.checkElementDisplayed(textonpage));
	}
	public void selectOptionOnRealityPage() {
		logReporter.log("click on single option on reality page", objMobileActions.click(option));

	}
	public void clickOnSaveChangesOnRealityPage() {
		logReporter.log("click on save changes on reality page", objMobileActions.click(savechangesbutton));
	}
	public void verifySuccessMessageOnLoginPage() {
		logReporter.log("", objMobileActions.checkElementDisplayed(successmessage));
	}
	public void closeSuccessPopup() {
		logReporter.log("", objMobileActions.click(closebutton));
	}

	public void enterPasswdForCurrentPassword(String passwd) {
		logReporter.log("Enter password for current password", objMobileActions.setText(CurrentPassword, passwd));
	}

	public void verifyGreenColorBelowCurrentPassword() {
		logReporter.log("Check color of textbox bottom border > >",
				objMobileActions.checkCssValue(CurrentPassword, "border-bottom-color", greencolor));
	}
	public void verifyGreenColorBelowNewPassword() {
		logReporter.log("Check color of textbox bottom border > >",
				objMobileActions.checkCssValue(NewPassword, "border-bottom-color", greencolor));
	}

	public void enterPasswdForNewPassword(String passwd) {
		logReporter.log("Enter password for new password", objMobileActions.setText(NewPassword, passwd));
	}

	public void verifyPaypalWindow() {
		String windowTitle = "";
		logReporter.log("",objMobileActions.switchToWindowUsingTitle(windowTitle));
	}


	public void verifyAllFieldsOnPage(String texts) {
		switch (texts) {
		case "Username":
			logReporter.log("Check Username text ", objMobileActions.checkElementDisplayed(Username));
			break;
		case "Close button":
			logReporter.log("Check Close button", objMobileActions.checkElementDisplayed(Closebutton));
			break;
		case "Playable Balance":
			logReporter.log(" Check Payable Balance", objMobileActions.checkElementDisplayed(PlayableBalance));
			break;
		case "Cashier":
			logReporter.log("Check Cashier", objMobileActions.checkElementDisplayed(Cashier));
			break;
		case "Bonuses":
			logReporter.log(" Check Bonuses", objMobileActions.checkElementDisplayed(Bonuses));
			break;
		case "Messages":
			logReporter.log(" Check Messages", objMobileActions.checkElementDisplayed(Messages));
			break;
		case "Account details":
			logReporter.log(" Check Account details", objMobileActions.checkElementDisplayed(Accountdetails));
			break;
		case "Responsible gambling":
			logReporter.log(" Check Responsible gambling", objMobileActions.checkElementDisplayed(Responsiblegambling));
			break;
		case "Log out icon":
			logReporter.log(" Check Log out icon", objMobileActions.checkElementDisplayed(Logouticon));
			break;
		case "Log out link":
			logReporter.log(" Check Log out link", objMobileActions.checkElementDisplayed(Logoutlink));
			break;
		case "Recently played section":
			logReporter.log(" Check Recently played section", objMobileActions.checkElementDisplayed(Recentlyplayedsection));
			break;
		case "Email":
			logReporter.log(" Check Email", objMobileActions.checkElementDisplayed(Email));
			break;
		case "Phone number":
			logReporter.log(" Check Phone number", objMobileActions.checkElementDisplayed(Phonenumber));
			break;
		case "Live chat":
			logReporter.log(" Check Live chat", objMobileActions.checkElementDisplayed(Livechat));
			break;
		}
	}

	public void verifyAllFieldsOnPlayableBalanceSection(String texts) {
		switch (texts) {
		case "Playable Balance":
			logReporter.log(" Check Payable Balance", objMobileActions.checkElementDisplayed(PlayableBalance));
			break;
		case "playable balance amount":
			logReporter.log(" Check Payable Balance", objMobileActions.checkElementDisplayed(PlayableBalanceAmount));
			break;
		case "playable balance minus icon":
			logReporter.log(" Check playable balance amount",
					objMobileActions.checkElementDisplayed(PlayableBalanceMinusIcon));
			break;
		case "cash text":
			logReporter.log(" Check cash text", objMobileActions.checkElementDisplayed(PlayableBalanceCashText));
			break;
		case "cash amount":
			logReporter.log(" Check cash amount", objMobileActions.checkElementDisplayed(PlayableBalanceCashAmount));
			break;
		case "bonuses text":
			logReporter.log(" Check bonuses text", objMobileActions.checkElementDisplayed(PlayableBalanceBonusesText));
			break;
		case "bonuses amount":
			logReporter.log(" Check bonuses amount", objMobileActions.checkElementDisplayed(PlayableBalanceBonusesAmount));
			break;
		case "detailed view button":
			logReporter.log(" Check etailed view button",
					objMobileActions.checkElementDisplayed(PlayableBalanceDetailedButton));
			break;
		case "deposit button":
			logReporter.log(" Check deposit button", objMobileActions.checkElementDisplayed(PlayableBalanceDepositButton));
			break;
		}
	}

	public void verifyOptionsOnCashierPage(String options) {
		switch (options) {
		case "Close button":
			logReporter.log(" Check Close button", objMobileActions.checkElementDisplayed(Closebutton));
			break;
		case "Back button":
			logReporter.log(" Check Back button", objMobileActions.checkElementDisplayed(backbutton));
			break;
		case "Deposit":
			logReporter.log(" Check Deposit button", objMobileActions.checkElementDisplayed(DepositOnCashierPage));
			break;
		case "Withdrawal":
			logReporter.log(" Check Withdrawal button", objMobileActions.checkElementDisplayed(WithdrawalOnCashierPage));
			break;
		case "Pending Withdrawals":
			logReporter.log(" Check Pending Withdrawals button",
					objMobileActions.checkElementDisplayed(PendingWithdrawalsOnCashierPage));
			break;
		case "Transaction History":
			logReporter.log(" Check Transaction History button",
					objMobileActions.checkElementDisplayed(TransactionHistoryOnCashierPage));
			break;
		case "Balance":
			logReporter.log(" Check Balance button", objMobileActions.checkElementDisplayed(BalanceOnCashierPage));
			break;

		case "Email":
			logReporter.log(" Check Email", objMobileActions.checkElementDisplayed(Email));
			break;
		case "Phone number":
			logReporter.log(" Check Phone number", objMobileActions.checkElementDisplayed(Phonenumber));
			break;
		case "Live chat":
			logReporter.log(" Check Live chat", objMobileActions.checkElementDisplayed(Livechat));
			break;
		}
	}

	public void clickFieldsOnPlayableBalanceSection(String options) {
		switch (options) {
		case "cash text":
			if (!objMobileActions.click(PlayableBalanceCashText))
				logReporter.log(" Click cash text", true);
			break;
		case "cash amount":
			if (!objMobileActions.click(PlayableBalanceCashAmount))
				logReporter.log(" Click cash amount", true);
			break;
		case "bonuses text":
			if (!objMobileActions.click(PlayableBalanceBonusesText))
				logReporter.log(" Click bonuses tex", true);
			break;
		case "bonuses amount":
			if (!objMobileActions.click(PlayableBalanceBonusesAmount))
				logReporter.log(" Click bonuses amount", true);
			break;

		}

	}

	public void verifyOptionsOnResponsibleGamblingPage(String options) {
		switch (options) {
		case "Close button":
			logReporter.log(" Check Close button", objMobileActions.checkElementDisplayed(Closebutton));
			break;
		case "Back button":
			logReporter.log(" Check Back button", objMobileActions.checkElementDisplayed(backbutton));
			break;
		case "Responsible Gambling Title":
			logReporter.log(" Check Responsible Gambling Title",
					objMobileActions.checkElementDisplayed(ResponsiblegamblingPageTitle));
			break;
		case "Reality Check":
			logReporter.log(" Check Reality Check button", objMobileActions.checkElementDisplayed(RealityCheck));
			break;
		case "Deposit Limits":
			logReporter.log(" Check Deposit Limits button", objMobileActions.checkElementDisplayed(DepositLimits));
			break;
		case "Take a break":
			logReporter.log(" Check Take a break button", objMobileActions.checkElementDisplayed(Takeabreak));
			break;
		case "Self Exclude":
			logReporter.log(" Check Self Exclude button", objMobileActions.checkElementDisplayed(SelfExclude));
			break;
		case "Gamstop":
			logReporter.log(" Check Gamstop button", objMobileActions.checkElementDisplayed(GamStop));
			break;
		case "Email":
			logReporter.log(" Check Email", objMobileActions.checkElementDisplayed(Email));
			break;
		case "Phone number":
			logReporter.log(" Check Phone number", objMobileActions.checkElementDisplayed(Phonenumber));
			break;
		case "Live chat":
			logReporter.log(" Check Live chat", objMobileActions.checkElementDisplayed(Livechat));
			break;
		}

	}

	public void verifyOptionsOnDepositPage(String options) {
		switch (options) {
		case "Close button":
			logReporter.log(" Check Close button", objMobileActions.checkElementDisplayed(Closebutton));
			break;
		case "Back button":
			logReporter.log(" Check Back button", objMobileActions.checkElementDisplayed(backbutton));
			break;
		case "Deposit Title":
			logReporter.log(" Check Deposit Title", objMobileActions.checkElementDisplayed(TitleOnCashierDepositPage));
			break;
		case "Live help link":
			logReporter.log(" Check Live help link", objMobileActions.checkElementDisplayed(LiveHelpLink));
			break;
		case "Deposit limit link":
			logReporter.log(" Check Deposit limit link", objMobileActions.checkElementDisplayed(DepositLimitLink));
			break;
		case "Safecharge frame":
			logReporter.log(" Check Take a break button", objMobileActions.checkElementDisplayed(SafechargeFrame));
			break;

		}

	}

	public void verifyOptionsOnAccountDetailsPage(String options) {
		switch (options) {
		case "Close button":
			logReporter.log(" Check Close button", objMobileActions.checkElementDisplayed(Closebutton));
			break;
		case "Back button":
			logReporter.log(" Check Back button", objMobileActions.checkElementDisplayed(backbutton));
			break;
		case "Account Details Title":
			logReporter.log(" Check account details Title", objMobileActions.checkElementDisplayed(AccountdetailsPageTitle));
			break;
		case "Account Details window":
			logReporter.log(" Check account details window", objMobileActions.checkElementDisplayed(DetailsBox));
			break;
		case "Edit button":
			logReporter.log(" Check Edit button", objMobileActions.checkElementDisplayed(EditButton));
			break;
		case "Change Password":
			logReporter.log(" Check change password", objMobileActions.checkElementDisplayed(ChangePassword));
			break;
		case "Marketing Preference":
			logReporter.log(" Check marketing preference button",
					objMobileActions.checkElementDisplayed(MarketinPreferences));
			break;
		case "Support Email":
			logReporter.log(" Check Email", objMobileActions.checkElementDisplayed(Email));
			break;
		case "Phone number":
			logReporter.log(" Check Phone number", objMobileActions.checkElementDisplayed(Phonenumber));
			break;
		case "Live chat":
			logReporter.log(" Check Live chat", objMobileActions.checkElementDisplayed(Livechat));
			break;
		case "Last Login":
			this.verifyFieldsOnAccountDetailsScreen("Last Login");
			break;
		case "Name":
			this.verifyFieldsOnAccountDetailsScreen("Name");
			break;
		case "D.O.B":
			this.verifyFieldsOnAccountDetailsScreen("D.O.B");
			break;
		case "Email":
			this.verifyFieldsOnAccountDetailsScreen("Email");
			break;
		case "Phone":
			this.verifyFieldsOnAccountDetailsScreen("Phone");
			break;
		case "Postcode":
			this.verifyFieldsOnAccountDetailsScreen("Postcode");
			break;
		case "Add Virtual Card":
			this.verifyButtonOnScreen("Add Virtual Card");
			break;
		}
	}

	public void verifyFieldsOnAccountDetailsScreen(String fieldNm)
	{
		By locator = By.xpath("//div[@class='content-box-details']//div//p[contains(.,'"+fieldNm+"')]");
		logReporter.log(" verify ' "+fieldNm+ " ' is displayed on screen", objMobileActions.checkElementDisplayed(locator));
	}

	public void verifyOptionsOnTakeABreakPage(String options) {
		switch (options) {
		case "Close button":
			logReporter.log(" Check Close button", objMobileActions.checkElementDisplayed(Closebutton));
			break;
		case "Back button":
			logReporter.log(" Check Back button", objMobileActions.checkElementDisplayed(backbutton));
			break;
		case "Take a break Title":
			logReporter.log(" Check Take a break Title", objMobileActions.checkElementDisplayed(TakeabreakTitle));
			break;
		case "Live help link":
			logReporter.log(" Check Live help link", objMobileActions.checkElementDisplayed(LiveHelpLink));
			break;
		case "Text box":
			logReporter.log(" Check Text box", objMobileActions.checkElementDisplayed(TextBox));
			break;
		case "Break collapsible box":
			logReporter.log(" Check Break collapsible box", objMobileActions.checkElementDisplayed(BreakCollapsibleBox));
			break;
		case "Break time question":
			logReporter.log(" Check Break time question", objMobileActions.checkElementDisplayed(BreakTimeQuestion));
			break;
		case "Break times buttons":
			logReporter.log(" Check Break times buttons", objMobileActions.checkElementDisplayed(BreakButton));
			break;

		}

	}

	public void verifyOptions(String options) {
		switch (options) {
		case "Locked until date":
			logReporter.log(" Check Locked until date",objMobileActions.checkElementDisplayed(BreakPeriodText));
			break;
		case "Break rules":
			logReporter.log(" Check Break rules", objMobileActions.checkElementDisplayed(BreakRules));
			break;
		case "Password field":
			logReporter.log(" Check Password field", objMobileActions.checkElementDisplayed(PasswordInput));
			break;
		case "Take a break button":
			logReporter.log(" Check Take a break button",objMobileActions.checkElementDisplayed(TakeABreak));
			break;

		}
	}

	public void verifyOptionsOnRealityCheckPage(String options) {
		switch (options) {
		case "Close button":
			logReporter.log(" Check Close button", objMobileActions.checkElementDisplayed(Closebutton));
			break;
		case "Back button":
			logReporter.log(" Check Back button", objMobileActions.checkElementDisplayed(backbutton));
			break;
		case "Reality Check Title":
			logReporter.log(" Check Take a break Title", objMobileActions.checkElementDisplayed(RealityCheckTitle));
			break;
		case "Live help link":
			logReporter.log(" Check Live help link", objMobileActions.checkElementDisplayed(LiveHelpLink));
			break;
		case "Tell me more collapsible icon":
			logReporter.log(" Check Tell me more collapsible icon",objMobileActions.checkElementDisplayed(tellmemoreicon));
			break;
		case "Set your reminder":
			logReporter.log(" Check Set your reminder",objMobileActions.checkElementDisplayed(setyourreminder));
			break;
		case "1 mins button":
			logReporter.log(" Check 15 min button",objMobileActions.checkElementDisplayed(fifteenminButton));
			break;
		case "15 mins button":
			logReporter.log(" Check 15 min button",objMobileActions.checkElementDisplayed(fifteenbutton));
			break;
		case "30 mins button":
			logReporter.log(" Check 30 min button", objMobileActions.checkElementDisplayed(firstbutton));
			break;
		case "1 hour button":
			logReporter.log(" Check 1 hour button", objMobileActions.checkElementDisplayed(secondbutton));
			break;
		case "2 hour button":
		//	logReporter.log(" Check 2 hour button", objMobileActions.checkElementDisplayed(thirdbutton));
			break;
		case "Save changes button":
			logReporter.log(" Check Save changes button",objMobileActions.checkElementDisplayed(savechangesbutton));
			break;
		case "Tell me more":
			logReporter.log(" Check 'tell me more text'", objMobileActions.checkElementDisplayed(textboxtext));
			break;
		}

	}


	public void verifyOptionsOnGamstopPage(String options) {
		switch (options) {
		case "Close button":
			logReporter.log(" Check Close button", objMobileActions.checkElementDisplayed(Closebutton));
			break;
		case "Back button":
			logReporter.log(" Check Back button", objMobileActions.checkElementDisplayed(backbutton));
			break;
		case "Gamstop Title":
			logReporter.log(" Check Gamstop Title", objMobileActions.checkElementDisplayed(GamStopTitle));
			break;
		case "Gamstop Text":
			logReporter.log(" Check Gamstop Text", objMobileActions.checkElementDisplayed(GamStoptext));
			break;
		}
	}


	public void verifyOptionsOnBonuses(String options) {
		switch (options) {
		case "Close button":
			logReporter.log(" Check Close button", objMobileActions.checkElementDisplayed(Closebutton));
			break;
		case "Back button":
			logReporter.log(" Check Back button", objMobileActions.checkElementDisplayed(backbutton));
			break;
		case "Bonuses Page Title":
			logReporter.log(" Check Enter Bonus Code Title", objMobileActions.checkElementDisplayed(EnterBonusCode));
			break;
		case "Live help link":
			logReporter.log(" Check Live help link", objMobileActions.checkElementDisplayed(GamStoptext));
			break;
		case "Enter code here text":
			logReporter.log(" Check Enter code here text", objMobileActions.checkElementDisplayed(EnterBonusCodeText));
			break;
		case "Enter code field":
			logReporter.log(" Enter code field", objMobileActions.checkElementDisplayed(EnterBonusCodeField));
			break;
		case "Clear button":
			logReporter.log(" Check Clear button", objMobileActions.checkElementDisplayed(btnClear));
			break;
		case "Submit button":
			logReporter.log(" Check Gamstop Text", objMobileActions.checkElementDisplayed(SubmitButton));
			break;

		}
	}

	public void verifyBalanceSectionDisplayed() {
		By balanceSection = By.xpath("//apollo-balance-block");
		objMobileActions.checkElementEnabled(balanceSection);
		logReporter.log("verify 'balance section' > >",
				objMobileActions.checkElementDisplayed(balanceSection));
	}

	public void clickOnMyAccount() {
		By myAccount = By.xpath("//i[contains(@class,'my-account')]");
		//if (objMobileActions.checkElementDisplayedWithMidWait(myAccount))
			logReporter.log("Click back to top button> >", objMobileActions.click(myAccount));
	}

	public void clickMenuOption(String strMenuName) {
		By locator = By.xpath("//a[contains(@heading,'" + strMenuName + "')]");
		
		if (objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("Click "+strMenuName, objMobileActions.click(locator));
	}

	public void verifyMenuHeaderDisplayed(String strMenuName) {
		By locator = By.xpath("//h4[text()='" + strMenuName + "']");
	
		logReporter.log("Check menuname in header : " + strMenuName + "> >",
				objMobileActions.checkElementDisplayedWithMidWait(locator));
	}

	public void enterWithdrawalAmt(String strAmt) {
		By withdrawAmt = By.id("input-amount");
		logReporter.log("enter withdrawal amount  : > >", objMobileActions.setTextWithClear(withdrawAmt, strAmt));
	}

	public void enterWithdrawalPassword(String passwd) {
		By locator = By.id("input-password");
		logReporter.log("enter withdrawal amount  : > >", objMobileActions.setTextWithClear(locator, passwd));
	}

	public void clickSpan(String strName) {

		By locator = By.xpath("//span[text()='" + strName + "']");
		//if (objMobileActions.checkElementDisplayed(locator))
		logReporter.log("click span '" + strName, objMobileActions.clickUsingJS(locator));
		wait.sleep(10);
	}

	public void verifyLiveHelpDisplayed() {
		By liveHelp = By.xpath("//a[contains(text(),'Live help')]");
		logReporter.log("Check Live help : > >", objMobileActions.checkElementDisplayedWithMidWait(liveHelp));
	}

	public void transactionFilterBoxDisplayed() {
		By locator = By.xpath("//div[contains(@class,'ip-select')]");
		logReporter.log("Transaction filter box displayed >>",
				objMobileActions.checkElementDisplayedWithMidWait(locator));
	}

	public void datesPopulatedWithTodayDate() {
		By locator = By.xpath("(//div[contains(@class,'date-picker')]//input)[2]");
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");  
		LocalDateTime now = LocalDateTime.now();  
		System.out.println(dtf.format(now));  
		String todayDate = dtf.format(now);
		String actualDate = objMobileActions.getAttribute(locator, "value");
		if(actualDate.equals(todayDate))
			logReporter.log("Dates populated with today's date", true);
		else
			logReporter.log("Dates populated with today's date", false);
	}

	public void transactionHistoryBoxDisplayed() {
		By locator = By.xpath("//table[@class='my-account-table']");
		logReporter.log("Transaction History box displayed >>",
				objMobileActions.checkElementDisplayedWithMidWait(locator));
	}

	public void selectFilterActivity(String strFilterActivity) {
		wait.sleep(15);
		//h5[text()='Filter by activity']//following::select//option[text()='Net deposits']
		//By locator = By.xpath("//option[text()='"+strFilterActivity+"']");
		By locator1 = By.xpath("//h5[text()='Filter by activity']//following::select//option[text()='"+strFilterActivity+"']");
		By locator = By.xpath("//h5[text()='Filter by activity']//following::select");
		logReporter.log("click filter activity  : > >", objMobileActions.click(locator));
		wait.sleep(configuration.getConfigIntegerValue("midwait"));
		logReporter.log("click filter activity  : > >", objMobileActions.click(locator1));
	}

	public void verifyTransactionHistoryRecords(String activity) {
		boolean flag=true;
		List  rows = objMobileActions.processMobileElements(By.xpath("//table[@class='my-account-table']/tbody/tr")); 
		System.out.println("No of rows are : " + rows.size());
		int totalNoOfRows = rows.size();
		for(int i=1;i<totalNoOfRows;i++) {
			String rowText= objMobileActions.processMobileElement(By.xpath("//table[@class='my-account-table']/tbody/tr["+i+"]/td[2]")).getText();
			System.out.println("row text = " + rowText);
			if(rowText.contains(activity)==false) {
				flag=false;
				break;
			}

		}

		if(flag)
			logReporter.log("Matching records found", true);
		else
			logReporter.log("Matching records found", false);
	}


	public void optionDisplayed(String strOption) {
		By locator = By.xpath("//option[text()='"+strOption+"']");
		logReporter.log("option displayed  : > >", objMobileActions.checkElementDisplayedWithMidWait(locator));
	}

	public void selectSortBy(String strSortBy) {
		//By locator = By.xpath("//option[text()='"+strSortBy+"']");
		wait.sleep(configuration.getConfigIntegerValue("maxwait"));
		By locator = By.xpath("//h5[text()='Sort By']//following::select//option[text()='"+strSortBy+"']");
		logReporter.log("click sort by  : > >", objMobileActions.click(locator));
	}

	public void collapseRecord() {
		By locator = By.xpath("(//table[@class='my-account-table']//i[contains(@class,'plus')])[1]");
		logReporter.log("Collapse first record", objMobileActions.click(locator));
	}

	public void verifyCollapsedRecord() {
		By locator1 = By.xpath("//td[contains(text(),'Reference:')]");
		logReporter.log("Reference displayed  : > >", objMobileActions.checkElementDisplayedWithMidWait(locator1));
		By locator2 = By.xpath("//td[contains(text(),'Wallet:')]");
		logReporter.log("Wallet displayed  : > >", objMobileActions.checkElementDisplayedWithMidWait(locator2));
		By locator3 = By.xpath("//td[contains(text(),'Balance')]");
		logReporter.log("Balance displayed  : > >", objMobileActions.checkElementDisplayedWithMidWait(locator3));
	}

	public void verifyDeleteIconDisplayed() {
		By locator = By.xpath("//i[contains(@class,'delete')]");
		logReporter.log("delete icon displayed  : > >", objMobileActions.checkElementDisplayedWithMidWait(locator));
	}

	public void verifyMessagesTableDisplayed() {
		By locator = By.xpath("//section[contains(@class,'my-account-messages')]");
		logReporter.log("Messages table displayed  : > >", objMobileActions.checkElementDisplayedWithMidWait(locator));
	}

	public void clickFirstMessage() {
		By locator = By.xpath("(//section[contains(@class,'my-account-messages')]//a[contains(@class,'messages')])[1]");
		logReporter.log("click first message  : > >", objMobileActions.click(locator));
	}
	
	public void verifyMessageInDetail() {
		verifyPaginationLinkOnMessageBodyScreen();
		verifyMessageContentDetailsScreen();	
	}
	public void clickMsgDeleteButton() {
		By locator = By.xpath("//i[contains(@class,'icon-delete active')]");
		logReporter.log("click message delete button  : > >", objMobileActions.click(locator));
	}

	public void verifyLinkOfSubMenuPage(String strLink) {
		By locator = By.xpath("//a[contains(@href,'" + strLink + "')]");
		logReporter.log("Link : " + strLink, objMobileActions.checkElementDisplayedWithMidWait(locator));
	}

	public void verifyH5TextDisplayed(String strText) {
		By locator = By.xpath("//h5[contains(text(),'" + strText + "')]");
		logReporter.log("Text displayed >>", objMobileActions.checkElementDisplayedWithMidWait(locator));
	}

	public void verifyMsgFromParaTag(String strErrorMsg) {
		By locator = By.xpath("//p[contains(text(),'" + strErrorMsg + "')]");
		logReporter.log("Err msg " + strErrorMsg + " displayed >",
				objMobileActions.checkElementDisplayedWithMidWait(locator));
	}

	public void verifySpanDisplayed(String strName) {
		By locator = By.xpath("//span[text()='" + strName + "']");
		logReporter.log("span '" + strName + "' displayed >>",
				objMobileActions.checkElementDisplayedWithMidWait(locator));
	}

	public void switchToParent() {
		objMobileActions.switchToParentWindow();
	}

	public int getMessageCount() {
		return messageCount;
	}

	public void setMessageCount() {
		By locator = By.xpath("//div[contains(@class,'slideout-overlay-heading')]/h4");
		String headerText = objMobileActions.getText(locator);
		int start = headerText.indexOf("(");
		messageCount = Integer.parseInt(headerText.substring(start + 1, headerText.length()-1));
	}

	public void verifyMsgCountReduced() {
		By locator = By.xpath("//div[contains(@class,'slideout-overlay-heading')]/h4");
		String headerText = objMobileActions.getText(locator);
		int start = headerText.indexOf("(");
		int actualMsgCount = Integer.parseInt(headerText.substring(start + 1, headerText.length()-1));
		if(actualMsgCount < messageCount)
			logReporter.log("Message count is reduced >",true);
		else
			logReporter.log("Message count is reduced >",false);
	}

	public void verifySystemdisplaysUpdatedMessageCountorNot(String oldcnt,String newCnt)
	{
		logReporter.log("Verify message count is displayed correctly ", 
				(Integer.valueOf(newCnt)>Integer.valueOf(oldcnt)));
	}

	public void verifyMsgCountIncreased() {
		objMobileActions.pageRefresh();
		wait.sleep(59);
		By locator = By.xpath("//div[contains(@class,'slideout-overlay-heading')]/h4");
		String headerText = objMobileActions.getText(locator);
		int start = headerText.indexOf("(");
		int actualMsgCount = Integer.parseInt(headerText.substring(start + 1, headerText.length()-1));
		this.verifySystemdisplaysUpdatedMessageCountorNot(String.valueOf(messageCount), String.valueOf(actualMsgCount));
	}

	public void verifyMenuOptions(String strMenuName) {
		By locator = By.xpath("//a[contains(@heading,'" + strMenuName + "')]");
		logReporter.log("Check menu : " + strMenuName + "> >",
				objMobileActions.checkElementDisplayedWithMidWait(locator));
	}

	public void enterPostalCode(String strPostalCode) {
		By locator = By.xpath("//input[@placeholder='Postcode']");
		logReporter.log("clear postal code >" + strPostalCode + ">", objMobileActions.setText(locator, ""));
		logReporter.log("enter postal code >" + strPostalCode + ">", objMobileActions.setText(locator, strPostalCode));
		if (strPostalCode.equals("") != true && strPostalCode.length() <= 8) {
			int randomSubscript = Integer.parseInt(RandomStringUtils.random(1, false, true));
			By searchDropDown;
			if (randomSubscript <= 7 && randomSubscript != 0)
				searchDropDown = By.xpath("//ul[contains(@class,'postcode')]/li[" + randomSubscript + "]");
			else
				searchDropDown = By.xpath("//ul[contains(@class,'postcode')]/li[1]");

			// if (objMobileActions.checkElementDisplayedWithMidWait(searchDropDown))
			if (objMobileActions.checkElementExists(searchDropDown))
				objMobileActions.click(searchDropDown);
		}
	}

	public void enterAddressLine1(String str) {
		logReporter.log("enter address line1", objMobileActions.setTextWithClear(addressLine1, str));
	}

	public void enterCity(String str) {
		logReporter.log("enter city", objMobileActions.setTextWithClear(city, str));
	}

	public void enterCounty(String str) {
		logReporter.log("enter county", objMobileActions.setTextWithClear(county, str));
	}

	public void reminderButtonHighlighted(String strBtn) {
		wait.sleep(10);
		By locator = By.xpath("//span[contains(text(),'" + strBtn + "')]/ancestor::button");
		String strValue = objMobileActions.getAttribute(locator, "class");
		if (strValue.contains("pink"))
			logReporter.log(strBtn + " button is hightlighted  : > >", true);
		else
			logReporter.log(strBtn + " button is hightlighted  : > >", false);

	}

	public void enterLimit(String strLimit) {
		By locator = By.id("deposit-limits");
		logReporter.log("enter limit", objMobileActions.setText(locator, strLimit));

	}

	public void clickDepositLimitButton(String strName) {
		By locator = By.xpath("(//span[text()='"+strName+"'])[2]");
		logReporter.log("click limit button", objMobileActions.click(locator));
	}

	public void clickButtonHavingGivenText(String strName) {
		By locator = By.xpath("//button[text()='"+strName+"']");
		logReporter.log("click limit button", objMobileActions.click(locator));
	}

	public void reduceDepositLimitByOne() {
		By locator = By.id("deposit-limits");
		String actualLimit = objMobileActions.getAttribute(locator, "value");
		int decreasedLimit = Integer.parseInt(actualLimit) -1;
		clickButtonHavingGivenText("Reset limit");
		enterLimit(String.valueOf(decreasedLimit));
	}

	public void increaseDepositLimitByOne() {
		By locator = By.id("deposit-limits");
		String actualLimit = objMobileActions.getAttribute(locator, "value");
		int increasedLimit = Integer.parseInt(actualLimit) +1;
		clickButtonHavingGivenText("Reset limit");
		enterLimit(String.valueOf(increasedLimit));
	}

	public void enterPasswordForTakeBreak(String strPassword) {
		By locator = By.xpath("//input[@placeholder='Password']");
		logReporter.log("enter take break password  : > >", objMobileActions.setTextWithClear(locator, strPassword));
	}

	public void clickLink(String linkName) {
		By locator = By.xpath("//a[contains(@href,'" + linkName + "')]");
		logReporter.log("Click Link : " + linkName, objMobileActions.click(locator));
	}

	public void enterRandomEmailIdWithGivenSuffix(String suffixName) {
		By editBoxEmail = By.xpath("//input[@placeholder='Email address']");
		// to form random email text
		Date dNow = new Date();
		SimpleDateFormat ft = new SimpleDateFormat("MMddhhmm");
		String datetime = ft.format(dNow);
		String strEmail = "k" + datetime + suffixName;
		logReporter.log("enter 'Random Email Id' > >" + strEmail, objMobileActions.setText(editBoxEmail, strEmail));
	}

	public void clickCloseButtonFromLiveChat() {
		By locator = By.xpath("//a[contains(@class,'icon-cross')]");
		if(objMobileActions.checkElementDisplayed(locator))
			logReporter.log("Click close of live chat : ", objMobileActions.click(locator));
	}

	public void clickOnPlusSignOfTakeABreakPage(String txt) {
		By locator = By.xpath("//*[contains(.,'"+txt+"')]/i[contains(@class,'icon-expand')]");
		if (objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("Click plus sign of take break> >", objMobileActions.click(locator));
	}

	public void verifyDivTextDisplayed(String strText) {
		By locator = By.xpath("//div[contains(text(),'" + strText + "')]");
		logReporter.log("Text displayed >>", objMobileActions.checkElementDisplayedWithMidWait(locator));
	}

	public void enterPasswordOnTakeABreakPage() {
		By locator = By.xpath("//input[@placeholder='Password']");
		logReporter.log("Enter password >>", objMobileActions.setText(locator, "Test@1234"));
	}

	//Bonuses

	public void verifyMyAccountLinks(String link)
	{
		By locator = By.xpath("//div[@class='my-account-menu']//a[contains(@heading,'"+link+"')]");
		logReporter.log("Verify '"+ link+" ' is displayed on Myaccount menu", objMobileActions.checkElementDisplayed(locator));
	}

	public void selectMyAccountLinks(String link)
	{
		By locator = By.xpath("//div[@class='my-account-menu']//a[contains(@heading,'\"+link+\"')]");
		logReporter.log("click on '"+ link, objMobileActions.click(locator));
	}

	public void setPromoCode(String promocode)
	{
		logReporter.log(" Set Promocode/Bonus code ", promocode,
				objMobileActions.setTextWithClear(EnterBonusCodeField, promocode));
	}
	public void clearTheBonusCode()
	{
		By clearBtn = By.xpath("//input[@id='bonus-code']/following-sibling::button[contains(text(),'Clear')]");
		logReporter.log(" Click on Clear button displayed",
				objMobileActions.click(clearBtn));
		wait.sleep(configuration.getConfigIntegerValue("midwait"));
		boolean flag = objMobileActions.checkElementEnabled(clearBtn);
		System.out.println("*********************boolean flag :::  "+flag);
		if(flag==false)
		{logReporter.log("Verify Clear button is displayed as disabled",
				true);}
		else
		{logReporter.log("Verify Clear button is displayed as disabled",
				false);}	
	}
	public void verifyBonusCodeInputFieldIsBlank()
	{
		String value = objMobileActions.getAttribute(EnterBonusCodeField, "value");
				System.out.println("****************** verifyBonusCodeInputFieldIsBlank ::: "+value);
	}
	
	public void clickSubmitcode()
	{
		logReporter.log(" Click on Submit ", 
				objMobileActions.click(SubmitButton));}

	public String getBonusName()
	{
		By bonusname = By.xpath("//p[@class='heading' and contains(text(),'Enter bonus code')]/following::p[@class='heading']");

		return objMobileActions.getText(bonusname);
	}

	public void verifyTermsnConditionsLinkDisplayed()
	{
		By TermsnConditionsLink = By.xpath("//a[contains(text(),'Terms & Conditions')]");
		logReporter.log("Verify 'Terms n Conditions' Link displayed", 
				objMobileActions.checkElementDisplayed(TermsnConditionsLink));
	}

	public void verifyAcceptCheckboxDisplayed()
	{
		By AcceptCheckbox=By.xpath("//input[@id='id-agreed']");
		logReporter.log("Verify 'Accept Checkbox' displayed", 
				objMobileActions.checkElementDisplayed(AcceptCheckbox));
	}

	public void verifyAcceptTnCsLabelDisplayed()
	{
		By AcceptTnCsLabel = By.xpath("//label[contains(text(),'I accept the Terms & Conditions for this promotion')]");
		logReporter.log("Verify 'I accept the Terms and Conditions for this promotion' label displayed", 
				objMobileActions.checkElementDisplayed(AcceptTnCsLabel));
	}

	public void SelectAcceptCheckboxDisplayed()
	{
		By AcceptCheckbox=By.xpath("//input[@id='id-agreed']");
		logReporter.log(" Select 'Accept Checkbox' displayed", 
				objMobileActions.click(AcceptCheckbox));
	}

	public void verifyClaimPromotionBTN()
	{
		By nextbtn = By.xpath("//button//span[contains(.,'Next')]");
		logReporter.log(" Verify 'Next' displayed", 
				objMobileActions.checkElementDisplayed(nextbtn));
	}
	public void clickClaimPromotionBTN()
	{
		By nextbtn = By.xpath("//button//span[contains(.,'Next')]");
		logReporter.log(" Click on 'Claim Promotion BTN' displayed", 
				objMobileActions.click(nextbtn));
	}
	public void verifyCancelLinkDisplayed()
	{
		By cancelLink = By.xpath("//a[contains(text(),'Cancel')]");
		logReporter.log("Verify 'Cancel Link' displayed", 
				objMobileActions.checkElementDisplayed(cancelLink));
	}
	public void verifySuccessHeaderDisplayed()
	{
		By SuccessHeader = By.xpath("//h4[contains(text(),'Success')]//following-sibling::p[contains(.,'You have successfully opted in to')]");
		logReporter.log("Verify 'Success Header' displayed", 
				objMobileActions.checkElementDisplayed(SuccessHeader));
	}

	public void verifyClaimedBonusNameDisplayed(String Bonusname)
	{
		By bonusname= By.xpath("//h4[contains(text(),'Success')]//following-sibling::p[contains(.,'You have successfully opted in to')]");
		String actualBonusName = objMobileActions.getText(bonusname);
		System.out.println("***************** actualBonusName ::: "+actualBonusName);
		System.out.println("***************** Bonusname ::: "+Bonusname);

		if(actualBonusName.contains(Bonusname))
		{
			System.out.println("Claimed bonus name is displayed correctly");
			logReporter.log("Claimed bonus name is displayed correctly", true);
		}
		else 
		{System.out.println("there is mismatch in Claimed bonus name displayed ");
		logReporter.log("there is mismatch in Claimed bonus name displayed", false);}
	}

	public void verifyCloseBTNDisplayed()
	{
		By CloseBTN= By.xpath("//button//span[contains(.,'Close')]");
		logReporter.log("Verify 'Close BTN' displayed", 
				objMobileActions.checkElementDisplayed(CloseBTN));

	}
	public void ClickCloseBTNDisplayed()
	{
		By CloseBTN= By.xpath("//button//span[contains(.,'Close')]");
		logReporter.log(" Click on 'Close BTN' displayed", 
				objMobileActions.click(CloseBTN));
	}
	public void verifyActivePromotionsBTNDisplayed()
	{
		By ActivePromotionsBTN= By.xpath("//button//span[contains(.,'Active promotions')]");
		logReporter.log("Verify 'Active Promotions BTN' displayed", 
				objMobileActions.checkElementDisplayed(ActivePromotionsBTN));
	}
	public void clickActivePromotionsBTNDisplayed()
	{
		By ActivePromotionsBTN= By.xpath("//button//span[contains(.,'Active promotions')]");
		logReporter.log(" Click on 'Active Promotions BTN' displayed", 
				objMobileActions.click(ActivePromotionsBTN));
	}

	public boolean verifyExpandButtonDisplayed(){
		//By expandBTN = By.xpath("//a[contains(@class,'bonus-item-button')]/i[@class=' icon-plus']");
		By expandBTN = By.xpath("//a[contains(@class,'bonus-item-button')]/i[@class=' icon-expand']");
		return objMobileActions.checkElementDisplayed(expandBTN);
	}
	public void clickExpandButton()
	{
		By expandBTN = By.xpath("//a[contains(@class,'bonus-item-button')]");
		logReporter.log(" click 'expand + BTN' ", 
				objMobileActions.clickUsingJS(expandBTN));
	}
	public void VerifyCollapseButton()
	{
		//By collapseBTN = By.xpath("//a[contains(@class,'bonus-item-button')]/i[@class=' icon-minus']");
		By collapseBTN = By.xpath("//a[contains(@class,'bonus-item-button')]/i[@class=' icon-collapse']");
		logReporter.log(" Verify 'collapse - BTN' displayed", 
				objMobileActions.checkElementDisplayed(collapseBTN));
	}

	public void clickCollapseButton()
	{
		By collapseBTN = By.xpath("//a[contains(@class,'bonus-item-button')]/i[@class=' icon-collapse']");
		logReporter.log(" click 'collapse - BTN' displayed", 
				objMobileActions.click(collapseBTN));
	}

	public void verifyNoitemsfoundMessageDisplyedOrNot()
	{
		By noTransactionFoundMSG = By.xpath("//p[contains(text(),'No items found')]");
		logReporter.log(" 'No items found' message is displayed on Bonuses screen ", 
				objMobileActions.checkElementDisplayed(noTransactionFoundMSG));
	}
	public void verifyActiveBonusesLabelsDisplayed(String labels)
	{
		if(labels.contains(","))
		{
			String[] arr1 = labels.split(",");

			for (String labels2 : arr1) 
			{
				By locator = By.xpath("//div[contains(@class,'bonus-item-body')]//following-sibling::div//p//span[contains(text(),'"+labels2+"')]");

				logReporter.log(" Verify "+labels2+" is displayed on Active Bonuses screen ",  
						objMobileActions.checkElementDisplayed(locator));
			}
		}
		else 
		{ 
			By locator = By.xpath("//div[contains(@class,'bonus-item-body')]//following-sibling::div//p//span[contains(text(),'"+labels+"')]");
			logReporter.log(" Verify "+labels+" is displayed on Active Bonuses screen ",  
					objMobileActions.checkElementDisplayed(locator));
		}
	}

	public void verifyFieldsUnderActiveBonusAftercollpsed(String labels)
	{
		//div[contains(@class,'bonus-item-body')]//following-sibling::div[contains(@class,'bonus-item-details')]//p//span
		By locator = By.xpath("//div[contains(@class,'bonus-item-body')]//following-sibling::div[contains(@class,'bonus-item-details')]//p//span[contains(text(),'"+labels+"')]");
		logReporter.log(" Verify "+labels+" is displayed on Active Bonuses screen ",  
				objMobileActions.checkElementDisplayed(locator));
	}
	public void verifyViewTCsLinkDisplayed()
	{
		By ViewTCsLink = By.xpath("//div[contains(@class,'bonus-item-details')]//div//a[text()='T&Cs']");
		logReporter.log("Verify 'View T&Cs Link' displayed on Active Bonuses screen ", 
				objMobileActions.checkElementDisplayed(ViewTCsLink));
	}
	public void verifyOptOutLinkDisplayed()
	{
		By OptOutLink = By.xpath("//div[contains(@class,'bonus-item-details')]//div//a[contains(text(),'Opt out')]");
		logReporter.log("Verify 'OptOut Link' displayed on Bonuses screen ", 
				objMobileActions.checkElementDisplayed(OptOutLink));
	}

	public void clickOptOutLink()
	{
		By OptOutLink = By.xpath("//div[contains(@class,'bonus-item-details')]//div//a[contains(text(),'Opt out')]");
		logReporter.log(" click on 'OptOut Link' displayed on Bonuses screen ", 
				objMobileActions.click(OptOutLink));
	}

	public void verifyAreUSureTXTonPopupDisplayed()
	{
		By AreUSureTXT = By.xpath("//h5[contains(text(),'Are you sure?')]");
		logReporter.log("Verify 'Are You Sure TXT' displayed on confirmation popup ", 
				objMobileActions.checkElementDisplayed(AreUSureTXT));
	}
	public void verifyOptOutTXTonPopupDisplayed()
	{
		By OptOutTXT = By.xpath("//p[contains(text(),'Opt out')]");
		logReporter.log("Verify 'Opt Out TXT' displayed on confirmation popup ", 
				objMobileActions.checkElementDisplayed(OptOutTXT));
	}

	public void verifyYesBTNonPopupDisplayed()
	{
		By YesBTN = By.xpath("//button//span[contains(.,'Yes')]");
		logReporter.log("Verify 'Yes Button' displayed on confirmation popup ", 
				objMobileActions.checkElementDisplayed(YesBTN));
	}

	public void clickYesBTNonPopup()
	{
		By YesBTN = By.xpath("//button//span[contains(.,'Yes')]");
		logReporter.log(" Click 'Yes Button' displayed on confirmation popup ", 
				objMobileActions.click(YesBTN));
	}
	public void verifyNoBTNonPopupDisplayed()
	{
		By NoBTN = By.xpath("//button//span[contains(.,'No')]");
		logReporter.log("Verify 'No Button' displayed on confirmation popup ", 
				objMobileActions.checkElementDisplayed(NoBTN));
	}

	public void closeConfirmationPopupDisplayed()
	{
		By closeXBTN = By.xpath("//button[@class='close']");
		logReporter.log("Verify 'Close X Button' displayed on confirmation popup ", 
				objMobileActions.checkElementDisplayed(closeXBTN));

		logReporter.log(" click 'Close X Button' displayed on confirmation popup ", 
				objMobileActions.click(closeXBTN));
	}

	public void validateBonusStatus(String status)
	{
		By locator = By.xpath("//p//span[contains(text(),'Bonus status')]/following::span[contains(text(),'"+status+"')]");

		logReporter.log(" Verify Bonus Status displayed is : "+status,
				(status.equals(objMobileActions.getText(locator))));
	}
	public void verifyBonusDetailsAfterExpandingBonus(String details)
	{
		switch (details) 
		{
		case "Type": 
			this.verifyActiveBonusesLabelsDisplayed(details);
			break;
		case "Bonus status": 
			this.verifyActiveBonusesLabelsDisplayed(details);
			break;
		case "Amount": 
			this.verifyActiveBonusesLabelsDisplayed(details);
			break;
		case "Wagering target":
			this.verifyFieldsUnderActiveBonusAftercollpsed(details);
			break;
		case "Progress": 
			this.verifyFieldsUnderActiveBonusAftercollpsed(details);
			break;
		case "Activation": 
			this.verifyFieldsUnderActiveBonusAftercollpsed(details);
			break;
		case "Expiry": 
			this.verifyFieldsUnderActiveBonusAftercollpsed(details);
			break;
		case "Opt out link":
			this.verifyOptOutLinkDisplayed();
			break;
		case "View T&Cs link":
			this.verifyViewTCsLinkDisplayed();
			break;
		}}

	public void selectBonusType(String bonus)
	{
		By bonustype = By.xpath("//h5[contains(.,'Filter bonus type')]//following-sibling::div/div/select");

		logReporter.log(" Select Bonus type as : " +bonus +" from list ", 
				objMobileActions.selectFromDropDown(bonustype, bonus,"Text"));
		wait.sleep(configuration.getConfigIntegerValue("midwait"));
	}

	public void verifyBonusTypesDisplayed(String type)
	{
		By locator = By.xpath("//h5[contains(.,'Filter bonus type')]//following-sibling::div/div/select/option[text()='"+type+"']");
		logReporter.log(" Verify bonus type : "+type+" is displayed under filter dropdown list ",  
				objMobileActions.checkElementDisplayed(locator));	
	}

	public void verifyTypeHDRDisplayed(String type)
	{
		By typeValue= By.xpath("//span[contains(.,'Type')]//following-sibling::span");
		if(type.equals(objMobileActions.getText(typeValue))){

			logReporter.log(" Filtered transaction Bonus type is : " +type +" on Bonus history ", 
					objMobileActions.checkElementDisplayed(typeValue));
		}
		else 
		{
			By noTransactionFoundMSG = By.xpath("//p[contains(text(),'No items found')]");
			logReporter.log(" 'No items found' message is displayed on Bonuses screen ", 
					objMobileActions.checkElementDisplayed(noTransactionFoundMSG));
		}
	}
	public void verifyFilterByDateRangeLabelDisplayed()
	{
		By FilterByDateRangeLabel = By.xpath("//div[@class='myaccount-details']/p[contains(text(),'Filter by date range')]");

		logReporter.log("Verify 'Filter By Date Range Label' displayed on Active Bonuses screen ", 
				objMobileActions.checkElementDisplayed(FilterByDateRangeLabel));

	}
	public void selectCurrentMonth(String currentMonth , String CurrentDay)
	{
		By Month = By.xpath("//div[@class='react-datepicker__current-month']");
		By Day = By.xpath("//div[@class='react-datepicker__week']/div[text()='"+CurrentDay+"'][not(contains(@class, '--outside-month'))]");

		//a[contains(@title, 'Design') and not(contains(@class, 'reMode_selected'))]
		System.out.println("currentMonth : "+currentMonth +"CurrentDay"+CurrentDay);

		while(true)
		{
			if(currentMonth.equals(objMobileActions.getText(Month)))
			{
				break;
			}
			else 
			{
				By NextMonthBTN = By.xpath("//button[contains(@class,'react-datepicker__navigation--next')]");
				logReporter.log(" Click on the Next month button on datePicker", 
						objMobileActions.click(NextMonthBTN));
			}
		}
		logReporter.log(" Selected the Day as: "+ objMobileActions.getText(Day)+"on datePicker ", 
				objMobileActions.click(Day));
	}

	public void setFromDate(String fromdate )
	{
		By inpFromDate= By.xpath("//div[@class='react-datepicker__input-container']/input[@class='date-picker-input']");

		logReporter.log(" Click 'from date' field to select the date ", 
				objMobileActions.click(inpFromDate));

		this.selectCurrentMonth(objUtilities.getRequiredDay("-1", "MMMM yyyy", ""),objUtilities.getRequiredDay("-1","d",""));
	}

	public void setFromDateSameDay()
	{
		By inpFromDate= By.xpath("//div[@class='react-datepicker__input-container']/input[@class='date-picker-input']");

		logReporter.log(" Click 'from date' field to select the date as current day ", 
				objMobileActions.click(inpFromDate));

		this.selectCurrentMonth(objUtilities.getRequiredDay("-1", "MMMM yyyy", ""),objUtilities.getRequiredDay("0","d",""));
	}

	public void verifyToDate(String currDate )
	{
		By inpToDate = By.xpath("//div[@class='react-datepicker__input-container']/input[@value='"+currDate+"']");

		if (currDate.equals(objMobileActions.getAttribute(inpToDate,"value")))
		{
			logReporter.log(" Verify Todate displayed is "+currDate, 
					objMobileActions.checkElementDisplayed(inpToDate));
		}
	}
	public void verifyResetLinkdisplayed()
	{
		By ResetLink = By.xpath("//div[@class='myaccount-details']/a[contains(text(),'Reset')]");
		logReporter.log(" Verify 'Reset Link' displayed on Bonus History screen ", 
				objMobileActions.checkElementDisplayed(ResetLink));
	}
	public void clickResetLink()
	{
		By ResetLink = By.xpath("//div[@class='myaccount-details']/a[contains(text(),'Reset')]");
		logReporter.log(" click 'Reset Link' ", 
				objMobileActions.click(ResetLink));
		wait.sleep(configuration.getConfigIntegerValue("midwait"));
	}
	public void verifyFilterButtonDisplayed()
	{
		By FilterBTN = By.xpath("//button[contains(text(),'Filter')]");
		logReporter.log("Verify 'Filter Button'displayed on Bonuses screen ", 
				objMobileActions.checkElementDisplayed(FilterBTN));
	}

	public void clickFilterButton()
	{
		By FilterBTN = By.xpath("//button[contains(text(),'Filter')]");
		logReporter.log(" click on 'Filter Button'displayed on  Bonuses screen ", 
				objMobileActions.click(FilterBTN));
		wait.sleep(configuration.getConfigIntegerValue("midwait"));
	}

	public void verifyBonusNameOnBonusHistoryDisplayed(String Bonusname)
	{
		By bonusname= By.xpath("//div[@class='toggle-module-content']/p[contains(text(),'"+Bonusname+"')]");

		if(Bonusname.equals(objMobileActions.getText(bonusname)))
		{
			logReporter.log("Claimed bonus name '"+ Bonusname +"'' is displayed correctly under Bonus history / Active Bonuses screen ", 
					objMobileActions.checkElementDisplayed(bonusname));
		}
		else {

			logReporter.log("Claimed bonus name "+ Bonusname +" is not displayed correctly under Bonus history / Active Bonuses screen", 
					objMobileActions.checkElementDisplayed(bonusname));
		}
	}
	public void clickXbtn()
	{
		By closeXbtn = By.xpath("//a[@class='close']");

		logReporter.log(" click on ' X' Button'displayed ", 
				objMobileActions.click(closeXbtn));
	}
	public void clickLogoutLink()
	{
		By logoutLink = By.xpath("//a[@class='logout-link']");

		logReporter.log(" click on logout-link displayed  ", 
				objMobileActions.click(logoutLink));
	}
	public String getBonusActivationDate()
	{
		By locator = By.xpath("//span[contains(text(),'Activation')]//following-sibling::span");
		return objMobileActions.getText(locator);
	}

	public void verifyBonusHistoryIsDisplayedAsPerSelectedDateRange(String frmdate,String toDate)
	{
		System.out.println("********************* frmdate  "+frmdate+ "   ***** toDate  "+toDate);
		String currentBonusActivationDate = getBonusActivationDate();

		currentBonusActivationDate =	currentBonusActivationDate.replaceAll("(?<=\\d)(st|nd|rd|th)", "");
		//	currentBonusActivationDate = currentBonusActivationDate.substring(0,currentBonusActivationDate.indexOf(" "));
		System.out.println("********************* formated  "+currentBonusActivationDate);
		//frmdate =	objUtilities.getFormatedDate(frmdate,"dd MMMM yyyy", "dd-MM-YY");
		toDate = objUtilities.getFormatedDate(toDate,"dd/MM/yyyy", "dd MMMM yyyy");

		System.out.println("********** gh gh***********  "+frmdate+ "  _____   "+toDate);
		if(currentBonusActivationDate.contains(frmdate)||currentBonusActivationDate.contains(toDate)){
			System.out.println("****dddd");
			logReporter.log(" Bonus history displayed correctly", true);}
		else
		{logReporter.log(" incorrect Bonus history display", false);}
	}

	public void verifyBreakPeriodOptions(String brkPeriod)
	{
		By BreakPeriod = By.xpath("//div[@class='take-a-break-set-range']/button/span[contains(text(),'"+brkPeriod+"')]");
		logReporter.log("Verify Take a Break Period option as : ",brkPeriod,
				objMobileActions.checkElementDisplayed(BreakPeriod));
	}

	public void clickOnGamestopLink()
	{
		By locator = By.xpath("//p[contains(.,'To find out more and to sign up with GAMSTOP please visit ')]//a[@title=\"http://www.gamstop.co.uk/\"]");
		logReporter.log("click on  'www.gamstop.co.uk' link ",
				objMobileActions.click(locator));
	}
	public void verifyGamestopLink()
	{
		By locator = By.xpath("//p[contains(.,'To find out more and to sign up with GAMSTOP please visit ')]//a[@title=\"http://www.gamstop.co.uk/\"]");
		logReporter.log("Verify 'www.gamstop.co.uk' link on gamestop link ",
				objMobileActions.checkElementDisplayed(locator));
	}
	//marketing
	public void verifyMarketingPreferencesOption(String pref)
	{
		if(pref.contains("~"))
		{
			String[] arr1 = pref.split("~");
			for (String pref1 : arr1) 
			{
				//By locator = By.xpath("//input[@class=' valid']/following-sibling::label[contains(text(),'"+pref1+"')]");
				By locator = By.xpath("//input[@type='checkbox']/following-sibling::label[contains(text(),'"+pref1+"')]");
				logReporter.log("Verify '" +pref1 + " ' option is displayed on marketing preferences screen",  
						objMobileActions.checkElementDisplayed(locator));
			}
		}
		else
		{
			//By locator = By.xpath("//input[@class=' valid']/following-sibling::label[contains(text(),'"+pref+"')]");
			By locator = By.xpath("//input[@type='checkbox']/following-sibling::label[contains(text(),'"+pref+"')]");
			logReporter.log("Verify '" +pref + " ' option is displayed on marketing preferences screen",  
					objMobileActions.checkElementDisplayed(locator));
		}
	}
	//my account

	public String getLastLoginFormat()
	{
		By lastLoginDate = By.xpath("//span[contains(text(),'Last Login')]//following-sibling::span");
		return objMobileActions.getText(lastLoginDate);
	}

	public void verifyNonEditableTXTDisplayed()
	{
		By NonEditableTXT = By.xpath("//span[contains(text(),'Date of birth: ')]/following-sibling::span/em[text()='Non editible']");
		logReporter.log(" Verify Non Editable TXT is displayed",
				objMobileActions.checkElementDisplayed(NonEditableTXT));
	}
	public void EditEmail()
	{
		By editEmail = By.xpath("//input[@id='email']");

		String newEmail = objUtilities.getEmail();
		System.out.println("***********email id :::::  "+newEmail);

		By clearBtn = By.xpath("//input[@id='email']/following-sibling::button[contains(text(),'Clear')]");
		logReporter.log(" Click on Clear button displayed",
				objMobileActions.click(clearBtn));

		logReporter.log(" Set Email ",  
				objMobileActions.setText(editEmail, newEmail));
	}

	public void EditPhoneNumber(String newMobile)
	{
		By editPhone = By.xpath("//input[@id='telephone']");
		//String newMobile ="077009" + objUtilities.getRandomNumeric(5);
		By clearBtn = By.xpath("//input[@id='telephone']/following-sibling::button[contains(text(),'Clear')]");
		logReporter.log(" Click on Clear button displayed",
				objMobileActions.click(clearBtn));

		logReporter.log(" Set Mobile number ",  
				objMobileActions.setText(editPhone, newMobile));
	}

	public void inputPassword(String password)
	{
		By inputPassword = By.xpath("//input[@id='password']");
		logReporter.log(" Enter your password to update",  
				objMobileActions.setText(inputPassword, password));
	}

	public void showPasswordBTNdisplayed()
	{
		By showBTN = By.xpath("//button[contains(text(),'Show')]");

		logReporter.log(" Verify Show button is displayed for Password ",
				objMobileActions.checkElementDisplayed(showBTN));

		logReporter.log(" click on Show button is displayed for Password ",
				objMobileActions.click(showBTN));

	}
	//change password
	public void setCurrentPassword(String currpassword)
	{
		logReporter.log(" Set on Current Password  ",  
				objMobileActions.setText(CurrentPassword, currpassword));
	}
	public void setNewPassword(String newPwd)
	{
		logReporter.log(" Set New Password: "+newPwd,  
				objMobileActions.setText(NewPassword, newPwd));
	}

	//msg
	public String getMessageCountFromMyAccountScreen()
	{
		String msgCount ;
		By locator = By.xpath("//div[contains(@class,'my-account-menu-messages-count ')]");
		if(objMobileActions.checkElementDisplayedWithMidWait(locator)){	
			msgCount = objMobileActions.getText(locator);}
		else{
			msgCount ="0";}
		System.out.println("************* msgCount "+msgCount);
		return msgCount;
	}

	public void verifyTakeAbReakMessage()
	{
		By locator = By.xpath("//h4[contains(.,'Your account is currently locked as being on ')]");
		logReporter.log(" Verify 'Your account is currently locked as being on 'Take a Break'. Please contact support.' ",  
				objMobileActions.checkElementDisplayed(locator));
	}


	public void validateLastLoginTimeFormat(String lastLoginTime)
	{
		lastLoginTime=lastLoginTime.replaceAll("(?<=\\d)(st|nd|rd|th)", "") ;
		lastLoginTime=lastLoginTime.replaceAll("\\s+", " ");
		DateFormat lastFormat = new SimpleDateFormat("dd MMM yyyy hh:mm:ss a");
		System.out.println("***** lastLoginTime ::: "+lastLoginTime+ "  lastFormat:::  "+lastFormat);
		try
		{
			Date lastlogindate = lastFormat.parse(lastLoginTime);
			logReporter.log("last login date: "+lastLoginTime+" displayed in correct format" , true);

		}catch(Exception e)
		{
			logReporter.log("last login date :"+lastLoginTime+" displayed in worng format" , false);
		}


	}

	public void verifyThatLastLoginTimeIsDisplayedCorrectly(String expectedLoginTime)
	{
		try {
			String CurrentlastLoginTime = this.getLastLoginFormat();
			CurrentlastLoginTime = CurrentlastLoginTime.replaceAll("(?<=\\d)(st|nd|rd|th)", "") ;
			CurrentlastLoginTime=CurrentlastLoginTime.replaceAll("\\s+", " ");
			expectedLoginTime = expectedLoginTime.substring(0,expectedLoginTime.lastIndexOf(":"));
			CurrentlastLoginTime = CurrentlastLoginTime.substring(0,CurrentlastLoginTime.lastIndexOf(":"));
			DateFormat lastFormat = new SimpleDateFormat("dd MMMM yyyy hh:mm");
			Date lastlogindate = lastFormat.parse(CurrentlastLoginTime);
			CurrentlastLoginTime = lastFormat.format(lastlogindate);
			System.out.println(" expectedLoginTime ::: "+expectedLoginTime+ "  CurrentlastLoginTime :: "+CurrentlastLoginTime);
			logReporter.log("last login Time is displayed correctly" , CurrentlastLoginTime.contains(expectedLoginTime));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//add membership number
	public void setMembershipNumber(String number)
	{
		By locator = By.xpath("//input[@placeholder='Membership No']");
		logReporter.log("Enter Membership Number", objMobileActions.setTextWithClear(locator, number));
		objMobileActions.pressKeybordKeys(locator, "tab");
	}
	public void verifyClearCTAisDisplayedNextToAddMembershipNumberField()
	{
		By locator = By.xpath("//input[@placeholder='Membership No']");
		logReporter.log("verify Clear CTA is Displayed Next To AddMembership NumberField", objMobileActions.checkElementDisplayed(locator));
	}

	public void clickOnSubmitLink()
	{
		//By locator = By.xpath("//a[contains(.,'Submit')]");
		By locator = By.xpath("//button[@type='submit']/span[text()='Find Details']");
		logReporter.log("click on Submit link", objMobileActions.click(locator));
		wait.sleep(configuration.getConfigIntegerValue("midwait"));
	}
	public void AddMembershipFieldNotApperedForRetailUserAccount()
	{
		By locator = By.xpath("//button[@type='button']/span[text()='Add Virtual Card']");
		logReporter.log(" verify ' Add Membership No ' is not displayed on screen", objMobileActions.checkElementNotDisplayed(locator));
	}

	public void verifyButtonOnScreen(String btnname)
	{
		By locator = By.xpath("//button[@type='button']/span[text()='"+btnname+"']");
		logReporter.log(" verify ' "+btnname+ " ' is displayed on screen", objMobileActions.checkElementDisplayed(locator));
	}
	public void clickOnButtonFromScreen(String btnname)
	{
		By locator = By.xpath("//button[@type='button']/span[text()='"+btnname+"']");
		logReporter.log(" click on "+btnname+ " ", objMobileActions.click(locator));
	}
	
	
	
	public String getFieldValueFromAccountDetailsScreen(String fieldNm)
	{
		By locator = By.xpath("//div[@class='content-box-details']//div//p[contains(.,'"+fieldNm+"')]//span");
		return  objMobileActions.getText(locator);
	}

	public void verifyExpectedValueIsDisplayedOnMyAccountscreen(String expValue,String fieldname)
	{
		String currentValue = getFieldValueFromAccountDetailsScreen(fieldname);
		logReporter.log(" Expected value is displayed for ' "+fieldname+ " ' field",currentValue.equalsIgnoreCase(expValue));
	}
	
	public void verifyOptionsOnSelfexcludeScreen(String options) {
		switch (options) {
		
		case "Close button":
			logReporter.log(" Check Close button", objMobileActions.checkElementDisplayed(Closebutton));
			break;
		case "Back button":
			logReporter.log(" Check Back button", objMobileActions.checkElementDisplayed(backbutton));
			break;
		case "Self Exclude Title":
			logReporter.log(" Check Gamstop Text", objMobileActions.checkElementDisplayed(SelfExcludeTitle));
			break;
		case "Question":
			logReporter.log(" Check Gamstop Text", objMobileActions.checkElementDisplayed(doyoufeelyouhaveaproblemtxt));
			break;
		case "Yes option":
			VerifyYesOrNoButtonFromExcludeScreen("Yes");
			break;
		case "No option":
			VerifyYesOrNoButtonFromExcludeScreen("No");
			break;
		}
	}
	
	public void clickOnYesOrNoFromExcludeScreen(String btn)
	{
		By locator = By.xpath("//a[contains(@href,'"+btn+"')]//span[contains(.,'"+btn+"')]");
		logReporter.log(" click on "+btn, objMobileActions.click(locator));
	}
	
	public void VerifyYesOrNoButtonFromExcludeScreen(String btn)
	{
		By locator = By.xpath("//a[contains(@href,'"+btn+"')]//span[contains(.,'"+btn+"')]");
		logReporter.log(" click on "+btn, objMobileActions.checkElementDisplayed(locator));
	}
	
	//msg

		public void verifyDeleteIconDisplyedInDisableState()
		{
			By locator = By.xpath("//section[contains(@class,'my-account-messages')]//div//a[contains(.,'Live help')]//following-sibling::div//i[not(contains(@class, 'delete active'))]");
			logReporter.log(" verify Delete Icon Displyed In Disable State", objMobileActions.checkElementDisplayed(locator));
		}

		public void verifyPaginationLinkOnMessageBodyScreen()
		{
			By lnkPrevious = By.xpath("//section[contains(@class,'my-account-messages')]//div//a[contains(.,'Live help')]//following-sibling::div[contains(@class,'messages-list-navigation')]//i[contains(@class,'icon-arrow-left')]");
			logReporter.log("Verify 'Previous' pagination is displayed on message screen ", 
					objMobileActions.checkElementDisplayed(lnkPrevious));

			By lnkNext = By.xpath("//section[contains(@class,'my-account-messages')]//div//a[contains(.,'Live help')]//following-sibling::div[contains(@class,'messages-list-navigation')]//i[contains(@class,'icon-arrow-right')]");
			logReporter.log("Verify 'Next' pagination is displayed on message screen ", 
					objMobileActions.checkElementDisplayed(lnkNext));
		}

		public void verifyMessageContentDetailsScreen()
		{
			By locator = By.xpath("//section[@class='component message-detail']//h5//following::div//span");
			logReporter.log("Verify Message Content Details Screen is displayed", 
					objMobileActions.checkElementDisplayed(locator));
		}

		public void selectAnyMessageFromList()
		{
			By locator = By.xpath("//div[@class='messages-list']//div//div[contains(@class,'messages-list-')][1]");
			if(objMobileActions.checkElementDisplayed(locator))
			{
				//List<WebElement> weRsults = (java.util.List<WebElement>) objDriverProvider.getWebDriver().findElements(locator);	
				//int index = RandomUtils.nextInt(0,weRsults.size());
				//System.out.println("index........    "+index);
				//	logReporter.log("Select any message from the list ", 
				//		objMobileActions.Click(objMobileActions.getElemnetOfIndex(locator,index)));
				logReporter.log("Select any message from the list ", 
						objMobileActions.click(locator));
			}
		}

		public void verifyunreadMessage(String Sender)
		{
			By lblMessageSenderName = By.xpath("//div//p[contains(@class,'messages-list-details-sender')][text()='"+Sender+"']//ancestor::a[contains(@class,'messages-list-details')]//div//i[not(contains(@class,'icon-email-read'))]");

			logReporter.log("Select unread message from "+Sender, 
					objMobileActions.checkElementDisplayed(lblMessageSenderName));
			wait.sleep(configuration.getConfigIntegerValue("midwait"));
		}

		public void selectUnreadMessage(String Sender)
		{
			By lblMessageSenderName = By.xpath("//div//p[contains(@class,'messages-list-details-sender')][text()='"+Sender+"']//ancestor::a[contains(@class,'messages-list-details')]//div//i[not(contains(@class,'icon-email-read'))]");

			logReporter.log("Select unread message from "+Sender, 
					objMobileActions.click(lblMessageSenderName));
			wait.sleep(configuration.getConfigIntegerValue("midwait"));
		}

		public void verifyMessageBodyDetails(String Sender,String subject,String  msgdet)
		{

			By lblsender = By.xpath("//section[@class='component message-detail']//h5[text()='"+subject+"']//following::div//span[text()='"+Sender+"']");
			logReporter.log("Verify Message ' "+subject+ "' is received from "+Sender , 
					objMobileActions.checkElementDisplayed(lblsender));

			By lblmessageDetails = By.xpath("//section[@class='component message-detail']//h5[text()='"+subject+"']//following::article[text()='"+msgdet+"']");
			logReporter.log("Verify "+msgdet+ " is displayed under message body details section", 
					objMobileActions.checkElementDisplayed(lblmessageDetails));

			By lblMessageDate = By.xpath("//section[@class='component message-detail']//h5[text()='"+subject+"']//following::div//span[2]");
			logReporter.log("Verify Message Date is displayed on screen", 
					objMobileActions.checkElementDisplayed(lblMessageDate));
		}

		public void verifyRecivedMessageDetails(String Sender,String subject)
		{
			By lnkMessageSubject = By.xpath("//div//h6[contains(@class,'messages-list-details-title')][text()='"+subject+"']//ancestor::a[contains(@class,'messages-list-details')]//div//i[(contains(@class,'icon-email-read'))]");
			logReporter.log("Verify Message Title is displayed as "+subject, 
					objMobileActions.checkElementDisplayed(lnkMessageSubject));


			By lblMessageSenderName = By.xpath("//div//p[contains(@class,'messages-list-details-sender')][text()='"+Sender+"']//ancestor::a[contains(@class,'messages-list-details')]//div//i[(contains(@class,'icon-email-read'))]");

			logReporter.log("Verify Sender is displayed as "+Sender, 
					objMobileActions.checkElementDisplayed(lblMessageSenderName));
		}

		public void verifyImage()
		{
			By locator = By.xpath("//article[@class='message-detail__body']//a//img");
			logReporter.log("Verify image is displayed on message screen ", 
					objMobileActions.checkElementDisplayed(locator));
		}

		public void verifyCTAText(String btntext)
		{
			By locator = By.xpath("//article[@class='message-detail__body']//following::a[text()='"+btntext+"']");
			logReporter.log("Verify ' "+ btntext + "' cta is displayed " ,
					objMobileActions.checkElementDisplayed(locator));
		}

		public void selectMessageCheckbox(String subject)
		{
			System.out.println(">>>>>>>>>> into subject");
			By locator = By.xpath("//div//h6[contains(@class,'messages-list-details-title')][text()='"+subject+"']//preceding::div[contains(@class,'messages-list-indicator')]//div");
			System.out.println(" locator:::   "+locator);
			logReporter.log("select message checkbox", 
					objMobileActions.clickUsingJS(locator));	
		}

		public void verifyDeletedMessageNotDisplayedInList(String Sender,String subject)
		{
			if(this.verifyYourInboxiIsEmptyMessageDisplayed())
			{
				logReporter.log("Verify 'Your inbox is empty' message is displayed", 
						true);
			}
			else
			{
				By lnkMessageSubject = By.xpath("//div//h6[contains(@class,'messages-list-details-title')][text()='"+subject+"']");
				logReporter.log("Verify Message Title is not displayed as "+subject, 
						objMobileActions.checkElementNotDisplayed(lnkMessageSubject));

				/*By lblMessageSenderName = By.xpath("//button[@class='message-list__details btn-link']//i//following-sibling::span[@class='message-list__sender'][text()='"+Sender+"']");
				logReporter.log("Verify Sender name is not displayed as "+Sender, 
						objobjMobileActions.checkElementNotDisplayed(lblMessageSenderName));*/}

		}
		public boolean verifyYourInboxiIsEmptyMessageDisplayed()
		{
			By lblYourinboxisempty = By.xpath("//div[@class='component message-list-empty']//p[text()='Your inbox is empty']");
			return objMobileActions.checkElementDisplayed(lblYourinboxisempty);
		}


		public void verifyUpdatedCountisDisplayedOnAllScrrens(String oldcnt,String newCnt)
		{
			logReporter.log("Verify message count is displayed correctly ", 
					(Integer.valueOf(oldcnt)>Integer.valueOf(newCnt)));
			
		}
		public String getMessageCountFromHomeScreen()
		{
			By locator = By.xpath("//a[@class='open-myaccount']//span[contains(@class,'component_messages')]");
			String count;
			if(objMobileActions.checkElementDisplayed(locator))
				count = objMobileActions.getText(locator);
			else
				count = "0";
			return count;
		}
}