package com.meccabingo.mobile.page.Mecca;

import org.openqa.selenium.By;

import com.generic.MobileActions;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.logger.LogReporter;


public class PayPalPage {
	
	private MobileActions objMobileActions;
	private AppiumDriverProvider objDriverProvider;
	private LogReporter logReporter;

	public PayPalPage(AppiumDriverProvider driverProvider, MobileActions objMobileActions,
			LogReporter logReporter) {
		this.objDriverProvider = driverProvider;
		this.objMobileActions = objMobileActions;
		this.logReporter = logReporter;
	}

	public void enterPayPalID(String strID) {
		By locator = By.id("email");
		if (objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("enter paypal id> >", objMobileActions.setText(locator, strID));
	}

	public void clickPayPalNext() {
		By locator = By.id("btnNext");
		if (objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("Click paypal next button> >", objMobileActions.click(locator));
	}

	public void enterPayPalPassword(String strPassword) {
		By locator = By.id("password");
		if (objMobileActions.checkElementDisplayedWithMidWait(locator)) {
			
			logReporter.log("enter paypal Password> >", objMobileActions.setTextWithClear(locator, strPassword));
		}
	}

	public void clickPayPalLogin() {
		By locator = By.id("btnLogin");
		if (objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("Click paypal login button> >", objMobileActions.click(locator));
	}

	public void clickPayPalConfirm() {
		By locator = By.id("confirmButtonTop");
		if (objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("Click paypal confirmButtonTop button> >", objMobileActions.click(locator));
	}
}
