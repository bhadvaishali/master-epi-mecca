/**
 * 
 */
package com.meccabingo.mobile.page.Mecca;

import org.openqa.selenium.By;

import com.generic.MobileActions;
import com.generic.logger.LogReporter;
import com.generic.utils.Utilities;
import com.mifmif.common.regex.Generex;

public class SearchPage {

	By search_loupe = By.xpath("//button[contains(@class,'search-open-btn')]"); // button[@class='search-open-btn']
	By search_overLay = By.xpath("//div[@class='field-container']");
	By search_TxtBox = By.xpath("//input[contains(@class,'search-input')]"); // input[@class='search-input']
	By search_clearButton = By.xpath("//button[contains(@class,'search-open-btn')]"); // button[@class='search-clear-btn']
	By searched_Section = By.xpath("//section[@class='search-results-items']");
	By searched_Section_NoResult = By.xpath("//div[@class='search-no-results']");
	By searched_Container = By.xpath("//div[contains(@class,'search-results-items-container')]");

	By searchedGame_Img = By.xpath("//div[contains(@class,'search-results-item')]/div/img"); // div[@class='search-results-item']//img
	By searchedGame_Name = By.xpath("//div[contains(@class,'search-results-item-content')]/h3/span"); // div[@class='search-results-item-content']//h3//span
	By searchedGame_PlayNow_btn = By.xpath("(//section[contains(@class,'search-result')]//button[contains(@class,'play')])[1]"); // a[contains(text(),'Play
																														// Now')]
	By searchedGame_Info = By.xpath("(//section[contains(@class,'search-result')]//a)[1]"); // *[contains(text(),'Play
																											// Now')]/following-sibling::a[contains(@class,'info-sign')]
	By search_WhatOthersArePlaying_Section = By
			.xpath("//section[contains(@class,'search-suggestions-what-others-play')]"); // section[@class='search-suggestions-what-others-play']
	By search_QuickLinks_Section = By.xpath("//section[contains(@class,'search-suggestions-quick-links')]"); // section[@class='search-suggestions-quick-links']
	By freeplaybtn = By.xpath("//button[contains(.,'Free Play')]");

	By linkSignUP = By.xpath("//a[contains(text(),'Sign Up')]");

	By closebtnOnDepositPage = By.xpath("//a[contains(@class,'icon-close')]");
	By depositTitleOnDepositPage = By.xpath("//h4[contains(text(),'Deposit')]");
	By cardNumberOnDepositPage = By.xpath("//input[contains(@id,'cc_card_number')]");
	By cardExpiryDateOnDepositPage = By.xpath("//input[contains(@id,'cc-exp-date')]");
	By cardSecurityCodeOnDepositPage = By.xpath("//input[contains(@id,'cc_cvv2')]");
	By cardEnterAmountOnDepositPage = By.xpath("//input[contains(@id,'item_amount_1')]");
	By cardSelectOnFirstAmount = By.xpath("//div[contains(text(),'£10')]");
	By cardSelectOnSecondAmount = By.xpath("//div[contains(text(),'£20')]");
	By cardSelectOnThirdAmount = By.xpath("//div[contains(text(),'£30')]");
	By cardDepositButton = By.xpath("//input[contains(@class,'continue')]");

	By closeButtonOnSuccessMessageAfterDeposit = By.xpath("//button[contains(text(),'Close')]");
	By depositSuccessPopupTitle = By.xpath("//h2[contains(text(),'Deposit successful')]");
	
	String paymentIframe = "payment-process";
	

	private MobileActions objMobileActions;
	private LogReporter logReporter;
	private Utilities objUtilities;

	public SearchPage(MobileActions objMobileActions, LogReporter logReporter, Utilities utilities) {
		this.objMobileActions = objMobileActions;
		this.logReporter = logReporter;
		this.objUtilities = utilities;
	}

	public void clickSearchLoupe() {
		logReporter.log("click 'Search Loupe' > >", objMobileActions.click(search_loupe));
	}

	public void verifySearchOverLay() {
		logReporter.log("Check 'search OverLay' ", objMobileActions.checkElementDisplayed(search_overLay));

	}

	public void enterGameName(String gameName) {
		objMobileActions.clearText(search_TxtBox);
	//	objMobileActions.click(search_clearButton);
		logReporter.log("Enter Value in search box' ", objMobileActions.setText(search_TxtBox, gameName));
	}

	public void verifySearchSection() {
		//if (objMobileActions.checkElementDisplayed(searched_Section)) {
			logReporter.log("Check 'searched item list' ", objMobileActions.checkElementDisplayed(searched_Container));
		//}
	}

	public void verifyGameDetails(String text) {
		switch (text) {

		case "Image": {
			logReporter.log("check element > >", objMobileActions.checkElementDisplayed(searchedGame_Img));
			break;
		}

		case "Name": {
			logReporter.log("check element > >", objMobileActions.checkElementDisplayed(searchedGame_Name));
			break;
		}

		case "Play Now CTA": {
			logReporter.log("check element > >", objMobileActions.checkElementDisplayed(searchedGame_PlayNow_btn));
			break;
		}

		case "Info CTA": {
			logReporter.log("check element > >", objMobileActions.checkElementDisplayed(searchedGame_Info));
			break;
		}

		}

	}

	public void enterValuesInSearchBox(String string) {

		logReporter.log("Check 'searched blank list' ", objMobileActions.checkElementDisplayed(searched_Section_NoResult));
	}

	public void verifyResultSectionWithNoMatch() {
		logReporter.log("Check 'searched blank list' ", objMobileActions.checkElementDisplayed(searched_Section_NoResult));

	}

	public void verifyQuickLinksAndWhatOthersArePlayingInSearchOverLay() {
		logReporter.log("Check 'What Others are playing' section ",
				objMobileActions.checkElementDisplayed(search_WhatOthersArePlaying_Section));
		logReporter.log("Check 'Quick Links' section ", objMobileActions.checkElementDisplayed(search_QuickLinks_Section));

	}

	public void clickOnSearchibtn() {
		logReporter.log("click on i button", objMobileActions.click(searchedGame_Info));
	}

	public void verifyGameDetailsPage() {
		logReporter.log("verify game detail page", objMobileActions.checkElementDisplayed(freeplaybtn));
	}

	public void clickOnSearchPlayNowbtn() {
		logReporter.log("click on play now button from search", objMobileActions.click(searchedGame_PlayNow_btn));
	}

	public void clickOnSignUpLink() {
		logReporter.log("click onn sign up link", objMobileActions.click(linkSignUP));
	}

	public void clickOnDepositClosebtn() {
		logReporter.log("click close button on depo ", objMobileActions.click(closebtnOnDepositPage));
	}

	public void enterCardNumber(String cardnumber) {
		objMobileActions.switchToFrameUsingNameOrId("payment-process");
		logReporter.log("enter card number", objMobileActions.setText(cardNumberOnDepositPage, cardnumber));
		objMobileActions.switchToDefaultContent();
	}

	public void enterCardDate(String carddate) {
		objMobileActions.switchToFrameUsingNameOrId("payment-process");
		logReporter.log("enter card number", objMobileActions.setText(cardExpiryDateOnDepositPage, carddate));
		objMobileActions.switchToDefaultContent();
	}

	public void enterSecurityCode() {
		String regex = "[0-9]{3}";
		String randomnumber = new Generex(regex).random();
		objMobileActions.switchToFrameUsingNameOrId("payment-process");
		logReporter.log("enter security code", objMobileActions.setText(cardSecurityCodeOnDepositPage, randomnumber));
		objMobileActions.switchToDefaultContent();
	}

	public void enterAmount(String amt) {
		objMobileActions.switchToFrameUsingNameOrId("payment-process");
		By amount = By.xpath("//input[contains(@placeholder,'enter amount here')]");
		logReporter.log("enter amount", objMobileActions.setText(amount,amt));
		objMobileActions.switchToDefaultContent();
	}

	public void switchToSuccessPopup() {
		logReporter.log("switch to popup", objMobileActions.switchToWindowUsingTitle("Deposit successful!"));

	}

	public void clickOnCloseButtonOnPopup() {
		logReporter.log("click on close button", objMobileActions.click(closeButtonOnSuccessMessageAfterDeposit));
	}

	public void closeSearch() {
		By locator = By.xpath("//a[contains(@class,'icon-close')]");
		logReporter.log("click on close button", objMobileActions.click(locator));
	}
	
	public void checkCheckboxOfIncludeClubs() {
		By locator = By.xpath("//div[contains(@class,'ip-checkbox')]/input");
		logReporter.log("click on checkbox", objMobileActions.click(locator));
	}
	
	public void VerifyGameDetailsPage()
	{
		By gameInfoDetailsSection = By.xpath("//div[@class='game-info']//section[contains(@class,'game-info-details')]");
		logReporter.log("Check game-info-detailspage is displayed", objMobileActions.checkElementDisplayed(gameInfoDetailsSection));
		verifySectionOnGameInfo("Description");
		verifySectionOnGameInfo("How To Play");
		verifyGameInformationSectionOn();
	}
	
	public void verifySectionOnGameInfo(String lbl)
	{
		By locator = By.xpath("//section[contains(@class,'gcb-text')]//div//h2[contains(.,'"+lbl+"')]");
		logReporter.log("Check ' "+lbl+ " ' section is displayed on game details page", objMobileActions.checkElementDisplayed(locator));
	}
	public void verifyGameInformationSectionOn()
	{
		By locator = By.xpath("//section[contains(@class,'game-info-box')]//div//h4[contains(.,'Game Information')]");
		logReporter.log("Check ' Game Information' section is displayed on game details page", objMobileActions.checkElementDisplayed(locator));
	}
	
	public void verifySearchResultIsDisplayedAsPerEnteredGame(String game)
	{
		//div[contains(@class,'search-results-items-container')]//div//img[contains(@alt,'Bing')]

		//section[contains(@class,'search-results-items')]//h2[contains(.,'Bingo Rooms')]
		By locator = By.xpath("//div[contains(@class,'search-results-items-container')]//div//img[contains(@src,'"+game+"')]");
		logReporter.log("Search result displayed correctly for "+game, objMobileActions.checkElementDisplayed(locator));
	}
	
	public void verifyMaximumof6ResultsForSearchstringunderclubHeading()
	{
		By locator = By.xpath("//section[contains(@class,'search-results-items')]//h2[contains(.,'Mecca Bingo clubs')]//following-sibling::div[contains(@class,'search-results-')]//div[contains(@class,'search-results-item-content')]");
		String expectedResultCnt ="6";
		int searchResults = objMobileActions.processMobileElements(locator).size();
		System.out.println(" ************* size:::  " +searchResults);
		logReporter.log("Verify 'Maximum of 6 Results Are Shown Below The Search field '",expectedResultCnt.equalsIgnoreCase(String.valueOf(searchResults)));
	}
	
	public void verifyClubNameonSearchedClub()
	{
		By locator = By.xpath("//section[contains(@class,'search-results-items')]//h2[contains(.,'Mecca Bingo clubs')]//following-sibling::div[contains(@class,'search-results-items')]//div[contains(@class,'search-results-item')][1]//div[contains(@class,'search-results-item-content')]//h3//span[contains(@class,'text-highlighted') and contains(.,'Mecc')]");
		logReporter.log("Verify Club name on search results ", objMobileActions.checkElementDisplayed(locator));
	}
	
	public void verifyClubDistanceonSearchedClub()
	{
		By locator = By.xpath("//section[contains(@class,'search-results-items')]//h2[contains(.,'Mecca Bingo clubs')]//following-sibling::div[contains(@class,'search-results-items')]//div[contains(@class,'search-results-item')][1]//div[contains(@class,'search-results-item-content')]//h3//following-sibling::p[contains(@class,'colour-purple') and contains(.,'miles')]");
		logReporter.log("Verify Club distance on search results ", objMobileActions.checkElementDisplayed(locator));
	}
	
	public void verifyClubInfoIcon()
	{
		By locator = By.xpath("//section[contains(@class,'search-results-items')]//h2[contains(.,'Mecca Bingo clubs')]//following-sibling::div[contains(@class,'search-results-items')]//div[contains(@class,'search-results-item')][1]//div[contains(@class,'search-results-item-content')]//div[contains(@class,'search-results-item-buttons')]//a[contains(@class,'info-sign')]");
		logReporter.log("Verify Club info icon on search results ", objMobileActions.checkElementDisplayed(locator));
	}
	
	public void clickOnClubInfoIcon()
	{
		By locator = By.xpath("//section[contains(@class,'search-results-items')]//h2[contains(.,'Mecca Bingo clubs')]//following-sibling::div[contains(@class,'search-results-items')]//div[contains(@class,'search-results-item')][1]//div[contains(@class,'search-results-item-content')]//div[contains(@class,'search-results-item-buttons')]//a[contains(@class,'info-sign')]");
		logReporter.log("click  Club info icon  ", objMobileActions.click(locator));
	}
	
	public void verifyClubInformationunderSearchResult(String sectionName)
	{
		switch (sectionName) {
		case "Name of the club":
			verifyClubNameonSearchedClub();
			break;
		case "Distance away":
			verifyClubDistanceonSearchedClub();
			break;
		case "Info cta":
			verifyClubInfoIcon();
			break;
		}
	}
}
