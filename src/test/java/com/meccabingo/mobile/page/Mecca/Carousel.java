package com.meccabingo.mobile.page.Mecca;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.generic.MobileActions;
import com.generic.logger.LogReporter;
import com.generic.utils.WaitMethods;

public class Carousel {

	private MobileActions mobileActions;
	private LogReporter logReporter;
	private WaitMethods waitMethods;
	String url;
	public Carousel(MobileActions mobileActions, LogReporter logReporter,WaitMethods waitMethods) {
		this.mobileActions = mobileActions;
		this.logReporter = logReporter;
		this.waitMethods = waitMethods;
	}

	By previousArrow = By.xpath("//*[@id=\"promoCarousel\"]/apollo-carousel/section/div[3]");
	By nextArrow = By.xpath("//*[@id=\"promoCarousel\"]/apollo-carousel/section/div[4]");
	By paginationDots = By.xpath("//div[contains(@class,'swiper-pagination-bullets')]");
	By image = By
			.xpath("//apollo-carousel-slide[contains(@class,'active')]");
    By feefo = By.xpath("//apollo-carousel/section");
    By feefoRightButton = By.xpath("//apollo-carousel/section/div[2]");
    By feefoLeftButton = By.xpath("//apollo-carousel/section/div[3]");
    By closePopup = By.xpath("//img[contains(@class,'walkthrough-close-icon')]");
    
	public void clickOnSlide() {

		//if(mobileActions.checkElementDisplayed(image))
		url = mobileActions.getAttribute(image, "cta-link");
		System.out.println("**** url" +url);
		logReporter.log("click on image", mobileActions.click(image));
		
	}

	public void verifyNavigateToPage() {
		System.out.println("current url ::  " +mobileActions.getUrl());
       logReporter.log("verify it navigate to game page", mobileActions.getUrl().contains(url));
	}

	public void verifyNavigationArrow() {
		logReporter.log("verify left arrow", mobileActions.checkElementDisplayed(previousArrow));
		logReporter.log("verify right arrow", mobileActions.checkElementDisplayed(nextArrow));
	}

	public void verifyPaginationDots() {
		logReporter.log("verify pagination dots on carousel", mobileActions.checkElementDisplayed(paginationDots));
	}

	public void clickOnLeftArrow() {
		logReporter.log("click on left arrow", mobileActions.click(previousArrow));
	}

	public void clickOnRightArrow() {
		logReporter.log("click on right arrow", mobileActions.click(nextArrow));
	}
	public void verifyFeefoCarousel() {
		logReporter.log("verify feefo carousel", mobileActions.checkElementDisplayed(feefo));
	}
    public void verifyRightButton() {
    	logReporter.log("verify right arrow", mobileActions.checkElementDisplayed(feefoRightButton));
    }
    public void verifyLeftButton() {
    	logReporter.log("verify left arrow", mobileActions.checkElementDisplayed(feefoLeftButton));
    	
    }
    public void closePopup() {
    	logReporter.log("close the welcome popup", mobileActions.click(closePopup));
    }
}
