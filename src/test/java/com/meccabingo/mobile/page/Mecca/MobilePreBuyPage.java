package com.meccabingo.mobile.page.Mecca;

import org.openqa.selenium.By;

import com.generic.MobileActions;
import com.generic.logger.LogReporter;
import com.generic.webdriver.DriverProvider;

public class MobilePreBuyPage {
	MobileActions objMobileActions;
	private DriverProvider objDriverProvider;
	private LogReporter logReporter;

	public MobilePreBuyPage(DriverProvider driverProvider, MobileActions mobileActions,
			LogReporter logReporter) {
		this.objDriverProvider = driverProvider;
		this.objMobileActions = mobileActions;
		this.logReporter = logReporter;
	}

	public void selectTicket(String str) {
		By locator = By.xpath("//div[contains(@class,'pre-buy-popup-buttons')]/button[text()='"+str+"']");
		if (objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("click tickets> >", objMobileActions.click(locator));
	}

	public void clickPreBuyButton() {
		By locator = By.xpath("(//button[text()='Pre-Buy'])[1]");
		if (objMobileActions.checkElementDisplayedWithMidWait(locator)) {
			objMobileActions.androidScrollToElement(locator);
			logReporter.log("click pre-buy button > >", objMobileActions.clickUsingJS(locator));
		}
	}
	
	public void verifyPreBuyPopupDisplayed() {
		By locator = By.xpath("//section[contains(@class,'pre-buy-popup')]");
		logReporter.log("check pre-buy popup displayed > >", objMobileActions.checkElementDisplayedWithMidWait(locator));
	}

}
