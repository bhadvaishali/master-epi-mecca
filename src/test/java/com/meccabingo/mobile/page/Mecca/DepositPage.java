package com.meccabingo.mobile.page.Mecca;

import org.openqa.selenium.By;

import com.generic.MobileActions;
import com.generic.WebActions;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.logger.LogReporter;
import com.generic.webdriver.DriverProvider;

import io.cucumber.java.en.Then;

public class DepositPage {
	private MobileActions objMobileActions;
	private AppiumDriverProvider objDriverProvider;
	private LogReporter logReporter;

	private String iFrameId = "payment-process";

	public DepositPage(AppiumDriverProvider driverProvider, MobileActions mobileActions,
			LogReporter logReporter) {
		this.objDriverProvider = driverProvider;
		this.objMobileActions = mobileActions;
		this.logReporter = logReporter;
	}

	public void clickOnDepositBesidesMyAcct() {
		By btnDeposit = By.xpath("//*[@id='logged-in-bar']/button");
		if (objMobileActions.checkElementDisplayedWithMidWait(btnDeposit))
			logReporter.log("Click deposit besides my acct button> >", objMobileActions.click(btnDeposit));
	}

	public void clickShowMore() {
		By showMore = By.xpath("//span[contains(text(),'Show More')]");
		objMobileActions.switchToFrameUsingNameOrId(iFrameId);
		if (objMobileActions.checkElementDisplayedWithMidWait(showMore))
			logReporter.log("Click Show more button> >", objMobileActions.click(showMore));
		objMobileActions.switchToDefaultContent();
	}

	public void selectPaymentMethod(String strPaymtMethod) {
		By pMethod = By.xpath("//div[@title='" + strPaymtMethod + "']");
		objMobileActions.switchToFrameUsingNameOrId(iFrameId);
		if (objMobileActions.checkElementDisplayedWithMidWait(pMethod))
			logReporter.log("Click payment method> >", objMobileActions.click(pMethod));
		objMobileActions.switchToDefaultContent();
	}

	public void enterCardNumber(String strNo) {
		By cardNo = By.xpath("//input[@id='cc_card_number']");
		objMobileActions.switchToFrameUsingNameOrId(iFrameId);
		if (objMobileActions.checkElementDisplayedWithMidWait(cardNo))
			logReporter.log("enter card no> >", objMobileActions.setText(cardNo, strNo));
		objMobileActions.switchToDefaultContent();
	}

	public void enterExpiry(String strExpiry) {
		By expiry = By.id("cc-exp-date");
		objMobileActions.switchToFrameUsingNameOrId(iFrameId);
		if (objMobileActions.checkElementDisplayedWithMidWait(expiry))
			logReporter.log("enter exp date> >", objMobileActions.setText(expiry, strExpiry));
		objMobileActions.switchToDefaultContent();

	}

	public void enterCVV(String strCVV) {
		By objCVV = By.xpath("//input[@id='cc_cvv2']");
		objMobileActions.switchToFrameUsingNameOrId(iFrameId);
		if (objMobileActions.checkElementDisplayedWithMidWait(objCVV))
			logReporter.log("enter card no> >", objMobileActions.setText(objCVV, strCVV));
		objMobileActions.switchToDefaultContent();
	}

	public void enterDepositAmt(String strAmt) {
		By amt = By.xpath("//input[contains(@placeholder,'amount here')]");
		objMobileActions.switchToFrameUsingNameOrId(iFrameId);
		if (objMobileActions.checkElementDisplayedWithMidWait(amt))
			logReporter.log("enter amount> >", objMobileActions.setText(amt, strAmt));
		objMobileActions.switchToDefaultContent();
	}

	public void clickDepositButton() {
		By locator = By.id("continueButton");
		objMobileActions.switchToFrameUsingNameOrId(iFrameId);
		if (objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("Click Deposit button> >", objMobileActions.click(locator));
		objMobileActions.switchToDefaultContent();
	}

	public void verifyDepositSuccessful() {
		By locator = By.xpath("//h2[contains(text(),'Deposit successful')]");
		
		objMobileActions.switchToFrameUsingNameOrId(iFrameId);
		logReporter.log("Verify Deposit Successful > >", objMobileActions.checkElementDisplayedWithMidWait(locator));
		objMobileActions.switchToDefaultContent();
	}

	public void clickCloseFromDepositSuccessfulPopup() {
		By locator = By.xpath("//button[contains(@class,'modal_close')]");
		objMobileActions.switchToFrameUsingNameOrId(iFrameId);
		if (objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("click close > >", objMobileActions.click(locator));
		objMobileActions.switchToDefaultContent();
	}

	public void clickWithdraw() {
		By locator = By.xpath("//button[contains(text(),'Withdraw')]");
		objMobileActions.switchToFrameUsingNameOrId(iFrameId);
		if (objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("click withdraw > >", objMobileActions.click(locator));
		objMobileActions.switchToDefaultContent();
	}

	public void verifyWithdrawSuccessful() {
		By locator = By.xpath("//h2[contains(text(),'Withdrawal request received')]");
		objMobileActions.switchToFrameUsingNameOrId(iFrameId);
		logReporter.log("Verify Withdraw Successful > >", objMobileActions.checkElementDisplayedWithMidWait(locator));
		objMobileActions.switchToDefaultContent();
	}

	public void selectSavedCard(String strCard) {
		By locator;
		objMobileActions.switchToFrameUsingNameOrId(iFrameId);
		if (strCard.equals("paysafe") == true)
			locator = By.xpath("//div[contains(@class,'paysafe')]");
		else
			locator = By.xpath("//div[contains(@title,'" + strCard + "')]");

		if (objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("click saved card > >", objMobileActions.click(locator));
		objMobileActions.switchToDefaultContent();
	}

	public void clickPayPal() {
		By locator = By.xpath("//img[contains(@title,'PayPal')]");
		objMobileActions.switchToFrameUsingNameOrId(iFrameId);
		if (objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("click PayPal > >", objMobileActions.click(locator));
		objMobileActions.switchToDefaultContent();
	}

	public void clickPaySafe() {
		By locator = By.xpath("//img[contains(@title,'paysafe')]");
		objMobileActions.switchToFrameUsingNameOrId(iFrameId);
		if (objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("click spaysafe > >", objMobileActions.click(locator));
		objMobileActions.switchToDefaultContent();
		objMobileActions.switchToParentWindow();
	}
}
