/**
 * 
 */
package com.meccabingo.mobile.page.Mecca;

import org.openqa.selenium.By;

import com.generic.MobileActions;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.Utilities;
import com.generic.utils.WaitMethods;


public class LogInPage {

	By closeXicon = By.xpath("//a[contains(@class,'icon-close')]");
	By tbUserName = By.xpath("//input[@id='input-username']");
	By tbPassword = By.xpath("//input[@id='input-password']");
	By rememberMeCheckbox = By.xpath("//*[contains(text(),'Remember Me')]/preceding-sibling::input[@type='checkbox']");
	By btnLogIn = By.xpath("//span[@class='button-content']/parent::button[@type='submit']"); // button[contains(text(),'Login')//
	// and//
	// @type='submit'],
	// //*[@id=\"header\"]/header/div[2]/button
	By forgotUserNameLink = By.xpath("//a[contains(text(),'Forgot username?')]");
	By forgotPasswordLink = By.xpath("//a[contains(text(),'Forgot password')]");

	By contactUsSection = By.xpath("//div[contains(@class,'contacts-wrapper')]/span/p[contains(text(),'Contact')]"); // p[contains(text(),'Contact
	// Us')]
	By contactUsCallNumber = By.xpath("//div[contains(@class,'contacts-wrapper')]/span/p[contains(text(),'Call')]"); // p[contains(text(),'Call:
	// 08000831988')]
	By contactUsemail = By.xpath("//a[contains(text(),'Support@meccabingo.com')]");

	By sendResetInstructions = By.xpath("//*[@class='login-wrapper-form']/button/child::span");
	By resetPasswordSuccessHeading = By.xpath("//h4[contains(text(),'Success')]");
	By resetPasswordSuccessMessage = By.xpath("//h4[contains(text(),'Success')]/following-sibling::p");
	By txtEmailAddress = By.xpath("//input[@id='input-email']");
	By sendUsernameReminder = By.xpath("//*[@class='login-wrapper-form']/button/child::span");
	By btnCTALogIn = By.xpath("//button[@type='submit']");


	By MyAccountButton = By.xpath("//a[contains(@class,'open-myaccount')]/i");
	By LogoutButton = By.xpath("//a[contains(text(),'Logout')]");

	private MobileActions mobileActions;
	private LogReporter logReporter;
	private RegistrationPage registrationPage;
	private Configuration configuration;
	private WaitMethods wait;
	public LogInPage(MobileActions mobileActions, LogReporter logReporter,Configuration configuration,WaitMethods wait) {
		this.mobileActions = mobileActions;
		this.logReporter = logReporter;
		this.configuration= configuration;
		this.wait = wait;
	}

	public void verifyLoginHeaderTitle() {
		By loginHeaderText = By.xpath("//h4[contains(text(),'Login')]"); // h4[starts-with(text(),'Login')]
		logReporter.log("Check Login window header > >", mobileActions.checkElementDisplayed(loginHeaderText));
		//mobileActions.useEmail();
	}

	public void clickJoinNowButton() {
	//	By btnJoinNow = By.xpath("//button[contains(text(),'Join Now')]");
		By btnJoinNow = By.xpath("//button[contains(text(),'Join')]");
		logReporter.log("click 'join now button' > >", mobileActions.click(btnJoinNow));

	}

	public void enterUserName(String userName) {
		logReporter.log("Enter Value in userName > >", mobileActions.setText(tbUserName, userName));
	}

	public void enterPassword(String password) {
		logReporter.log("Enter Value in password > >", mobileActions.setText(tbPassword, password));
	}

	public void clickLogin() {
		logReporter.log("click 'login button' > >", mobileActions.click(btnLogIn));
	}

	public void verifyLoginDisabled() {
		By btnLogInDisabled = By.xpath("//button[contains(text(),'Login') and contains(@class,'button-disabled')]");
		logReporter.log("check element > >", mobileActions.checkElementDisplayed(btnLogInDisabled));
	}

	public void verifyLogInErrorMessage(String errorMessage) {
		// System.out.println("Text captured: " +
		// objmobileActions.getText(logInErrorMessage, "text"));
		//
		// logReporter.log("Check error Message ", errorMessage,
		// objmobileActions.getText(logInErrorMessage, "text"));
		By errormessage = By.xpath("//h4[contains(text(),'" + errorMessage + "')]");
		logReporter.log("check element > >", mobileActions.checkElementDisplayed(errormessage));
	}

	public void verifyLoginPage() {
		By loginHeader = By.xpath("//div[contains(@class,'slideout-overlay-heading')]/h4[contains(text(),'Login')]");
		logReporter.log("Check login page displayed", mobileActions.checkElementDisplayed(loginHeader));

	}

	public void verifyCloseXicon() {

		logReporter.log("Check Close X icon displayed", mobileActions.checkElementDisplayed(closeXicon));
	}

	public void clickOnCloseXicon() {

		logReporter.log("Check Close X icon displayed", mobileActions.click(closeXicon));
	}

	public void verifyTBUserNameDisplayed() {
		logReporter.log("Check Username textbox displayed", mobileActions.checkElementDisplayed(tbUserName));

	}

	public void verifyTBPasswordDisplayed() {
		logReporter.log("Check Password textbox displayed", mobileActions.checkElementDisplayed(tbPassword));
	}

	public void verifyPasswordToggle() {
		By passwordToggle = By.xpath("//input[@id='input-password']/following::button[contains(text(),'Show')]");
		logReporter.log("Check Password toggle displayed", mobileActions.checkElementDisplayed(passwordToggle));

	}

	public void verifyRememberMeCheckbox() {
		logReporter.log("Check Remember Me checkbox displayed",
				mobileActions.checkElementDisplayed(rememberMeCheckbox));
	}

	public void verifyLoginCTA() {
		logReporter.log("Check login Button displayed", mobileActions.checkElementDisplayed(btnCTALogIn));
	}

	public void veryForgotUserNameLink() {

		logReporter.log("Check Forgot UserName Link displayed",
				mobileActions.checkElementDisplayed(forgotUserNameLink));

	}

	public void veryForgotPasswordLink() {

		logReporter.log("Check Forgot Password Link displayed",
				mobileActions.checkElementDisplayed(forgotPasswordLink));

	}

	public void verifyNewToMeccaText() {
		By newToMeccaText = By
				.xpath("//a[contains(text(),'Sign Up')]/parent::p[contains(text(),'New to Mecca bingo')]");
		logReporter.log("Check New to Mecca text displayed", mobileActions.checkElementDisplayed(newToMeccaText));
	}

	public void verifySignUpLinkDisplayed() {
		//By linkSignUP = By.xpath("//a[contains(text(),'Sign Up')]");
		By linkSignUP = By.xpath("//a[contains(text(),'Join')]");	
		logReporter.log("Check Sign Up Link displayed", mobileActions.checkElementDisplayed(linkSignUP));

	}

	public void verifyHelpContactDetails() {
		logReporter.log("Check Contact Us section displayed", mobileActions.checkElementDisplayed(contactUsSection));
		logReporter.log("Check Contact Us Call NUmber displayed",
				mobileActions.checkElementDisplayed(contactUsCallNumber));
		logReporter.log("Check Contact Us Email displayed", mobileActions.checkElementDisplayed(contactUsemail));
	}

	public void verifyLiveChat() {
		By liveChat = By.xpath("//p[contains(text(),'Live Help')]/preceding-sibling::span");
		logReporter.log("Check Live Chat displayed", mobileActions.checkElementDisplayed(liveChat));
	}

	public void verifyLogInEnabled() {
		if (!mobileActions.checkElementDisplayed(
				By.xpath("//span[@class='button-content']/parent::button[@type='submit' and  @disabled]")))
			logReporter.log("Check login button Enabled", true);

		// logReporter.log("Check login button Enabled",
		// objmobileActions.checkElementEnabled(btnLogIn));
	}

	public void clickOnRememberMe() {
		logReporter.log("Clcik Remember Me checkbox", mobileActions.click(rememberMeCheckbox));
	}

	public void VerifyTextFromUserName(String useName) {
		logReporter.log("Verify Text from UserName", useName, mobileActions.getAttribute(tbUserName, "value"));
	}

	public void verifyForgottenPasswordHeader() {
		By forgottenPasswordPageHeader = By
				.xpath("//div[contains(@class,'slideout-overlay-heading')]/h4[contains(text(),'Forgot your password')]");
		logReporter.log("Check 'Forgotten Password' header",
				mobileActions.checkElementDisplayed(forgottenPasswordPageHeader));
	}

	public void clickOnForgotPasswordLink() {
		logReporter.log("Click 'Forgot Password'", mobileActions.click(forgotPasswordLink));
	}

	public void verifyBackArrow() {
		By backArrow = By.xpath("//a[contains(@class,'icon-arrow-left')]");
		logReporter.log("Check 'Back Arrow'", mobileActions.checkElementDisplayed(backArrow));

	}

	public void verifyHelperTextFromForgottenPasswordPage(String text) {
		By forgottenPasswordPageHelperText = By.xpath("//div[contains(@class,'slideout-overlay-content')]/p");
		/*logReporter.log("Check 'Forgotten Password additional info'", text,
				mobileActions.getAttribute(forgottenPasswordPageHelperText, "text"));*/

		String currentText = mobileActions.getText(forgottenPasswordPageHelperText);
		System.out.println("***********currentText   "+currentText);
		logReporter.log("Check 'Forgotten Password additional info'", text,
				text.equalsIgnoreCase(currentText));
	}

	public void verifySendResetInstructionsCTA() {
		logReporter.log("Check 'Send Reset Instructions 'CTA'",
				mobileActions.checkElementDisplayed(sendResetInstructions));
	}

	public void clickOnSendResetInstructions() {
		logReporter.log("Click 'Send reset instructions' Link", mobileActions.click(sendResetInstructions));
	}

	public void verifyPasswordResetSuccessMessage() {
		logReporter.log("Check 'Reset Password Sucess'",
				mobileActions.checkElementDisplayed(resetPasswordSuccessHeading));
		logReporter.log("Check 'Reset Password Sucess Message'",
				mobileActions.checkElementDisplayed(resetPasswordSuccessMessage));
	}

	public void verifyIDidnotReceiveAnEmailLink() {
		By iDidnotReceiveAnEmail = By.xpath("//div[contains(@class,'login-wrapper-links')]");
		logReporter.log("Check 'I didnot receive an email' link",
				mobileActions.checkElementDisplayed(iDidnotReceiveAnEmail));
	}

	public void clickOnForgotUsernameLink() {
		logReporter.log("Click 'Forgot username' Link", mobileActions.click(forgotUserNameLink));
	}

	public void verifyForgottenUsernameHeader() {
		By forgottenUsernamePageHeader = By.xpath("//h4[contains(text(),'Forgotten your username')]"); // div[@class='slideout-overlay-heading']/h4[contains(text(),'Forgotten
		// your
		// Username')]
		logReporter.log("Check 'Forgotten Username' header",
				mobileActions.checkElementDisplayed(forgottenUsernamePageHeader));
	}

	public void verifyHelperTextFromForgottenUsernamePage(String text) {
		
		By forgottenUsernamePageHelperText = By.xpath("//div[contains(@class,'slideout-overlay-content')]/p[contains(text(),'Please supply the following account')]");
		//By forgottenUsernamePageHelperText = By.xpath("//div[contains(@class,'slideout-overlay-content')]/p[contains(text(),'"+text+"')]");
		System.out.println("**** forgottenUsernamePageHelperText  :: "+forgottenUsernamePageHelperText);
		logReporter.log("Check 'Forgotten username additional info'", text,
				mobileActions.checkElementDisplayed(forgottenUsernamePageHelperText));
	}

	public void verifyEmailFieldFromForgottenUsername() {
		logReporter.log("Check 'Email' from Forgotten Username", mobileActions.checkElementDisplayed(txtEmailAddress));
	}

	public void verifySendUsernameReminderCTA() {
		logReporter.log("Check 'Send Username Reminder 'CTA'",
				mobileActions.checkElementDisplayed(sendUsernameReminder));
	}

	public void enterEmailAddressInForgotUsernameSection(String emailAddress) {
		logReporter.log("Enter Value in email address > >", mobileActions.setText(txtEmailAddress, emailAddress));
	}

	public void clickOnSendUsernameReminder() {
		logReporter.log("Click 'Send Username Reminder'", mobileActions.click(sendUsernameReminder));
	}

	public void clickOnLoginCTAbutton() {

		logReporter.log("Click on CTA login button", mobileActions.click(btnCTALogIn));
	}

	public void clearUsernameText() {
		logReporter.log("Clear username", mobileActions.clearText(tbUserName));
	}

	public void clearPasswordText() {
		logReporter.log("Clear password", mobileActions.clearText(tbPassword));
	}

	public void clickOnMyAccountButtonFromHeader() {
		logReporter.log("Click on My Account button", mobileActions.click(MyAccountButton));

	}

	public void clickOnLogoutbutton() {
		logReporter.log("Click on logout button", mobileActions.click(LogoutButton));
	}

	//mailinator

	public void enterEmailId(String email)
	{	
		By mailinatoremail = By.xpath("");
		logReporter.log("", mobileActions.setText(mailinatoremail, email));
	}

	public void setUserNameFromConfig() {
		wait.sleep(5);
		mobileActions.clickUsingJS(tbUserName);
		String userName = configuration.getConfig("web.userName");
		logReporter.log("Enter Value in userName > >", mobileActions.setTextWithClear(tbUserName, userName));
	}

	public void setPasswordFromConfig() {
		String password = configuration.getConfig("web.password");
		logReporter.log("Enter Value in password > >", mobileActions.setTextWithClear(tbPassword, password));}



	///
	// Reset Password Link from email
	public void verifyResetPasswordHdrDisplayed()
	{
		By resetPasswordHdr = By.xpath("//h4[text()='Reset password']");

		logReporter.log("Verify Reset Password header is displayed when link is invoked", 
				mobileActions.checkElementDisplayed(resetPasswordHdr));

	}

	public void setNewPassword(String newpwd)
	{
		By inpnewpwd = By.xpath("//input[@id=\"input-password\" and @type=\"password\"]");
		logReporter.log("Set new password as : ", newpwd,
				mobileActions.setText(inpnewpwd, newpwd));
		mobileActions.pressKeybordKeys(inpnewpwd, "tab");

	}
	public void clickSubmitForNewPassword()
	{
		By submitNewPasswordBtn = By.xpath("//button[@type='submit']//span[text()='Reset password']");

		logReporter.log("Click 'Submit' button for reset password", 
				mobileActions.click(submitNewPasswordBtn));
	}

	public void verifyGoToHomepageBTNDisplayed()
	{
		By locator = By.xpath("//a[@class='btn' and text() ='GO TO THE HOMEPAGE']");

		logReporter.log(" Verify 'GO TO THE HOMEPAGE' button displayed on reset password success screen", 
				mobileActions.checkElementDisplayed(locator));
	}

	public void clickContinueToLoginBtn()
	{
		By ContinueToLoginBtn = By.xpath("//a[@class='btn' and text() ='Continue to login']");

		logReporter.log("Click 'Continue to login' button on reset password", 
				mobileActions.click(ContinueToLoginBtn));
	}

	public void verifyPasswordResetSuccessMessageDisplayed() {

		//By locatorYourPasswordReset = By.xpath("//p[contains(text(),'You have successfully reset your password.')]");
		//By locatorYourPasswordReset = By.xpath("//i[contains(@class,'icon-login-tick')]//following-sibling::h4[contains(.,'Your password has been reset. Log in to play')]");
		By locatorYourPasswordReset = By.xpath("//i[contains(@class,'icon-check-mark')]//following-sibling::h4[contains(.,'Your password has been reset. Log in to play')]");
		logReporter.log("Verify 'Your password has been reset. Log in to play' message displayed", 
				mobileActions.checkElementDisplayed(locatorYourPasswordReset));
	}
}
