package com.meccabingo.mobile.page.Mecca;

import org.openqa.selenium.By;

import com.generic.MobileActions;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.Utilities;
import com.generic.utils.WaitMethods;

public class GameWindow {

	By exitarrowbtn = By.xpath("//div[contains(@class,'game-window-header')]/ul/li/a");
	By sessiontime = By.xpath("//div[contains(@class,'game-window-header')]/ul/span/li[2]/i");
	By playforrealbtn = By.xpath("//button[contains(.,'Free Play')]");
	By titleofthegame = By.xpath("//div[contains(@class,'game-window-header')]/ul/span/li[3]");
	By expandbtn = By.xpath("//div[contains(@class,'game-window-header')]/ul/span/li[4]/a/i");
	By reducebtn = By.xpath("//div[contains(@class,'game-window-header')]/ul/span/li[4]/a/i");
	By depositbtn = By.xpath("//div[contains(@class,'game-window-header')]/ul/span/li[5]");
	By myaccountbtn = By.xpath("//div[contains(@class,'game-window-header')]/ul/li[2]");
	By sessiontimewithmmss = By.xpath("//div[contains(@class,'game-window-header')]/ul/span/li[2]/span");
	By plyNowbtnOnGamedetailsPage = By.xpath("(//button[text()='Play Now'])[1]");

	private MobileActions objMobileActions;
	private LogReporter logReporter;
	private WaitMethods wait;
	private Configuration configuration;
	private Utilities objUtilities;
	
	private By getPlayNowBtn(String nameofthegame) {
		return By.xpath("//p[contains(.,'" + nameofthegame
				+ "')]/parent::div/div[contains(@class,'game-panel-buttons')]/apollo-play-game-cta/button[contains(.,'Play Now')]");
	}

	public GameWindow(MobileActions mobileActions, LogReporter logReporter,WaitMethods wait,Configuration configuration,Utilities objUtilities) {
		this.objMobileActions = mobileActions;
		this.logReporter = logReporter;
		this.configuration= configuration;
		this.wait = wait;
		this.objUtilities = objUtilities ;
	}

	public void clickOnPlayNow(String nameofthegame) {
		By playNowbtn = getPlayNowBtn(nameofthegame);
		objMobileActions.checkElementDisplayedWithMidWait(playNowbtn);
		logReporter.log("check element > >", objMobileActions.click(playNowbtn));

	}

	public void clickOnibtnOfHeroTile(String nameofthegame) {
		By ibtn = By.xpath("//apollo-bingo-tile[contains(@game-type,'"+nameofthegame+"')]//i[contains(@class,'info colour')]");
		logReporter.log("click on 'i' > >", objMobileActions.click(ibtn)); 
	}

	//	public void clickOnibtnOfSlotHeroTile(String nameofthegame) {
	//		By ibtn = By.xpath("//p[contains(text(),'" + nameofthegame +"')]/parent::div/preceding-sibling::div/a/i");
	//		logReporter.log("click on 'i' > >", objMobileActions.click(ibtn)); 
	//	}

	public void clickOnibtnOfGameTile(String nameofthegame) {
		By ibtn = By.xpath("//apollo-bingo-tile[contains(@game-type,'"+nameofthegame+"')]//i[contains(@class,'info colour')]");
		logReporter.log("click on 'i' > >", objMobileActions.click(ibtn)); // change apollo-slot-hero-tile for hero tile
	}

	public void clickOnibtnOfBingoTile(String nameofthegame) {
		By ibtn = By.xpath("//apollo-bingo-tile[contains(@game-type,'"+nameofthegame+"')]//i[contains(@class,'info colour')]");
		logReporter.log("click on 'i' > >", objMobileActions.click(ibtn)); // change apollo-slot-hero-tile for hero tile
	}

	public void clickExpandbutton() {
		logReporter.log("click on expand button > >", objMobileActions.click(expandbtn));

	}

	public void clickReducebutton() {
		logReporter.log("click on reduce button > >", objMobileActions.click(reducebtn));
	}

	public void clickOnFreePlayBtn() {
		By freeplaybtn = By.xpath("//button[contains(.,'Free Play')]");
		logReporter.log("click on 'Free play button' > >", objMobileActions.click(freeplaybtn));
	}

	public void verifyExitArrowBtn() {
		logReporter.log("check element > >", objMobileActions.checkElementDisplayed(exitarrowbtn));
	}

	public void verifySessionTime() {
		logReporter.log("check element > >", objMobileActions.checkElementDisplayed(sessiontime));
	}

	public void verifyNonSessionTime() {
		if (!objMobileActions.checkElementDisplayed(sessiontime))
			logReporter.log("check element > >", true);
	}

	public void verifyPlayforRealBtn() {
		logReporter.log("check element > >", objMobileActions.checkElementDisplayed(playforrealbtn));
	}

	public void verifyTitleoftheGame() {
		logReporter.log("check element > >", objMobileActions.checkElementDisplayed(titleofthegame));
	}

	public void verifyExpandBtn() {
		logReporter.log("check element > >", objMobileActions.checkElementDisplayed(expandbtn));
	}

	public void verifyDepositBtn() {
		logReporter.log("check element > >", objMobileActions.checkElementDisplayed(depositbtn));
	}

	public void verifyMyaccountBtn() {
		logReporter.log("check element > >", objMobileActions.checkElementDisplayed(myaccountbtn));
	}

	public void verifyDemoheadercomponents(String options) {

		switch (options) {

		case "“Exit game arrow": {
			logReporter.log("check element > >", objMobileActions.checkElementDisplayed(exitarrowbtn));
			break;
		}

		case "Play for Real CTA": {
			logReporter.log("check element > >", objMobileActions.checkElementDisplayed(playforrealbtn));
			break;
		}

		case "Title of the game": {
			logReporter.log("check element > >", objMobileActions.checkElementDisplayed(titleofthegame));
			break;
		}

		case "Expand CTA": {
			logReporter.log("check element > >", objMobileActions.checkElementDisplayed(expandbtn));
			break;
		}
		case "Deposit CTA": {
			logReporter.log("check element > >", objMobileActions.checkElementDisplayed(depositbtn));
			break;
		}
		case "My account CTA": {
			logReporter.log("check element > >", objMobileActions.checkElementDisplayed(myaccountbtn));
			break;
		}
		}
	}

	public void imageAtExpandbtn() {
		/*	Boolean isFullScreen = false;
		Object result = objMobileActions.executeJavascript("return document.fullscreen");
		if (result instanceof Boolean) {
			isFullScreen = (Boolean) result;
		}
		logReporter.log("check element > >", isFullScreen);*/

	}

	public void imageAtReducebtn() {
		/*Boolean isFullScreen = true;
		Object result = objMobileActions.executeJavascript("return document.fullscreen");
		if (result instanceof Boolean) {
			isFullScreen = (Boolean) result;
		}
		logReporter.log("check element > >", !isFullScreen);*/
	}

	public void verifyHeaderInGame() {
		By header = By.xpath("//div[contains(@class,'game-window-header')]");
		logReporter.log("check element > >", objMobileActions.checkElementDisplayed(header));

	}

	public void verifyBackroungimageInGame() {

		By bagroundimage = By.xpath("//*[contains(@class,'game-window-image')]"); // *[contains(@id,'Desktop')]/body/canvas
		logReporter.log("check element > >", objMobileActions.checkElementDisplayed(bagroundimage));
	}

	public void clickOnImageOfHerotile(String nameofthegame) {
		By image = By.xpath("//apollo-bingo-tile[contains(@game-type,'"+nameofthegame+"')]//img");
		if (!objMobileActions.click(image)) {
			logReporter.log("Check element", true);
		}
	}

	public void clickOnTitleOfHerotile(String nameofthegame) {
		By title = By.xpath("//apollo-bingo-tile[contains(@game-type,'"+nameofthegame+"')]//p[contains(text(),'" + nameofthegame + "')]");
		if (!objMobileActions.click(title)) {
			logReporter.log("Check element", true);
		}
	}

	public void clickOnDescriptionOfHerotile(String nameofthegame) {
		By description = By.xpath("//apollo-bingo-tile[contains(@game-type,'"+nameofthegame+"')]////p[contains(text(),'" + nameofthegame + "')]/parent::div/p[2]");
		if (!objMobileActions.click(description)) {
			logReporter.log("Check element", true);
		}
	}

	public void clickOnPrizeofHerotile() {
		By prizebtn = By.xpath("//apollo-bingo-tile[contains(@game-type,'Best Odds Bingo')]//i[contains(@class,'money-bag')]");
		if (!objMobileActions.click(prizebtn)) {
			logReporter.log("Check element", true);
		}
	}

	public void clickOnPlayNowOnGameDetailsPage() {
		logReporter.log("play now button on games detail page", objMobileActions.click(plyNowbtnOnGamedetailsPage));
	}

	public void clickJoinNowOfFirstBingoGame() {

	}

	public void clickExitGameCTA() {
		objMobileActions.processMobileElement(exitarrowbtn);
		if (objMobileActions.checkElementDisplayedWithMidWait(exitarrowbtn))
			logReporter.log("Click Exit Game> >", objMobileActions.click(exitarrowbtn));
	}

	public void clickMyAccFromGame() {
		By locator = By.xpath("//span[text()='Deposit']/following::i[contains(@class,'my-account')]");
		objMobileActions.processMobileElement(locator);
		if (objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("Click my acc from game> >", objMobileActions.click(locator));
	}

	public void navigateThroughHamburgerMenu(String strMenuName) {
		//By hamburgerMenu = By.xpath("//apollo-top-navigation");
		//if (objMobileActions.checkElementDisplayedWithMidWait(hamburgerMenu))
		//	logReporter.log("Click hamburger menu> >", objMobileActions.click(hamburgerMenu));

		By locator = By.xpath("//a[contains(@class,'top-navigation-link') and contains(@href,'" + strMenuName + "')]");
		logReporter.log("Click menu> >", objMobileActions.click(locator));
	}

	public void clickOnPlayNow() {
		By playNowbtn = By
				.xpath("//h2[text()='Slots & Games']/following::apollo-slot-tile[1]//button[text()='Play Now']");
		objMobileActions.processMobileElement(playNowbtn);
		//objMobileActions.androidScrollToElement(playNowbtn);
		if (objMobileActions.checkElementDisplayed(playNowbtn))
			logReporter.log("click element play now > >", objMobileActions.click(playNowbtn));
	}




	public void verifySessionTimeInmmssformat() {

		logReporter.log("check element > >", objMobileActions.checkElementDisplayedWithMidWait(sessiontimewithmmss));
	}


	public void clickOnPlayforRealBtn() {
		if (objMobileActions.checkElementDisplayedWithMidWait(playforrealbtn))
			logReporter.log("check element > >", objMobileActions.click(playforrealbtn));
	}

	public void verifyNonExpandBtn() {
		if (!objMobileActions.checkElementDisplayedWithMidWait(expandbtn))
			logReporter.log("check element > >", true);
	}

	public void usernameDisplayed(String strUName) {
		strUName = strUName.toUpperCase();
		By locator = By.xpath("//h4[text()='" + strUName + "']");
		logReporter.log("Check username in header : " + strUName + "> >",
				objMobileActions.checkElementDisplayedWithMidWait(locator));
	}

	public void clickInfoOfFirstGame(String sectionName) {
		/*By locator = By.xpath("(//h2[text()='Slots & Games']//following::a/i)[1]");
		logReporter.log("Click i> >", objMobileActions.click(locator));*/
		By locator = By.xpath("(//h2[text()='"+sectionName+"']//following::a/i)[1]");
		wait.sleep(5);
		logReporter.log("Click info icon > >", objMobileActions.clickUsingJS(locator));
	}

	public void clickOnPlayNowFromGameDetails() {
		By playNowbtn = By.xpath("(//button[text()='Play Now'])[1]");
		logReporter.log("click element play now > >", objMobileActions.click(playNowbtn));

	}

	public void clickOnImageOfTile(String nameofthegame) {
		By image = By.xpath("//p[contains(text(),'" + nameofthegame + "')]/parent::div/preceding-sibling::div/img");
		logReporter.log("click on 'image' > >", objMobileActions.click(image));
	}

	public void clickOnTitleoftheGame(String nameofthegame) {
		By titleofthegame = By.xpath("//p[contains(text(),'" + nameofthegame + "')]");
		if (!objMobileActions.click(titleofthegame)) {
			logReporter.log("Check element", true);
		}
	}

}
