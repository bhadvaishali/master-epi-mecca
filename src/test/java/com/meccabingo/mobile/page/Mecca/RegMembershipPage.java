package com.meccabingo.mobile.page.Mecca;

import org.openqa.selenium.By;

import com.generic.MobileActions;
import com.generic.logger.LogReporter;
import com.generic.utils.Utilities;
import com.generic.utils.WaitMethods;
import com.mifmif.common.regex.Generex;

public class RegMembershipPage {
	
	private MobileActions objMobileActions;
	private LogReporter logReporter;
	private Utilities utilities;
	private WaitMethods wait;

	public RegMembershipPage(MobileActions objMobileActions, LogReporter logReporter, WaitMethods wait, Utilities utilities) {
		this.objMobileActions = objMobileActions;
		this.logReporter = logReporter;
		this.utilities = utilities;
		this.wait = wait;

	}
	
	String greencolor = "#5ea85a";
	String greycolor = "#e6dae6";
	String redcolor = "#e22c2c";
	By locator = By.xpath("");
	
	
	By title = By.xpath("//li/span/p[contains(text(),'Mr')]");
	By titletext = By.xpath("//li/span/p[contains(text(),'Title')]");
	By name = By.xpath("//li/span/p[contains(text(),'John Ray')]");
	By nametext = By.xpath("//li/span/p[contains(text(),'Name')]");
	By DOB = By.xpath("//li/span/p[contains(text(),'5th June 1996')]");
	By dateofbirthtext = By.xpath("//li/span/p[contains(text(),'Date of Birth')]");
	
	By username = By.xpath("//input[contains(@id,'username')]");
	By password = By.xpath("//input[contains(@id,'password')]");
	By selectAll = By.xpath("//button[contains(@class,'colour-button-green')]");
	By email = By.xpath("//input[contains(@id,'gdpr-email')]");
	By sms = By.xpath("//input[contains(@id,'gdpr-sms')]");
	By phone = By.xpath("//input[contains(@id,'gdpr-phone')]");
	By post = By.xpath("//input[contains(@id,'gdpr-post')]");
	By offersandcommunication = By.xpath("//label[text()=\"I’d rather not receive offers and communications\"]");
	By depositlimit = By.xpath("");
	By MPC = By.xpath("//input[contains(@id,'gdpr-not-receive')]");
	By register = By.xpath("//button/span[contains(text(),'Register')]");
	By successmessage = By.xpath("");
	By EmailAdrress = By.xpath("//input[contains(@type,'email')]");
	
	By errorofemail = By.xpath("//ul[@class='errors']/li[text()='You must enter a valid email address']");
	By errorofmobile = By.xpath("//ul[@class='errors']/li[text()='Please enter a valid phone number between 8 and 11 digits long']");
	By errorofusername = By.xpath("//ul[@class='errors']/li[text()='Your username must be between 6-15 characters long.']");
	By errorofpassword = By.xpath("");
	By errorofmarketingpreferencefield = By.xpath("//ul[@class='errors']/li[text()='Please select your contact preferences ']");
	By errorofpostcode = By.xpath("//ul[@class='errors']/li[text()='Please enter your postcode']");
	By countryname = By.xpath("");
	By postcodename = By.xpath("");
	By gibcountry = By.xpath("");
	By agecheckboxofmembership = By.xpath("//*[@id=\"id-agreed\"]");
	
	
	public void verify() {
		logReporter.log("", objMobileActions.checkElementDisplayed(locator));
	}  
	public void clickOn() {
		logReporter.log("", objMobileActions.click(locator));
	}	
	public void verifyTitle() {
		logReporter.log("", objMobileActions.checkElementDisplayed(title));
		logReporter.log("", objMobileActions.checkElementDisplayed(titletext));
	}
	public void verifyName() {
		logReporter.log("", objMobileActions.checkElementDisplayed(name));
		logReporter.log("", objMobileActions.checkElementDisplayed(nametext));
	}
	
	public void verifyDateofBirth() {
		logReporter.log("", objMobileActions.checkElementDisplayed(DOB));
		logReporter.log("", objMobileActions.checkElementDisplayed(dateofbirthtext));
	}
	
	public void verifyUsernameTextbox() {
		logReporter.log("", objMobileActions.checkElementDisplayed(username));
	}
	public void verifyPasswordTextbox() {
		logReporter.log("", objMobileActions.checkElementDisplayed(password));
	}
	public void verifySelectAllButton() {
		logReporter.log("", objMobileActions.checkElementDisplayed(selectAll));	
	}
	public void verifyCheckboxes(String options) {
		switch (options) {
		case "Email":
			logReporter.log("", objMobileActions.checkElementDisplayed(email));
			break;
		case "SMS":
			logReporter.log("", objMobileActions.checkElementDisplayed(sms));
			break;
		case "Phone":
			logReporter.log("", objMobileActions.checkElementDisplayed(phone));
			break;
		case "Post":
			logReporter.log("", objMobileActions.checkElementDisplayed(post));
			break;

		}

	}
	public void verifyOffersCheckbox() {
		logReporter.log("", objMobileActions.checkElementDisplayed(offersandcommunication));
	} 
	public void verifyDepositLimitSection() {
		logReporter.log("", objMobileActions.checkElementDisplayed(depositlimit));
	}  
	public void clickOnSelectAllCTA() {
		logReporter.log("", objMobileActions.click(selectAll));
	}
	public void clickOnMarketingPreferenceCheckbox() {
		logReporter.log("", objMobileActions.click(MPC));
	}	
	public void clickOnRegisterButton() {
		logReporter.log("", objMobileActions.click(register));
	}
	public void verifySuccessMessage() {
		logReporter.log("", objMobileActions.checkElementDisplayed(successmessage));
	}
	public void verifyCheckboxIsNotSelected() {
		if (!objMobileActions.selectCheckbox(email, false)) {
			logReporter.log("Check element", true);
		}
	}  
	public void verifyRedColorBelowEmailAdrress() {
		logReporter.log("Check color of textbox bottom border > >",
				objMobileActions.checkCssValue(EmailAdrress, "border-bottom-color", redcolor));
	}
	public void enterInvalidEmailAdrress() {
		logReporter.log("enter invalid email", objMobileActions.setText(EmailAdrress, "20ehc.mailianator.com"));
	}
	public void verifyErrorMessageBelowEmailAdrress() {
		logReporter.log("", objMobileActions.checkElementDisplayed(errorofemail));
	}
	public void verifyErrorMessageBelowEmailAdrressInRedColor() {
		logReporter.log("", objMobileActions.checkCssValue(errorofemail, "color", redcolor));
	}
	public void verifyErrorMessageBelowMobilenumber() {
		logReporter.log("", objMobileActions.checkElementDisplayed(errorofmobile));
	}
	
	public void verifyErrorMessageBelowUsername() {
		logReporter.log("", objMobileActions.checkElementDisplayed(errorofusername));
	}
	public void verifyErrorMessageBelowPassword() {
		logReporter.log("", objMobileActions.checkElementDisplayed(errorofpassword));
	}
	public void enterExistingUsername(String name) {
		logReporter.log("", objMobileActions.setTextWithClear(username, name));
	}
	public void verifyErrorMessageBelowMarketingPreferenceField() {
		logReporter.log("", objMobileActions.checkElementDisplayed(errorofmarketingpreferencefield));
	}
	public void verifyErrorMessageBelowPostcode() {
		logReporter.log("", objMobileActions.checkElementDisplayed(errorofpostcode));
	}
	public void verifyCountryNameIsDisplayed() {
		logReporter.log("", objMobileActions.checkElementDisplayed(countryname));
	}
	public void verifyPostcodeIsDisplayed() {
		logReporter.log("", objMobileActions.checkElementDisplayed(postcodename));
	}
	public void clickOnGibRoiCountry() {
		By option = By.xpath("//option[text()='Gibraltar']");
		logReporter.log("Select country", objMobileActions.click(option));
	}
	public void clickOnAgeCheckboxofMembership() {
		logReporter.log("", objMobileActions.clickUsingJS(agecheckboxofmembership));
	}
	public void clickOnNotYouLink() {
		By notyou = By.xpath("//a[contains(text(),'Not You')]");
		logReporter.log("click on Not You link > >", objMobileActions.click(notyou));
	}
	
	public void clickOnDisableRegisterCTA() {
		By register = By.xpath("//button[contains(.,'Register')]");
		logReporter.log("click on post checkbox > >", objMobileActions.click(register));
	}
	
	public void verifyPostcodeField() {
		By postcode = By.xpath("//input[contains(@id,'address-lookup-search')]"); 
		logReporter.log("verify countrys field", objMobileActions.checkElementDisplayed(postcode));
	}
	
	public void verifyLink(String text) {
		By locator = By.xpath("//a[contains(text(),'"+text+"')]");
		logReporter.log("Verify link", objMobileActions.checkElementDisplayed(locator));
	}
	public void ClickOnLink(String text) {
		By locator = By.xpath("//a[contains(text(),'"+text+"')]");
		logReporter.log("Verify link", objMobileActions.click(locator));
	}
	public void verifyRedColorBelowMobileNumber() {
		By mobilenumber = By.xpath("//input[contains(@id,'mobile')]");
		logReporter.log("Check color of textbox bottom border > >",
				objMobileActions.checkCssValue(mobilenumber, "border-bottom-color", "#E22C2C"));
	}
	
	public void enterInvalidMobileNumber() {
		By mobilenumber = By.xpath("//input[contains(@id,'mobile')]");
		String regex = "[a-zA-Z]{8}";
		String randomnumber = new Generex(regex).random();
		logReporter.log("enter mobile number", objMobileActions.setText(mobilenumber, randomnumber));
	}
	
	public void verifyRedColorBelowUsername() {
		logReporter.log("Check color of textbox bottom border > >",
				objMobileActions.checkCssValue(username, "border-bottom-color", redcolor));
	}
	
	public void verifyFieldUnderYourPersonalDetailsSection(String fieldNm)
	{
		By locator = By.xpath("//h5[contains(.,'Your personal details')]//following-sibling::li//span//p[text()='"+fieldNm+"']");
		logReporter.log("Verify ' "+fieldNm+ " ' is displayed under Your Personal Details Section", objMobileActions.checkElementDisplayed(locator));
	}
}
