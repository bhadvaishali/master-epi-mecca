package com.meccabingo.mobile.page.Mecca;

import org.openqa.selenium.By;

import com.generic.MobileActions;
import com.generic.WebActions;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.WaitMethods;
import com.generic.webdriver.DriverProvider;

/**
 * @author Harshvardhan Yadav, Namrata Donikar (Expleo)
 *
 */
public class FooterPage {

	By linkTwitter = By.xpath("//a[@rel='Twitter']"); // a[@title='Twitter']
	By linkFacebook = By.xpath("//a[@rel='Facebook']");
	By linkYouTube = By.xpath("//a[@rel='YouTube']");
	By linkInstagram = By.xpath("//a[@rel='Instagram']");

	By txtTwitter = By.xpath("//span[@class='footer-social-text']/span[contains(text(),'Twitter')]"); // p[@class='footer-social-text']/span[contains(text(),'Twitter')]
	By txtFacebook = By.xpath("//span[@class='footer-social-text']/span[contains(text(),'Facebook')]");
	By txtYouTube = By.xpath("//span[@class='footer-social-text']/span[contains(text(),'YouTube')]");
	By txtInstagram = By.xpath("//span[@class='footer-social-text']/span[contains(text(),'Instagram')]");

	By imageTwitter = By.xpath("//img[@alt='Twitter']");
	By imageFacebook = By.xpath("//img[@alt='Facebook']");
	By imageYouTube = By.xpath("//img[@alt='YouTube']");
	By imageInstagram = By.xpath("//img[@alt='Instagram']");

	By descriptionTwitter = By
			.xpath("//span[@class='footer-social-text']/span[contains(text(),'Twitter')]//preceding-sibling::span"); // p[@class='footer-social-text']/span[contains(text(),'Twitter')]//preceding-sibling::span
	By descriptionFacebook = By
			.xpath("//span[@class='footer-social-text']/span[contains(text(),'Facebook')]//preceding-sibling::span");
	By descriptionYouTube = By
			.xpath("//span[@class='footer-social-text']/span[contains(text(),'YouTube')]//preceding-sibling::span");
	By descriptionInstagram = By
			.xpath("//span[@class='footer-social-text']/span[contains(text(),'Instagram')]//preceding-sibling::span");

	By linkPrivacyPolicy = By.xpath("//a[contains(text(),'Privacy Policy')]"); // a[contains(text(),'Privacy Policy')]
	By linkTermsAOndConditions = By.xpath("//a[contains(text(),'Terms and Conditions')]");
	By linkOurMeccaPromise = By.xpath("//a[contains(text(),'Our Mecca Promise')]");
	By linkAffiliates = By.xpath("//a[contains(text(),'Affiliates')]");
	By linkMeccaClubTerms = By.xpath("//a[contains(text(),'Mecca Club Terms')]");
	By linkPlayOnlineCasino = By.xpath("//a[contains(text(),'Play Online Casino')]");
	By linkMeccaBlog = By.xpath("//a[contains(text(),'Mecca Blog')]");

	By logo_Essa = By.xpath("//a[@title='ESSA']//img");
	By logo_Ibsa = By.xpath("//a[@title='IBAS']//img");
	By logo_18 = By.xpath("//img[@alt='Over 18s only']");
	By logo_GamblingControl = By.xpath("//a[@title='Gambling Control']//img");
	By logo_GamCare = By.xpath("//a[@title='GamCare']//img");
	By logo_GamStop = By.xpath("//a[@title='GamStop']//img");
	By logo_GamblingCommission = By.xpath("//a[@title='Gambling Commission']//img");
	By logo_saferGamblingStandard = By.xpath("//a[@title='Safer Gambling Standard']//img");

	By logo_VeriSignSecured = By.xpath("//img[contains(@title,'Verisign')]"); // a[@title='Verisign Title']/img

	By paymentTitleBlock = By.xpath("//section[contains(@class,'footer-payments')]//h2[contains(.,'Secure Payments')]"); // div[@class='footer-payments-header']
	By paymentDescriptionBlock = By.xpath("//section[contains(@class,'footer-payments')]//h5[contains(.,'We use 128bit encryption and accept all major card providers.')]"); // div[@class='footer-payments-subheader']
	By paymentLogosBlock = By.xpath("//li[contains(@class,'footer-payment')]"); // ul[@class='footer-payments-list']

	By link_Alderney_Gambling_Control_Commission = By
			.xpath("//a[contains(text(),'Alderney Gambling Control Commission')]");
	By link_UK_Gambling_Commission = By.xpath("//a[contains(@title,'Gambling Commission')]");
	By link_BeGambleAware = By.xpath("//a[contains(@href,'begambleaware')]");
	
	By link_Rank_Group = By.xpath("//div//a[contains(@title,'Rank Group')]//img");

	private MobileActions objMobileActions;
	private AppiumDriverProvider objDriverProvider;
	private LogReporter logReporter;
	private WaitMethods waitMethods;
	private Configuration configuration;
	
	public FooterPage(MobileActions objMobileActions, AppiumDriverProvider driverProvider, LogReporter logReporter,
			WaitMethods waitMethods,Configuration configuration) {
		this.objMobileActions = objMobileActions;
		this.objDriverProvider = driverProvider;
		this.logReporter = logReporter;
		this.waitMethods = waitMethods;
		this.configuration= configuration;
	}

	public void verifySocialMediaComponents(String text) {

		switch (text) {

		case "Twitter": {
			logReporter.log("check element > >", objMobileActions.checkElementDisplayed(linkTwitter));
			break;
		}

		case "Facebook": {
			logReporter.log("check element > >", objMobileActions.checkElementDisplayed(linkFacebook));
			break;
		}

		case "YouTube": {
			logReporter.log("check element > >", objMobileActions.checkElementDisplayed(linkYouTube));
			break;
		}

		case "Instagram": {
			logReporter.log("check element > >", objMobileActions.checkElementDisplayed(linkInstagram));
			break;
		}

		}
	}

	public void verifyImageAndTextOfSocial() {

		logReporter.log("check element > >", objMobileActions.checkElementDisplayed(imageTwitter));
		logReporter.log("check element > >", objMobileActions.checkElementDisplayed(imageFacebook));
		logReporter.log("check element > >", objMobileActions.checkElementDisplayed(imageYouTube));
		logReporter.log("check element > >", objMobileActions.checkElementDisplayed(imageInstagram));

		logReporter.log("check element > >", objMobileActions.checkElementDisplayed(txtTwitter));
		logReporter.log("check element > >", objMobileActions.checkElementDisplayed(txtFacebook));
		logReporter.log("check element > >", objMobileActions.checkElementDisplayed(txtYouTube));
		logReporter.log("check element > >", objMobileActions.checkElementDisplayed(txtInstagram));

		logReporter.log("check element > >", objMobileActions.checkElementDisplayed(descriptionTwitter));
		logReporter.log("check element > >", objMobileActions.checkElementDisplayed(descriptionFacebook));
		logReporter.log("check element > >", objMobileActions.checkElementDisplayed(descriptionYouTube));
		logReporter.log("check element > >", objMobileActions.checkElementDisplayed(descriptionInstagram));
	}

	public void clickFacebookBlock() {
		logReporter.log("click 'Facebook' > >", objMobileActions.click(linkFacebook));
	}

	public void clickTwitterBlock() {
		logReporter.log("click 'Twitter' > >", objMobileActions.click(linkTwitter));
	}

	public void clickYouTubeBlock() {
		logReporter.log("click 'YouTube' > >", objMobileActions.click(linkYouTube));
	}

	public void clickInstagramBlock() {
		logReporter.log("click 'Instagram' > >", objMobileActions.click(linkInstagram));
	}

	public boolean verifyUrl(String url) {
		if (objDriverProvider.getAppiumDriver().getCurrentUrl().equals(url))
			return true;
		else
			return false;
	}

	public void mouseHoverOnFacebook() {
		objMobileActions.mouseHover(linkFacebook);
	}

	public void verifyColorCode(String area, String colorCode) {

		switch (area) {

		case "Twitter": {
			logReporter.log("check CSS property > >", objMobileActions.checkCssValue(linkTwitter, "color", colorCode));
			break;
		}

		case "Facebook": {
			logReporter.log("check CSS property > >", objMobileActions.checkCssValue(linkFacebook, "color", colorCode));
			break;
		}

		case "YouTube": {
			logReporter.log("check CSS property > >", objMobileActions.checkCssValue(linkYouTube, "color", colorCode));
			break;
		}

		case "Instagram": {
			logReporter.log("check CSS property > >", objMobileActions.checkCssValue(linkInstagram, "color", colorCode));
			break;
		}
		}

	}

	public void mouseHoverOnTwitter() {
		objMobileActions.mouseHover(linkTwitter);
	}

	public void mouseHoverOnInstagram() {
		objMobileActions.mouseHover(linkInstagram);
	}

	public void mouseHoverOnYouTube() {
		objMobileActions.mouseHover(linkYouTube);
	}

	public void setBrowserSize(String width, String height) {
		getElementSizeBeforeResize();
		objMobileActions.setBrowserWindowSize(width, height);
		// logReporter.log("Set browser window > >",
		// objMobileActions.setBrowserWindowSize(width, height));
		// objUtilities.assertEquals("Set browser window > >", true,
		// objMobileActions.setBrowserWindowSize(width, height));
	}

	private int getFontSize(By locator) {
		return Integer.parseInt(objMobileActions.getCssValue(locator, "font-size").split("px")[0]);
	}

	public void verifyFontSizeOfElements(String text) {

		switch (text) {

		case "Twitter": {
			this.checkFontSizeOfTwitter("name");
			this.checkFontSizeOfTwitter("description");
			this.checkFontSizeOfTwitter("logo");
			break;
		}

		case "Facebook": {
			this.checkFontSizeOfFacebook("name");
			this.checkFontSizeOfFacebook("description");
			this.checkFontSizeOfFacebook("logo");
			break;
		}

		case "YouTube": {
			this.checkFontSizeOfYouTube("name");
			this.checkFontSizeOfYouTube("description");
			this.checkFontSizeOfYouTube("logo");
			break;
		}

		case "Instagram": {
			this.checkFontSizeOfInstagram("name");
			this.checkFontSizeOfInstagram("description");
			this.checkFontSizeOfInstagram("logo");
			break;
		}

		}
	}

	private int font_TwitterText = 0;
	private int font_TwitterDescription = 0;
	private int font_TwitterLogo = 0;

	private int font_FacebookText = 0;
	private int font_FacebookDescription = 0;
	private int font_FacebookLogo = 0;

	private int font_YouTubeText = 0;
	private int font_YouTubeDescription = 0;
	private int font_YouTubeLogo = 0;

	private int font_InstagramText = 0;
	private int font_InstagramDescription = 0;
	private int font_InstagramLogo = 0;

	private void getElementSizeBeforeResize() {

		font_TwitterText = this.getFontSize(txtTwitter);
		font_TwitterDescription = this.getFontSize(descriptionTwitter);
		font_TwitterLogo = objMobileActions.getWidth(imageTwitter);

		font_FacebookText = this.getFontSize(txtFacebook);
		font_FacebookDescription = this.getFontSize(descriptionFacebook);
		font_FacebookLogo = objMobileActions.getWidth(imageFacebook);

		font_YouTubeText = this.getFontSize(txtYouTube);
		font_YouTubeDescription = this.getFontSize(descriptionYouTube);
		font_YouTubeLogo = objMobileActions.getWidth(imageYouTube);

		font_InstagramText = this.getFontSize(txtInstagram);
		font_InstagramDescription = this.getFontSize(descriptionInstagram);
		font_InstagramLogo = objMobileActions.getWidth(imageInstagram);
	}

	private void checkFontSizeOfTwitter(String text) {
		switch (text) {
		case "name": {
			if (this.font_TwitterText > this.getFontSize(txtTwitter)) {

				logReporter.log("Check Twitter text font size after window resize: ", true);
			} else
				logReporter.log("Check Twitter text font size after window resize: ", false);
			break;
		}

		case "description": {
			if (this.font_TwitterDescription > this.getFontSize(descriptionTwitter)) {
				logReporter.log("Check Twitter description font size after window resize: ", true);
			} else
				logReporter.log("Check Twitter description font size after window resize: ", false);
			break;
		}

		case "logo": {
			if (this.font_TwitterLogo > objMobileActions.getWidth(imageTwitter)) {
				logReporter.log("Check Twitter logo size after window resize: ", true);
			} else
				logReporter.log("Check Twitter logo size after window resize: ", false);
			break;
		}

		}

	}

	private void checkFontSizeOfFacebook(String text) {
		switch (text) {
		case "name": {
			if (this.font_FacebookText > this.getFontSize(txtFacebook)) {
				logReporter.log("Check Facebook text font size after window resize: ", true);
			} else
				logReporter.log("Check Facebook text font size after window resize: ", false);
			break;
		}

		case "description": {
			if (this.font_FacebookDescription > this.getFontSize(descriptionFacebook)) {
				logReporter.log("Check Facebook description font size after window resize: ", true);
			} else
				logReporter.log("Check Facebook description font size after window resize: ", false);
			break;
		}

		case "logo": {
			if (this.font_FacebookLogo > this.getFontSize(imageFacebook)) {
				logReporter.log("Check Facebook logo size after window resize: ", true);
			} else
				logReporter.log("Check Facebook logo size after window resize: ", false);
			break;
		}

		}

	}

	private void checkFontSizeOfYouTube(String text) {
		switch (text) {
		case "name": {
			if (this.font_YouTubeText > this.getFontSize(txtYouTube)) {

				logReporter.log("Check YouTube text font size after window resize: ", true);
			} else
				logReporter.log("Check YouTube text font size after window resize: ", false);
			break;
		}

		case "description": {
			if (this.font_YouTubeDescription > this.getFontSize(descriptionYouTube)) {
				logReporter.log("Check YouTube description font  size after window resize: ", true);
			} else
				logReporter.log("Check YouTube description font size after window resize: ", false);
			break;
		}

		case "logo": {
			if (this.font_YouTubeLogo > this.getFontSize(imageYouTube)) {
				logReporter.log("Check YouTube logo size after window resize: ", true);
			} else
				logReporter.log("Check YouTube logo size after window resize: ", false);
			break;
		}

		}

	}

	private void checkFontSizeOfInstagram(String text) {
		switch (text) {
		case "name": {
			if (this.font_InstagramText > this.getFontSize(txtInstagram)) {

				logReporter.log("Check Instagram Text font size after window resize: ", true);
			} else
				logReporter.log("Check Instagram Text font size after window resize: ", false);
			break;
		}

		case "description": {
			if (this.font_InstagramDescription > this.getFontSize(descriptionInstagram)) {
				logReporter.log("Check Instagram Description font block size after window resize: ", true);
			} else
				logReporter.log("Check Instagram Description font size after window resize: ", false);
			break;
		}

		case "logo": {
			if (this.font_InstagramLogo > this.getFontSize(imageInstagram)) {
				logReporter.log("Check Instagram Logo size after window resize: ", true);
			} else
				logReporter.log("Check Instagram Logo size after window resize: ", false);
			break;
		}

		}

	}
	public void clickOnUsefulLinks(String lnk)
	{
		By lnks = By.xpath("//ul[contains(@class,'useful-links')]//li//a[text()='"+lnk+"']");
		logReporter.log("Click on  " +lnk+ "under userful link section ", 
				objMobileActions.click(lnks));
	}
	public void verifyUsefulLinksInFooter(String text) {
		By lnks = By.xpath("//ul[contains(@class,'useful-links')]//li//a[text()='"+text+"']");
		logReporter.log("verify  " +text+ "under userful link section ", 
				objMobileActions.checkElementDisplayed(lnks));
		/*switch (text) {

		case "Privacy Policy": {
			logReporter.log("Check Privacy Policy Link > >", objMobileActions.checkElementDisplayed(linkPrivacyPolicy));
			break;
		}

		case "Terms and Conditions": {
			logReporter.log("Check Privacy Policy Link > >",
					objMobileActions.checkElementDisplayed(linkTermsAOndConditions));
			break;
		}

		case "Our Mecca Promise": {
			logReporter.log("Check Privacy Policy Link > >", objMobileActions.checkElementDisplayed(linkOurMeccaPromise));
			break;
		}

		case "Affiliates": {
			logReporter.log("Check Privacy Policy Link > >", objMobileActions.checkElementDisplayed(linkAffiliates));
			break;
		}

		case "Mecca Club Terms": {
			logReporter.log("Check Privacy Policy Link > >", objMobileActions.checkElementDisplayed(linkMeccaClubTerms));
			break;
		}
		case "Play Online Casino": {
			logReporter.log("Check Privacy Policy Link > >", objMobileActions.checkElementDisplayed(linkPlayOnlineCasino));
			break;
		}
		case "Mecca Blog": {
			logReporter.log("Check Privacy Policy Link > >", objMobileActions.checkElementDisplayed(linkMeccaBlog));
			break;
		}
		}*/
	}

	public void clickPrivacyPolicyLink() {
		logReporter.log("click 'Privacy Policy Link' > >", objMobileActions.click(linkPrivacyPolicy));
	}

	public void clickTermsAndConditionsLink() {
		logReporter.log("click 'Terms And Conditions Link' > >", objMobileActions.click(linkTermsAOndConditions));
	}

	public void clickOurMeccaPromiseLink() {

		if (objMobileActions.checkElementDisplayed(linkOurMeccaPromise)) {
			logReporter.log("click 'Our Mecca Promise Link' > >", objMobileActions.click(linkOurMeccaPromise));
		}
	}

	public void clickAffiliatesLink() {

		if (objMobileActions.checkElementDisplayed(linkAffiliates)) {
			logReporter.log("click 'Affiliates Link' > >", objMobileActions.click(linkAffiliates));
		}
	}

	public void clickMeccaClubTermsLink() {
		if (objMobileActions.checkElementDisplayed(linkMeccaClubTerms)) {
			logReporter.log("click 'Club Terms Link' > >", objMobileActions.click(linkMeccaClubTerms));
		}

	}

	public void clickPlayOnlineCasinoLink() {
		if (objMobileActions.checkElementDisplayed(linkPlayOnlineCasino)) {
			logReporter.log("click 'Privacy Policy Link' > >", objMobileActions.click(linkPlayOnlineCasino));
		}
	}

	public void clickmeccaBlogLink() {
		if (objMobileActions.checkElementDisplayed(linkMeccaBlog)) {
			logReporter.log("click 'Play Mecca Blog Link' > >", objMobileActions.click(linkMeccaBlog));
		}
	}

	public void verifyUsefulLink(String url) {
		// System.out.println(url);
		// System.out.println(objMobileActions.getUrl());
		waitMethods.sleep(10);
		logReporter.log("check redirected url > >", url, objMobileActions.getUrl());
	}

	public void verifyPartnersLogoInFooter(String text) {

		switch (text) {

		case "ESSA": {
			logReporter.log("Check ESSA Logo > >", objMobileActions.checkElementDisplayed(logo_Essa));
			break;
		}

		case "IBAS": {
			logReporter.log("Check IBSA logo > >", objMobileActions.checkElementDisplayed(logo_Ibsa));
			break;
		}

		case "18": {
			verify18LogoDisplayed();
			break;
		}

		case "Gambling Control": {
			logReporter.log("Check Privacy Policy Link > >", objMobileActions.checkElementDisplayed(logo_GamblingControl));
			break;
		}

		case "GamCare": {
			logReporter.log("Check Privacy Policy Link > >", objMobileActions.checkElementDisplayed(logo_GamCare));
			break;
		}
		case "GamStop": {
			logReporter.log("Check Privacy Policy Link > >", objMobileActions.checkElementDisplayed(logo_GamStop));
			break;
		}
		case "Keep it fun": {
			verifyKeepItFunDisplayed();
			break;
		}
		case "Gambling Commision": {
			logReporter.log("Check Gambling Commision LOGO> >",
					objMobileActions.checkElementDisplayed(logo_GamblingCommission));
			break;
		}
		case "Safer Gambling Standard": 
		{
			logReporter.log("Check Safer Gambling Standard LOGO > >",
					objMobileActions.checkElementDisplayed(logo_saferGamblingStandard));
			break;}
		}
	}
	public void verify18LogoDisplayed() {
		logReporter.log("Verify '18+ Logo' displayed", 
				objMobileActions.checkElementDisplayed(logo_18));
	}
	public void clickLogoEssa() {
		if (objMobileActions.checkElementDisplayed(logo_Essa)) {
			logReporter.log("click 'Essa partner logo' > >", objMobileActions.click(logo_Essa));
		}
	}

	public void clickLogoIbsa() {
		if (objMobileActions.checkElementDisplayed(logo_Ibsa)) {
			logReporter.log("click 'Ibsa partner logo' > >", objMobileActions.click(logo_Ibsa));
		}
	}

	public void clickLogoGamblingControl() {

		objMobileActions.androidScrollToElement(logo_GamblingControl);
		logReporter.log("click 'Gambling Control partner logo' > >", objMobileActions.click(logo_GamblingControl));
	}

	public void clickLogoGamCare() {
		logReporter.log("click 'GamCare partner logo' > >", objMobileActions.click(logo_GamCare));
	}

	public void clickLogoGamStop() {
		logReporter.log("click 'GamStop partner logo' > >", objMobileActions.click(logo_GamStop));
	}

	public void clickLogoGamblingCommission() {

		logReporter.log("click 'Gambling Commission partner logo' > >",
				objMobileActions.click(logo_GamblingCommission));
	}

	public void verifyVeriSignLogoNinformation() {
		By locator = By.xpath("//img[@title='Verisign Logo']//following-sibling::div//p[contains(.,'Your privacy and security is our number one priority here at Mecca Bingo. We protect your account with market-leading security technology so we')]");
		logReporter.log("Check VeriSign Secured Logo > >", objMobileActions.checkElementDisplayed(logo_VeriSignSecured));
		logReporter.log("Check VeriSign Secured information > >", objMobileActions.checkElementDisplayed(locator));
	}

	public void verifyPaymentProvidersBlockInFooter(String text) {

		switch (text) {

		case "Title": {
			logReporter.log("Check Title block in payment providers > >",
					objMobileActions.checkElementDisplayed(paymentTitleBlock));
			break;
		}

		case "Description": {
			logReporter.log("Check Description block in payment providers > >",
					objMobileActions.checkElementDisplayed(paymentDescriptionBlock));
			break;
		}

		case "Logos": {
			verifyPaymentMethodLogos("MasterCard~PayPal~Visa~paysafecard");
			break;
		}
		}
	}

	public void verifyPaymentMethodLogos(String paymentMethodNm)
	{
		if(paymentMethodNm.contains("~"))
		{
			String[] arr1 = paymentMethodNm.split("~");

			for (String labels2 : arr1) 
			{
				By locator = By.xpath("//li[contains(@class,'footer-payment')]//img[@title='"+labels2+"']");
				logReporter.log(" Verify ' "+labels2+ " ' paymentmethod logo is displayed under footer",  
						objMobileActions.checkElementDisplayed(locator));
			}
		}
		else
		{
			By locator = By.xpath("//li[contains(@class,'footer-payment')]//img[@title='"+paymentMethodNm+"']");
			logReporter.log(" Verify ' "+paymentMethodNm+ " ' paymentmethod logo is displayed under footer",  
					objMobileActions.checkElementDisplayed(locator));
		}
	}
	public void switchToChild() {
		objMobileActions.switchToChildWindow();
	}

	public void click_Alderney_Gambling_Control_Commission_link() {

		logReporter.log("click 'Aderney Gambling Control Commision Link' > >",
				objMobileActions.click(link_Alderney_Gambling_Control_Commission));
	}

	public void click_UK_Gambling_Commission_link() {

		logReporter.log("click 'UK Gambling Commission Link' > >", objMobileActions.click(link_UK_Gambling_Commission));
	}

	public void click_BeGambleAware_link() {
		logReporter.log("click 'BeGambleAware Link' > >", objMobileActions.click(link_BeGambleAware));
	}

	public void click_Rank_Group_link() {
		logReporter.log("click 'Rank_Group Link' > >", objMobileActions.click(link_Rank_Group));

	}

	public void Verifyessalogo() {
		By essalogo = By.xpath("//*[@id=\"rstpl-main-menu-position\"]/div/div/div/nav/div/div/div[1]/a/img");
		logReporter.log("Verify essa logo", objMobileActions.checkElementDisplayed(essalogo));
	}

	public void verifyKeepItFunDisplayed() {
		By logoKeepItFun = By.xpath("//a[contains(@href,'https://keepitfun.rank.com/')]//img[contains(@src,'keep-it-fun-logo_mecca_violet_.png')]");
		//By logoKeepItFun = By.xpath("//a[contains(@href,'https://keepitfun.rank.com/')]/img[contains(@src,'footer/keep-it-fun.png')]");
		logReporter.log("Verify 'Keep It Fun Logo' displayed", 
				objMobileActions.checkElementDisplayed(logoKeepItFun));

	}

	public void clickKeepItFunLogo() {
		By logoKeepItFun = By.xpath("//a[contains(@href,'https://keepitfun.rank.com/')]//img[contains(@src,'keep-it-fun-logo_mecca_violet_.png')]");
		//By logoKeepItFun = By.xpath("//a[contains(@href,'https://keepitfun.rank.com/')]/img[contains(@src,'footer/keep-it-fun.png')]");
		String url = "https://keepitfun.rank.com/";
		logReporter.log("Click 'Keep It Fun Logo'", 
				objMobileActions.click(logoKeepItFun));
		objMobileActions.switchToChildWindow();
		String currentURL =objDriverProvider.getAppiumDriver().getCurrentUrl();
		logReporter.log("Redirect to the correct url ", currentURL.equalsIgnoreCase(url));
	}

	public void verifyLogoLinkRedirectToTheCorrectPage(String logoName)
	{
		switch(logoName)
		{

		case "Keep it fun":
			this.clickKeepItFunLogo();
			break;

		}
	}

	public void verifyAssociatedLicenseStatement()
	{
		/*By locator = By.xpath("//p[contains(.,'Licensed and regulated by The Gambling Commission under licence ') and contains(.,' for GB customers playing on our online site. Licensed and regulated by ')and contains(.,' 
(licences ') and contains(.,') for non-GB customers playing on our online site. Licensed and regulated by The Gambling Commission under licence') and contains(.,'for customer playing in our land-based bingo clubs. 
Mecca Bingo is part of the ') and contains(.,'and the MECCA logos are registered trade marks of Rank Leisure Holdings Ltd.')]");
		 */		
		By locator = By.xpath("//p[contains(.,'Mecca Bingo is Licensed and regulated by The Gambling Commission under licence') and contains(.,'GB customers playing on our online site. Licensed and regulated by ') and contains(.,'(licences') and contains(.,') for non-GB customers playing on our online site. Licensed and regulated by The Gambling Commission under licence') and contains(.,'customers playing in our land-based bingo clubs.')]");
		logReporter.log("verify Associated license statements ", 
				objMobileActions.checkElementDisplayed(locator));
	}

	public void verifyLinksUnderAssociatedLicenseStatement(String lnk)
	{

		//a[text()='38750'][@href='https://registers.gamblingcommission.gov.uk/38750']

		//a[text()='The Alderney Gambling Control Commission'][@href='https://www.gamblingcontrol.org/']
		//a[text()='13 C1 & C2'][@href='https://www.gamblingcontrol.org/licensees/egambling-licensees/']
		//a[text()='2396'][@href='https://registers.gamblingcommission.gov.uk/2396']
		//a[text()='Rank Group'][@href='https://www.rank.com/']

		String[] arrSingleEntry = lnk.split("~"); //
		for(String singleEntry : arrSingleEntry) 
		{
			String[] entry = singleEntry.split(",");

			By locator = By.xpath("//a[text()='"+entry[0]+"'][@href='"+entry[1]+"']");
			logReporter.log(" Verify "+entry[1]+" section is displayed with "+entry[0]+" icon",
					objMobileActions.checkElementDisplayed(locator));

		}
	}

	public void clickUKGamblingCommissionLink() {
		By loctor = By.xpath("//a[text()='38750'][@href='https://registers.gamblingcommission.gov.uk/38750']");
		//String url = "https://secure.gamblingcommission.gov.uk/PublicRegister/Search/Detail/38750";
		String url = "https://www.gamblingcommission.gov.uk/public-register/business/detail/38750";
		
		
		logReporter.log("Click '38750' Link", 
				objMobileActions.click(loctor));
		objMobileActions.switchToChildWindow();
		String currentURL = objDriverProvider.getAppiumDriver().getCurrentUrl();
		System.out.println("*** current url ::   "+currentURL+ "  +++ expected::::  "+url);
		logReporter.log("Redirect to the correct url ", currentURL.equalsIgnoreCase(url));

	}

	public void verifyMeccaLogoInFooter()
	{
		By locator = By.xpath("//a[@title='Mecca Bingo']//img[contains(@src,'/mecca-bingo-new/logos/logo-mecca')]");
		logReporter.log(" Verify Mecca Bingo logo in footer",
				objMobileActions.checkElementDisplayed(locator));
	}

	public void verifyAwardsListInFooter(String awdNm)
	{
		if(awdNm.contains("~")){
			String[] arr1 = awdNm.split("~");
			for (String links : arr1  ) {
				By locator = By.xpath("//ul[contains(@class,'list-of-awards')]//li//span//img[@title='"+links+"']");
				logReporter.log(links+" award displayed  ",
						objMobileActions.checkElementDisplayed(locator));}}
		else{
			By locator = By.xpath("//ul[contains(@class,'list-of-awards')]//li//span//img[@title='"+awdNm+"']");
			logReporter.log(awdNm+" award displayed   ",
					objMobileActions.checkElementDisplayed(locator));}

	}

	public void verifyKeepItFuninformation() {
		//By locator = By.xpath("//a[@title=\"Keep it fun\"]//following-sibling::div//p[contains(.,'Whether you play with Mecca Bingo or Grosvenor Casinos, and whether you play online, via mobile or in one of our land-based bingo clubs or casinos, we believe that gambling is something') and contains(.,'keepitfun.rank.com')]");
		By locator = By.xpath("//a[@title='Keep it fun']//following-sibling::div//p[contains(.,'At Mecca Bingo, we want you to enjoy every second that you play with us. Find tips on how to play safely, discover places to get support and get information on all the tools we have in place at: ') and contains(.,'keepitfun.rank.com')]");			
			logReporter.log("Check Keep it fun  information > >", objMobileActions.checkElementDisplayed(locator));
	}


	public void verifyFooterMainSections(String section)
	{
		switch (section) {
		case "Mecca Logo": 
			this.verifyMeccaLogoInFooter();
			break;
		case "Awards Section": 
			//this.verifyAwardsListInFooter("Bingo Marketing Campaign~Innovation in Mobile and Tablet~Best Marketing Campaign 2020~Best Mobile Bingo Site 2020~Best Customer Service Mecca Bingo");
			this.verifyAwardsListInFooter("Best Marketing Campaign 2020~Best Mobile Bingo Site 2020~Best Customer Service Mecca Bingo");
			break;
		case "verisigned information": 
			verifyVeriSignLogoNinformation();
			break;
		case "Keep it fun information": 
			verifyKeepItFuninformation();
			break;
		case "Licensed Information":
			verifyAssociatedLicenseStatement();
			break;
		}
	}
	public void verifyArticleBlockHeaders(String hdrs)
	{
		String[] arrSingleEntry = hdrs.split("~"); //
		for(String singleEntry : arrSingleEntry) {
			String[] entry = singleEntry.split(";");
			By locator = By.xpath("//"+entry[0]+"[contains(.,'"+entry[1]+"')]");	
			objMobileActions.androidScrollToElement(locator);
			logReporter.log(" Verify '"+entry[1]+"' ",
					objMobileActions.checkElementDisplayed(locator));	}
	}
	public void verifyURLRedirection(String expectedurl) {

		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
		String currentURL =objDriverProvider.getAppiumDriver().getCurrentUrl();
		System.out.println("currentURL "+currentURL);
		System.out.println("currentURL "+currentURL+" exp "+expectedurl);
		logReporter.log("Redirect to the correct url ", currentURL.equalsIgnoreCase(expectedurl));
	}
}
