package com.meccabingo.mobile.page.Mecca;

import org.openqa.selenium.By;

import com.generic.MobileActions;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.logger.LogReporter;
import com.generic.utils.WaitMethods;

public class Games {
	private MobileActions objMobileActions;
	private AppiumDriverProvider objDriverProvider;
	private LogReporter logReporter;
	private WaitMethods waitMethods;
	
	public Games(MobileActions objMobileActions, AppiumDriverProvider driverProvider, LogReporter logReporter,WaitMethods waitMethods) {
		this.objMobileActions = objMobileActions;
		this.objDriverProvider = driverProvider;
		this.logReporter = logReporter;
		this.waitMethods = waitMethods;
	}

	public void clickOnAutoInBingo() {
		By autobtn = By.xpath("//apollo-bingo-container/div/div/ul/li[contains(text(),'Auto')]");
		logReporter.log("click on 'Auto button in bingo section' > >", objMobileActions.click(autobtn));
	}

	public void clickOnAutoInGames() {
		By autobtn = By.xpath("//apollo-game-container/div/div/ul/li[contains(text(),'Auto')]");
		logReporter.log("click on 'Auto button in games section' > >", objMobileActions.click(autobtn));
	}

	public void hoverOnIbtnOfHerotile(String nameofthegame) {
		By ibtn = By.xpath("//apollo-bingo-tile[contains(@game-type,'"+nameofthegame+"')]//i[contains(@class,'info colour')]");
		if (!objMobileActions.mouseHover(ibtn)) {
			logReporter.log("Check element", true);
		}
	}

	public void hoverOnIbtnOfGametile(String nameofthegame) {
		By ibtn = By.xpath("//p[contains(text(),'" + nameofthegame + "')]/parent::div/preceding-sibling::div/a/i");
		logReporter.log("Hover on i of normal tile", objMobileActions.mouseHover(ibtn));

	}

	public void clickOnPrizeofGametile(String nameofthegame) {
		By prizebtn = By.xpath("//p[contains(text(),'" + nameofthegame + "')]/parent::div/div/div[1]");
		if (!objMobileActions.click(prizebtn)) {
			logReporter.log("Check element", true);
		}

	}

	public void verifyOpacityOfImageOnGameDetailsPage(String nameofthegame) {
		String opacity = "0.4";
		By image = By.xpath("//img[contains(@class,'background')]/following::div[contains(@class,'overlay opacity')]");
		logReporter.log("verify opacity of image", objMobileActions.checkElementDisplayed(image));

	}

	public void verifyContainsUrl(String url) {
		waitMethods.sleep(10);
		String currentURL = objDriverProvider.getAppiumDriver().getCurrentUrl();
		logReporter.log("verify URL > >" + url, currentURL.contains(url));
	}

	public void clickJoinNowOfFirstBingoGame() {
		By locator = By.xpath("(//h2[text()='Bingo']/following::button[contains(@class,'-green')])[1]");
		objMobileActions.processMobileElement(locator);
		if (objMobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("Click Join Now> >", objMobileActions.click(locator));
	}

	public void clickInfoOfFirstBingoGame() {
		waitMethods.sleep(8);
		By locator = By.xpath("(//apollo-play-bingo-cta//following::a)[1]");
	//	objMobileActions.checkElementDisplayedWithMidWait(locator);
		logReporter.log("Click info icon of First bingo game> >", objMobileActions.clickUsingJS(locator));
	}

	public void clickOnImageOfTile(String nameofthegame) {
		By image = By.xpath("//p[contains(text(),'" + nameofthegame + "')]/parent::div/preceding-sibling::div/img");
		logReporter.log("click on 'image' > >", objMobileActions.click(image));
	}

	public void clickOnTitleoftheGame(String nameofthegame) {
		By titleofthegame = By.xpath("//p[contains(text(),'" + nameofthegame + "')]");
		if (!objMobileActions.click(titleofthegame)) {
			logReporter.log("Check element", true);
		}
	}

	public void bingoContainerSectionDisplayed() {
		By bingoContainerSection = By.xpath("//apollo-filter-heading[@title='Bingo']");
		logReporter.log("bingo container displayed > >", objMobileActions.checkElementDisplayed(bingoContainerSection));
	}

	public void clickTopFlagOfBingoGame() {
		By topFlagOfBingoGame = By.xpath("(//apollo-bingo-tile//div[contains(@class,'pot-flag')])[1]");
		objMobileActions.androidScrollToElement(topFlagOfBingoGame);
		objMobileActions.click(topFlagOfBingoGame);
	}
}
