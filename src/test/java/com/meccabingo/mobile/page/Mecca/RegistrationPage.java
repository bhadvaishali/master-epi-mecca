/**
 * 
 */
package com.meccabingo.mobile.page.Mecca;

import org.openqa.selenium.By;

import com.generic.MobileActions;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.Utilities;
import com.generic.utils.WaitMethods;
import com.mifmif.common.regex.Generex;

public class RegistrationPage {
	
	private MobileActions objMobileActions;
	private LogReporter logReporter;
	private Utilities utilities;
	private WaitMethods wait;
	private Configuration configuration;

	public RegistrationPage(MobileActions objMobileActions, LogReporter logReporter, WaitMethods wait, Utilities utilities,Configuration configuration) {
		this.objMobileActions = objMobileActions ;
		this.logReporter = logReporter;
		this.utilities = utilities;
		this.wait = wait;
		this.configuration = configuration;
	}

	String randomEmail;

	/*String greencolor = "#5ea85a";
	String greycolor = "#B3B3B3";//"#e6dae6";
	String redcolor = "#e22c2c";*/
	String greencolor = "#009D7A";
	String greycolor = "#5ECCF3";
//	#5ECCF3";
//	#60CCF3
	String redcolor = "#B41D68";
	
	By emailcheckbox = By.xpath("//input[contains(@id,'gdpr-email')]");
	By yesbutton = By.xpath("//button[contains(.,'Yes')]");
	By selectall = By.xpath("//button[contains(@class,'colour-button-green')]//span[contains(.,'Select All')]");
	By SMScheckbox = By.xpath("//input[contains(@id,'gdpr-sms')]");
	By phonecheckbox = By.xpath("//input[contains(@id,'gdpr-phone')]");
	By postcheckbox = By.xpath("//input[contains(@id,'gdpr-post')]");
	By username = By.xpath("//input[contains(@id,'username')]");
	By password = By.xpath("//input[contains(@id,'password')]");

	By title = By.xpath("//h4[contains(text(),'Title')]");
	By firstname = By.xpath("//input[contains(@id,'firstname')]");
	By surname = By.xpath("//input[contains(@id,'surname')]");
	By dateofbirth = By.xpath("//div[contains(@class,'ip-dob-inner')]");
	By day = By.xpath("//input[contains(@id,'dob-day')]");
	By month = By.xpath("//input[contains(@id,'dob-month')]");
	By year = By.xpath("//input[contains(@id,'dob-year')]");
	By notyou = By.xpath("//a[contains(text(),'Not You')]");

	By Ms = By.xpath("//button[contains(.,'Ms')]");
	By Mr = By.xpath("//button[contains(.,'Mr')]");
	By Miss = By.xpath("//button[contains(.,'Miss')]");
	By Mrs = By.xpath("//button[contains(.,'Mrs')]");
	By Dr = By.xpath("//button[contains(.,'Dr')]");
	By Mx = By.xpath("//button[contains(.,'Mx')]");

	By emailtextbox = By.xpath("//input[@placeholder='Email address']");
	By agecheckbox = By.xpath("//input[contains(@id,'id-agreed')]");
	By next = By.xpath("//div[contains(@class,'join-now')]/form/fieldset[3]/button");
	By membership = By.xpath("//input[contains(@type,'number')]");
	By secondemail = By.xpath("//input[contains(@type,'email')]");
	By mobilenumber = By.xpath("//input[contains(@id,'mobile')]");

	By EnterAddressManuallyBtn = By.xpath("//form[contains(@class,'join-now')]/fieldset[3]/button/span[contains(.,'Enter address manually')]");
	By firstAddress = By.xpath("//input[contains(@id,'input-address-line1')]");
	By secondAddress = By.xpath("//input[contains(@id,'input-address-line2')]");
	By Town_City = By.xpath("//input[contains(@id,'town-city')]");
	By country = By.xpath("//input[contains(@id,'county')]");
	By postcode = By.xpath("//input[contains(@id,'address-lookup-search')]"); // input[contains(@id,'postcode')]
	By uk = By.xpath("//select[contains(@id,'input-country')]/option[contains(text(),'United Kingdom')]");
	By countrydropdown = By.xpath("//select[contains(@id,'input-country')]");
	By addressFromPostcodeSearch = By.xpath("//ul[contains(@class,'postcode-search-results')]/li[1]");

	public void clickJoindNowbtn() {
		//By JoinNow = By.xpath("//a[contains(text(),'Join Now')]");
		By JoinNow = By.xpath("//a[contains(text(),'Join')]");
		logReporter.log("click on join now button > >", objMobileActions.click(JoinNow));
	}

	public String enterEmailaddress() {
		wait.sleep(5);
		String randomemail = utilities.getEmail();
		System.out.println("*******   randomemail  "+randomemail);
		objMobileActions.click(emailtextbox);
		logReporter.log("enter email address", objMobileActions.setTextWithClear(emailtextbox, randomemail));
		return randomemail;
	}
	//	public String getEmail() {
	//		String regex = "[a-zA-Z0-9]{5}\\@mailinator\\.com";
	//		this.randomEmail = new Generex(regex).random();
	//		return randomEmail;
	//	}

	public void enterEmailaddressOnSecondPage() {
		//objMobileActions.androidScrollToElement(secondemail);
		//objMobileActions.click(secondemail);
		logReporter.log("enter email address", objMobileActions.setTextWithClear(secondemail, "6abd32@gmail.com"));
	}

	public void verifyEmailaddressOnSecondPageTextboxColor() {

		wait.sleep(3);
		logReporter.log("Check color of textbox bottom border > >",
				objMobileActions.checkCssValue(secondemail, "border-bottom-color", greencolor));
	}

	public void enterRegisteredEmailaddress(String email) {
		logReporter.log("enter email address", objMobileActions.setTextWithClear(emailtextbox, email));
	}

	public void clickAgecheckbox() {

		logReporter.log("click on age checkbox > >", objMobileActions.clickUsingJS(agecheckbox));
	}

	public void clickOnNextbtn() {

		logReporter.log("click on next button > >", objMobileActions.click(next));
	}

	public void clickOnEmailCheckbox() {

		logReporter.log("click on email checkbox > >", objMobileActions.click(emailcheckbox));
		logReporter.log("email checkbox is selected > >", objMobileActions.isCheckBoxSelected(emailcheckbox));
	}

	public void clickOnSMSCheckbox() {

		logReporter.log("click on SMS checkbox > >", objMobileActions.click(SMScheckbox));
		logReporter.log("sms checkbox is selected > >", objMobileActions.isCheckBoxSelected(SMScheckbox));
	}

	public void clickOnPhoneCheckbox() {
		logReporter.log("click on Phone checkbox > >", objMobileActions.click(phonecheckbox));
		logReporter.log("phone checkbox is selected > >", objMobileActions.isCheckBoxSelected(phonecheckbox));
	}

	public void clickOnPostCheckbox() {

		logReporter.log("click on post checkbox > >", objMobileActions.click(postcheckbox));
		logReporter.log("post checkbox is selected > >", objMobileActions.isCheckBoxSelected(postcheckbox));
	}

	public void clickOnDisableRegisterCTA() {
		By register = By.xpath("//button[@disabled]//span[contains(.,'Register')]");
		objMobileActions.androidScrollToElement(register);
		
		logReporter.log("click on post checkbox > >", objMobileActions.clickUsingJS(register));
	}

	public void clickRegisterCTA() {
		By register = By.xpath("//button[@type='submit']/span[text()='Register']");
		logReporter.log("click on Register button > >", objMobileActions.clickUsingJS(register));
	}
	public void clickOnSelectAllbtn() {
	//	objMobileActions.scrollToElement(selectall);
		
		//objMobileActions.checkElementToBeClickablWithMidWait(selectall);
		logReporter.log("click on SelectAll checkbox > >", objMobileActions.clickUsingJS(selectall));
		wait.sleep(5);
	}

	public void clickNoIhaveaMembershipCardbtn() {
		
	//	By membershipbtn = By.xpath("//button[contains(@name,'join-with-membership')]");
		By membershipbtn = By.xpath("//button[contains(.,'NO')]");
	//	By membershipbtn = By.xpath("//button[contains(.,'NO (I have a membership card)')]");
		 objMobileActions.checkElementDisplayedWithMidWait(membershipbtn);
		logReporter.log("click on No (I have a membership card) button > >", objMobileActions.clickUsingJS(membershipbtn));
		wait.sleep(5);
	}

	public void clickOnNotYouLink() {
		logReporter.log("click on Not You link > >", objMobileActions.click(notyou));
	}

	public void verifyNotYouLink() {
		logReporter.log("", objMobileActions.checkElementDisplayed(notyou));
	}

	public void verifyAllSelectedCheckboxes(String checkboxes) {
		switch (checkboxes) {
		case "Email":
			logReporter.log("email checkbox is selected > >", objMobileActions.isCheckBoxSelected(emailcheckbox));
			break;
		case "SMS":
			logReporter.log("sms checkbox is selected > >", objMobileActions.isCheckBoxSelected(SMScheckbox));
			break;
		case "Phone":
			logReporter.log("phone checkbox is selected > >", objMobileActions.isCheckBoxSelected(phonecheckbox));
			break;
		case "post":
			logReporter.log("post checkbox is selected > >", objMobileActions.isCheckBoxSelected(postcheckbox));
			break;
		}

	}

	public void verifyMembershipScreenOptions(String options) {
		switch (options) {
		case "membershipnumberbox":
			logReporter.log(" > >", objMobileActions.checkElementDisplayed(membership));
			break;
		case "DOBbox":
			logReporter.log(" > >", objMobileActions.isCheckBoxSelected(dateofbirth));
			break;
		case "agecheckbox":
			logReporter.log("p > >", objMobileActions.isCheckBoxSelected(agecheckbox));
			break;
		case "nextbtn":
			logReporter.log(" > >", objMobileActions.isCheckBoxSelected(next));
			break;
		}

	}

	public void verifyColorOfErrorMessage() {
		By errormessage = By.xpath("//li[contains(.,'contact preference')]");
		String errorcolor = "#e22c2c";// "rgba(226, 44, 44, 1)";
		logReporter.log("Check color of error message > >",
				objMobileActions.checkCssValue(errormessage, "color", errorcolor));
	}

	public String enterUsername() {
		String randomUserName = "MB" + utilities.getUsername();
		logReporter.log("enter email address", objMobileActions.setTextWithClear(username, randomUserName));
		return randomUserName;
	}

	public void enterInvalidUsername() {
		String regex = "[a-zA-Z]{4}";
		String randomusername = new Generex(regex).random();
		logReporter.log("enter email address", objMobileActions.setTextWithClear(username, randomusername));
	}

	public void verifyUsernameTextboxColor() {

		logReporter.log("Check color of error message > >",
				objMobileActions.checkCssValue(username, "border-bottom-color", greencolor));
	}

	public void enterExistingUsername(String existingusername) {
		logReporter.log("enter username", objMobileActions.setTextWithClear(username, existingusername));
	}

	public void verifyErrorMessages(String errormessage) {
		By error = By.xpath("//li[contains(.,'" + errormessage + "')]");
		logReporter.log("Check error message '" +errormessage+ " '", objMobileActions.checkElementDisplayed(error));
	}

	public void verifyAllFieldsOnPage(String textboxes) {
		switch (textboxes.toLowerCase()) {
		case "title":
			logReporter.log("Check title text ", objMobileActions.checkElementDisplayed(title));
			break;
		case "first name":
			logReporter.log("Check firstname field", objMobileActions.checkElementDisplayed(firstname));
			break;
		case "surname":
			logReporter.log(" Check surname field", objMobileActions.checkElementDisplayed(surname));
			break;
		case "date of birth":
			logReporter.log("Check dateofbirth field", objMobileActions.checkElementDisplayed(dateofbirth));
			break;
		case "username":
			logReporter.log(" Check username field", objMobileActions.checkElementDisplayed(username));
			break;
		case "password":
			logReporter.log(" Check password field", objMobileActions.checkElementDisplayed(password));
			break;
		}
	}

	public void verifyAllTitlesOnPage(String titles) {
		switch (titles) {
		case "Ms":
			logReporter.log("Check title 'Ms' is displayed", objMobileActions.checkElementDisplayed(Ms));
			break;
		case "Mr":
			logReporter.log("Check title 'Mr' is displayed", objMobileActions.checkElementDisplayed(Mr));
			break;
		case "Miss":
			logReporter.log("Check title 'Miss' is displayed", objMobileActions.checkElementDisplayed(Miss));
			break;
		case "Mrs":
			logReporter.log("Check title 'Mrs' is displayed", objMobileActions.checkElementDisplayed(Mrs));
			break;
		case "Mx":
			logReporter.log("Check title 'Mx' is displayed", objMobileActions.checkElementDisplayed(Mx));
			break;
		}

	}

	public void enterFirstName() {
		String regex = "[a-zA-Z]{4}";
		String randomfirstname = new Generex(regex).random();
		logReporter.log("enter firstname", objMobileActions.setTextWithClear(firstname, randomfirstname));
		objMobileActions.pressKeybordKeys(firstname, "tab");
	}

	public void enterFirstName(String fname) {
		logReporter.log("enter firstname as "+fname, objMobileActions.setTextWithClear(firstname, fname));
		objMobileActions.pressKeybordKeys(firstname, "tab");
	}
	public void verifyFirstNameTextboxColor() {

		logReporter.log("Check color of textbox bottom border > >",
				objMobileActions.checkCssValue(firstname, "border-bottom-color", greencolor));
	}

	public void enterSurName() {
		String regex = "[a-zA-Z]{4}";
		String randomsurname = new Generex(regex).random();
		logReporter.log("enter surname ", objMobileActions.setTextWithClear(surname, randomsurname));
	}

	public void enterSurName(String sname) {
		logReporter.log("enter surname as "+sname, objMobileActions.setTextWithClear(surname, sname));
		objMobileActions.pressKeybordKeys(surname, "tab");
	}
	public void verifySurNameTextboxColor() {

		logReporter.log("Check color of textbox bottom border > >",
				objMobileActions.checkCssValue(surname, "border-bottom-color", greencolor));
	}

	public void enterDateOfBirth(String Day, String Month, String Year) {
		logReporter.log("Enetr day", objMobileActions.setTextWithClear(day, Day));
		logReporter.log("Enetr day", objMobileActions.setTextWithClear(month, Month));
		logReporter.log("Enetr day", objMobileActions.setTextWithClear(year, Year));
		objMobileActions.pressKeybordKeys(year, "tab");
	}

	public void verifyDateOfBirthTextboxColor() {

		logReporter.log("Check color of textbox bottom border > >",
				objMobileActions.checkCssValue(dateofbirth, "border-bottom-color", greencolor));
	}

	public String enterPassword() {
		String randomPassword = utilities.genarateRandomPassword();
		//String randomPassword = "Test@1234";
		logReporter.log("enter password", objMobileActions.setTextWithClear(password, randomPassword));
		return randomPassword;
	}
	
	public void setPassword(String pwd) {
		logReporter.log("enter password", objMobileActions.setTextWithClear(password, pwd));
	}


	public void enterInvalidPassword() {
		String regex = "[a-zA-Z]{8}";
		String randompassword = new Generex(regex).random();
		logReporter.log("enter password", objMobileActions.setTextWithClear(password, randompassword));
	}

	public void verifyPasswordTextboxColor() {

		logReporter.log("Check color of textbox bottom border > >",
				objMobileActions.checkCssValue(password, "border-bottom-color", greencolor));
	}

	public void verifyPasswordTextboxGreyColor() {
		logReporter.log("Check color of textbox bottom border > >",
				objMobileActions.checkCssValue(password, "border-bottom-color", greycolor));
	}
	public void verifyRedColorBelowPassword() {
		logReporter.log("Check color of textbox bottom border > >",
				objMobileActions.checkCssValue(password, "border-bottom-color", redcolor));
	}
	public void verifyUsernameTextboxGreyColor() {
		logReporter.log("Check color of textbox bottom border > >",
				objMobileActions.checkCssValue(username, "border-bottom-color", greycolor));
	}
	public void verifyRedColorBelowUsername() {
		logReporter.log("Check color of textbox bottom border > >",
				objMobileActions.checkCssValue(username, "border-bottom-color", redcolor));
	}
	public void verifyEmailTextboxGreyColor() {
		logReporter.log("Check color of textbox bottom border > >",
				objMobileActions.checkCssValue(secondemail, "border-bottom-color", greycolor));
	}

	public void verifyMobileNumberTextboxGreyColor() {
		logReporter.log("Check color of textbox bottom border > >",
				objMobileActions.checkCssValue(mobilenumber, "border-bottom-color", greycolor));
	}

	public void verifyHelpTextBelowInput(String hinttext) {
		By helptextbelowinput = By.xpath("//span[contains(text(),'" + hinttext + "')]");
		logReporter.log("Check color of textbox bottom border > >",
				objMobileActions.checkElementDisplayed(helptextbelowinput));
	}

	public void verifyYesBtnIsSelected() {
		String color = "#e12482";
		logReporter.log("Check color of textbox bottom border > >",
				objMobileActions.checkCssValue(yesbutton, "background-color", color));

	}

	public void verifyEmailTextboxColorOnFirstPage() {

		logReporter.log("Check color of textbox bottom border > >",
				objMobileActions.checkCssValue(emailtextbox, "border-bottom-color", greencolor));
	}

	public void verifySelectAllbtn() {
		logReporter.log("Verify select all button", objMobileActions.checkElementDisplayed(selectall));
	}

	public void clickOnDisabledNextbtn() {
		By next = By.xpath("//div[contains(@class,'join-now')]/form/fieldset[3]/button[@disabled]/span[contains(.,'Next')]");
		logReporter.log("click on next button > >", objMobileActions.clickUsingJS(next));
	}

	public void verifyNextbtnIsEnabled() {

		logReporter.log("Verify next button is enabled", objMobileActions.click(next));
	}

	//	public void verifyHelpbtnDisappear() {
	//		By helpbtn = By.xpath("//a[contains(@class,'livechat')]");
	//		if (!objMobileActions.checkElementDisplayed(helpbtn)) {
	//			logReporter.log("check help button dissappears", true);
	//		}
	//
	//	}

	public void verifyErrorOnFirstRegPage(String message) {
		By errormessage = By.xpath("//ul[contains(@class,'errors')]/li[contains(text()," + message + ")]");
		logReporter.log("click on next button > >", objMobileActions.checkElementDisplayed(errormessage));
	}

	public void enterInvalidEmailaddress() {
		String regex = "[a-zA-Z0-9]{5}\\@gmail\\com";
		String randomInvalidEmail = new Generex(regex).random();
		By locator = By.xpath("//div[contains(@class,'join-now')]/form/fieldset[2]/div/div/input");
		objMobileActions.androidScrollToElement(locator);
		wait.sleep(5);
		objMobileActions.clickUsingJS(locator);
		wait.sleep(8);
		
		logReporter.log("enter email address", objMobileActions.setTextWithClear(locator, "sdsds@xx.d"));
		objMobileActions.pressKeybordKeys(locator, "tab");
	}

	public void enterMembershipNumber(String number) {
		logReporter.log("enter Membership Number", objMobileActions.setTextWithClear(membership, number));
	}

	public void clickOnDrtitle() {
		logReporter.log("click on title mrs", objMobileActions.click(Mrs));
	}

	public void verifyMrstitleIsSelected() {
	//	String color = "#A94976";//"#d35b94";
		wait.sleep(30);
		By locator = By.xpath("//button[contains(@class,'button-pink')]//span[contains(.,'Mrs')]");
		logReporter.log("Mrs highlighted",
				objMobileActions.checkElementDisplayed(locator));

	}

	public void verifyFirstRegPage() {
		logReporter.log("verify first registration page", objMobileActions.checkElementDisplayed(next));
	}

	public void enterMobileNumber() {
		String regex = "[1-9]{9}";
		String randomnumber = new Generex(regex).random();
		logReporter.log("enter mobile number", objMobileActions.setTextWithClear(mobilenumber, randomnumber));
		objMobileActions.pressKeybordKeys(mobilenumber, "tab");
	}

	public void enterInvalidMobileNumber() {
		String regex = "[a-zA-Z][1-9]{6}";
		String randomnumber = new Generex(regex).random();
		logReporter.log("enter mobile number", objMobileActions.setTextWithClear(mobilenumber, randomnumber));
		objMobileActions.pressKeybordKeys(mobilenumber, "tab");
		wait.sleep(5);
	}

	public void setMobileNumber(String no) {
		logReporter.log("enter mobile number", objMobileActions.setTextWithClear(mobilenumber, no));
		objMobileActions.pressKeybordKeys(mobilenumber, "tab");
		wait.sleep(5);
	}
	public void verifyMobileNumberTextboxColor() {
		logReporter.log("Check color of textbox bottom border > >",
				objMobileActions.checkCssValue(mobilenumber, "border-bottom-color", greencolor));
	}

	public void verifyUKasDefaultValue() {

		logReporter.log("verify default value as UK", objMobileActions.checkElementDisplayed(uk));
	}

	public void selecyUKInDropdown() {
		By locator = By.xpath("//option[text()='United Kingdom']");
		logReporter.log("select second option from dropdown", objMobileActions.click(locator));
	}

	public void selectRepublicOptionInCountry() {
		By locator = By.xpath("//option[text()='Republic Of Ireland']");
		logReporter.log("select second option from dropdown", objMobileActions.click(locator));
	}

	public void verifyEnterAddressManuallyBtn() {
		logReporter.log("check Enter Manually button", objMobileActions.checkElementDisplayed(EnterAddressManuallyBtn));
	}

	public void clickOnEnterAddressManuallyBtn() {
		logReporter.log("click on enter address manually button", objMobileActions.click(EnterAddressManuallyBtn));
	}

	public void verifyEnterAddressManuallyBtnDisappears() {
		if (!objMobileActions.checkElementExists(EnterAddressManuallyBtn)) {
			logReporter.log("check Enter Manually button dissappears", true);
		}
	}

	public void verifyTownCityField() {
		logReporter.log("verify town and city field", objMobileActions.checkElementDisplayed(Town_City));
	}

	public void enterValidInputInTownCityField() {
		logReporter.log("enter valid input in town/city field", objMobileActions.setTextWithClear(Town_City, "United Kingdom"));
	}
	
	public void enterinvalidInputInTownCityField() {
		logReporter.log("enter invalid input in town/city field", objMobileActions.setTextWithClear(Town_City, "United Kingdom"));
	}

	public void verifyTownCityFieldDisappears() {
		if (!objMobileActions.checkElementExists(Town_City)) {
			logReporter.log("check Enter Manually button dissappears", true);
		}
	}

	public void verifyGreenColorBelowTownCityField() {
		System.out.println("************* town");
		logReporter.log("Check color of textbox bottom border > >",
				objMobileActions.checkCssValue(Town_City, "border-bottom-color", greencolor));
	}

	public void verifyCountryField() {
		logReporter.log("verify countrys field", objMobileActions.checkElementDisplayed(country));
	}

	public void verifyCountryFieldDisappears() {
		if (!objMobileActions.checkElementExists(country)) {
			logReporter.log("check Enter Manually button dissappears", true);
		}
	}

	public void verifyPostcodeField() {
		logReporter.log("verify countrys field", objMobileActions.checkElementDisplayed(postcode));
	}

	public void clickOnPostcodeField() {
	objMobileActions.androidScrollToElement(postcode);
		wait.sleep(8);
		logReporter.log("click on postcode field", objMobileActions.click(postcode));
	}

	public void enterValidPostcode(String Postcode) {
		//objMobileActions.androidScrollToElement(postcode);
		//wait.sleep(10);
		//objMobileActions.clickUsingJS(postcode);
		logReporter.log("enter valid postcode", objMobileActions.setText(postcode, Postcode));
		wait.sleep(10);
	}

	public void enterInvalidPostcode() {
		logReporter.log("enter invalid postcode", objMobileActions.setTextWithClear(postcode, "t6776767@#$5fghg"));
		try {
			wait.sleep(30);
		}

		catch(Exception e) {
			e.printStackTrace();
		}
	}

	public void verifyRedColorBelowPostcode() {
		logReporter.log("Check color of textbox bottom border > >",
				objMobileActions.checkCssValue(postcode, "border-bottom-color", redcolor));
	}

	public void verifyGreyColorBelowPostcode() {
		
		String actualValue = objMobileActions.getCssValue(postcode, "border-bottom-color");
		System.out.println("*************** actualValue :: "+actualValue);
		actualValue = utilities.toConvertHexValue(actualValue);
		System.out.println("*************** actualValue :: "+actualValue);
		if(actualValue.equalsIgnoreCase("#6DCFF1")||actualValue.equalsIgnoreCase("#60CCF3")||actualValue.equalsIgnoreCase("#5ECCF3"))
		{	logReporter.log("Check color of textbox bottom border ", true);}
		else
		{	logReporter.log("Check color of textbox bottom border ", false);}
		/*logReporter.log("Check color of textbox bottom border > >",
				objMobileActions.checkCssValue(postcode, "border-bottom-color", greycolor));*/
	}

	public void verifyGreenColorBelowPostcode() {
		System.out.println("************* postcode");
		logReporter.log("Check color of textbox bottom border > >",
				objMobileActions.checkCssValue(postcode, "border-bottom-color", greencolor));
	}

	public void verifyHelpTextDisappearsBelowPostcode() {
		By helptext = By.xpath("//li[contains(text(),'Please enter a postcode')]");
		if (!objMobileActions.checkElementExists(helptext)) {
			logReporter.log("help text below postcode disappear", true);
		}
	}

	public void clickOnMobileNumberField() {
		logReporter.log("click on mobile number field", objMobileActions.click(mobilenumber));
		wait.sleep(3);
	}

	public void verifyGreyColorBelowMobileNumber() {
		logReporter.log("Check color of textbox bottom border > >",
				objMobileActions.checkCssValue(mobilenumber, "border-bottom-color", greycolor));
	}

	public void verifyGreenColorBelowMobileNumber() {
		logReporter.log("Check color of textbox bottom border > >",
				objMobileActions.checkCssValue(mobilenumber, "border-bottom-color", greencolor));
	}

	public void verifyRedColorBelowMobileNumber() {
		logReporter.log("Check color of textbox bottom border > >",
				objMobileActions.checkCssValue(mobilenumber, "border-bottom-color",redcolor));
	}

	public void verifyFirstAddressField() {
		logReporter.log("verify address line 1", objMobileActions.checkElementDisplayed(firstAddress));
	}

	public void enterValidInputInFirstAddress() {
		logReporter.log("enter first address", objMobileActions.setTextWithClear(firstAddress, "washington"));
		objMobileActions.pressKeybordKeys(firstAddress, "tab");
	}

	public void verifyFirstAddressFieldDissappears() {
		if (!objMobileActions.checkElementExists(firstAddress))
			logReporter.log("", true);
	}

	public void verifyGreenColorBelowFirstAddress() {
		logReporter.log("Check color of textbox bottom border > >",
				objMobileActions.checkCssValue(firstAddress, "border-bottom-color", greencolor));
	}

	public void verifyRedColorBelowFirstAddress() {
		wait.sleep(15);
		logReporter.log("Check color of textbox bottom border > >",
				objMobileActions.checkCssValue(firstAddress, "border-bottom-color", redcolor));
	}

	public void verifySecondAddressField() {
		logReporter.log("verify address line 2", objMobileActions.checkElementDisplayed(secondAddress));
	}

	public void enterValidInputInSecondAddress() {
		logReporter.log("enter second address", objMobileActions.setTextWithClear(secondAddress, "DC"));
	}

	public void verifySecondAddressFieldDissappears() {
		if (!objMobileActions.checkElementExists(secondAddress))
			logReporter.log("", true);
	}

	public void verifyGreenColorBelowSecondAddress() {
		logReporter.log("Check color of textbox bottom border > >",
				objMobileActions.checkCssValue(secondAddress, "border-bottom-color", greencolor));
	}

	public void verifyRedColorBelowSecondAddress() {
		logReporter.log("Check color of textbox bottom border > >",
				objMobileActions.checkCssValue(secondAddress, "border-bottom-color",redcolor));
	}

	public void selectAddressFromPostcodeSearch() {
		//if(objMobileActions.checkElementDisplayedWithMidWait(addressFromPostcodeSearch))
		logReporter.log("select address from postcode search", objMobileActions.click(addressFromPostcodeSearch));
	}

	public void EnterInvalidInputInFirstAddress() {
		logReporter.log("click on fiest address", objMobileActions.click(firstAddress));
		logReporter.log("click tab", objMobileActions.pressKeybordKeys(firstAddress, "tab"));
	}

	public void EnterInvalidInputInSecondAddress() {
		logReporter.log("click on second address ", objMobileActions.click(secondAddress));
		logReporter.log("click tab", objMobileActions.pressKeybordKeys(secondAddress, "tab"));
	}
	public void clearUsername() {
		logReporter.log("", objMobileActions.clearText(username));
	}

	public void verifyH4HeadingDisplayed(String strText) {
		System.out.println("*********************************msgggg");
		By locator = By.xpath("//h4[contains(text(),'" + strText + "')]");
		if (objMobileActions.checkElementExists(locator))
			logReporter.log("text displayed'" + strText + "' displayed >>", true);
		else
			logReporter.log( strText + "'\"text is not displayed'\" +", false);
	}

	public void clickCheckbox(String chkboxName) {
		By locator = By.xpath("//input[@type='checkbox' and @id='gdpr-" + chkboxName + "']");
		logReporter.log("select checkbox > >"+chkboxName, objMobileActions.click(locator));
	}

	public void verifyCheckboxIsChecked(String chkboxName) {
		By locator = By.xpath("//input[@id='gdpr-" + chkboxName + "']");
		String checkValue = objMobileActions.getAttribute(locator, "value");
		if (checkValue.equals("true"))
			logReporter.log(chkboxName + "checkbox is checked :", true);
		else
			logReporter.log(chkboxName + "checkbox is checked :", false);

	}

	public void verifyCheckboxIsUnChecked(String chkboxName) {
		By locator = By.xpath("//input[@id='gdpr-" + chkboxName + "']");
		String checkValue = objMobileActions.getAttribute(locator, "value");
		if (checkValue.equals("false"))
			logReporter.log(chkboxName + "checkbox is unchecked :", true);
		else
			logReporter.log(chkboxName + "checkbox is unchecked :", false);

	}

	public void verifyAllCheckboxesChecked() {
		By chkboxEmail = By.xpath("//input[@id='gdpr-email']");
		By chkboxSMS = By.xpath("//input[@id='gdpr-sms']");
		By chkboxPhone = By.xpath("//input[@id='gdpr-phone']");
		By chkboxPost = By.xpath("//input[@id='gdpr-post']");

		String valueEmail = objMobileActions.getAttribute(chkboxEmail, "value");
		String valueSMS = objMobileActions.getAttribute(chkboxSMS, "value");
		String valuePhone = objMobileActions.getAttribute(chkboxPhone, "value");
		String valuePost = objMobileActions.getAttribute(chkboxPost, "value");
		if (valueEmail.equals("true") == true && valueSMS.equals("true") == true && valuePhone.equals("true") == true
				&& valuePost.equals("true") == true)
			logReporter.log("All the checkboxes are checked", true);
		else
			logReporter.log("All the checkboxes are checked", false);

	}

	public void verifyAllCheckboxesUnChecked() {
		By chkboxEmail = By.xpath("//input[@id='gdpr-email']");
		By chkboxSMS = By.xpath("//input[@id='gdpr-sms']");
		By chkboxPhone = By.xpath("//input[@id='gdpr-phone']");
		By chkboxPost = By.xpath("//input[@id='gdpr-post']");

		String valueEmail = objMobileActions.getAttribute(chkboxEmail, "value");
		String valueSMS = objMobileActions.getAttribute(chkboxSMS, "value");
		String valuePhone = objMobileActions.getAttribute(chkboxPhone, "value");
		String valuePost = objMobileActions.getAttribute(chkboxPost, "value");
		if (valueEmail.equals("true") == false && valueSMS.equals("true") == false && valuePhone.equals("true") == false
				&& valuePost.equals("true") == false)
			logReporter.log("All the checkboxes are unchecked", true);
		else
			logReporter.log("All the checkboxes are unchecked", false);

	}

	public void verifyValidPasswordInfoText(String txt)
	{
		By locator = By.xpath("//ul[contains(@class,'valid-password')]//li[contains(@class,'p-v-2 flex-d-f flex-ai-c')][contains(.,'"+txt+"')]");
		logReporter.log("Verify ' "+txt+" '", objMobileActions.checkElementDisplayed(locator));
	}

	public boolean verifyCheckboxIsCheckedOrNot(String chkboxName) {
		boolean flag =false;
		By locator = By.xpath("//input[@id='gdpr-" + chkboxName + "']");
		String checkValue = objMobileActions.getAttribute(locator, "value");
		if (checkValue.equals("true"))
			flag=true;
		else
			flag=false;

		return flag;
	}

	public void setmanualAddress(String addressLine1, String addressLine2, 
			String townCity, String country1, String postCode) 
	{
		logReporter.log("Set address line1", addressLine1, 
				objMobileActions.setTextWithClear(firstAddress, addressLine1));
		logReporter.log("Set address line2", addressLine2, 
				objMobileActions.setTextWithClear(secondAddress, addressLine2));
		logReporter.log("Set Town/City", townCity, 
				objMobileActions.setTextWithClear(Town_City, townCity));
		logReporter.log("Set country", country1, 
				objMobileActions.setTextWithClear(country, country1));
		setPostCode(postCode);
	}

	public void setPostCode(String postCode)
	{
		By locator = By.id("input-postcode");
		logReporter.log("Set post code", postCode, 
				objMobileActions.setTextWithClear(locator, postCode));
	}
	public void setUserName(String usname) {
		logReporter.log("enter email address", objMobileActions.setTextWithClear(username, usname));
		objMobileActions.pressKeybordKeys(username, "tab");
	}
	public void verifyErrorMessageDoesNotDisplay(String err)
	{
		if(err.contains("~")){
			String[] arr1 = err.split("~");
			for (String links : arr1  ) {
				By errMsg = By.xpath("//ul[@class='errors']//li[text()='"+links+"']");
				logReporter.log(links+"  not displayed  ",
						objMobileActions.checkElementNotDisplayed(errMsg));}}
		else{
			By errMsg = By.xpath("//ul[@class='errors']//li[text()='"+err+"']");
			logReporter.log(err+" not displayed  ",
					objMobileActions.checkElementNotDisplayed(errMsg));}

	}
	public void verifyDepositNowHeader()
	{
		wait.sleep(configuration.getConfigIntegerValue("midwait"));
		By hdrDeposit = By.xpath("//h4[contains(.,'Deposit')]");
		By DepositInfoText = By.xpath("//p[contains(.,'By depositing, you understand we hold your funds securely in a designated bank account with a medium level of protection.')]//following-sibling::a[contains(.,'See details')]");
		//By hdrDeposit = By.xpath("//form[@class=\"safecharge\"]");
		verifyStepCount("Create Account", "3");
		/*logReporter.log(" verify 'Deposit' header is displayed on screen" ,
				objMobileActions.checkElementDisplayed(hdrDeposit));*/
		//logReporter.log(" verify 'By depositing, you understand we hold your funds securely in a designated bank account with a medium level of protection.' text is displayed on screen" ,
			//	objMobileActions.checkElementDisplayed(DepositInfoText));
	
	
	}
	public void verifyStepCount(String headerTxt,String stepNo)
	{
		By locator = By.xpath("//h4[contains(.,'"+headerTxt+"')]//following-sibling::p[contains(.,'Step') and contains(.,'"+stepNo+"') and contains(.,'of') and contains(.,'3')]");
		logReporter.log(" verify ' "+headerTxt+ " ' header is displayed with step ' " +stepNo+ " 'of 3 ",
				objMobileActions.checkElementDisplayed(locator));
				
	}
	public void verifyEmailFieldonScreen()
	{
		logReporter.log(" verify 'Email' field on regisration screen" ,objMobileActions.checkElementDisplayed(secondemail));
	}

	public void verifyMobileNumberFieldOnScreen()
	{
		logReporter.log(" verify 'Mobile number' field on regisration screen" ,objMobileActions.checkElementDisplayed(mobilenumber));
	}
	public void setMembershipNumberFromConfig() {
		String memNumber = configuration.getConfig("web.Retail.membershipNumber");
		logReporter.log("Enter Value in userName > >", objMobileActions.setTextWithClear(membership, memNumber));
		}

	public void enterDateOfBirthFromConfig() {
		String dob = configuration.getConfig("web.Retail.DOB");
		String date[] = dob.split("/");
		logReporter.log("Enetr day", objMobileActions.setTextWithClear(day, date[0]));
		logReporter.log("Enetr Month", objMobileActions.setTextWithClear(month, date[1]));
		logReporter.log("Enetr year", objMobileActions.setTextWithClear(year, date[2]));
		objMobileActions.pressKeybordKeys(year, "tab");
	}

	
	
}