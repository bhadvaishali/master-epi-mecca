package com.meccabingo.mobile.page.Mecca;

import org.openqa.selenium.By;

import com.generic.MobileActions;
import com.generic.WebActions;
import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.WaitMethods;
import com.generic.webdriver.DriverProvider;

import io.cucumber.java.en.Then;

public class ClubsPage {
	private MobileActions objmobileActions;
	private AppiumDriverProvider objDriverProvider;
	private LogReporter logReporter;
	private WaitMethods waitMethods;
	private Configuration configuration;


	public ClubsPage(AppiumDriverProvider driverProvider,WaitMethods waitMethods, MobileActions mobileActions,
			LogReporter logReporter,Configuration configuration) {
		this.objDriverProvider = driverProvider;
		this.waitMethods = waitMethods;
		this.objmobileActions = mobileActions;
		this.configuration= configuration;
		this.logReporter = logReporter;
	}

	/*public void searchClub(String str) {
		By search = By.xpath("//input[@type='search']");
		objmobileActions.androidScrollToElement(search);
		logReporter.log("populate text for search> >", objmobileActions.setText(search, str));
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
	}

	public void selectFirstClubFromList() {
		waitMethods.sleep(configuration.getConfigIntegerValue("minwait"));
		By firstClub = By.xpath("//ul[@class='find-venue-results']/li[1]"); 
		logReporter.log("click first club from list> >", objmobileActions.click(firstClub));
	}

	public void clickMoreInfo() {
		waitMethods.sleep(configuration.getConfigIntegerValue("minwait"));
		By locator = By.xpath("//a[text()='More Info']");
		logReporter.log("click more info> >", objmobileActions.click(locator));
	}

	public void clickFavorite() {
		By locator = By.xpath("//i[contains(@class,'favorites')]");
		logReporter.log("click favorite> >", objmobileActions.click(locator));
	}

	public void clickLinkText(String strText) {
		By locator = By.xpath("//a[contains(text(),'"+strText+"')]");
		logReporter.log("click link text> >", objmobileActions.click(locator));
	}

	public void verifyMarkedAsFavorite() {
		By locator = By.xpath("//a[contains(@class,'hovered')]");
		if (objmobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("marked as favorite> >",true);
		else
			logReporter.log("marked as favorite> >",false);
	}

	public void verifyNotMarkedAsFavorite() {
		By locator = By.xpath("//a[contains(@class,' hovered')]");
		waitMethods.sleep(2);
		if (objmobileActions.checkElementExists(locator))
			logReporter.log("Not marked as favorite> >",false);
		else
			logReporter.log("Not marked as favorite> >",true);
	}

	public void enterHomePhone(String str) {
		By locator = By.id("home-phone");
		logReporter.log("set text home phone> >", objmobileActions.setText(locator,str));
	}

	public void enterJoinClubEmail(String str) {
		By locator = By.xpath("//input[@type='email']");
		logReporter.log("set text joine club email> >", objmobileActions.setText(locator,str));
	}

	public void userJoinedClub() {
		By locator = By.xpath("//h3[contains(.,'Congratulations!')]//following-sibling::p[contains(.,'re one step closer to enjoying all the fun and excitement of playing bingo at your local Mecca club. Just let a Team Member know that you registered online when you go to play the very first time. Good luck and happy dabbing!)')]");
		if (objmobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("joined club successfully > >", true);
		else
			logReporter.log("joined club successfully > >", false);
	}


	public void verifyFindVenuImage()
	{
		By locator = By.xpath("//section[contains(@class,'find-venue')]//img[contains(@src,'find-nearest-graphic.png')]");
		logReporter.log("Verify Finf venue image", objmobileActions.checkElementDisplayed(locator));
	}

	public void verifyFindyournearestMeccaBingoClubTitle()
	{
		By locator = By.xpath("//section[contains(@class,'find-venue')]//img[contains(@src,'find-nearest-graphic.png')]//following::div//h2[contains(.,'Find your nearest Mecca Bingo Club')]");
		logReporter.log("Verify ' Find your nearest Mecca Bingo Club ' text", objmobileActions.checkElementDisplayed(locator));
	}
	public void verifyClubFinderInfoText()
	{
		By locator = By.xpath("//h2[contains(.,'Find your nearest Mecca Bingo Club')]//following::p[contains(.,'Mecca is not just the home of great bingo games and superb slots - enjoy live entertainment, tasty food and great value drinks, all wrapped up in one venue.')]");
		logReporter.log("Verify 'Mecca is not just the home of great bingo games and superb slots - enjoy live .... ' info text", objmobileActions.checkElementDisplayed(locator));
	}

	public void verifySearchField()
	{
		By locator = By.xpath("//input[@placeholder='Postcode, place name, or region']");
		logReporter.log("Verify 'Postcode, place name, or region' input field", objmobileActions.checkElementDisplayed(locator));
	}
	public void verifyfindNearestCTA()
	{
		By locator = By.xpath("//button[contains(.,'Or find nearest')]");
		logReporter.log("Verify 'find nearest'cta", objmobileActions.checkElementDisplayed(locator));
	}

	public void verifyThesearethenearestcasinostoyourlocationText()
	{
		By locator = By.xpath("//div[contains(@class,'find-venue-results')]//div//h6[contains(.,'These are the nearest casinos to your location')]");
		logReporter.log("Verify 'These are the nearest casinos to your location '", objmobileActions.checkElementDisplayed(locator));
	}
	public void verifyMaximumof5ResultsAreShownBelowTheSearchfield()
	{
		By locator = By.xpath("//div[contains(@class,'find-venue-results')]//div//h6[contains(.,'These are the nearest casinos to your location')]//following-sibling::ul//li//a");
		String expectedResultCnt ="5";
		int searchResults = objmobileActions.processMobileElements(locator).size();
		System.out.println(" ************* size:::  " +searchResults);
		logReporter.log("Verify 'Maximum of 5 Results Are Shown Below The Search field '",expectedResultCnt.equalsIgnoreCase(String.valueOf(searchResults)));
	}
	public void verifyClubNameNMilesDisplayedUnderSearchResult()
	{
		By locator = By.xpath("//ul[@class=\"find-venue-results\"]//li//a//span[@class='miles']");
		logReporter.log("Verify club 'miles' is displayed' under search results", objmobileActions.checkElementDisplayed(locator));
		By locator1 = By.xpath("//ul[@class=\"find-venue-results\"]//li//a//span[@class='miles']//following-sibling::span[contains(@class,'name')]");
		logReporter.log("Verify club 'Name' is displayed' under search results'", objmobileActions.checkElementDisplayed(locator1));
	}
	public void verifyMapArea()
	{
		By locator = By.xpath("//div[contains(@class,'find-venue-map flex')]");
		logReporter.log("Verify 'Google Map area is displayed'", objmobileActions.checkElementDisplayed(locator));
	}

	public void verifyClubInfo(String clubInfoHeaderNm)
	{
		if(clubInfoHeaderNm.contains(",")){
			String[] arr1 = clubInfoHeaderNm.split(",");
			for (String links : arr1  ) {
				By locator = By.xpath("//div[contains(@class,'find-venue-heading-item')]//p[contains(.,'"+links+"')]");
				logReporter.log(links+" is displayed under info box  ",
						objmobileActions.checkElementDisplayed(locator));}}
		else{
			By locator = By.xpath("//div[contains(@class,'find-venue-heading-item')]//p[contains(.,'"+clubInfoHeaderNm+"')]");
			logReporter.log(clubInfoHeaderNm+" is displayed under info box  ",
					objmobileActions.checkElementDisplayed(locator));}
	}
	public void verifyFindVenuButtonsOnClubInfoBox(String btnname)
	{
		if(btnname.contains(",")){
			String[] arr1 = btnname.split(",");
			for (String links : arr1  ) {
				By locator = By.xpath("//div[contains(@class,'find-venue-buttons')]//a[text()='"+links+"']");
				logReporter.log(links+" button is displayed under info box  ",
						objmobileActions.checkElementDisplayed(locator));}}
		else{
			By locator = By.xpath("//div[contains(@class,'find-venue-buttons')]//a[text()='"+btnname+"']");
			logReporter.log(btnname+"  button is displayed under info box  ",
					objmobileActions.checkElementDisplayed(locator));}
	}

	public void clickOnFindVenuButtons(String btnname)
	{
		waitMethods.sleep(configuration.getConfigIntegerValue("minwait"));
		By locator = By.xpath("//div[contains(@class,'find-venue-buttons')]//a[text()='"+btnname+"']");
		logReporter.log("Click on "+btnname,
				objmobileActions.click(locator));
	}

	public void verifyClubDetailsPage()
	{
		By locator = By.xpath("//div[contains(@class,'club-detail')]");
		logReporter.log("Club details page is opened",
				objmobileActions.checkElementDisplayed(locator));
		String url = objmobileActions.getUrl();
		logReporter.log("Club details page is opened",
				url.contains(configuration.getConfig("web.Url")+"bingo-clubs/"));
	}

	public void verifyGoogleMapLinkIsOpend()
	{
		String url = objmobileActions.getUrl();
		logReporter.log("Club details page is opened",
				url.contains("https://www.google.com/maps"));
	}
	public void verifyFieldsUnderVenuFinder(String sectionName)
	{

		switch (sectionName) {
		case "Image":
			verifyFindVenuImage();
			break;
		case "Find your nearest Mecca Bingo Club text":
			verifyFindyournearestMeccaBingoClubTitle();
			break;
		case "Description":
			verifyClubFinderInfoText();
			break;
		case "Field Postcode or town":
			verifySearchField();
			break;
		case "Find Nearest CTA":
			verifyfindNearestCTA();
			break;
		}
	}

	public void verifyClubInformation(String sectionName)
	{
		switch (sectionName) {
		case "Club name":
			verifyClubInfo(sectionName);
			break;
		case "Club phone number":
			verifyClubInfo(sectionName);
			break;
		case "Club address":
			verifyClubInfo(sectionName);
			break;
		case "Todays Opening time":
			verifyClubInfo("s opening time");
			break;
		case "More Info cta":
			verifyFindVenuButtonsOnClubInfoBox("More Info");
			break;
		case "Directions cta":
			verifyFindVenuButtonsOnClubInfoBox("Directions ");
			break;
		}
	}
}
*/
	public void searchClub(String str) {
		By search = By.xpath("//input[@type='search']");
		objmobileActions.click(search);
		logReporter.log("populate text for search> >"+str, objmobileActions.setText(search, str));
		waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
	}

	public void selectFirstClubFromList() {
		waitMethods.sleep(configuration.getConfigIntegerValue("minwait"));
		By firstClub = By.xpath("//ul[@class='find-venue-results']/li[1]"); 
		logReporter.log("click first club from list> >", objmobileActions.click(firstClub));
	}

	public void clickMoreInfo() {
		By locator = By.xpath("//a[text()='More Info']");
		logReporter.log("click more info> >", objmobileActions.click(locator));
	}

	public void clickFavorite() {
		By locator = By.xpath("//i[contains(@class,'icon-favourite-fill')]");
		logReporter.log("click favorite> >", objmobileActions.click(locator));
	}

	public void clickLinkText(String strText) {
		By locator = By.xpath("//a[contains(text(),'"+strText+"')]");
		logReporter.log("click link text> >", objmobileActions.click(locator));
	}
	public void verifybutton(String strText) {
		By locator = By.xpath("//a[contains(text(),'"+strText+"')]");
		logReporter.log("verify link text> >", objmobileActions.checkElementDisplayed(locator));
	}
	public void verifyMarkedAsFavorite() {
		By locator = By.xpath("//a[contains(@class,'active')]//i[contains(@class,'icon-favourite-fill')]");
		String bgcolor = objmobileActions.getCssValue(locator,"color");
		System.out.println("************ bgcolor::  "+bgcolor);
		logReporter.log("Check color of textbox bottom border > >",
				objmobileActions.checkCssValue(locator, "color","#e12482"));

	}

	public void verifyNotMarkedAsFavorite() {
		By locator = By.xpath("//a[not(contains(@class, 'active'))]//i[contains(@class,'icon-favourite-fill')]");
		waitMethods.sleep(2);
		String bgcolor = objmobileActions.getCssValue(locator,"color");
		System.out.println("************ bgcolor::  "+bgcolor);
		System.out.println("************ bgcolor::  "+objmobileActions.checkCssValue(locator, "color","#FFFFFF"));
		/*logReporter.log("Check color of textbox bottom border > >",
				objmobileActions.checkCssValue(locator, "color","#FFFFFF"));*/
		logReporter.log("Verify '", objmobileActions.checkElementDisplayed(locator));
	}

	public void verifyConfirmationMessage(String msg)
	{
		By locator = By.xpath("//p[contains(.,'"+msg+"')]");
		logReporter.log("Verify ' "+msg+ " '", objmobileActions.checkElementDisplayed(locator));
		
	}
	public void enterHomePhone(String str) {
		By locator = By.id("input-mobile");
		logReporter.log("set text home phone> >", objmobileActions.setText(locator,str));
	}

	public void enterJoinClubEmail(String str) {
		By locator = By.xpath("//input[@type='email']");
		logReporter.log("set text joine club email> >", objmobileActions.setText(locator,str));
	}

	public void userJoinedClub() {
	//	By locator = By.xpath("//h5[contains(.,'Congratulations!')]//following::p[contains(.,'re one step closer to enjoying all the fun and excitement of playing bingo at your local Mecca club. Just let a Team Member know that you registered online when you go to play the very first time. Good luck and happy dabbing!)')]");
		By locator = By.xpath("//h5[contains(.,'Congratulations!')]//following::p[contains(.,'re one step closer to enjoying all the fun and excitement of playing bingo at your local Mecca club.  Just let a Team Member know that you registered online when you go to play the very first time.  Good luck and happy dabbing')]");
		if (objmobileActions.checkElementDisplayedWithMidWait(locator))
			logReporter.log("joined club successfully > >", true);
		else
			logReporter.log("joined club successfully > >", false);
	}


	public void verifyFindVenuImage()
	{
		By locator = By.xpath("//section[contains(@class,'find-venue')]//img[contains(@src,'find-nearest-graphic')]");
		logReporter.log("Verify Finf venue image", objmobileActions.checkElementDisplayed(locator));
	}

	public void verifyFindyournearestMeccaBingoClubTitle()
	{
		By locator = By.xpath("//section[contains(@class,'find-venue')]//img[contains(@src,'find-nearest-graphic')]//following::div//h2[contains(.,'Find your nearest Mecca Bingo Club')]");
		logReporter.log("Verify ' Find your nearest Mecca Bingo Club ' text", objmobileActions.checkElementDisplayed(locator));
	}
	public void verifyClubFinderInfoText()
	{
		By locator = By.xpath("//h2[contains(.,'Find your nearest Mecca Bingo Club')]//following::p[contains(.,'Mecca is not just the home of great bingo games and superb slots - enjoy live entertainment, tasty food and great value drinks, all wrapped up in one venue.')]");
		logReporter.log("Verify 'Mecca is not just the home of great bingo games and superb slots - enjoy live .... ' info text", objmobileActions.checkElementDisplayed(locator));
	}

	public void verifySearchField()
	{
		By locator = By.xpath("//input[@placeholder='Postcode, place name, or region']");
		logReporter.log("Verify 'Postcode, place name, or region' input field", objmobileActions.checkElementDisplayed(locator));
	}
	public void verifyfindNearestCTA()
	{
		By locator = By.xpath("//button[contains(.,'Or find nearest')]");
		logReporter.log("Verify 'find nearest'cta", objmobileActions.checkElementDisplayed(locator));
	}

	public void verifyThesearethenearestcasinostoyourlocationText()
	{
		By locator = By.xpath("//div[contains(@class,'find-venue-results')]//div//h6[contains(.,'These are the nearest casinos to your location')]");
		logReporter.log("Verify 'These are the nearest casinos to your location '", objmobileActions.checkElementDisplayed(locator));
	}
	public void verifyMaximumof5ResultsAreShownBelowTheSearchfield()
	{
		By locator = By.xpath("//div[contains(@class,'find-venue-results')]//div//h6[contains(.,'These are the nearest casinos to your location')]//following-sibling::ul//li//a");
		String expectedResultCnt ="5";
		int searchResults = objmobileActions.processMobileElements(locator).size();
		System.out.println(" ************* size:::  " +searchResults);
		logReporter.log("Verify 'Maximum of 5 Results Are Shown Below The Search field '",expectedResultCnt.equalsIgnoreCase(String.valueOf(searchResults)));
	}
	public void verifyClubNameNMilesDisplayedUnderSearchResult()
	{
		By locator = By.xpath("//ul[@class=\"find-venue-results\"]//li//a//span[@class='miles']");
		logReporter.log("Verify club 'miles' is displayed' under search results", objmobileActions.checkElementDisplayed(locator));
		By locator1 = By.xpath("//ul[@class=\"find-venue-results\"]//li//a//span[@class='miles']//following-sibling::span[contains(@class,'name')]");
		logReporter.log("Verify club 'Name' is displayed' under search results'", objmobileActions.checkElementDisplayed(locator1));
	}
	public void verifyMapArea()
	{
		By locator = By.xpath("//div[contains(@class,'find-venue-map flex')]");
		logReporter.log("Verify 'Google Map area is displayed'", objmobileActions.checkElementDisplayed(locator));
	}

	public void verifyClubInfo(String clubInfoHeaderNm)
	{
		if(clubInfoHeaderNm.contains(",")){
			String[] arr1 = clubInfoHeaderNm.split(",");
			for (String links : arr1  ) {
				By locator = By.xpath("//div[contains(@class,'find-venue-heading-item')]//p[contains(.,'"+links+"')]");
				logReporter.log(links+" is displayed under info box  ",
						objmobileActions.checkElementDisplayed(locator));}}
		else{
			By locator = By.xpath("//div[contains(@class,'find-venue-heading-item')]//p[contains(.,'"+clubInfoHeaderNm+"')]");
			logReporter.log(clubInfoHeaderNm+" is displayed under info box  ",
					objmobileActions.checkElementDisplayed(locator));}
	}
	public void verifyFindVenuButtonsOnClubInfoBox(String btnname)
	{
		if(btnname.contains(",")){
			String[] arr1 = btnname.split(",");
			for (String links : arr1  ) {
				By locator = By.xpath("//div[contains(@class,'find-venue-buttons')]//a[text()='"+links+"']");
				logReporter.log(links+" button is displayed under info box  ",
						objmobileActions.checkElementDisplayed(locator));}}
		else{
			By locator = By.xpath("//div[contains(@class,'find-venue-buttons')]//a[text()='"+btnname+"']");
			logReporter.log(btnname+"  button is displayed under info box  ",
					objmobileActions.checkElementDisplayed(locator));}
	}

	public void clickOnFindVenuButtons(String btnname)
	{
		By locator = By.xpath("//div[contains(@class,'find-venue-buttons')]//a[text()='"+btnname+"']");
		logReporter.log("Click on "+btnname,
				objmobileActions.click(locator));
	}

	public void verifyClubDetailsPage()
	{
		By locator = By.xpath("//div[contains(@class,'club-detail')]");
		logReporter.log("Club details page is opened",
				objmobileActions.checkElementDisplayed(locator));
		String url = objmobileActions.getUrl();
		logReporter.log("Club details page is opened",
				url.contains(configuration.getConfig("web.Url")+"bingo-clubs/"));
	}

	public void verifyGoogleMapLinkIsOpend()
	{
		String url = objmobileActions.getUrl();
		logReporter.log("Club details page is opened",
				url.contains("https://www.google.com/maps"));
	}
	public void verifyFieldsUnderVenuFinder(String sectionName)
	{

		switch (sectionName) {
		case "Image":
			verifyFindVenuImage();
			break;
		case "Find your nearest Mecca Bingo Club text":
			verifyFindyournearestMeccaBingoClubTitle();
			break;
		case "Description":
			verifyClubFinderInfoText();
			break;
		case "Field Postcode or town":
			verifySearchField();
			break;
		case "Find Nearest CTA":
			verifyfindNearestCTA();
			break;
		}
	}

	public void verifyClubInformation(String sectionName)
	{
		switch (sectionName) {
		case "Club name":
			verifyClubInfo(sectionName);
			break;
		case "Club phone number":
			verifyClubInfo(sectionName);
			break;
		case "Club address":
			verifyClubInfo(sectionName);
			break;
		case "Todays Opening time":
			verifyClubInfo("s opening time");
			break;
		case "More Info cta":
			verifyFindVenuButtonsOnClubInfoBox("More Info");
			break;
		case "Directions cta":
			verifyFindVenuButtonsOnClubInfoBox("Directions ");
			break;
		}
	}
	
	public void verifyFavouritedClubSection()
	{
		By locator = By.xpath("//h2[contains(.,'Your favourite club is')]");
		logReporter.log("'Your favourite club is' is displayed on screen ",
				objmobileActions.checkElementDisplayed(locator));
		//verifybutton("Tell Me More");
		//verifybutton("Change Club");
		//verifyFavouritedClubNavigation("Order food,Map,Bingo Schedule,Book Now");
	}
	
	public void verifyFavouritedClubNavigation(String btnname)
	{
		if(btnname.contains(",")){
			String[] arr1 = btnname.split(",");
			for (String links : arr1  ) {
				By locator = By.xpath("//apollo-club-detail-sub-menu-item[@title='"+links+"']");
				logReporter.log(links+"  is displayed  ",
						objmobileActions.checkElementDisplayed(locator));}}
		else{
			By locator = By.xpath("//apollo-club-detail-sub-menu-item[@title='"+btnname+"']");
			logReporter.log(btnname+" is displayed  ",
					objmobileActions.checkElementDisplayed(locator));}
	}
}
