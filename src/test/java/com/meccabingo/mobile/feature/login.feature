Feature: Login 

@Android @AndroidHiddenLive
Scenario: Check whether system displays Title, close “X“ icon, Fileds, Buttons, Links and Support area on Login page
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
And Verify system displays login Title
And Verify close 'X' icon
And Verify 'Username' field
And Verify 'Password' field
And Verify Toggle within password field
And Verify Remember me checkbox
And Verify 'Log In' CTA
And Verify Forgot username link
And Verify Forgot password link
And Verify 'New to Mecca Bingo?' text
And Verify Sign up link
And Verify Help contact information
And Verify Live Chat link


@Android @AndroidHiddenLive
Scenario: Login Success_Check whether when user login successfully to the site login overlay gets closed and display page from whether user started journey
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username
And User enters password
Then Verify Login button gets enabled
Then User clicks on login button
Then Verify user is successfully logged in

@Android
Scenario: Login Remember Me_Check whether system remember username for next login when user selected Remember me tick box while login to site
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username "MBxRocdz"
Then User enters password "KHIilu87"
Then User clicks on 'Remember Me' tick
Then User clicks on login button
Then Click on my account button
Then Click on logout button
Then User clicks on Login Button from header of the Page
Then Verify Username auto pupulated with "MBxRocdz"

@AndroidHiddenLive
Scenario: Login Remember Me_Check whether system remember username for next login when user selected Remember me tick box while login to site
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username "hl27aug21"
Then User enters password "Test1234"
Then User clicks on 'Remember Me' tick
Then User clicks on login button
Then Click on my account button
Then Click on logout button
Then User clicks on Login Button from header of the Page
Then Verify Username auto pupulated with "hl27aug21"

@Android @AndroidHiddenLive
Scenario: Check whether system displays Title “Forgotten your password”, Back arrow, close “X“ icon, helper text “Username” field, “Send reset instructions” CTA, “Forgotten username” link and  Help contact information, and “Live support“ link on Forgotten your password page
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User clicks on forgot password link
Then Verify system displays Forgotten your password page with header as 'Forgot your password'
And Verify back arrow '<'
And Verify close 'X' icon
And Verify helper text "Please supply the following account information and we'll send you an email with password reset instructions"
And Verify 'Username' field
And Verify 'Send reset instructions' CTA
And Verify Forgot username link
And Verify Help contact information
And Verify Live Chat link

@Android @AndroidHiddenLive
Scenario: Check whether system displays Success message when system sends email on Registered Email ID
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User clicks on forgot password link
Then Verify system displays Forgotten your password page with header as 'Forgot your password'
Then User enters username
Then User clicks on 'Send reset instructions'
Then Verify Success Message displayed to user
Then Verify I didnot receive an email link

@Android @AndroidHiddenLive
Scenario: Check whether system displays Title, back arrow, close “X“ icon, helper text, Fileds, Buttons, Links and Support area on Forgotten your password page
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User clicks on forgot usename link
Then Verify system displays Forgotten your username page with header as 'Forgotten your Username'
And Verify back arrow '<'
And Verify close 'X' icon
And Verify helper text "Please supply the following account information and we'll send you an email with username"
And Verify 'Email' field
And Verify 'Send usename reminder' CTA
And Verify Forgot password link
And Verify Help contact information
And Verify Live Chat link

@Android
Scenario: Check whether system displays Success message when system sends username remainder email on Registered Email ID
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User clicks on forgot usename link
Then Verify system displays Forgotten your username page with header as 'Forgotten your Username'
Then User enters email "q6F4V@mailinator.com"
Then User clicks on 'Send username reminder'
Then Verify Success Message displayed to user
Then Verify I didnot receive an email link

@AndroidHiddenLive
Scenario: Check whether system displays Success message when system sends username remainder email on Registered Email ID
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User clicks on forgot usename link
Then Verify system displays Forgotten your username page with header as 'Forgotten your Username'
Then User enters email "WKi4R@mailinator.com"
Then User clicks on 'Send username reminder'
Then Verify Success Message displayed to user
Then Verify I didnot receive an email link

@Android @AndroidHiddenLive
Scenario: Check whether the system displays a notification message when user tries to login with invalid details
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
#Then User enters username "meccaauto01"
Then User enters username
And User enters password "Password12s3"
Then User clicks on login button
Then Verify text "Sorry, your login details are not valid" displayed

@Android
Scenario: Login Failed Break user_Check whether the system displays a notification message when user tries to login during “break set period” and does not allow user to login
When User clicks on Login Button from header of the Page
Then User enters username "auto_breakuser"
Then User enters password "Tbreak1234"
Then User clicks on login button
Then Verify text "You have chosen to take a break from Mecca Bingo. You can login again from" displayed

@Android
Scenario: FLOG Password_Check whether system send reset instructions mail on Registered Email ID when user enters username in Username field and click on Send Reset instructions CTA from Forgotten your password Page
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User clicks on forgot password link
Then User enters username "mbgmmpyz"
Then User clicks on 'Send reset instructions'
Then Verify Success Message displayed to user
Then Invoke Mailinator site
Then Verify system navigate user to "https://www.mailinator.com/" url
Then Sreach the mailbox of the "q6F4V@mailinator.com" email address which associated with the username
Then Verify User receive Reset Password mail

@Android
Scenario: FLOG Username_Check whether system send username remainder mail on Registered Email ID when user enters valid Email in Email Address field and click on Send username reminder CTA from Forgotten your username Page
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User clicks on forgot usename link
Then Verify system displays Forgotten your username page with header as 'Forgotten your Username'
Then User enters email "q6F4V@mailinator.com"
Then User clicks on 'Send username reminder'
Then Invoke Mailinator site
Then Verify system navigate user to "https://www.mailinator.com/" url
Then Sreach the mailbox of the "q6F4V@mailinator.com" email address which associated with the username
Then Verify User receive Username Reminder mail
And Verify "mbgmmpyz" username is displayed in received mail

@Android
Scenario: FLOG Password_Check whether user able to change password using received link
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User clicks on forgot password link
Then User enters username "mbgmmpyz"
Then User clicks on 'Send reset instructions'
Then Verify Success Message displayed to user
Then Invoke Mailinator site
Then Verify system navigate user to "https://www.mailinator.com/" url
Then Sreach the mailbox of the "q6F4V@mailinator.com" email address which associated with the username
Then Verify User receive Reset Password mail
Then Open Email and Click on Password Reset link
Then Verify Reset Password screen is displayed
Then Enter new password and hit submit button
Then Verify 'Your password has been reset. Log in to play' message
Then Verify user can login with new password

@Android
Scenario: FLOG Password_Check whether system displays appropriate error message provided user enters invalid / mismatch new and confirm passwords
Then Invoke Mailinator site
Then Verify system navigate user to "https://www.mailinator.com/" url
Then Sreach the mailbox of the "q6F4V@mailinator.com" email address which associated with the username
Then Verify User receive Reset Password mail
Then Open Email and Click on Password Reset link
Then Verify Reset Password screen is displayed
Then Enter invalid passwords and hit submit button
Then Verify error message "Your password must be at least 8 characters long, contain 1 upper-case letter, and a minimum of 1 number. "


@Android
Scenario: Login Failed Self Excluded User_Check whether the system displays a notification message when Self Excluded Status user tries to Login to the Site
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username "test17march"
And User enters password "Test1234"
Then User clicks on login button
#Then Verify text "The user is self excluded" displayed
Then Verify text "The user is self-excluded" displayed

@Android
Scenario: Account Lock_Check whether account gets lock after 5 attempts of incorrect Password.
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username
Then User enters password "jjfgh"
Then User clicks on login button
Then Verify text "Sorry, your login details are not valid" displayed
Then User clicks on login button
Then Verify text "Sorry, your login details are not valid" displayed
Then User clicks on login button
Then Verify text "Sorry, your login details are not valid" displayed
Then User clicks on login button
Then Verify text "Sorry, your login details are not valid" displayed
Then User clicks on login button
Then Verify text "Your account has been locked. Please contact support" displayed
Then Invoke the Web portal "https://cogs_staging.dagacube.net/Default.aspx"
#And Enter username as "Aishwarya" and password as "AishwaryaB1234@"
And Enter username as "Satya" and password as "0o9i8u7y^T"
Then Click on Sign In button
Then Select "Search Players" submenu from "Players" menu
And Search players by username
And Verify Player Account status is displayed as "Multiple invalid login attempts"
Then Verify Player account status is displayed as "Multiple invalid login attempts"
Then Change player status to active
Then Back to Mecca site
And User enters password
Then User clicks on login button
Then Verify user is successfully logged in