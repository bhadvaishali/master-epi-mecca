Feature: Recently Played

@Android
Scenario: Check that the customer can enter the address manually by selecting the enter it manually option available near Address field
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Enter the password in valid format
Then Enter the firstname in valid format
Then Enter the surname in valid format
Then Enter Date of birth "15" "04" "1954"
Then Enter valid mobile number
Then Click on enter address manually button
Then Enter address manually
Then Click on select all in marketing preference section
Then Click on Register CTA
Then Verify Deposit Now header
Then Verify "Why not set a deposit limit?" link
#Then Click on deposit close button

@Android @AndroidHiddenLive
Scenario: Check whether System displays recently played tab evnthough user do not have any recently played games for Slots Game Panel
 Given User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
And Verify user is successfully logged in
Then Verify "Recently Played Games" tab displayed on home page
And Verify You dont have any recently played games message displayed
		