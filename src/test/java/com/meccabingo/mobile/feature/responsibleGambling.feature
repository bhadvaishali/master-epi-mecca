Feature: Responsible Gambling

@Android @AndroidHiddenLive
Scenario: Check whether system displays Header Body and Footer section on My Account Responsible Gambling section
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Responsible Gambling" option
Then Verify following option on responsible gambling page:
|Close button|
|Back button|
|Responsible Gambling Title|
|Reality Check|
|Deposit Limits|
|Take a break|
|Self Exclude|
|Gamstop|
|Email|
|Phone number|
|Live chat|

@Android @AndroidHiddenLive
Scenario: Check whether system Navigate to respective Pages when click on Links available on Responsible Gambling page
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Responsible Gambling" option
Then Click on "Reality Check" option
Then Verify text "Reality Check" displayed
Then Click on back button
Then Click on "Deposit Limits" option
Then Verify text "Deposit Limits" displayed
Then Click on back button
Then Click on "Take a break" option
Then Verify text "Take a break" displayed
Then Click on back button
Then Click on "Self Exclude" option
Then Verify text "Self Exclude" displayed
Then Click on back button
Then Click on "GamStop" option
Then Verify text "GamStop" displayed
Then Click on back button

@Android
Scenario: Check whether system displays Header , Body on My Account Responsible gaming - Reality Checks page
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Responsible Gambling" option
Then Click on "Reality Check" option
Then Verify following option on  reality check page:
|Close button|
|Back button|
|Reality Check Title|
|Live help link|
|Tell me more|
|Tell me more collapsible icon|
|Set your reminder|
|1 mins button|
|30 mins button|
|1 hour button|
|Save changes button|

@AndroidHiddenLive
Scenario: Check whether system displays Header , Body on My Account Responsible gaming - Reality Checks page
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Responsible Gambling" option
Then Click on "Reality Check" option
Then Verify following option on  reality check page:
|Close button|
|Back button|
|Reality Check Title|
|Live help link|
|Tell me more|
|Tell me more collapsible icon|
|Set your reminder|
|15 mins button|
|30 mins button|
|1 hour button|
|Save changes button|

@Android @AndroidHiddenLive
Scenario: Check whether system displays Text box on My Account Responsible gaming - Reality Checks page
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Responsible Gambling" option
Then Click on "Reality Check" option
And Verify text in text box
Then Click on uncollapsible icon
And Verify system displays text

@Android
Scenario: Check whether user able to set new relaity check by selecting any of the options from list on My Account Responsible gaming - Reality Checks page
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Responsible Gambling" option
Then Click on "Reality Check" option
Then click button having label as "1 mins"
Then click button having label as "Save changes"
And Click on back button
Then Click on "Reality Check" option
Then Verify "1 mins" reminder button is highlighted

@AndroidHiddenLive
Scenario: Check whether user able to set new relaity check by selecting any of the options from list on My Account Responsible gaming - Reality Checks page
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Responsible Gambling" option
Then Click on "Reality Check" option
Then click button having label as "15 mins"
Then click button having label as "Save changes"
And Click on back button
Then Click on "Reality Check" option
Then Verify "15 mins" reminder button is highlighted

@Android @AndroidHiddenLive
Scenario: Check whether system displays Header , Body on My Account Responsible gaming - Take a break page
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Responsible Gambling" option
Then Click on "Take a break" option
Then Verify following option on take a break page:
|Close button|
|Back button|
|Take a break Title|
|Live help link|
|Text box|
|Break collapsible box|
|Break time question|
|Break times buttons|
And Verify following break periods
|1 Day|
|2 Days|
|1 Week|
|2 Weeks|
|4 Weeks|
|6 Weeks|
Then Click on break time button
Then Verify following options:
|Locked until date|
|Break rules|
|Password field|
|Take a break button|

@Android @AndroidHiddenLive
Scenario: Check whether system displays Text box along with “Why to take a break“ Box + a collapse/uncollapse icon on My Account Responsible gaming - Take a break page
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account button
Then Click on "Responsible Gambling" option
Then Click on "Take a break" option
Then Verify break period text
Then Click on plus sign of "take a break" page
Then Verify message "If you wish to take a break for a different length of time, please contact Customer Support and a member of our team will be able to help you. By choosing to take a break, you will not be able to access your account and you will be prevented from gambling until your break period has finished."
Then Verify message "You may extend your break period at any time by contacting our Customer Support Team."
Then Verify message "Your break will end automatically once the selected time period has passed and your account will be re-opened at this time."

@Android @AndroidHiddenLive
Scenario: Check whether system displays error message when user enter try to apply for Take a break page with invalid password
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Responsible Gambling" option
Then Click on "Take a break" option
Then Verify "Take a break" as header displayed
Then click button having label as "1 Day"
Then Enter password "@#$%" for take a break
Then click button having label as "Take A Break"
Then Verify error message "You have entered a wrong password"

@Android @AndroidHiddenLive
Scenario: Check whether system displays Take a break confirmation pop up when user selects any of the option for Take a break and Password on My Account Responsible gaming - Take a break page and click on Take a break CTA
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Responsible Gambling" option
Then Click on "Take a break" option
Then Verify "Take a break" as header displayed
Then click button having label as "1 Day"
Then Enter valid password for take a break
Then click button having label as "Take A Break"
Then Verify message " you are choosing to start your break. Your account will be locked until "
Then Verify button having label as "Take a break & Log Out" displayed
Then Verify button having label as "Cancel" displayed

@Android
Scenario: Check whether user gets logged out from Site and unabel to login back till break time completed on click of “Take a break & Log Out” CTA from Confirmation Pop up
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Responsible Gambling" option
Then Click on "Take a break" option
Then Verify text "Take a break" displayed
Then click button having label as "1 Day"
Then Enter valid password for take a break
Then click button having label as "Take A Break"
Then click button having label as "Take a break & Log Out"
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Verify "Your account is currently locked as being on 'Take a Break'. Please contact support" displayed
Then Invoke the Web portal "https://cogs_staging.dagacube.net/Default.aspx"
#And Enter username as "Aishwarya" and password as "AishwaryaB1234@"
And Enter username as "Satya" and password as "0o9i8u7y^T"
Then Click on Sign In button
Then Select "Search Players" submenu from "Players" menu
And Search players by username
And Verify Player Account status is displayed as "Take a Break"
Then Verify Take a break end date is displayed correctly
Then Remove break period 
#Then Back to Mecca site
Then User clicks on login button from login window
Then Verify user is successfully logged in

@Android @AndroidHiddenLive
Scenario: Check whether System displays Header and Body for My Account - Responsible gaming - Self Exclusion screen 1
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Responsible Gambling" option
Then Click on "Self Exclude" option
Then Verify following options are displayed on self exclude screen
|Close button|
|Back button|
|Self Exclude Title|
|Question|
|Yes option|
|No option|

@Android @AndroidHiddenLive
Scenario: Check whether System displays different options for My Account - Responsible gaming - Self Exclusion screen 2(NO)
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Responsible Gambling" option
Then Click on "Self Exclude" option
And Click on "No" from the screen
Then Verify "Reality Check" option
Then Verify "Deposit Limits" option
Then Verify "Take a Break" option
Then Verify information text "Alternatively, please get in touch if you want to Self Exclude for different reasons on 0800 083 1990" displayed

@Android @AndroidHiddenLive @Android @AndroidHiddenLiveRegression
Scenario: Check whether System displays different options for My Account - Responsible gaming - Self Exclusion screen 2(Yes)
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Responsible Gambling" option
Then Click on "Self Exclude" option
And Click on "Yes" from the screen
Then Verify information box
Then Verify message "You can block yourself from playing with Mecca for a chosen period of time. This can only be reversed by written request after that period has ended."
Then Click on plus sign of "Self Exclude" page
Then Verify message "If you believe you might have a problem with gambling, it is advisable to stop gambling altogether, or for a prolonged period. We would also recommend that you seek guidance from the support agencies listed on our keepitfun website."
And Verify bold text "Why Self Exclude?" displayed
And Verify bold text "How long do you want to lock your account for?" displayed
Then Verify button "6 months"
Then Verify button "1 year"
Then Verify button "2 years"
Then Verify button "5 years"
Then Click on button "6 months"
And Verify date is visible
And Verify password field
Then Verify button "Yes, I want to Self Exclude"

@Android @AndroidHiddenLive
Scenario: Check whether system displays error message when user enter try to apply for Self Exclusion with invalid password
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Responsible Gambling" option
Then Click menu option "Self Exclude"
Then Verify "Self Exclude" as header displayed
Then click button having label as "Yes"
Then click button having label as "6 Months"
Then Enter password "@#$%" for take a break
Then click button having label as "Yes, I want to Self Exclude"
Then Verify error message "You have entered a wrong password"

@Android55 @AndroidHiddenLive55
Scenario: Check whether user gets self excluded and logged out from site when user clicks on Self Exclude and Log out cta from confirmation pop up
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Self Exclude"
Then Verify "Self Exclude" as header displayed
Then click button having label as "Yes"
Then click button having label as "6 Months"
Then Enter valid password for take a break
Then click button having label as "Yes, I want to Self Exclude"
Then Verify bold text "Are you sure?" displayed
Then Verify button having label as "Self exclude & Log out" displayed
Then click button having label as "Cancel"
Then click button having label as "Yes, I want to Self Exclude"
Then Verify button having label as "Self exclude & Log out" displayed
Then click button having label as "Self exclude & Log out"
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
#Then Verify text "The user is self excluded" displayed
Then Verify text "The user is self-excluded" displayed

@Android @AndroidHiddenLive
Scenario: Check whether system displays Header , Body on My Account Responsible gaming - Gamstop page  
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Responsible Gambling" option
Then Click on "GamStop" option
Then Verify following option on gamstop page:
|Close button|
|Back button|
|Gamstop Title|
Then Verify message "If you are considering self-exclusion, you may wish to register with GAMSTOP."
Then Verify message "GAMSTOP is a free service that enables you to self-exclude from all online gambling companies licensed in Great Britain."
Then Verify message "To find out more and to sign up with GAMSTOP please visit "

@Android @AndroidHiddenLive
Scenario: Check whether system displays Text box on My Account Responsible gaming - Gamstop page 
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Responsible Gambling" option
Then Click on "GamStop" option
Then Verify and Click on Gamestop link from gamestop screen
Then Switch to child window
Then Verify system navigate user to "https://www.gamstop.co.uk/" url

@Android @AndroidHiddenLive
Scenario: Responsible Gambling > Check whether an option is always available for customer to set their deposit limit at all time
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
#Then Click on my account button
Then Click on Deposit button besides myaccount
Then Verify "Why not set deposit limits?" link
Then Click on "Why not set deposit limits?" link
Then Verify "Deposit Limits" as header displayed
Then Click close 'X' icon
Then Click on my account
Then Click menu option "Responsible Gambling"
Then Verify "Responsible Gambling" as header displayed
Then Click menu option "Deposit Limits"
Then Verify "Deposit Limits" as header displayed