Feature: My Account

@Android @AndroidHiddenLive
Scenario: My Account Main Menu_Check whether system displays all required sections on My Account Overlay page when accessed
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account button
Then Verify following options are displayed on my account page: 
|Username|
|Close button|
|Playable Balance|
|Cashier|
|Bonuses|
|Messages|
|Account details|
|Responsible gambling|
|Log out icon|
|Log out link|
|Email|
|Phone number|
|Live chat|

@Android @AndroidHiddenLive
Scenario: System displays Balance block in expanded state when user access My Account Page and click on + button from Balance section
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account button
Then Click on playable balance icon
Then Verify following options are displayed on playable balance section:
|playable Balance|
|playable balance amount|
|playable balance minus icon|
|cash text|
|cash amount|
|bonuses text|
|bonuses amount|
|detailed view button|
|deposit button|

@Android @AndroidHiddenLive
Scenario: Check whether no action trigger when user click on Cash and Bonuses title or amount from Balance section
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account button
Then Click on playable balance icon
Then Click on following options:
|cash text|
|cash amount|
|bonuses text|
|bonuses amount|

@Android @AndroidHiddenLive
Scenario: Check whether system navigate user to respective page when user clicks on Detailed View CTA and Deposit CTA
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account button
Then Click on playable balance icon
Then Click on detailed view button
Then Verify button navigates to balance page
Then Click on back button
Then Click on playable balance icon
Then click button having label as "Deposit"
Then Verify "Deposit" as header displayed

@Android @AndroidHiddenLive
Scenario: Check whether system displays Header,Body on My Account ACCOUNT DETAILS page
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Account Details" option
Then Verify following option on account details page:
|Close button|
|account details Title|
|Back button|
|Last Login|
|Name|
|D.O.B|
|Email|
|Phone|
|Postcode|
|Add Membership No|
|edit button|
|change password|
|marketing preference|
|Support Email|
|Phone number|
|Live chat|

@Android @AndroidHiddenLive
Scenario: My Account Edit Details_Check whether user unable to edit name, DOB
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on my account button
Then Click menu option "Account Details"
Then Verify "Account Details" as header displayed
Then click button having label as "Edit"
Then Verify "Edit Details" as header displayed
Then Verify Name and date of birth field is non editable

@Android @AndroidHiddenLive
Scenario: Check whether user able to select populated address for address fields from My Account > Edit Details Page
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Account Details" option
Then click button having label as "Edit"
Then click button having label as "Clear"
Then Enter valid postcode "ha01ab"
And Select one address from dopdown
Then Verify address is selected

@Android @AndroidRegression @AndroidHiddenLive
Scenario: Check whether changed details gets saved for customer account on click of Update CTA
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Account Details" option
Then click button having label as "Edit"
Then Edit email address,Phone number and address
Then Enter password "@#$%" for take a break
Then click button having label as "Update"
Then Verify error message "You have entered a wrong password"
Then Enter valid password for take a break
Then click button having label as "Update"
Then Verify text "Account details updated" displayed
Then Click on back button
Then Click on logout button
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Verify user is successfully logged in

@Android @AndroidHiddenLive
Scenario: Check whether user navigate to respective section on click of links from My Account > Balance overlay
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Verify "Cashier" as header displayed
Then Click menu option "Balance"
Then Verify "Balance" as header displayed
Then Verify link "deposit_limits" of sub menu page
Then Verify 'Live help' link
And Verify back arrow '<'
And Verify close 'X' icon
Then Verify bold text "Playable Balance" displayed
Then Verify button having label as "Cash" displayed
Then Verify button having label as "All game Bonuses" displayed
Then Verify button having label as "Deposit" displayed
Then Verify link "BonusHistory" of sub menu page

@Android26 @AndroidRegression
Scenario: Check whether system displays success message when clicks on Withdraw button from Safecharge iFrame  when user do not have active bonuses for account
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Verify "Cashier" as header displayed
Then Click menu option "Withdrawal"
Then Verify "Withdrawal" as header displayed
Then Enter withdrawal amount "10"
Then Enter withdrawal password "Test1234"
Then click button having label as "Next"
Then Verify text "Are you sure" displayed
Then click button having label as "Continue"
Then Click withdraw
Then Verify withdraw is successful 


@Android26 @AndroidRegression
Scenario: Check whether system displays Transactions History according to option selected from list 
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Verify "Cashier" as header displayed
Then Click menu option "Transaction History"
Then Verify "Transaction History" as header displayed
Then Select "Deposits" filter activity
Then Click button Filter
Then Verify records are found of "Deposit"
Then Select "Withdrawals" filter activity
Then Click button Filter
Then Verify records are found of "Withdrawal"
Then Select "Withdrawals" filter activity
Then Click button Filter
Then Select "Net deposits" filter activity
Then Select "Bonuses" filter activity
Then Click button Filter

@Android @AndroidHiddenLive
Scenario: Check whether system displays new drop down list when user selects Net Deposits from filter option
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Verify "Cashier" as header displayed
Then Click menu option "Transaction History"
Then Verify "Transaction History" as header displayed
Then Select "Net deposits" filter activity
#Then Verify option "Last 24 Hours" displayed
#Then Verify option "6 Months" displayed
#Then Verify option "Last Week" displayed
Then Verify option "All Time" displayed
#Then Select SortBy "Last 30 Days"

@Android26
Scenario: Check whether system displays Collapsed and uncollapsed version for Transactions
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Cashier"
Then Verify "Cashier" as header displayed
Then Click menu option "Transaction History"
Then Verify "Transaction History" as header displayed
Then Select "Deposits" filter activity
Then Click button Filter
Then Collapse one record
Then Verify the collapsed record

@Android @AndroidHiddenLive
Scenario: Check whether user able to update options selected for marketting preferences from my account 
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Verify balance section is displayed
Then Click on my account
Then Click menu option "Account Details"
Then Verify "Account Details" as header displayed
Then Click menu option "Marketing Preferences"
Then Verify "Keeping you informed" as header displayed
Then User will get the option to select between email, phone, sms, post and Select all
Then Uncheck all the options
Then Click on "post" checkbox
Then Verify "post" checkbox is checked
Then Click on "post" checkbox
Then Verify "post" checkbox is unchecked
Then Click on "all" checkbox
Then contact preferences checkboxes are checked
Then Click on "all" checkbox
Then contact preferences checkboxes are unchecked
Then Click on "email" checkbox
Then click button having label as "UPDATE"
Then Verify text "Account details updated" displayed

@Android @AndroidHiddenLive
Scenario: Check whether system displays error message when user enters incorrect current password on change password screen
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on my account button
Then Click menu option "Account Details"
Then Click menu option "Change Password"
Then Verify "Change Password" as header displayed
Then User enters incorrect current password as "ffhjf"
And Enter valid New password as "Password1234"
Then click button having label as "UPDATE"
Then Verify error message "Current password is incorrect"

@Android @AndroidHiddenLive
Scenario: My Account Change Password_Check whether system displays Password Updated message when user enters Current passsword and New password in valid format and click on Update CTA
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on my account button
Then Click menu option "Account Details"
Then Click menu option "Change Password"
Then Verify "Change Password" as header displayed
Then User enters correct current password
And Enter valid New password as "Test1234"
Then click button having label as "UPDATE"
Then Password updated confirmation message will display
Then Click on back button
Then Click on logout button
Then User clicks on Login Button from header of the Page
Then User enters username
And Enter updated password
Then User clicks on login button from login window
Then Verify user is successfully logged in

@Android @AndroidHiddenLive
Scenario: Account Information > Check whether customer can view Last Login time on My Account / Account history section under Personal Details section in date-time format
When User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on my account button
Then Click menu option "Account Details"
Then User can see Last Login time in date-time format
Then Click on back button
Then Logout user and get logout time
When User clicks on Login Button from header of the Page
Then Verify Login Page displayed
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on my account button
Then Click menu option "Account Details"
And Verify that previous login date and time is displayed as Last login time

@Android @AndroidHiddenLive
Scenario: My Account-Add membership number_ Check that error message is displayed when user tries to add membership number to Digital user account
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Account Details" option
Then Verify "Account Details" as header displayed
And Verify "Add Virtual Card" button is displayed on screen
Then Click on "Add Virtual Card" button from screen
Then Add the membership number "907277501" in Membership no Field
Then Click on Submit link
Then Verify error message "Something’s not quite right. Contact customer services and we’ll give you a hand."

@Android7733
Scenario: My Account->Account Details_Check whether membership number is appeared on Account details page when user add membership number
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Account Details" option
Then Verify "Account Details" as header displayed
And Verify "Add Membership No" field is displayed on screen
Then Add the membership number "907277501" in Membership no Field
Then Click on Submit link
Then Membership number added successfully 
And Verify membership number "907277501" is displayed on Account details page