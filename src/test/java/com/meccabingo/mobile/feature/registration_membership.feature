Feature: Registration Page (Membership)

#EMC-45
@Android @AndroidHiddenLive
Scenario: Check whether system displays “Membership number sign up” screen with “Card membership number”, “Date of Birth” fields, "Terms & Conditions" checkbox, "Next" CTA on click of “No (I have a membership card)” CTA from First step of Registration page 
Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Verify following option displays on “Membership number sign up” screen 
|Card membership number” field|
|Date of Birth fields|
|Terms & Conditions checkbox|
|Next button|

@Android @AndroidHiddenLive
Scenario: Check whether Next Cta gets active when user enters membership number and DOB in correct format and selected “Terms & Conditions”  checkbox on “Membership number sign up” screen 
Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Enter membership number "907276932"
Then Enter Date of birth "11" "08" "1996"
Then Click on age checkbox on membership 
Then Verify Next button gets enabled

#EMC-771
@Android @AndroidHiddenLive
Scenario: Check whether system displays error message along with Login link when user enters existing membership number while registration
Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Enter membership number "907277483"
Then Enter Date of birth "23" "10" "1973" 
Then Click on age checkbox on membership 
Then Click on Next button
Then Verify message "Good news - it looks like you’ve already got an account! Enter your username and password to log in. Forgotten your password? You can reset it here."

#EMC-801 
@Android @AndroidHiddenLive
Scenario: if membership user with membership details are not found with backend then the system should display an error message below the “Card membership number” field
Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Enter membership number "123456456"
Then Enter Date of birth "11" "08" "1996" 
Then Click on age checkbox on membership 
Then Click on Next button
Then Verify message "Something’s not quite right. Contact "

#EMC-770
@Android @AndroidHiddenLive
Scenario: Check whether system displays Account details and Registration Form for entered membership user when Number and DOB entered correctly
Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Enter membership number "907277413"
Then Enter Date of birth "21" "07" "1974" 
Then Click on age checkbox on membership 
Then Click on Next button
Then Verify text "Create a login and get started!" displayed
Then Verify following fields are displayed under 'Your personal details' section
|Title|
|Name|
|Date of Birth|
Then Verify "Not You?" link
Then Verify text "Almost there!" displayed
Then Verify username field is displayed
Then Verify password field is displayed
Then Verify Email id field is displayed on screen
Then Verify Mobile number field is displayed on screen
Then Verify enter address manually button
Then Verify "Select All" button
Then Verify following checkboxes:
|Email|
|SMS|
|Phone|
|Post| 
#Then Verify "I’d rather not receive offers and communications" checkbox

@Android @AndroidHiddenLive
Scenario: Check whether system navigate back to Registration page 1 when user click on “Not you?“ link from account detaisl section from Registration page 2
Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Enter membership number "907277413"
Then Enter Date of birth "21" "07" "1974" 
Then Click on age checkbox on membership 
Then Click on Next button
Then Click on Not you? link
Then Verify link navigate to 'Sign Up' page

@Android @AndroidHiddenLive
Scenario: Check whether system correct validations for Email Address fields on Membership Registration page 2
Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Enter membership number "907277413"
Then Enter Date of birth "21" "07" "1974" 
Then Click on age checkbox on membership 
Then Click on Next button
Then Enter invalid email address
Then Verify error message "You must enter a valid email address"
Then Enter valid email on second page
Then Verify green line is displayed below email field


@Android @AndroidHiddenLive
Scenario: Check whether system correct validations for Mobile fields on Membership Registration page 2
Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Enter membership number "907277413"
Then Enter Date of birth "21" "07" "1974" 
Then Click on age checkbox on membership 
Then Click on Next button
Then Enter invalid mobile number
Then Verify error message "Please enter a valid phone number between 8 and 11 digits long"
Then Verify red line displays below mobile number field
Then Enter valid mobile number
Then Verify green line is displayed below mobile number field

@Android @AndroidHiddenLive
Scenario: Check whether system correct validations for Username fields on Membership Registration page 2
Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Enter membership number "907277413"
Then Enter Date of birth "21" "07" "1974" 
Then Click on age checkbox on membership 
Then Click on Next button
Then Enter invalid username
Then Verify help text "Please enter your username. It must be between 6-15 characters long"
Then Verify grey line is displayed below username field
Then Enter valid username
Then Verify green line is displayed below username field

@Android @AndroidHiddenLive
Scenario: Check whether system correct validations for Password fields on Membership Registration page 2
Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Enter membership number "907277413"
Then Enter Date of birth "21" "07" "1974"
Then Click on age checkbox on membership 
Then Click on Next button
Then Enter invalid password
Then Verify help text "Please enter your password"
Then Verify grey line is displayed below password field
Then Enter valid password
Then Verify green line is displayed below password field

@Android @AndroidHiddenLive
Scenario: Check whether system correct validations for Marketing Preferences section on Membership Registration page 2
Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
Then Enter membership number "907277413"
Then Enter Date of birth "21" "07" "1974"
Then Click on age checkbox on membership 
Then Click on Next button
Then Click on email checkbox
Then Click on SMS checkbox
Then Click on phone checkbox
Then Click on post checkbox
Then Click on select all button
Then Click on marketing preference checkbox
Then Verify selected checkbox gets deselect

@Android @AndroidHiddenLive44
Scenario: Check whether user able to register with Memebrship details when all details are correctly entered
Then Click on Join Now button
Then Click on 'No (I have a membership card)' button
#Then Enter membership number "907277641"
#Then Enter Date of birth "19" "03" "1973" 
Then Enter the Membership details
Then Click on age checkbox on membership 
Then Click on Next button
Then Verify text "Create a login and get started!" displayed
Then Verify following fields are displayed under 'Your personal details' section
|Title|
|Name|
|Date of Birth|
Then Verify "Not You?" link
Then Verify text "Almost there!" displayed
Then Enter the username
Then Enter the password in valid format
Then Click on select all in marketing preference section
Then Click on Register CTA
Then Verify Deposit Now header
Then Verify "Why not set a deposit limit?" link

@Android @AndroidHiddenLive
Scenario: My Account-Adding Membership Number_ Check that Add membership number field is not displayed for existing retail user account
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Account Details" option
Then Verify "Account Details" as header displayed
Then Verify Add membership number field is not displayed on screen

@Android @AndroidHiddenLive
Scenario: My Account-Add Membership number_ Check that Club card number is displayed on account details page for retail user account
When User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Click on my account button
Then Click on "Account Details" option
Then Verify "Account Details" as header displayed
And Verify "View Card" button is displayed on screen