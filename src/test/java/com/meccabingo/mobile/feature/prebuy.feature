Feature: Pre Buy

@Android @AndroidHiddenLive
Scenario: Check whether system displays pre-buy pop up after successful Login when user clicks on Pre-buy Cta from bingo tile in logged out mode
Then Navigate through hamburger to "Bingo" menu
Then Click pre-buy from bingo tile
#Then User enters username "automecca2020"
#Then User enters password "Password123"
Then User enters username
And User enters password
Then User clicks on login button
Then Verify balance section is displayed
Then Verify pre-buy popup is displayed


@Android @AndroidHiddenLive
Scenario: Check whether system displays confirmation pop up message (“You have successfully purchased tickets“) when user purchase ticket using pre-buy pop up
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button
Then Verify balance section is displayed
Then Navigate through hamburger to "Bingo" menu
Then Click pre-buy from bingo tile
Then Verify pre-buy popup is displayed
Then Select ticket "6"
Then Click button having text "Buy Tickets"
Then Verify text "You have successfully purchased tickets" displayed