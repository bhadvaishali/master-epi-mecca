Feature: Footer - Social Media, Useful links, Payments, Partener section etc

#EMC-22 Social media TCs
@iOS
Scenario: Footer Social Media_Check whether system displays following Social media components and system navigate user to respective URL when user clicks on 'Facebook'
-Facebook
-Twitter
-Instagram
-Youtube
Then Verify system displays following Social media components:
|Header|
|Twitter|
|Facebook|
|Youtube|
|Instagram|
And Verify system displays Icon, Text and Name for each social media block
Then Click on Facebook block from footer
Then Switch to child window
Then Verify system navigate user to "https://www.facebook.com/MeccaBingo" url

@iOS
Scenario: Footer Social Media_Check whether system navigate user to respective URL when user clicks on 'Instagram'
Then Click on Instagram block from footer
Then Switch to child window
Then Verify system navigate user to "https://www.instagram.com/meccabingo/" url

@iOS
Scenario: Footer Social Media_Check whether system navigate user to respective URL when user clicks on 'twitter'
Then Click on twitter block from footer
Then Switch to child window
Then Verify system navigate user to "https://twitter.com/MeccaBingo" url

@iOS
Scenario: Footer Social Media_Check whether system navigate user to respective URL when user clicks on 'youtube'
Then Click on youtube block from footer
Then Switch to child window
Then Verify system navigate user to "https://www.youtube.com/user/MeccaBingoClubs" url

#EMC-23 : Footer - Trusted Partners - Payment methods
@iOS
Scenario: Footer Trusted Partners_Check whether system displays following section for Payment Providers components:-Title-Description -LogosCheck whether system displays following section for Trusted partners components:
-Title
-Description 
-Logos
Then Verify system displays following section for payment providers components:
|Title|
|Description|
|Logos|

#EMC-27 test Cases for useful links
@iOS @iOSRegression
Scenario: Check whether system displays folllowing URL under Useful Links block:
-Privacy Policy
-Terms and Conditions
-Affiliates
-Mecca Club Terms
-Play Online Casino
-Mecca Blog

Then Verify following URLs are displaying under Useful Links block:
|Headaer|
|Privacy Policy|
|Terms and Conditions|
|Affiliates|
|Mecca Club Terms|
|Play Online Casino|
|Mecca Blog|

@iOS
Scenario: Check whether system navigate to appropriate URL when user clicks on 'Privacy Policy' Useful Links
Then Click on Privacy Policy Link
Then Switch to child window
Then Verify system navigates user to "https://tst01-mecc-cms4.rankgrouptech.net/privacy-policy"  

@iOS
Scenario: Check whether system navigate to appropriate URL when user clicks on 'Terms and Conditions' Useful Links
Then Click on Terms and Conditions Link
Then Switch to child window
Then Verify system navigates user to "https://tst01-mecc-cms4.rankgrouptech.net/terms-and-conditions"

@iOS
Scenario: Check whether system navigate to appropriate URL when user clicks on Affiliates Useful Links
Then Click on Affiliates Link
Then Switch to child window
Then Verify system navigates user to "https://www.rankaffiliates.com/"

@iOS
Scenario: Check whether system navigate to appropriate URL when user clicks on 'Mecca Club Terms' Useful Links
Then Click on Mecca Club Terms Link
Then Switch to child window
Then Verify system navigates user to "https://tst01-mecc-cms4.rankgrouptech.net/club-terms-and-conditions"

@iOS
Scenario: Check whether system navigate to appropriate URL when user clicks on 'Play Online Casino' Useful Links
Then Click on Play Online Casino Link
Then Switch to child window
Then Verify system navigates user to "https://www.grosvenorcasinos.com/"

@iOS
Scenario: Check whether system navigate to appropriate URL when user clicks on 'Mecca Blog' Useful Links
Then Click on Mecca Blog Link
Then Switch to child window
Then Verify device navigate user to containing url "https://blog.meccabingo.com/"

#Sprint 7
#EMC-36 : Footer - GamCare, 18, GamStop, Gambling commision, Gambling control Logos
@iOS  @iOSRegression
Scenario: Check whether user able to view following logos at Footer section:
-GamCare
-18
-GamStop
-Gambling commision
-Gambling control
-IBAS
-ESSA
Then Verify user able to view following logos at Footer in partners block:
|ESSA|
|IBAS|
|18|
|Gambling Control|
|GamCare|
|GamStop|
|Gambling Commision|

@iOS
Scenario: Page Layout > Check whether footer contains a essa logo and it should be hyperlinked to correct landing page
Then User clicks on essa logo
Then Switch to child window
Then Verify system navigates user to "https://www.essa.uk.com/"
Then Verify essa logo

@iOS
Scenario: Page Layout > Check whether footer contains a IBSA logo and it should be hyperlinked to correct landing page
Then User clicks on IBSA logo
Then Switch to child window
Then Verify system navigates user to "https://www.ibas-uk.com/"


#Sprint 7
#EMC-272 : Footer - Verisign Secured
@iOS1111
Scenario: Check whether user able to view Verisign Secured logo at Footer section
Then Verify user should be able to view Verisign Secured logo at Footer section


@iOS  @iOSRegression
Scenario: Footer Main Area_Check whether system displays following section footer Main area:
- Mecca Logo
- Awards Section
- verisigned information
- Keep it fun information
- Licensed Information
Then Verify user is able to view following sections in footer
|Mecca Logo|
|Awards Section|
|verisigned information|
|Keep it fun information|
|Licensed Information|


#EMC-10 : Footer - Rank group
@iOS
Scenario: Check whether footer contains a Rank Group logo and it should be hyperlinked to correct landing page
Then User clicks on Rank Group link
Then Switch to child window
Then Verify system navigates user to "https://www.rank.com/en/index.html"


@iOS
Scenario: Page Layout > Check whether footer contains a UK Gambling Commission logo and it should be hyperlinked to correct landing page
Then User clicks on Gambling Commission logo
Then Switch to child window
Then Verify system navigates user to "https://www.gamblingcommission.gov.uk/"

@iOS
Scenario: Page Layout > Check whether footer contains an Alderney Gambling Control Commission logo and it should be hyperlinked to correct landing page
Then User clicks on Gambling Control logo
Then Switch to child window
Then Verify system navigates user to "https://www.gamblingcontrol.org/"


@iOS
Scenario: Page Layout > Check whether footer contains a Gamcare logo and it should be hyperlinked to correct landing page
Then User clicks on GamCare logo
Then Switch to child window
Then Verify system navigates user to "https://www.gamcare.org.uk/"

@iOS
Scenario: Page Layout > Check whether footer contains a GamStop logo and it should be hyperlinked to correct landing page
Then User clicks on GamStop logo
Then Switch to child window
Then Verify system navigates user to "https://www.gamstop.co.uk/"

@iOS
Scenario: Page Layout > Check whether footer contains a link which redirects user to BeGambleAware.org page.
Then User click on BeGambleAware link
Then Switch to child window
Then Verify system navigates user to "https://www.begambleaware.org/"

@iOS
Scenario: Page Layout > Check whether footer contains 18+ logo and it is NOT a hyperlink (It should be just a logo)
Then Verify "18+" logo is displayed in footer section
Then Verify 18+ logo should not be a hyperlink

@iOS
Scenario: Page Layout > Check whether footer contains a Keep it fun logo and it should be hyperlinked to correct landing page
Then Verify "Keep it fun" logo is displayed in footer section
Then Verify "Keep it fun" logo hyperlinked to the correct page

@iOS
Scenario: Page Layout > Check whether footers identify the regulators. A link should be present which should take the customer to the Gambling Commission page, which displays the relevant company license status
Then Verify Regulators (UK and Alderney) and associated license statements is displayed under License section of the footer
And Click and verify 'UK Gambling Commission' link redirect to the correct page

@iOS
Scenario: Limiting collusion/cheating > Ensure that information is available to the customer regarding cheating, recovered player funds and how to complain
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Verify user is successfully logged in
Then Verify "Terms and Conditions" link is displayed on under Usefullinks footer section
Then Access Terms & Conditions link and Verify that information is available to the customer regarding cheating, recovered player funds and how to complain

@iOS
Scenario: Peer to Peer customers - Third party software > Ensure that information is available to the customer regarding the rules around the use of third-party software, and how to report suspected use that is against the operators T&Cs
Then User clicks on Login Button from header of the Page
Then User enters username
And User enters password
Then User clicks on login button from login window
Then Verify user is successfully logged in
Then Verify "Terms and Conditions" link is displayed on under Usefullinks footer section
Then Access Terms & Conditions link and Verify that information is available to the customer regarding third party software