Feature: Registration Page (Digital)

#EMC-59
@Android @AndroidHiddenLive
Scenario: Check whether all marketing preference checkboxes (Email, SMS, Phone and Post) are checked when user select 'Select All' CTA from marketing preference section
Then Click on Join Now button
Then Enter email address 
Then Click on age checkbox
Then Click on Next button
Then Click on select all in marketing preference section
Then Verify following checkboxes are selected
|Email|
|SMS|
|Phone|
|Post|

@Android @AndroidHiddenLive
Scenario: Check whether error message is displayed if user keeps any of the following field empty:
 Username Password Title (Mr., Miss, Mrs. or Ms.) First Name (minimum 2 characters) Surname Date of Birth Country of Residence Address Mobile Number
 
Then Click on Join Now button
Then Enter email address 
Then Click on age checkbox
Then Click on Next button
Then Click on disable Register CTA
Then Verify below error message
|You must enter a username|
|Your password must be at least 8 characters long, contain 1 upper-case letter, and a minimum of 1 number.|
|Please enter your full first name|
|Please enter your surname|
|Please enter your Date of Birth (dd-mm-yyyy)|
|Please enter a valid phone number|
#|Please enter your postcode|
|You must enter an address or postcode|
#|Please select your contact preferences|

@Android @AndroidHiddenLive
Scenario: Check whether user able to select single marketing preference checkboxes (Email, SMS, Phone and Post) from marketing preference section
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Click on email checkbox
Then Click on SMS checkbox
Then Click on phone checkbox
Then Click on post checkbox

#EMC-871
@Android6666
Scenario: Check whether Marketing preference checkboxes are displayed in red color and displays err below fields when user do not select any of the option from Marketing preference section and click on Register button
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Click on disable Register CTA
Then Verify error message "Please select contact preference" is displayed in red color 

#EMC-863
@Android @AndroidHiddenLive
Scenario: Check whether system displays green line below the username field when user enters username in valid format
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Verify green line displays below username field

@Android @AndroidHiddenLive
Scenario: Check whether system displays error message when user enters the existing username in username field
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the existing username "vaishali" in username field
#Then Verify error message "Sorry, that name is taken. Pick another! "
Then Enter the password in valid format
Then Enter the firstname in valid format
Then Enter the surname in valid format
Then Enter Date of birth "11" "08" "1996"
Then Enter valid mobile number
Then Enter valid postcode "ha01ab"
Then Select one address from dopdown
Then Click on select all in marketing preference section
Then Click on Register CTA
Then Verify message "The selected username is unavailable"
#Then Verify message "The supplied username is already associated with a different user."

@Android @AndroidHiddenLive
Scenario: Check whether system displays error message when user enters Password same as username
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter username as "Vaishali1"
Then Enter password as "Vaishali1"
Then Enter the firstname in valid format
Then Enter the surname in valid format
Then Enter Date of birth "11" "08" "1996"
Then Enter valid mobile number
Then Enter valid postcode "ha01ab"
Then Select one address from dopdown
Then Click on select all in marketing preference section
Then Click on Register CTA
#Then Verify message "The username and password cannot be the same"
Then Verify message "Your new password cannot be the same as your username"

#EMC-61
@Android @AndroidHiddenLive
Scenario: Check whether below fields are displayed on second step of registration journey:
- Title
- First name
- Surname
- Date of birth
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Verify fields are displayed on page
|Title|
|First name|
|Surname|
|Date of birth| 
|Username|
|Password|

@Android @AndroidHiddenLive
Scenario: Check whether all titles are displayed on second step of registration journey:
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Verify all titles are displayed on page
|Ms|
|Mr|
|Miss|
|Mrs|
|Mx|

@Android @AndroidHiddenLive
Scenario: Check whether user able to select any option from Title section
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Select any option from title section
Then Verify selected option gets highlighted

@Android @AndroidHiddenLive
Scenario: Check whether error message is displayed when user enters invalid first name
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter firstname as "A123@#$"
Then Verify error message "Your full name may only contain letters, spaces, hyphens, apostrophes or full stop symbols"

@Android @AndroidHiddenLive
Scenario: Check whether green line is displayed when user enter the first name in correct format
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the firstname in valid format
Then Verify green line displays below firstname field

@Android @AndroidHiddenLive
Scenario: Check whether error message is displayed when user enters invalid surname
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter surname as "123@#$"
Then Verify error message "Your surname may only contain letters, spaces, hyphens, apostrophes or full stop symbols"

@Android @AndroidHiddenLive
Scenario: Check whether green line is displayed when user enter the surname in correct format
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the surname in valid format
Then Verify green line displays below surname field

@Android @AndroidHiddenLive
Scenario: Check whether error message is displayed if user enters age less than 18 years
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
And Enter the age less than 18 years in DOB field
Then Verify error message "You must be over 18 years old to play on MeccaBingo.com. Need help? Please contact "

@Android @AndroidHiddenLive
Scenario: Check whether green line is displayed when user enter the DOB in correct format
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter Date of birth "11" "08" "1996"
Then Verify green line displays below DOB field

@Android @AndroidHiddenLive
Scenario: Check whether system displays green line below the password field when user enters the password in correct format
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the password in valid format
Then Verify green line displays below password field

@Android @AndroidHiddenLive
Scenario: Check whether system accepts the username in following format:
-Minimum 8 and Maximum 15 characters
-Username starting with no any special characters or symbols

Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter username as "asd"
Then Verify error message "Your username must be between 6-15 characters long."
Then Enter username as "@#%$1214"
Then Verify error message "Please enter a username. It can only contain numbers, letters and underscores, and must be between 6-15 characters long."
Then Enter the username in valid format
Then Verify green line displays below username field
And Verify system does not display any below error message
|Your username must be between 6-15 characters long.|
|Please enter a username. It can only contain numbers, letters and underscores, and must be between 6-15 characters long.|


@Android @AndroidHiddenLive
Scenario: Check whether system displays grey line along with password guidance below the password field:
- Please use at least one capital letter
- Please use at least one number
- Please use at least one lower case
- Minimum 8 characters
#Given Invoke Mecca Site
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the password in valid format
Then Verify following guidlines are displayed below password field
|Please use at least one capital letter|
|Please use at least one number|
|Please use at least one lower case|
|Minimum 8 and maximum 15 characters|


#Then Verify <text> below password field
# Examples:
#|text|

#EMC-63
@Android @AndroidHiddenLive
Scenario: Check whether Yes option is selected by default on first step registration page
Then Click on Join Now button
Then Verify Yes option is selected

@Android @AndroidHiddenLive
Scenario: Check whether green line is displayed below email address field when user enter the email address in correct format
Then Click on Join Now button
Then Enter email address
Then Verify green line displays below email field

@Android
Scenario: Check whether error message is displayed if already taken email address is entered.
Then Click on Join Now button
Then Enter registered email address "vaishali.bhad@expleogroup.com"
Then Click on age checkbox
Then Click on Next button
Then Verify message "These details are already in use with a different account. Check your details and try again."

@AndroidHiddenLive
Scenario: Check whether error message is displayed if already taken email address is entered.
Then Click on Join Now button
Then Enter registered email address "mb16dec@mailinator.com"
Then Click on age checkbox
Then Click on Next button
Then Verify message "These details are already in use with a different account. Check your details and try again."

@Android @AndroidHiddenLive
Scenario: Check whether system displays Registration Page 2 when user enters valid email address n tick TnC check box and click on Next button
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Verify select all button

#EMC-810
@Android @AndroidHiddenLive
Scenario: Check whether error message displayed under respective fields when user clicks on next button on first step registration page
Then Click on Join Now button
Then Click on disabled Next button
Then Verify error message "You must enter a valid email address"  
Then Verify error message "You must agree to the terms and conditions in order to continue" 

@Android @AndroidHiddenLive
Scenario: Check whether help text disappear and error message display when user enters invalid email address on first step registration screen and leave field
Then Click on Join Now button
Then Enter invalid email address
Then Click on disabled Next button
Then Verify help text gets disappear 
Then Verify error message "You must enter a valid email address"

#EMC-60
@Android
Scenario: Check whether system displays correct validation when user selects Country field on Digital Registration Page 2
Then Click on Join Now button
Then Enter email address 
Then Click on age checkbox
Then Click on Next button
Then Verify UK is default value in country field
Then Select republic of ireland 
Then Verify enter address manually button gets disappear
Then Verify first address line field
Then Verify second address line field
Then Verify town/city field
Then Verify country field
Then Select united kingdom
Then Verify enter address manually button
Then Verify first address line field gets disappear
Then Verify second address line field gets disappear
Then Verify town/city field gets disappear
Then Verify country field gets disappear

@Android @AndroidHiddenLive
Scenario: Check whether system displays correct validation when user selects Postcode field on Digital Registration Page 2
Then Click on Join Now button
Then Enter email address 
Then Click on age checkbox
Then Click on Next button
Then Click on postcode field
Then Verify help text appears below postcode field
Then Enter valid postcode "ha01ab"
Then Select one address from dopdown
Then Verify first address line field
Then Verify second address line field
Then Verify town/city field

@Android @AndroidHiddenLive
Scenario: Check whether system displays correct validation when user selected Enter address manually option for entering Address on Digital Registration Page 2
Then Click on Join Now button
Then Enter email address 
Then Click on age checkbox
Then Click on Next button
Then Click on enter address manually button
Then Enter valid input in address first line
Then Enter valid input in town/city field
Then Verify green line displays below first address field
Then Verify green line displays below town/city field

@Android @AndroidHiddenLive
Scenario: Check whether system displays correct validation for Mobile Number field on Digital Registration Page 2
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Click on mobile number field
Then Enter valid mobile number
Then Verify green line displays below mobile number field

#EMC-868
@Android11
Scenario: Check whether system displays correct validations for postcode when user enters incomplete or invalid postcode on Digital Registration page 2
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter invalid postcode
Then Verify red line displays below postcode field
Then Verify error message "Too many results, please enter more characters"

@Android @AndroidHiddenLive
Scenario: Check whether system displays correct validations for Mobile number fields when entered incorrect or incomplete information on Digital Registration page 2
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter "@$fdfgg" invalid mobile number
Then Verify red line displays below mobile number field
Then Verify error message "Please enter a valid phone number between 8 and 11 digits long"
Then Enter "332" invalid mobile number
Then Verify error message "Please enter a valid phone number"

#EMC-1401
@Android @AndroidHiddenLive
Scenario: Check whether system do not display Dr for title while regsitration
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Verify title "Dr" does not appear

@Android
Scenario: Check whether digital user gets registered successfully on Click of Register button on entering all valid details on Page 1 and Page 2
Then Click on Join Now button
Then Enter email address
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Enter the password in valid format
Then Enter the firstname in valid format
Then Enter the surname in valid format
Then Enter Date of birth "15" "04" "1954"
Then Enter valid mobile number
Then Enter valid postcode "ha01ab"
Then Select one address from dopdown
Then Click on select all in marketing preference section
Then Click on Register CTA
Then Verify Deposit Now header
Then Verify "Why not set a deposit limit?" link
#Then Click on deposit close button

@AndroidOptimized1
Scenario: Check whether  system displays error when user tries to register user with details who is already suspended/ blocked
Then Click on Join Now button
Then Enter registered email address "testman012@mailinator.com"
Then Click on age checkbox
Then Click on Next button
Then Verify message "An error has occurred and has been logged, please contact system administrator"

@Android21 @AndroidRegression
Scenario: Check whether system displays Documents required pop up when “KYC check not possible“ response received while registration
Then Click on Join Now button
Then Enter random email id with suffix ".kycrejected@mailinator.com"
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Enter the password in valid format
Then Enter the firstname in valid format
Then Enter the surname in valid format
Then Enter Date of birth "11" "08" "1996"
Then Enter valid mobile number
Then Enter valid postcode "ha01ab"
Then Select one address from dopdown
Then Click on select all in marketing preference section
Then Click on Register CTA
Then Verify button having label as "Upload Documents" displayed
Then Verify button having label as "Live Help" displayed