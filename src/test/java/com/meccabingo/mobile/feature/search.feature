Feature: Search functionality, Search title

#EMC-217 : Search title
@Android @AndroidHiddenLive
Scenario: Check whether system displays following options for search title:
-Image
-Name
-""Play Now"" CTA
-”Info"" CTA"
When User clicks on search filed
Then Verify Search overlay opens
When Search Game "Reel king" in Search field from header
Then Verify system displays search results to user 
Then Verify system displays following options for search title:
|Image|
|Name|
|Play Now CTA|
|Info CTA|

#EMC-30 : Header - Search functionality
@Android @AndroidHiddenLive
Scenario: Check whether search trigger when user enters three or more characters in search field
When User clicks on search filed
Then Verify Search overlay opens
Then User enters three characters in search filed
Then Verify system displays search result section

#EMC-563 : Search functionality - Quick links/ what others are playing
@Android @AndroidHiddenLive
Scenario: Check Whether user able to view "Quick Links" and "What others are playing" section in search overlay till user enters 3 or more characters in search field
When User clicks on search filed
Then Verify Search overlay opens
When Search Game "b" in Search field from header
Then Verify user able to view 'Quick Links' and 'What others are playing' section in search overlay
Then Search Game "i" in Search field from header
Then Verify user able to view 'Quick Links' and 'What others are playing' section in search overlay
Then Search Game "ngo" in Search field from header
Then Verify 'Quick Links' and 'Whats others are playing' section dissapear
And Verify system displays search results to user 

#EMC-212 
@Android @AndroidHiddenLive
Scenario: Check whether system displays Game / bingo details Page when user clicks on i button from Search overlay
When User clicks on search filed
Then Verify Search overlay opens
When Search Game "Reel king" in Search field from header
Then Click on i button from search 
Then Verify user navigate to game details page

@Android @AndroidHiddenLive
Scenario: Check whether system displays Search results for Slots Games for Valid Text
When User clicks on search filed
Then Verify Search overlay opens
When Search Game "Reel king" in Search field from header
Then Verify system displays search results for "slots" game

@Android @AndroidHiddenLive
Scenario: Check whether system displays Search results for bingo Games for Valid Text
When User clicks on search filed
Then Verify Search overlay opens
When Search Game "Emoji" in Search field from header
Then Verify system displays search results for "bingo" game

@Android @AndroidHiddenLive
Scenario: Check whether system displays Search results for Clubs for Valid Text and Include clubs in search result checkbox is ON
When User clicks on search filed
Then Verify Search overlay opens
Then Check checkbox of include clubs in search result
And Search Game "Mecc" in Search field from header
Then Verify maximum of 6 results for search string under 'Mecca Bingo clubs' heading
Then Verify system displays following options for search tile
|Name of the club|
|Distance away|
|Info cta|
And Verify info icon redirct to the club details page

#EMC-238
@Android1
Scenario: Check whether user able Navigate back to Page from where registration Journey started without deposit after clicking on play button in non logged in mode
When User clicks on search filed
Then Verify Search overlay opens
When Search Game "Reel king" in Search field from header
Then Click on play now button from search
Then Verify system displays login window
Then Click on sign up link
Then Enter email address 
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Enter the password in valid format
Then Enter the firstname in valid format
Then Enter the surname in valid format
Then Enter Date of birth "11" "08" "1996"
Then Enter valid mobile number
Then Enter valid postcode "wr53da"
Then Select one address from dopdown
Then Click on select all in marketing preference section
Then Click on Register CTA
Then Verify "Deposit" as header displayed
Then Click on deposit close button
Then Verify game launches successfully

#EMC-237
@Android1
Scenario: Check whether system launch searched game after completing registration journey with deposit
When User clicks on search filed
Then Verify Search overlay opens
When Search Game "Reel king" in Search field from header
Then Click on play now button from search
Then Verify system displays login window
Then Click on sign up link
Then Enter email address 
Then Click on age checkbox
Then Click on Next button
Then Enter the username in valid format
Then Enter the password in valid format
Then Enter the firstname in valid format
Then Enter the surname in valid format
Then Enter Date of birth "11" "08" "1996"
Then Enter valid mobile number
Then Enter valid postcode "wr53da"
Then Select one address from dopdown
Then Click on select all in marketing preference section
Then Click on Register CTA
Then Verify "Deposit" as header displayed
Then Enter card number "4026201382933139"
Then Enter card date "0222"
Then Enter security code
Then Enter the amount 
Then Click on deposit button
Then Switch to popup
Then Click on close button on popup
Then Verify game launches successfully