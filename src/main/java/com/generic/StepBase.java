package com.generic;

import java.io.File;
import java.io.IOException;
import java.util.Base64;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;

import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.appiumDriver.AppiumManager;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.Utilities;
import com.generic.utils.WaitMethods;
import com.generic.webdriver.DriverProvider;

import io.appium.java_client.screenrecording.CanRecordScreen;
import io.cucumber.java.Scenario;

/**
 * @ScriptName : StepBase
 * @Description : This class contains generic functionalities like
 *              initialize/teardown test environment
 * @Author : Harshvardhan Yadav(SQS)
 * @modified :Harhvardhan Yadav, Namrata Donikar (Expleo)
 */
public class StepBase {

	// Local variables
	private Configuration objConfiguration;
	private DriverProvider objDriverProvider;
	private Utilities objUtilities;
	private LogReporter objLogReporter;
	private AppiumDriverProvider objAppiumDriverProvider;
	private AppiumManager objAppiumManager;
	private WaitMethods waitMethods;

	public StepBase(Configuration configuration, DriverProvider driverProvider, Utilities utilities, 
			AppiumDriverProvider appiumDriverProvider, LogReporter logReporter,WaitMethods waitMethods) {
		this.objConfiguration = configuration;
		this.objDriverProvider = driverProvider;
		this.objUtilities = utilities;
		this.objAppiumDriverProvider = appiumDriverProvider;	
		this.objLogReporter = logReporter;
		this.waitMethods = waitMethods;
	}


	/**
	 * initialize environment
	 * 
	 * @throws IOException
	 * @param objScenario
	 */
	public void initializeEnvironment(Scenario objScenario) {
		// updated code to get the environment based on Tag used in scenarion i.e CMS or
		//System.out.println(objScenario.getSourceTagNames());		
		try {
			objDriverProvider.initialize();
			objDriverProvider.getWebDriver().get(objConfiguration.getConfig("web.Url"));
			CloseComplianceBannerAfterURLLoaded();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	/**
	 * initialize android environment without scenario
	 * 
	 * @author :Harhvardhan Yadav, Namrata Donikar (Expleo)
	 */
	public void initializeAndroidEnvironment(Scenario objScenario) {
		try {
			objConfiguration.loadConfigProperties();
			objAppiumManager = new AppiumManager();
			objAppiumManager.startAppium();
			objAppiumDriverProvider.initializeConsoleBaseAppium("android", objConfiguration,
					objAppiumManager.getCurrentAppiumIpAddress(), objAppiumManager.getCurrentAppiumPort());
			CloseComplianceBannerFromMobilePlatform();
			if((objConfiguration.getConfig("mobile.scriptVideoRecording").equalsIgnoreCase("true")))
			{objAppiumDriverProvider.startRecording();}
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	/**
	 * initialize android environment without scenario
	 * 
	 * @author :Harhvardhan Yadav, Namrata Donikar (Expleo)
	 */
	public void initializeIOSEnvironment() {

		try {
			objConfiguration.loadConfigProperties();
			objAppiumDriverProvider.initializeDesktopAppium("ios", objConfiguration);
			CloseComplianceBannerFromMobilePlatform();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	/**
	 * teardown environment
	 * 
	 * @author Harshvardhan Yadav(SQS)
	 */
	public void tearDownEnvironment(Scenario objScenario) {
		try {
			if (objScenario.isFailed()) {
				objLogReporter.takeScreenShotBytes(objScenario);
			}
			this.tearDownEnvironment();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	public void tearDownEnvironment() {
		try {
			objDriverProvider.tearDown();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	/**
	 * teardown appium environment for android
	 * 
	 * @author Dheerendra Pal(expleo)
	 */

	public void tearDownAppiumEnvironmentAndroid(Scenario scenario) {
		try {
			if (scenario.isFailed()) {
				objLogReporter.takeScreenShotBytesFoMobile(scenario);
			}
			if((objConfiguration.getConfig("mobile.scriptVideoRecording").equalsIgnoreCase("true")))
			{
				String featureName = scenario.getId().split(".*/")[1];
				featureName =  featureName.substring(0, featureName.indexOf("."));
				System.out.println("Executing Cucumber Feature :- " +featureName );
				
				objAppiumDriverProvider.stopRecording(scenario.getName(),featureName);
			}
			objAppiumDriverProvider.tearDown();
			objAppiumManager.stopServer();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	/**
	 * teardown appium environment for android
	 * 
	 * @author Dheerendra Pal(expleo)
	 */

	public void tearDownAppiumEnvironmentIOS(Scenario scenario) {
		try {
			if (scenario.isFailed()) {
				objLogReporter.takeScreenShotBytesFoMobile(scenario);
			}
			objAppiumDriverProvider.tearDown();
		} catch (Exception exception) {
			exception.printStackTrace();			
		}
	}

	public void CloseComplianceBannerAfterURLLoaded() {
		waitMethods.sleep(10);
		System.out.println("**************** looooo clikcedd");
		//By locator = By.xpath("//i[contains(@class,'icon-close')]");
		By btnContinue = By.xpath("//section[contains(@class,'cookie-notification')]//button[contains(.,'Continue')]");
		
		//if (objDriverProvider.getWebDriver().findElement(locator).isEnabled())
		//{objDriverProvider.getWebDriver().findElement(locator).click();}
		//waitMethods.sleep(20);
		
		if (objDriverProvider.getWebDriver().findElement(btnContinue).isDisplayed())
		{	objDriverProvider.getWebDriver().findElement(btnContinue).click();

		System.out.println("**************** continue clikcedd");}
		
		
		waitMethods.sleep(2);
	}
	public void CloseComplianceBannerFromMobilePlatform() {
		By locator = By.xpath("//i[contains(@class,'icon-close')]");
		By btnContinue = By.xpath("//section[contains(@class,'cookie-notification')]//button[contains(.,'Continue')]");
		By logo = By.xpath("//header[@id='header']//a//img[@alt='Mecca Bingo']");
		//waitMethods.sleep(5);
		/*if (objAppiumDriverProvider.getAppiumDriver().findElement(locator).isDisplayed())
			{objAppiumDriverProvider.getAppiumDriver().findElement(locator).click();}

		if (objAppiumDriverProvider.getAppiumDriver().findElement(logo).isDisplayed())
		{objAppiumDriverProvider.getAppiumDriver().findElement(logo).click();
		System.out.println("**************** looooo clikcedd");}
*/
		waitMethods.sleep(10);
		System.out.println("**************** convvvvvvtinue clikcedd");
		if (objAppiumDriverProvider.getAppiumDriver().findElement(btnContinue).isDisplayed())
		{
			objAppiumDriverProvider.getAppiumDriver().findElement(btnContinue).click();
			System.out.println("**************** continue clikcedd");}
		waitMethods.sleep(6);
	}
}
