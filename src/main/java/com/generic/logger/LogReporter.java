package com.generic.logger;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.TakesScreenshot;
import org.testng.Assert;

import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.cucumberReporting.CustomFormatter;
import com.generic.utils.Configuration;
import com.generic.webdriver.DriverProvider;

import io.cucumber.java.Scenario;

/**
 * @ScriptName : LogReporter
 * @Description : This class contains logging function
 * @Author : Harshvardhan Yadav(SQS)
 * @Modified :Harhvardhan Yadav, Namrata Donikar (Expleo)
 */
public class LogReporter {
	private Configuration objConfiguration;
	private DriverProvider objDriverProvider;
	private AppiumDriverProvider objAppiumDriverProvider;

	public LogReporter(Configuration configuration, DriverProvider driverProvider,
			AppiumDriverProvider appiumDriverProvider) {
		this.objConfiguration = configuration;
		this.objDriverProvider = driverProvider;
		this.objAppiumDriverProvider = appiumDriverProvider;
	}

	public void log(String step, boolean resultLog) {
		System.out.println("*****************Step - " + step);
		this.assertTrue(step, resultLog);
	}

	public void log(String step, String inputValue, boolean resultLog) {
		System.out.println("*****************Step - " + step + "inputValue--->" + inputValue);
		this.assertTrue(step, resultLog);
	}

	public void log(String step, String actualValue, String expectedValue) {
		System.out.println("*****************Step - " + step + "actualValue-->" + actualValue + "expectedValue-->"
				+ expectedValue);
		this.assertEquals(step, expectedValue, actualValue);
	}

	public void log(String step, boolean expectedValue, boolean actualValue) {
		System.out.println("*****************Step - " + step + "actualValue-->" + actualValue + "expectedValue-->"
				+ expectedValue);
		this.assertEquals(step, expectedValue, actualValue);
	}

	public void assertEquals(String step, String expectedValue, String actualValue) {
		Assert.assertEquals(actualValue, expectedValue);
	}

	public void assertEquals(String step, boolean expectedValue, boolean actualValue) {
		Assert.assertEquals(actualValue, expectedValue);
	}

	public void assertTrue(String step, boolean condition) {
		Assert.assertTrue(condition);
	}

	public void assertEquals(String step, ScreenOrientation expectedDeviceOrientation,
			ScreenOrientation actualDeviceOrientation) {
		Assert.assertEquals(actualDeviceOrientation, expectedDeviceOrientation);

	}

	/**
	 * @param objScenario
	 * @author Dheerendra Pal(expleo)
	 */
	public void takeScreenShotBytes(Scenario objScenario) {

		try {
			final byte[] screenshot = ((TakesScreenshot) objDriverProvider.getWebDriver())
					.getScreenshotAs(OutputType.BYTES);
			objScenario.attach(screenshot, "image/png", objScenario.getName());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method is used to take the screenshot which works with Appium Driver
	 * 
	 * @param objScenario
	 * @author Dheerendra Pal(expleo)
	 */
	public void takeScreenShotBytesFoMobile(Scenario objScenario) {

		try {
			final byte[] screenshot = ((TakesScreenshot) objAppiumDriverProvider.getAppiumDriver())
					.getScreenshotAs(OutputType.BYTES);
			objScenario.attach(screenshot, "image/png", objScenario.getName());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String captureScreenShot() throws Exception {
		// String imagePath = System.getProperty("user.dir") +
		// objConfiguration.getConfig("screenshot.dir") + "/screenshots" +
		// System.currentTimeMillis()+".jpg";
		// Modified screenshot name to feature name
		String imagePath = System.getProperty("user.dir") + objConfiguration.getConfig("screenshot.dir") + "/"
				+ CustomFormatter.getCurrentFeatureName().replaceAll("(?=[]\\[+&|!(){}^\"~*?:\\\\-])", "")
						.replaceAll("\\s+", "_")
				+ ".jpg";

		System.out.println("imagePath------->" + imagePath);
		File scrFile = ((TakesScreenshot) objDriverProvider.getWebDriver()).getScreenshotAs(OutputType.FILE);
		File targetFile = new File(imagePath);
		FileUtils.copyFile(scrFile, targetFile);
		return targetFile.getName();
	}

}
