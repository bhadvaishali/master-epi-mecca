package com.generic.cucumberReporting;
import java.util.List;

import gherkin.formatter.Formatter;
import gherkin.formatter.NiceAppendable;
import gherkin.formatter.Reporter;
import gherkin.formatter.model.Background;
import gherkin.formatter.model.BasicStatement;
import gherkin.formatter.model.Examples;
import gherkin.formatter.model.Feature;
import gherkin.formatter.model.Match;
import gherkin.formatter.model.Result;
import gherkin.formatter.model.Scenario;
import gherkin.formatter.model.ScenarioOutline;
import gherkin.formatter.model.Step;
import gherkin.formatter.model.TagStatement;
import io.cucumber.plugin.EventListener;
import io.cucumber.plugin.Plugin;
import io.cucumber.plugin.event.EventPublisher;
import io.cucumber.plugin.event.HookTestStep;
import io.cucumber.plugin.event.PickleStepTestStep;
import io.cucumber.plugin.event.TestCaseStarted;
import io.cucumber.plugin.event.TestRunFinished;
import io.cucumber.plugin.event.TestSourceRead;
import io.cucumber.plugin.event.TestStepStarted;


/**
 * CustomFormatter
 * @author Harshvardhan Yadav(SQS)
 *
 */
public class CustomFormatter implements Reporter, EventListener, Plugin {

	private NiceAppendable output;
	private static String currentFeatureName = "";

	public CustomFormatter(Appendable appendable) {
		output = new NiceAppendable(appendable);
		//output.println("CustomFormatter()");
		//System.out.println("CustomFormatter(): " + output.toString());
	}
	
	@Override
	public void setEventPublisher(EventPublisher publisher) {
		publisher.registerHandlerFor(TestSourceRead.class, this::feature);
		publisher.registerHandlerFor(TestCaseStarted.class, this::scenario);
		publisher.registerHandlerFor(TestStepStarted.class, this::step);
		publisher.registerHandlerFor(TestRunFinished.class, this::close);
	}
	
	public void feature(TestSourceRead feature) {
		String featureSource = feature.getSource().toString();
		String featureName = featureSource.split(".*/")[1];
		
	 	System.out.println("@@@@Executing Cucumber Feature : " + featureName);
	 	this.setCurrentFeatureName(featureName);
	}
	
	public void scenario(TestCaseStarted scenario) {
		System.out.println("Executing Scenario : " + scenario.getTestCase().getName());
	}
	
	public void step(TestStepStarted step) {
		String stepName;
		String keyword;

		if (step.getTestStep() instanceof PickleStepTestStep) {
			PickleStepTestStep steps = (PickleStepTestStep) step.getTestStep();
			stepName = steps.getStep().getText();
			keyword = steps.getStep().getKeyword();

		} else {
			HookTestStep hoo = (HookTestStep) step.getTestStep();
			stepName = hoo.getHookType().name();
			keyword = "Triggered Hook:";
		}

		// System.out.println("Executing Step : " + keyword + " " + stepName);
	}
	
	public void close(TestRunFinished close) {
		output.close();
		//System.out.println("close()");
	}

	@Override
	public void before(Match match, Result result) {
		//System.out.println("before(): " + printMatch(match) + "; " + printResult(result));
	}

	@Override
	public void result(Result result) {
		output.println(printResult(result));
		//System.out.println("result(): " + printResult(result));
	}

	@Override
	public void after(Match match, Result result) {
		//System.out.println("after(): " + printMatch(match) + "; " + printResult(result));
	}

	@Override
	public void match(Match match) {
		//System.out.println("match(): " + printMatch(match));
	}

	@Override
	public void embedding(String s, byte[] bytes) {
		//System.out.println("embedding(): s=");
	}

	@Override
	public void write(String s) {
		//System.out.println("write(): " + s);
	}
	
	private void setCurrentFeatureName(String featureName){
		currentFeatureName = featureName;
	}
	
	public static String getCurrentFeatureName(){
		return currentFeatureName;
	}

	private String printResult(Result result) {
		return String.format("status=%s, duration=%s, error_message=%s",
				result.getStatus(), result.getDuration(), result.getErrorMessage());
	}

}
