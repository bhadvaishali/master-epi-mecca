package com.generic;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.time.Duration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.generic.interfaces._WebActions;
import com.generic.utils.CommandPrompt;
import com.generic.utils.Configuration;
import com.generic.utils.Utilities;
import com.generic.utils.WaitMethods;
import com.generic.webdriver.DriverProvider;
import com.mifmif.common.regex.Generex;

import io.appium.java_client.MobileElement;

/**
 * wrapper functions for performing actions on web element 
 ** @author Harshvardhan Yadav(SQS)
 *  @modified 	  :Harhvardhan Yadav, Namrata Donikar (Expleo)
 */
public class WebActions implements _WebActions 
{
	// Local variables
	private Configuration configuration;
	private DriverProvider driverProvider;
	private WaitMethods waitMethods;
	private Utilities utilities;

	private static final Exception Exception = null;
	private int DEFAULT_SLEEP_TIMEOUT = 2;
	private int FLUENTWAIT_WAIT_MID_TIMEOUT = 20;
	private int FLUENTWAIT_WAIT_MIN_TIMEOUT = 10;
	private int Explicit_WAIT_TIMEOUT = 20;
	private String randomEmail;

	public WebActions(Configuration configuration, DriverProvider driverProvider, WaitMethods waitMethods, Utilities utilities) {
		this.configuration = configuration;
		this.driverProvider = driverProvider;
		this.waitMethods = waitMethods;
		this.utilities = utilities;
	}
	
	public boolean checkElementToBeClickablWithMidWait(By locator){
		try{
			WebDriverWait wait = new WebDriverWait(driverProvider.getWebDriver(), (long)FLUENTWAIT_WAIT_MID_TIMEOUT);
			wait.until(ExpectedConditions.elementToBeClickable(locator));
			driverProvider.getWebDriver().findElement(locator);
			return true;
		}catch(Exception exception){
			System.out.println("--->  check elementToBeClickabl --->>Exception");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			return false;
		}
	}

	/**
	 * checkElementExists - this returns element is exist or not in DOM
	 * not has relevance for displayed and not displayed.
	 * @author Dayanand Dhange (Expleo)
	 * @return 
	 */
	public boolean checkElementExists(By locator) {
		WebElement objMobileElement =null;
		boolean flag=false;
		try {
		objMobileElement = (WebElement) driverProvider.getWebDriver().findElement(locator);
		if (objMobileElement!=null)
			flag=true;
		}
	catch(NoSuchElementException exceptionNoSuchElementException){
		System.out.println("----->>>NoSuchElementException");
		flag=false;
		}
		return flag;
	}
	
	/**
	 * switchTo parent Window
	 * @author Harshvardhan Yadav (Expleo)
	 **/
	public void switchToParentWindow() 
	{	
		try
		{
			waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
			String popupHandle = this.getCurrentWindowHandle();
			Set<String> windowNames = driverProvider.getWebDriver().getWindowHandles();

			@SuppressWarnings("rawtypes")
			Iterator ite = windowNames.iterator();

			while (ite.hasNext()) 
			{
				String parentHandle = ite.next().toString();
				if(parentHandle.equals(null))
					parentHandle="";
				if (!popupHandle.equals(parentHandle)) {
					driverProvider.getWebDriver().close();
					driverProvider.getWebDriver().switchTo().window(parentHandle);
					System.out.println("Switched to Parent window . . . . . .");
					break;
				}
			}
		}catch(Exception exception){
			System.out.println("Unable to siwtch to child window");
			if(configuration.getConfig("printStackTraceForActions").equalsIgnoreCase("true"))
				exception.printStackTrace();
		} 
	}

	
	/**
	 * @author Harshvardhan Yadav (Expleo)
	 */
	public void setBrowserWindowSize(String sWidth, String sHeight) {
		try {
			int iWidth = Integer.parseInt(sWidth);
			int iHeight = Integer.parseInt(sHeight);
			Dimension dimension = new Dimension(iWidth, iHeight);
			driverProvider.getWebDriver().manage().window().setSize(dimension);
			System.out.println("Browser window set to the dimension: " + dimension );
		} catch (Exception exception) {
			System.out.println("Unable to ser Browser window to the dimension: ");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
		}
	}

	/**
	 *@author Harhvardhan Yadav
	 */
	public boolean browserMaximize() {
		try {
			driverProvider.getWebDriver().manage().window().maximize();
			System.out.println("Browser maximize successful");
			return true;
		} catch (Exception exception) {
			System.out.println("Fail to maximize browser");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			return false;
		}
	}

	/**
	 *@author Harhvardhan Yadav
	 */
	public boolean clickUsingJS(By locator) {
		try {
			WebElement objWebElement = this.processElement(locator);
			((JavascriptExecutor) driverProvider.getWebDriver()).executeScript("arguments[0].click();", objWebElement);
			System.out.println("Click using javascript successful");
			return true;
		} catch (Exception exception) {
			System.out.println("Failed to click using javascript");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			return false;
		}
	}
	
	/**
	 *@author Namrata Donikar
	 */
	public boolean navigateToInvalidUrl() {
		try {
			String regex = "[a-zA-Z]{6}";
			String randomtext = "/" + new Generex(regex).random();
			String currentUrl = driverProvider.getWebDriver().getCurrentUrl();
			String newUrl = currentUrl + randomtext;
			driverProvider.getWebDriver().navigate().to(newUrl);
			return true;
		}catch(Exception exception) {
			System.out.println("Failed to navigate");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			return false;
		}
		
	}

	public boolean click(By locator){
		return this.invokeOnLocator(locator, "click");
	}

	public boolean clickOnDisabledElement(By locator){
		return this.invokeOnLocator(locator, "clickondisabledelement");
	}
	
	public boolean checkElementDisplayed(By locator){
		return this.invokeOnLocator(locator, "checkElementDisplayed");
	}

	public boolean checkElementNotDisplayed(By locator){
		return this.invokeOnLocator(locator, "checkElementNotDisplayed");
	}

	public boolean checkElementEnabled(By objLocator) {
		return this.invokeOnLocator(objLocator, "checkElementEnabled");
	}

	public boolean clearText(By locator) {
		return this.invokeOnLocator(locator, "clearText");
	}
	public boolean setText(By locator, String textToSet){
		return this.invokeOnLocator(locator, "setText", textToSet);
	}

	public boolean isCheckBoxSelected(By locator){
		return this.invokeOnLocator(locator, "isCheckBoxSelected");
	}

	public boolean isRadioButtonSelected(By locator){
		return this.invokeOnLocator(locator, "isRadioButtonSelected");
	}

	public boolean mouseHover(By locator){
		return this.invokeOnLocator(locator, "mousehover");
	}
	public boolean scrollToElement(By objWebElement) {
		return this.invokeOnLocator(objWebElement, "scrollToElement");

	}

	public boolean bootStrapSetText(By locator, String textToSet){
		return this.invokeOnLocator(locator, "bootStrapSetText", textToSet);
	} 

	public boolean setTextWithClear(By locator, String textToSet){
		return this.invokeOnLocator(locator, "setTextWithClear", textToSet);
	}

	public boolean selectFromCustomDropDown(By locator, String optionToSelect){
		return this.invokeOnLocator(locator, "selectFromCustomDropDown", optionToSelect);
	}

	public boolean selectFromDropDown(By locator, String... option){
		return this.invokeOnLocator(locator, "selectDropDown", option);
	}

	public String getText(By locator){
		return this.getFromLocator(locator, "getText");
	}

	public String getText(By locator, String getBy) {
		return this.getFromLocator(locator, "getText", getBy);
	}

	public String getAttribute(By locator, String strategy){
		return this.getFromLocator(locator, "getAttribute", strategy);
	}

	public boolean selectCheckbox(By locator, Boolean status){
		return this.invokeOnLocator(locator, "selectCheckbox", status);
	}

	public boolean selectRadioButton(By locator, Boolean status){
		return this.invokeOnLocator(locator, "selectRadioButton", status);
	}

	public String getSelectedValueFromDropDown(By locator){
		return this.getFromLocator(locator, "getSelectedValueFromDropDown");
	}

	/**
	 * Perform specific action on web element
	 * 
	 */
	public boolean invokeOnLocator(By locator, String action){
		int intAttempts = 0;
		Actions actionBuilder;
		int maxTries = Integer.parseInt(configuration.getConfig("maxTriesForElement"));
		boolean blnResult = false;
		while(intAttempts <= maxTries){
			System.out.println("**** Attempt - " + intAttempts + " to perform " + action +" on element, MaxAttempts" + maxTries);
			try
			{
				WebElement objWebElement;
				action = action.toLowerCase();
				switch(action)
				{
				case "click":
					objWebElement = this.processElement(locator);
					objWebElement.click();
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
					blnResult =  true;
					System.out.println("Performed click action on element");
					break;
					
				case "clickondisabledelement":
					objWebElement = this.processElement(locator);
					actionBuilder = new Actions(driverProvider.getWebDriver());
					actionBuilder.moveToElement(objWebElement).click().build().perform();
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
					blnResult =  true;
					System.out.println("Performed click action on disabled element");
					break;
					
				case "checkelementdisplayed":
					objWebElement = this.processElement(locator);
					if(objWebElement == null){
						waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
						blnResult =  false;
						throw Exception;
					}
					else {
						waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
						blnResult =  true;
					}
					break;

				case "checkelementnotdisplayed":
					objWebElement = this.processElement(locator);
					if(objWebElement == null) {
						waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
						blnResult = true;
					}
					else{
						waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
						blnResult =  false;
						throw Exception;
					}
					break;

				case "ischeckboxselected":
					objWebElement = this.processElement(locator);
					if (objWebElement.getAttribute("type").equals("checkbox"))
						blnResult = objWebElement.isSelected();
					System.out.println("Performed verify selected action on checkbox element");
					break;
				case "checkElementEnabled":
					objWebElement = this.processElement(locator);
					blnResult = objWebElement.isEnabled();
					System.out.println("Performed verify element enabled");
					break;

				case "isradiobuttonselected":
					objWebElement = this.processElement(locator);
					if (objWebElement.getAttribute("type").equals("radio"))
						blnResult = objWebElement.isSelected();
					System.out.println("Performed verify selected action on radiobox element");
					break;

				case "clearText":
					objWebElement = this.processElement(locator);
					objWebElement.clear();
					System.out.println("Performed clear text on element");
					break;

				case "mousehover":
					objWebElement = this.processElement(locator);
					actionBuilder = new Actions(driverProvider.getWebDriver());
					actionBuilder.moveToElement(objWebElement).build().perform();
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
					System.out.println("Performed mouse over action on element");
					break;

				case "scrollToElement":			 
					objWebElement = this.processElement(locator);
					((JavascriptExecutor) driverProvider.getWebDriver())
					.executeScript("arguments[0].scrollIntoView();", objWebElement);
					System.out.println("Performed scroll to element");
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
					break;
				}

				break;
			}catch(StaleElementReferenceException exceptionStaleElement){
				System.out.println("----->>>StaleElementReferenceException");
				intAttempts ++;
				if(intAttempts <= maxTries){
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exceptionStaleElement.printStackTrace();
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
				}
				else{
					System.out.println("----->>>Exception");				
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exceptionStaleElement.printStackTrace();
					return false;
				}
			}
			catch(NoSuchElementException exceptionNoSuchElementException){
				System.out.println("----->>>NoSuchElementException");
				intAttempts ++;
				if(intAttempts <= maxTries){
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exceptionNoSuchElementException.printStackTrace();
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
				}
				else{
					System.out.println("----->>>Exception");				
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exceptionNoSuchElementException.printStackTrace();
					return false;
				}
			}
			catch(WebDriverException exceptionWebDriverException){
				System.out.println("----->>>WebDriverException");
				intAttempts ++;
				if(intAttempts <= maxTries){
					if(exceptionWebDriverException.getMessage().contains("Other element would receive the click:")){
						waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
					}
					else if(exceptionWebDriverException.getMessage().contains("Element is not clickable")) {
						waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
					}
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exceptionWebDriverException.printStackTrace();
				}
				else{
					System.out.println("----->>>Exception");				
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exceptionWebDriverException.printStackTrace();
					return false;
				}
			}
			catch(Exception exception){
				intAttempts ++;
				if(intAttempts <= maxTries){
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exception.printStackTrace();
				}
				else{
					System.out.println("----->>>Exception");				
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exception.printStackTrace();
					return false;
				}
			}
		}  
		return blnResult;
	}

	/**
	 * Perform specific action on web element
	 * 
	 */
	public boolean invokeOnLocator(By locator, String action, String... values){
		int intAttempts = 0;
		int maxTries = Integer.parseInt(configuration.getConfig("maxTriesForElement"));
		boolean blnResult = false;
		while(intAttempts <= maxTries){
			System.out.println("**** Attempt - " + intAttempts + " to perform " + action +" on element, MaxAttempts" + maxTries);
			try
			{
				WebElement objWebElement;
				action = action.toLowerCase();
				switch(action)
				{
				case "settext":
					objWebElement = this.processElement(locator);
					objWebElement.sendKeys(new CharSequence[]{values[0]});
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
					blnResult =  true;
					System.out.println("Performed set text action on element");
					break;

				case "bootstrapsettext":
					objWebElement = this.processElement(locator);
					objWebElement.sendKeys(new CharSequence[]{values[0]});
					WebElement objWebElementAutoPopulated =  this.processElement(By.xpath("//ul[@class='dropdown-menu ng-isolate-scope']/li/a/strong[contains(text(),'" + values[0] + "')]"));
					objWebElementAutoPopulated.click();
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
					blnResult =  true;
					System.out.println("Performed bootstrap set text action on element");
					break;

				case "settextwithclear":
					objWebElement = this.processElement(locator);
					//objWebElement.clear();
					objWebElement.click();
					String s = objWebElement.getAttribute("value");
					if(s.length() > 0)
					{
						for(int i=0;i<s.length();i++)
						{
							objWebElement.sendKeys(Keys.BACK_SPACE);
						}
					}
					//objWebElement.sendKeys(Keys.chord(Keys.CONTROL,"a", Keys.DELETE));
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);			
					
					
					
					objWebElement.sendKeys(new CharSequence[]{values[0]});
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
					blnResult =  true;
					System.out.println("Performed clear with set text action on element");
					break;

				case "selectfromcustomdropdown":
					objWebElement = this.processElement(locator);
					By sub_locator = By.xpath(".//li/div/span[text()='" + values[0] + "']");
					WebDriverWait wait = new WebDriverWait(driverProvider.getWebDriver(), (long) configuration.getConfigIntegerValue("driver.fluentWaitTimeout"));
					wait.until(ExpectedConditions.visibilityOfNestedElementsLocatedBy(objWebElement, sub_locator));
					List<WebElement> objCustomOptions = objWebElement.findElements(sub_locator); 
					for(WebElement weOption : objCustomOptions){  
						if (weOption.getText().trim().equals(values[0])){
							weOption.click();
							waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
							System.out.println("Selected dropdown option for element");
							blnResult =  true;
						}
					} 		 
					break;

				case "selectdropdown":
					objWebElement = this.processElement(locator);
					Select sltDropDown = new Select(objWebElement);

					if(values.length > 1 && !values[1].equals(""))
					{
						if(values[1].equalsIgnoreCase("Value"))
							sltDropDown.selectByValue(values[0]);
						else if(values[1].equalsIgnoreCase("Text"))
							sltDropDown.selectByVisibleText(values[0]);
						else if(values[1].equalsIgnoreCase("Index"))
							sltDropDown.selectByIndex(Integer.parseInt(values[0]));

						waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
						System.out.println("Selected dropdown options for element");
						blnResult =  true;
					}
					else
					{
						// Web elements from dropdown list 
						List<WebElement> objOptions = sltDropDown.getOptions();
						boolean bOptionAvailable = false;
						int iIndex = 0;
						for(WebElement weOptions : objOptions)  
						{  
							if (weOptions.getText().trim().equalsIgnoreCase(values[0]))
							{
								sltDropDown.selectByIndex(iIndex);
								waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
								bOptionAvailable = true;
								System.out.println("Selected dropdown options for element");
								break;
							}
							else
								iIndex++;
						}
						return bOptionAvailable;
					}
					break;

				}
				break;
			}catch(StaleElementReferenceException exceptionStaleElement){
				System.out.println("----->>>StaleElementReferenceException");
				intAttempts ++;
				if(intAttempts <= maxTries){
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exceptionStaleElement.printStackTrace();
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
				}
				else{
					System.out.println("----->>>Exception");				
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exceptionStaleElement.printStackTrace();
					return false;
				}
			}
			catch(NoSuchElementException exceptionNoSuchElementException){
				System.out.println("----->>>NoSuchElementException");
				intAttempts ++;
				if(intAttempts <= maxTries){
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exceptionNoSuchElementException.printStackTrace();
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
				}
				else{
					System.out.println("----->>>Exception");				
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exceptionNoSuchElementException.printStackTrace();
					return false;
				}
			}
			catch(WebDriverException exceptionWebDriverException){
				System.out.println("----->>>WebDriverException");
				intAttempts ++;
				if(intAttempts <= maxTries){
					if(exceptionWebDriverException.getMessage().contains("Other element would receive the click:")){
						waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
					}
					else if(exceptionWebDriverException.getMessage().contains("Element is not clickable")) {
						waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
					}
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exceptionWebDriverException.printStackTrace();
				}
				else{
					System.out.println("----->>>Exception");				
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exceptionWebDriverException.printStackTrace();
					return false;
				}
			}
			catch(Exception exception){
				intAttempts ++;
				if(intAttempts <= maxTries){
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exception.printStackTrace();
				}
				else{
					System.out.println("----->>>Exception");				
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exception.printStackTrace();
					return false;
				}
			}
		}  
	 		return blnResult;
	}

	/**
	 * Perform specific action on web element
	 * 
	 */
	public String getFromLocator(By locator, String action, String... strategy){
		int intAttempts = 0;
		int maxTries = Integer.parseInt(configuration.getConfig("maxTriesForElement"));
		String returnVal = "";
		while(intAttempts <= maxTries){
			System.out.println("Attempt - " + intAttempts + " to perform " + action +" on element, MaxAttempts" + maxTries);
			try{
				WebElement webElement;
				action = action.toLowerCase();

				switch(action) 
				{
				case"getattribute":
					webElement = this.processElement(locator);
					returnVal = webElement.getAttribute(strategy[0]);
					System.out.println("Performed get attribute action on web element");
					break;

				case "gettext":
					webElement = this.processElement(locator);
					if(strategy!=null && strategy.length > 0) {	
						if (strategy[0].equalsIgnoreCase("value"))
							returnVal = webElement.getAttribute("value");
						else if (strategy[0].equalsIgnoreCase("title"))
							returnVal = webElement.getAttribute("title");
						else if (strategy[0].equalsIgnoreCase("text"))
							returnVal = webElement.getText();
					}
					else
						returnVal = webElement.getText();

					System.out.println("Performed get text action on web element");
					break;

				case "getselectedvaluefromdropdown":
					webElement = this.processElement(locator);
					Select selectDorpDown = new Select(webElement);
					String selectedDorpDownValue = selectDorpDown.getFirstSelectedOption().getText();
					returnVal = selectedDorpDownValue;
					System.out.println("Performed get text selected action on dropdown element");
					break;	
				}
				break;
			}catch(StaleElementReferenceException exceptionStaleElement){
				System.out.println("----->>>StaleElementReferenceException");
				intAttempts ++;
				if(intAttempts <= maxTries){
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exceptionStaleElement.printStackTrace();
				}
				else{
					System.out.println("----->>>Exception");				
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exceptionStaleElement.printStackTrace();
					return null;
				}
			}
			catch(NoSuchElementException exceptionNoSuchElementException){
				System.out.println("----->>>NoSuchElementException");
				intAttempts ++;
				if(intAttempts <= maxTries){
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exceptionNoSuchElementException.printStackTrace();
				}
				else{
					System.out.println("----->>>Exception");				
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exceptionNoSuchElementException.printStackTrace();
					return null;
				}
			}
			catch(WebDriverException exceptionWebDriverException){
				System.out.println("----->>>WebDriverException");
				intAttempts ++;
				if(intAttempts <= maxTries){
					if(exceptionWebDriverException.getMessage().contains("Other element would receive the click:")){
						waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
					}
					else if(exceptionWebDriverException.getMessage().contains("Element is not clickable")) {
						waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
					}
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exceptionWebDriverException.printStackTrace();
				}
				else{
					System.out.println("----->>>Exception");				
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exceptionWebDriverException.printStackTrace();
					return null;
				}
			}
			catch(Exception exception){
				intAttempts ++;
				if(intAttempts <= maxTries){
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exception.printStackTrace();
				}
				else{
					System.out.println("----->>>Exception");				
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exception.printStackTrace();
					return null;
				}
			}
		}  
		System.out.println("returnVal------->" + returnVal);
		return returnVal;
	}

	/**
	 * Perform specific action on web element
	 *
	 */
	public boolean invokeOnLocator(By locator, String action, boolean... status){
		int intAttempts = 0;
		int maxTries = Integer.parseInt(configuration.getConfig("maxTriesForElement"));
		boolean blnResult = false;
		while(intAttempts <= maxTries){
			System.out.println("**** Attempt - " + intAttempts + " to perform " + action +" on element, MaxAttempts" + maxTries);
			try
			{
				WebElement objWebElement;
				action = action.toLowerCase();
				switch(action)
				{
				case "selectcheckbox":
					objWebElement = this.processElement(locator);
					if (objWebElement.getAttribute("type").equals("checkbox")) {
						if ((objWebElement.isSelected() && !status[0]) || (!objWebElement.isSelected() && status[0]))
							objWebElement.click();
						blnResult = true;
					} else
						blnResult = false;
					System.out.println("Performed click action on checkbox element");
					break;

				case "selectradiobutton":
					objWebElement = this.processElement(locator);
					if (objWebElement.getAttribute("type").equals("radio")) {
						if ((objWebElement.isSelected() && !status[0]) || (!objWebElement.isSelected() && status[0]))
							objWebElement.click();
						blnResult = true;
					} else
						blnResult = false;
					System.out.println("Performed click action on radio element");
					break;
				}
				break;
			}catch(StaleElementReferenceException exceptionStaleElement){
				System.out.println("----->>>StaleElementReferenceException");
				intAttempts ++;
				if(intAttempts <= maxTries){
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exceptionStaleElement.printStackTrace();
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
				}
				else{
					System.out.println("----->>>Exception");				
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exceptionStaleElement.printStackTrace();
					return false;
				}
			}
			catch(NoSuchElementException exceptionNoSuchElementException){
				System.out.println("----->>>NoSuchElementException");
				intAttempts ++;
				if(intAttempts <= maxTries){
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exceptionNoSuchElementException.printStackTrace();
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
				}
				else{
					System.out.println("----->>>Exception");				
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exceptionNoSuchElementException.printStackTrace();
					return false;
				}
			}
			catch(WebDriverException exceptionWebDriverException){
				System.out.println("----->>>WebDriverException");
				intAttempts ++;
				if(intAttempts <= maxTries){
					if(exceptionWebDriverException.getMessage().contains("Other element would receive the click:")){
						waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
					}
					else if(exceptionWebDriverException.getMessage().contains("Element is not clickable")) {
						waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
					}
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exceptionWebDriverException.printStackTrace();
				}
				else{
					System.out.println("----->>>Exception");				
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exceptionWebDriverException.printStackTrace();
					return false;
				}
			}
			catch(Exception exception){
				intAttempts ++;
				if(intAttempts <= maxTries){
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exception.printStackTrace();
				}
				else{
					System.out.println("----->>>Exception");				
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exception.printStackTrace();
					return false;
				}
			}
		}  
	 	return blnResult;
	}



	/**
	 * Process web element with all defined synchronization 
	 * @return processed web element
	 * 
	 */
	public WebElement processElement(final By locator)
	{
		System.out.println("\n webdriver processing web element");

		try{
			Wait<WebDriver> wait = new FluentWait<WebDriver>(driverProvider.getWebDriver())
					.withTimeout(Duration.ofSeconds(configuration.getConfigIntegerValue("driver.fluentWaitTimeout")))
					.pollingEvery(Duration.ofSeconds(configuration.getConfigIntegerValue("driver.fluentWaitPullingTimeout")))
					.ignoring(NoSuchElementException.class)
					.ignoring(InvalidElementStateException.class)
					.ignoring(StaleElementReferenceException.class);

			WebElement webElement = wait.until(new Function<WebDriver, WebElement>(){
				public WebElement apply(WebDriver driver){
					return driverProvider.getWebDriver().findElement(locator);
				}
			}); 

			WebDriverWait waitExplicit = new WebDriverWait(driverProvider.getWebDriver(), Explicit_WAIT_TIMEOUT);
			webElement = waitExplicit.until(new Function<WebDriver, WebElement>() {
				public WebElement apply(WebDriver driver) {
					return driverProvider.getWebDriver().findElement(locator);
				}
			});

			return webElement;
		}catch(Exception exception){
			System.out.println("---> Process Elelement --->>Exception -- webdriver unable to find element");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			return null;
		}  
	}

	/**
	 * Process web element with all defined synchronization 
	 * @return processed web element list
	 * 
	 */
	public List<WebElement> processElements(final By locator)
	{
		System.out.println("Attempt to process selenium web element");
		try{
			Wait<WebDriver> wait = new FluentWait<WebDriver>(driverProvider.getWebDriver())
					.withTimeout(Duration.ofSeconds(configuration.getConfigIntegerValue("driver.fluentWaitTimeout")))
					.pollingEvery(Duration.ofSeconds(configuration.getConfigIntegerValue("driver.fluentWaitPullingTimeout")))
					.ignoring(NoSuchElementException.class)
					.ignoring(InvalidElementStateException.class)
					.ignoring(StaleElementReferenceException.class);

			List<WebElement> webElements = wait.until(new Function<WebDriver,  List<WebElement>>(){
				public  List<WebElement> apply(WebDriver driver){
					return driverProvider.getWebDriver().findElements(locator);
				}
			}); 

			WebDriverWait waitExplicit = new WebDriverWait(driverProvider.getWebDriver(), Explicit_WAIT_TIMEOUT);
			webElements = waitExplicit.until(new Function<WebDriver, List<WebElement>>() {
				public List<WebElement> apply(WebDriver driver) {
					return driverProvider.getWebDriver().findElements(locator);
				}
			});

			return webElements;
		}catch(Exception exception){
			System.out.println("---> Process Elelement --->>Exception");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			return null;
		} 
	}

	/**
	 *  wait for completion of active ajax control
	 * 
	 */
	public void waitForAjaxControls(int timeoutInSeconds) {
		System.out.println("Querying active AJAX controls by calling jquery.active");
		try {
			if (driverProvider.getWebDriver() instanceof JavascriptExecutor) 
			{
				JavascriptExecutor jsDriver = (JavascriptExecutor) driverProvider.getWebDriver();
				for (int i = 0; i < timeoutInSeconds; i++) {
					Object numberOfAjaxConnections = jsDriver
							.executeScript("return jQuery.active");
					// return should be a number
					if (numberOfAjaxConnections instanceof Long) 
					{
						Long n = (Long) numberOfAjaxConnections;
						System.out.println("Number of active jquery AJAX controls: " + n);
						if (n.longValue() == 0L)
							break;
					}
					Thread.sleep(1000);
				}
			} else {
				System.out.println("Web driver: can't run javascript.");
			}
		} catch (InterruptedException interruptedException) {
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				interruptedException.printStackTrace();
		}
	}

	// Framework User can configure loading indicator over here 
	public void waitForPageLoadingIndicator() {
		WebElement waitElement = null;
		try 
		{
			//Sets FluentWait Setup
			Wait<WebDriver> fluentWait  = new FluentWait<WebDriver>(driverProvider.getWebDriver())
					.withTimeout(Duration.ofSeconds(configuration.getConfigIntegerValue("driver.fluentWaitTimeout")))
					.pollingEvery(Duration.ofSeconds(configuration.getConfigIntegerValue("driver.fluentWaitPullingTimeout")))
					.ignoring(NoSuchElementException.class)
					.ignoring(TimeoutException.class);

			// First checking to see if the loading indicator is found
			// we catch and throw no exception here in case they aren't ignored

			waitElement = fluentWait.until(new Function<WebDriver, WebElement>() {
				public WebElement apply(WebDriver driver) {
					return driver.findElement(By.xpath("// Loading  "));
				}
			});

			//checking if loading indicator was found and if so we wait for it to disappear
			if (waitElement != null) {
				WebDriverWait wait = new WebDriverWait(driverProvider.getWebDriver(), 
						configuration.getConfigIntegerValue("driver.fluentWaitPullingTimeout"));
				wait.until(ExpectedConditions.invisibilityOfElementLocated(
						By.xpath("//Loading  "))
						);
			}
		} catch (Exception exception) {
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
		}
	}

	/*public boolean checkElementElementDisplayedWithMinWait(By locator){
		try{
			WebDriverWait wait = new WebDriverWait(driverProvider.getWebDriver(), (long)FLUENTWAIT_WAIT_MIN_TIMEOUT);
			wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
			driverProvider.getWebDriver().findElement(locator);
			return true;
		}catch(Exception exception){
			System.out.println("---> checkMobileElementDisplayedWithMinWait --->>Exception");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			return false;
		}
	}
*/
	public boolean checkElementDisplayedWithMidWait(By locator){
		try{
			WebDriverWait wait = new WebDriverWait(driverProvider.getWebDriver(), (long)FLUENTWAIT_WAIT_MID_TIMEOUT);
			wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
			driverProvider.getWebDriver().findElement(locator);
			return true;
		}catch(Exception exception){
			System.out.println("---> checkMobileElementDisplayedWithMidWait --->>Exception");
			exception.printStackTrace();
			return false;
			
		}
	}

	/**
	 * Press keyboard keys
	 *  @author Harshvardhan Yadav (Expleo)
	 */
	public boolean pressKeybordKeys(By locator, String key){
		try{
			WebElement objWebElement = this.processElement(locator);
			if(key.toLowerCase().equals("tab"))
				objWebElement.sendKeys(Keys.TAB);
			if(key.toLowerCase().equals("enter"))
				objWebElement.sendKeys(Keys.ENTER);
			return true;
		}catch(Exception exception){
			System.out.println("---> pressKeybordKeys --->>Exception");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			return false;
		}
	}

	/**
	 * @Method : pageRefresh
	 * @Description : This method is to refresh page
	 *  @author Harshvardhan Yadav (Expleo)
	 */
	public boolean pageRefresh() {
		try {
			waitMethods.sleep(configuration.getConfigIntegerValue("midwait"));
			driverProvider.getWebDriver().navigate().refresh();
			return true;
		}catch(Exception exception){
			System.out.println("---> Unable to --->>refresh page");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			return false;
		}
	}

	/**
	 * Get the CSS property value for given property name
	 *  @author Harshvardhan Yadav (Expleo)
	 * 
	 */
	public String getCssValue(By locator, String propertyName) {
		try {
			processElement(locator);
			WebElement webElement = driverProvider.getWebDriver().findElement(locator);
			return webElement.getCssValue(propertyName);
		}catch(Exception exception){
			System.out.println("Failed to get getCssValue");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			return "";
		}
	}

	/**
	 * param: locator, cssProperty(i.e background-color, font-size etc), value(i.e
	 * expected value)
	 *  @author Harshvardhan Yadav (Expleo)
	 * 
	 *
	 */
	public boolean checkCssValue(By locator, String propertyName, String value) {
		try {
			String rgb = this.getCssValue(locator, propertyName);
			String actualValue = utilities.toConvertHexValue(rgb);
			System.out.println(actualValue);
			if (value.equalsIgnoreCase(actualValue)) return true;
			else return false;
		}catch(Exception exception){
			System.out.println("Failed to check css value");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			return false;
		}
	}

	/**
	 * Get the size of the element
	 *  @author Harshvardhan Yadav (Expleo)
	 */
	public Dimension getSize(By locator) {

		try {
			this.processElement(locator);
			WebElement webElement = driverProvider.getWebDriver().findElement(locator);
			return webElement.getSize();
		}catch(Exception exception){
			System.out.println("Failed to element size");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			return null;
		}

	}

	/**
	 * Get the Height of the element
	 * @author Harshvardhan Yadav (Expleo)
	 */
	public int getHeight(By locator) {
		try {
			return this.getSize(locator).getHeight();
		}catch(Exception exception){
			System.out.println("Failed to element height");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			return 0;
		}
	}

	public int getWidth(By locator) {
		try {
			return this.getSize(locator).getWidth();
		}catch(Exception exception){
			System.out.println("Failed to element height");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			return 0;
		}

	}

	/**
	 * Get the url 
	 *  @author Harshvardhan Yadav (Expleo)
	 */
	public String getUrl() {
		try {
			waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
			return driverProvider.getWebDriver().getCurrentUrl();
		}catch(Exception exception){
			System.out.println("Failed to get current URL");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			return "";
		}

	}

	/**
	 * Return current window handle
	 * @return current window handle
	 *  @author Harshvardhan Yadav (Expleo)
	 */
	public String getCurrentWindowHandle() {
		try {
			waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
			return driverProvider.getWebDriver().getWindowHandle();
		}catch(Exception exception){
			System.out.println("Unable to get main window handel");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			return null;
		} 
	}

	/**
	 * switchTo child Window
	 *  @author Harshvardhan Yadav (Expleo)
	 **/
	public void switchToChildWindow() 
	{	
		try
		{
			waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
			String parentHandle = this.getCurrentWindowHandle();
			Set<String> windowNames = driverProvider.getWebDriver().getWindowHandles();

			@SuppressWarnings("rawtypes")
			Iterator ite = windowNames.iterator();

			while (ite.hasNext()) 
			{
				String popupHandle = ite.next().toString();
				if (!popupHandle.equals(parentHandle)) {
					driverProvider.getWebDriver().switchTo().window(popupHandle);
					break;
				}
			}
		}catch(Exception exception){
			System.out.println("Unable to siwtch to child window");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
		} 
	}

	/**
	 * switchTo child Window using window handle
	 *  @author Harshvardhan Yadav (Expleo)
	 **/
	public void switchToWindowUsingHandle(String windowHandle) 
	{	
		try
		{
			waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
			Set<String> windowNames = driverProvider.getWebDriver().getWindowHandles();

			@SuppressWarnings("rawtypes")
			Iterator ite = windowNames.iterator();

			while (ite.hasNext()) 
			{
				String popupHandle = ite.next().toString();
				if (popupHandle.equals(windowHandle)) {
					driverProvider.getWebDriver().switchTo().window(popupHandle);
					break;
				}
			}
		}catch(Exception exception){
			System.out.println("Unable to siwtch to window using handle");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
		} 
	}

	/**
	 * switch to window using the given title
	 * @param : locator - Window title
	 *  @author Harshvardhan Yadav (Expleo)
	 */
	public boolean switchToWindowUsingTitle(String windowTitle) {
		try {
			waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
			String mainWindowHandle = driverProvider.getWebDriver().getWindowHandle();
			Set<String> openWindows = driverProvider.getWebDriver().getWindowHandles();

			if (!openWindows.isEmpty()) {
				for (String windows : openWindows) {
					String window = driverProvider.getWebDriver().switchTo().window(windows).getTitle();
					System.out.println("****window title >>> "+window);
					if (windowTitle.equals(window)) {
						return true;
					}
					else
						driverProvider.getWebDriver().switchTo().window(mainWindowHandle);
				}
			}
			return false;
		} catch (Exception exception) {
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			return false;
		}
	}

	/**
	 * execute autoit script
	 *  @author Harshvardhan Yadav (Expleo)
	 **/
	public void executeAutoIIscriptForFileUpload(String filePath) {
		try {
			String commandToRun = System.getProperty("user.dir") + "/src/test/resources/uploads/FileUploadScript/FileUpload.exe" + " " + filePath;
			CommandPrompt.runCommand(commandToRun);
			waitMethods.sleep(configuration.getConfigIntegerValue("midwait")); 
		}catch(Exception exception){
			System.out.println("Unable to execute autoit script");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
		} 
	}

	/**
	 *  switch to the Frame by Frame name
	 *  @param : locator - The most common one. You locate your iframe like other
	 *        		elements, then pass it into the method
	 *       		eg.driver.switchTo().frame(driver.findElement(By.xpath(".//iframe[@title='Test']")))
	 *  @author Harshvardhan Yadav (Expleo) 
	 */
	public void switchToFrameUsingIframe_Element(By locator) {
		try {
			driverProvider.getWebDriver().switchTo().defaultContent();
			driverProvider.getWebDriver().switchTo().frame(this.processElement(locator));
			System.out.println("Switched to frame");
		} catch (Exception exception) {
			System.out.println("unable to switch to frame");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
		}
		
	}

	/**
	 * switch to the Frame by Frame name or Id
	 * @param : frameName - Name/Id of frame you want to switch
	 *  @author Harshvardhan Yadav (Expleo)
	 */
	public void switchToFrameUsingNameOrId(String frameNameOrID) {
		try {
			driverProvider.getWebDriver().switchTo().defaultContent();
			driverProvider.getWebDriver().switchTo().frame(frameNameOrID);
			System.out.println("Switched to frame");
		} catch (Exception exception) {
			System.out.println("unable to switch to frame");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
		}
	}

	/**
	 * switch to the Frame by index
	 * @param : index - Index on page
	 * @author Harshvardhan Yadav (Expleo)
	 */
	public void switchToFrameUsingIndex(int index) {
		try {
			driverProvider.getWebDriver().switchTo().defaultContent();
			driverProvider.getWebDriver().switchTo().frame(index);
			System.out.println("Switched to frame");
		} catch (Exception exception) {
			System.out.println("unable to switch to frame");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
		}
	}

	/**
	 *	switch to the default content
	 * 	 @author Harshvardhan Yadav (Expleo)
	 */
	public void switchToDefaultContent() {
		try {
			driverProvider.getWebDriver().switchTo().defaultContent();
			System.out.println("Switched to default content");
		} catch (Exception exception) {
			System.out.println("unable to switch to default content");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
		}
	}

	/**
	 * Highlight element
	 * @author Harshvardhan Yadav (Expleo)
	 */
	public void elementHighlight(By locator) {
		WebElement element = this.processElement(locator);
		for (int i = 0; i < 10; i++) {
			JavascriptExecutor js = (JavascriptExecutor) driverProvider.getWebDriver();
			js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element,
					"color: red; border: 3px solid red;");
			js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "");
		}
	}

	/**
	 * Execute javascript
	 * 
	 */
	public Object executeJavascript(String javascript) {
		JavascriptExecutor js = (JavascriptExecutor) driverProvider.getWebDriver();
		return js.executeScript(javascript); 
	}
	
	/*******************************************************
	 * 
	 * Following are common function examples update as per project need 
	 * 
	 *******************************************************/

	/**
	 * Check given content is available under whole table with respect to table header
	 * @param : locator - By identification of element (xpath up to table )
	 * @param : columnHeader - String column header
	 * @param : ContentToVerify - String Content to be verify 
	 * 
	 */
	public boolean verifyTableContent(By locator, String columnHeader, String ContentToVerify) {
		Hashtable<String, String> dataColumnHeader = new Hashtable<String, String>();
		int intColumnNumber = 1;
		boolean blnverify = false;
		try {
			WebElement weResultTable = this.processElement(locator);
			List<WebElement> weColumnsHeaders = weResultTable.findElements(By.xpath(".//thead/tr/th"));
			for (WebElement weColumnHeader : weColumnsHeaders) {
				String strHeader = weColumnHeader.getText().trim();
				if (!strHeader.equals(""))
					dataColumnHeader.put(strHeader, String.valueOf(intColumnNumber));
				intColumnNumber++;
			}

			List<WebElement> weRows = weResultTable.findElements(By.xpath(".//tbody/tr"));
			for (WebElement weRow : weRows) {
				WebElement weExceptedClm = weRow.findElement(By.xpath
						(".//td[" + dataColumnHeader.get(columnHeader) + "]"));
				if (weExceptedClm.getText().trim().equals(ContentToVerify)) {
					blnverify = true;
					return blnverify;
				}
			}
			return blnverify;
		} catch (Exception exception) {
			exception.printStackTrace();
			return false;
		}
	}

	/**
	 * Common function to verify column content
	 *  @author Harshvardhan Yadav (Expleo)
	 **/
	public boolean verifyGraphContentAgainstToColumn(By locatorGraph, String XaxisHeader, String contentToVerifyFirst)
	{
		Hashtable<String, String> dataColumnHeader = new Hashtable<String, String>();
		int intColumnNumber = 1;
		boolean blnverify = false;

		try {
			WebElement weGraph = this.processElement(locatorGraph);
			List<WebElement> weGraphXAxis = weGraph.findElements(By.xpath(".//*[@class='highcharts-axis-labels highcharts-xaxis-labels']//*"));
			for (WebElement weXAxisHeader : weGraphXAxis)
			{
				String strHeader = weXAxisHeader.getText().trim();
				if (!strHeader.equals(""))
					dataColumnHeader.put(strHeader, String.valueOf(intColumnNumber));
				intColumnNumber++;
			}

			WebElement weExceptedYAxisHeader = weGraph.findElement(By.xpath(".//*[@class='highcharts-data-labels highcharts-series-0 highcharts-tracker']/*[" + dataColumnHeader.get(XaxisHeader) + "]/*/*"));
			System.out.println(" weExceptedClm : " + weExceptedYAxisHeader.getText());
			System.out.println(" contentToVerifyFirst : " + contentToVerifyFirst);
			if (weExceptedYAxisHeader.getText().trim().equalsIgnoreCase(contentToVerifyFirst))
			{
				blnverify = true;
				System.out.println("blnverify" + blnverify);
				return blnverify;
			}

			return blnverify;
		} catch (Exception exception) {
			System.out.println(exception.getMessage());
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			return false;
		}
	}
	
	public String getEmail() {
		String regex = "[a-zA-Z0-9]{5}\\@mailinator\\.com";
		this.randomEmail = new Generex(regex).random();
		return randomEmail;
	}
	
	public String useEmail() {
		return randomEmail;
	}
	public void openNewTab()
	{
		try {
			Robot r = new Robot();		
			r.keyPress(KeyEvent.VK_CONTROL);
			r.keyPress(KeyEvent.VK_T);
			r.keyRelease(KeyEvent.VK_CONTROL);
			r.keyRelease(KeyEvent.VK_T);
			waitMethods.sleep(5);
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}