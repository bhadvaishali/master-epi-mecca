package com.generic.webdriver;

import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
/**
 * used to initiate selenium webdriver for automation  
 * @author Harshvardhan Yadav(SQS)
 * @modified 	  :Harhvardhan Yadav, Namrata Donikar (Expleo)
 *
 */
public class WebDriverFactory
{
	 String URL = "https://vaishali_arZiuV:eNBXCEXJTnQWrxRsgRtz@hub.browserstack.com/wd/hub";
	public WebDriver setWebDriver(Properties objConfig) throws Exception
	{
		WebDriver webDriver;
		String browser = objConfig.getProperty("web.browser").trim().toLowerCase();
		switch (browser)
		{
		case "ie":
		 	webDriver = new InternetExplorerDriver(this.ieDriverDesiredCapabilities(objConfig));
			break;

		case "firefox": 
			webDriver = new FirefoxDriver();
			break;

		case "chrome": 		
			
		 	webDriver = new ChromeDriver(this.chromeDriverDesiredCapabilities(objConfig));
			/*DesiredCapabilities desiredCapabilities = DesiredCapabilities.chrome();
			desiredCapabilities.setCapability("browser", "chrome");
			desiredCapabilities.setCapability("browser_version", "92.0");
			desiredCapabilities.setCapability("os", "Windows");
			desiredCapabilities.setCapability("os_version", "10");
	 		webDriver = new RemoteWebDriver(new URL(URL), desiredCapabilities);*/
			break;
		case "safari":
			webDriver = new SafariDriver();
			break;
		case "nonincognito":
			webDriver = new ChromeDriver();
			break;
		default:
			webDriver = new FirefoxDriver();
		}
		return webDriver;
	} 
  
	private InternetExplorerOptions ieDriverDesiredCapabilities(Properties objConfig) throws IOException
	{
		System.setProperty("webdriver.ie.driver", System.getProperty("user.dir") + objConfig.getProperty("webdriver.ie.driver").trim());
		InternetExplorerOptions ieDesiredCapabilities =new InternetExplorerOptions();
		//DesiredCapabilities ieDesiredCapabilities = DesiredCapabilities.internetExplorer(); 
		ieDesiredCapabilities.setCapability("initialBrowserUrl", "about:blank");
		//ieDesiredCapabilities.setCapability("requireWindowFocus", true);
		//ieDesiredCapabilities.setCapability("ie.ensureCleanSession", true);
		//ieDesiredCapabilities.setCapability("enableElementCacheCleanup", true);
		ieDesiredCapabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
		//ieDesiredCapabilities.setCapability(InternetExplorerDriver.FORCE_CREATE_PROCESS, true);
		//ieDesiredCapabilities.setCapability(InternetExplorerDriver.IE_SWITCHES, "-private");
		//ieDesiredCapabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
//		Proxy seleniumProxy = ClientUtil.createSeleniumProxy(proxyServer);
//		seleniumProxy.setNoProxy("");
//		//seleniumProxy.setSslProxy("trustAllSSLCertificates");
//		ieDesiredCapabilities.setCapability(CapabilityType.PROXY, seleniumProxy);
//		ieDesiredCapabilities.setCapability( "ie.setProxyByServer", true ); //$NON-NLS-1$
//		ieDesiredCapabilities.setCapability( "ignoreZoomSetting", true );
//		
	 	return ieDesiredCapabilities;
	}
	
	/* Set desired capabilities for chrome driver 
	 * @author:  Harhvardhan Yadav, Namrata Donikar (Expleo)
	 * */
	private ChromeOptions chromeDriverDesiredCapabilities(Properties objConfig) throws IOException
	{
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/src/main/resources/drivers/chromedriver_win32/chromedriver.exe");	
			
		//System.setProperty("webdriver.chrome.driver", "C:\\Users\\bhadv\\Downloads\\chromedriver_win32 (10)\\chromedriver.exe");	
		//DesiredCapabilities chromeDesiredCapabilities = DesiredCapabilities.chrome(); 
		
		
		ChromeOptions objChromeOption = new ChromeOptions();
//		Boolean isIncognito = Boolean.parseBoolean(System.getProperty(""));
//		if (isIncognito) {			
//			objChromeOption.addArguments("--incognito");
//		}
		objChromeOption.addArguments("--incognito");
		//objChromeOption.addArguments("--headless");
		//chromeDesiredCapabilities.setCapability("w3c", false);		
		objChromeOption.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		//objChromeOption.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
		//objChromeOption.setCapability("chrome.switches", Arrays.asList("--ignore-certificate-errors"));
		objChromeOption.setAcceptInsecureCerts(true);
		//objChromeOption.setCapability("acceptSslCerts" , true);
		objChromeOption.addArguments("--disable-notifications");
		//[START] to disable know your location popup of chrome
		Map<String, Object> prefs = new HashMap<String, Object>();
		 Map<String, Object> profile = new HashMap<String, Object>();
		 Map<String, Object> contentSettings = new HashMap<String, Object>();
		// Specify the ChromeOption we need to set
		 contentSettings.put("geolocation", 1);
		 profile.put("managed_default_content_settings", contentSettings);
		 prefs.put("profile", profile);
		 objChromeOption.setExperimentalOption("prefs", prefs);
		 //objChromeOption.setCapability(ChromeOptions.CAPABILITY, options);
		//[END]
		//objChromeOption.addArguments("--enable-strict-powerful-feature-restrictions");
	
		//objChromeOption.setCapability("chrome.switches", Arrays.asList("--ignore-certificate-errors,--web-security=false,--ssl-protocol=any,--ignore-ssl-errors=true, --illegal-access=warn"));
		
		/*
		 * objChromeOption.addArguments("--headless");
		 * objChromeOption.addArguments("--no-sandbox");
		 * objChromeOption.addArguments("disable-infobars");
		 * objChromeOption.addArguments("start-maximized"); 
		 * objChromeOption.addArguments("--disable-extensions");
		 */		
	//	objChromeOption.setBinary(System.getProperty("user.dir") + "/src/main/resources/browsers/Chrome/Application/chrome.exe");
		//objChromeOption.setCapability(ChromeOptions.CAPABILITY, chromeDesiredCapabilities);		
		//objChromeOption.merge(chromeDesiredCapabilities);
		//objChromeOption.setCapability("initialBrowserUrl", "about:blank");
				
		/*
		 * Proxy seleniumProxy = ClientUtil.createSeleniumProxy(proxyServer);
		 * chromeDesiredCapabilities.setCapability(CapabilityType.PROXY, seleniumProxy);
		 */				
		return objChromeOption;
	}
 
	public String getRequiredTime(String incrementMin, String expectedDateFormat, String timeZoneId)
	{
		try 
		{
			DateFormat dateFormat;
			Calendar calendar = Calendar.getInstance();
			dateFormat = new SimpleDateFormat(expectedDateFormat);
			if(timeZoneId != null && ! timeZoneId.equals(""))
				dateFormat.setTimeZone(TimeZone.getTimeZone(timeZoneId));
			if(incrementMin != null && ! incrementMin.equals(""))
				calendar.add(Calendar.MINUTE, Integer.parseInt(incrementMin));
			Date time = calendar.getTime();
			String formattedTime = dateFormat.format(time);
			return formattedTime;
		}
		catch (Exception exception) 
		{
			exception.printStackTrace();
			return null;
		}
	} 
}
