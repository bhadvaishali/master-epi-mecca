package com.generic;

import java.time.Duration;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeoutException;
import java.util.function.Function;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.interfaces._MobileActions;
import com.generic.utils.Configuration;
import com.generic.utils.Utilities;
import com.generic.utils.WaitMethods;
import com.mifmif.common.regex.Generex;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.LongPressOptions;
import io.appium.java_client.touch.offset.ElementOption;

/**
 * wrapper functions for performing actions on mobile element 
 * * @author Harshvardhan Yadav(SQS)
 *  @modified 	  :Harhvardhan Yadav, Namrata Donikar (Expleo)
 */
public class MobileActions implements _MobileActions 
{
	private Configuration configuration;
	private AppiumDriverProvider appiumDriverProvider;
	private WaitMethods waitMethods;
	private Utilities utilities;

	private static final Exception Exception = null;
	private int DEFAULT_SLEEP_TIMEOUT = 2;
	private int FLUENTWAIT_WAIT_MID_TIMEOUT = 30;//20;
	private int FLUENTWAIT_WAIT_MIN_TIMEOUT = 20;//10;
	private int Explicit_WAIT_TIMEOUT = 15;

	public MobileActions(Configuration configuration, AppiumDriverProvider appiumDriverProvider, WaitMethods waitMethods , Utilities utilities){
		this.configuration = configuration;
		this.appiumDriverProvider = appiumDriverProvider;
		this.waitMethods = waitMethods;
		this.utilities = utilities;
	}


	/**
	 * waitTillPageLoadCompletes : This method for to wait till the page loading completes.
	 * 							   This method is used for only few of the scenarios.
	 * 
	 * @author Dayanand Dhange  (Expleo)
	 *
	 */
	public void waitTillPageLoadCompletes() {
		//boolean flag = false;
		/*WebDriverWait wait = new WebDriverWait(appiumDriverProvider.getAppiumDriver(), 30); 
		Predicate<WebDriver> predicate = driver1 -> ((JavascriptExecutor)driver1).executeScript("return document.readyState").equals("complete"); 
		wait.until(predicate); 
		System.out.println("page loaded !");**/

		new WebDriverWait(appiumDriverProvider.getAppiumDriver(), 30).until(
				webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
	}


	/**
	 * checkAlertPresentAndAcceptIt : This method for iOS to handle the popup with Allow button to 
	 * 								  handle the allow button
	 * 
	 * @author Dayanand Dhange  (Expleo)
	 *
	 */
	public void checkAlertPresentAndAcceptIt() {
		boolean flag=false;
		try {
			WebDriverWait wait = new WebDriverWait(appiumDriverProvider.getAppiumDriver(), 10 /*timeout in seconds*/);
			if(wait.until(ExpectedConditions.alertIsPresent())==null){
				System.out.println("alert was not present");
				flag=false;

			}
			else
				flag=true;
		}
		catch(Exception e) {
			flag=false;
		}

		if(flag)
		{
			Alert alert = appiumDriverProvider.getAppiumDriver().switchTo().alert();
			String alertText=alert.getText();
			alert.accept();
			System.out.println(alertText+" alert was present and accepted");
		}
	}

	/**
	 * param: locator, cssProperty(i.e background-color, font-size etc), value(i.e
	 * expected value)
	 * 
	 * @author Harhvardhan Yadav, Namrata Donikar (Expleo)
	 *
	 */
	public boolean checkCssValue(By locator, String propertyName, String value) {
		try {
			String rgb = this.getCssValue(locator, propertyName);
			String actualValue = utilities.toConvertHexValue(rgb);
			System.out.println(actualValue);
			if (value.equalsIgnoreCase(actualValue)) return true;
			else return false;
		}catch(Exception exception){
			System.out.println("Failed to check css value");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			return false;
		}
	}


	/**
	 * Get the CSS property value for given property name
	 * 
	 * @author Harhvardhan Yadav, Namrata Donikar (Expleo)
	 */
	public String getCssValue(By locator, String propertyName) {
		try {
			//processMobileElement(locator);
			MobileElement mobileElement = processMobileElement(locator);//(MobileElement)appiumDriverProvider.getAppiumDriver().findElement(locator);
			//System.out.println("mobile elemnt :"+mobileElement.toString());
			return mobileElement.getCssValue(propertyName);
		}catch(Exception exception){
			System.out.println("Failed to get getCssValue");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			return "";
		}
	}


	public boolean clickUsingActionClass(By locator){
		boolean flag=false;
		try {
			Actions builder=new Actions(this.appiumDriverProvider.getAppiumDriver());
			builder.moveToElement(this.processMobileElement(locator)).click().build().perform();
			flag= true;
		}
		catch (Exception exceptionNoSuchElementException) {
			System.out.println("----->>>Exception");				
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exceptionNoSuchElementException.printStackTrace();
			flag= false;
		}
		return flag;
	}
	public boolean setTextWithClear(By locator, String textToSet){
		return this.invokeActionOnMobileLocator(locator, "setTextWithClear", textToSet);
	}

	public boolean clearText(By locator) {
		return this.invokeActionOnMobileLocator(locator, "clearText");
	}
	public void sendKeys(By locator,CharSequence charKeyName) {
		this.processMobileElement(locator).sendKeys(charKeyName);
	}
	public boolean click(By locator){
		return this.invokeActionOnMobileLocator(locator, "click");
	}
	public boolean isCheckBoxSelected(By locator){
		return this.invokeActionOnMobileLocator(locator, "isCheckBoxSelected");
	}
	public boolean setText(By locator, String textToSet){
		return this.invokeActionOnMobileLocator(locator, "setText", textToSet);
	}
	public boolean mouseHover(By locator){
		return this.invokeActionOnMobileLocator(locator, "mousehover");
	}
	public boolean checkElementDisplayed(By locator){ 
		return this.invokeActionOnMobileLocator(locator, "checkElementDisplayed");
	}

	public boolean checkElementEnabled(By objLocator) {
		return this.invokeActionOnMobileLocator(objLocator, "checkElementEnabled");
	}

	public boolean checkElementNotDisplayed(By locator){
		return this.invokeActionOnMobileLocator(locator, "checkElementNotDisplayed");
	}

	public boolean selectFromCustomDropDown(By locator, String optionToSelect){
		return this.invokeActionOnMobileLocator(locator, "selectFromCustomDropDown", optionToSelect);
	}

	public boolean selectFromDropDown(By locator, String... option){
		/*StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
		for(StackTraceElement stackTraceElement : stackTraceElements)
			System.out.println("------------->" + stackTraceElement.getMethodName());
		 */
		return this.invokeActionOnMobileLocator(locator, "selectDropDown", option);
	} 

	public boolean selectCheckbox(By locator, Boolean status){
		return this.invokeActionOnMobileLocator(locator, "selectCheckbox", status);
	}

	public boolean selectRadioButton(By locator, Boolean status){
		return this.invokeActionOnMobileLocator(locator, "selectRadioButton", status);
	}

	/**
	 * Perform specific action on mobile element
	 * @author Harshvardhan Yadav (Expleo)
	 */
	public boolean invokeActionOnMobileLocator(By locator, String action){
		Actions actionBuilder;
		int intAttempts = 1;
		int maxTries = Integer.parseInt(configuration.getConfig("maxTriesForElement"));
		boolean blnResult = false;
		while(intAttempts <= maxTries){
			System.out.println("\n **** Attempt - " + intAttempts + " to perform " + action +" on element, MaxAttempts" + maxTries);
			try
			{
				MobileElement objMobileElement;
				action = action.toLowerCase();
				switch(action)
				{
				case "click":
					objMobileElement = this.processMobileElement(locator);
					objMobileElement.click();
					blnResult =  true;
					System.out.println("Performed click action on element");
					break;

				case "checkelementdisplayed":
					objMobileElement = this.processMobileElement(locator);
					if(objMobileElement == null){
						waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
						blnResult =  false;
						throw Exception;
					}
					else {
						waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
						blnResult =  true;
					}
					break;
				case "mousehover":
					objMobileElement = this.processMobileElement(locator);
					actionBuilder = new Actions(appiumDriverProvider.getAppiumDriver());
					actionBuilder.moveToElement(objMobileElement).build().perform();
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
					System.out.println("Performed mouse over action on element");
					break;
				case "ischeckboxselected":
					objMobileElement = this.processMobileElement(locator);
					if (objMobileElement.getAttribute("type").equals("checkbox"))
						blnResult = objMobileElement.isSelected();
					System.out.println("Performed verify selected action on checkbox element");
					break;
				case "checkElementEnabled":
					objMobileElement = this.processMobileElement(locator);						
					blnResult = objMobileElement.isEnabled();						
					break;
				case "clearText":
					objMobileElement = this.processMobileElement(locator);
					objMobileElement.clear();
					System.out.println("Performed clear text on element");
					break;

				case "checkelementnotdisplayed":
					objMobileElement = this.processMobileElement(locator);
					if(objMobileElement == null) {
						waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
						blnResult = true;
					}
					else{
						waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
						blnResult =  false;
						throw Exception;
					}
					break;
				}
				break;
			}catch(StaleElementReferenceException exceptionStaleElement){
				System.out.println("----->>>StaleElementReferenceException");
				intAttempts ++;
				if(intAttempts <= maxTries){
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
				}
				else{
					System.out.println("----->>>Exception");				
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exceptionStaleElement.printStackTrace();
					return false;
				}
			}
			catch(NoSuchElementException exceptionNoSuchElementException){
				System.out.println("----->>>NoSuchElementException");
				intAttempts ++;
				if(intAttempts <= maxTries){
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
				}
				else{
					System.out.println("----->>>Exception");				
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exceptionNoSuchElementException.printStackTrace();
					return false;
				}
			}
			catch(WebDriverException exceptionWebDriverException){
				System.out.println("----->>>WebDriverException");
				intAttempts ++;
				if(intAttempts <= maxTries){
					if(exceptionWebDriverException.getMessage().contains("Other element would receive the click:")){
						waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
					}
					else if(exceptionWebDriverException.getMessage().contains("Element is not clickable")) {
						waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
					}
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
				}
				else{
					System.out.println("----->>>Exception");				
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exceptionWebDriverException.printStackTrace();
					return false;
				}
			}
			catch(Exception exception){
				intAttempts ++;
				if(intAttempts <= maxTries){
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
				}
				else{
					System.out.println("----->>>Exception");				
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exception.printStackTrace();
					return false;
				}
			}
		}  
		return blnResult;
	}



	/**
	 * Perform specific action on mobile element
	 * @author Harshvardhan Yadav (Expleo)
	 */
	public boolean invokeActionOnMobileLocator(By locator, String action, String... values){
		int intAttempts = 1;
		int maxTries = Integer.parseInt(configuration.getConfig("maxTriesForElement"));
		boolean blnResult = false;
		while(intAttempts <= maxTries){
			System.out.println("\n **** Attempt - " + intAttempts + " to perform " + action +" on element, MaxAttempts" + maxTries);
			try
			{
				MobileElement objMobileElement;
				action = action.toLowerCase();
				switch(action)
				{
				case "settext":
					objMobileElement = this.processMobileElement(locator);
					objMobileElement.sendKeys(new CharSequence[]{values[0]});
					blnResult =  true;
					System.out.println("Performed set text on element");
					break;

				case "selectfromcustomdropdown":
					objMobileElement = this.processMobileElement(locator);
					By sub_locator = By.xpath(".//li/a[contains(text(),'" + values[0] + "')]");
					WebDriverWait wait = new WebDriverWait(appiumDriverProvider.getAppiumDriver(), (long) configuration.getConfigIntegerValue("driver.fluentWaitTimeout"));
					wait.until(ExpectedConditions.visibilityOfNestedElementsLocatedBy(objMobileElement, sub_locator));
					List<MobileElement> objCustomOptions = objMobileElement.findElements(sub_locator); 
					for(WebElement weOption : objCustomOptions){  
						if (weOption.getText().trim().equals(values[0])){
							weOption.click();
							waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
							System.out.println("Selected dropdown option for element");
							blnResult =  true;
							break;
						}
					} 		
					break;	

				case "selectdropdown":
					objMobileElement = this.processMobileElement(locator);
					Select sltDropDown = new Select(objMobileElement);

					if(values.length > 1 && !values[1].equals(""))
					{
						if(values[1].equalsIgnoreCase("Value"))
							sltDropDown.selectByValue(values[0]);
						else if(values[1].equalsIgnoreCase("Text"))
							sltDropDown.selectByVisibleText(values[0]);
						else if(values[1].equalsIgnoreCase("Index"))
							sltDropDown.selectByIndex(Integer.parseInt(values[0]));

						waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
						System.out.println("Selected dropdown options for element");
						blnResult =  true;
					}
					else
					{
						// Web elements from dropdown list 
						List<WebElement> objOptions = sltDropDown.getOptions();
						boolean bOptionAvailable = false;
						int iIndex = 0;
						for(WebElement weOptions : objOptions)  
						{  
							if (weOptions.getText().trim().equalsIgnoreCase(values[0]))
							{
								sltDropDown.selectByIndex(iIndex);
								waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
								bOptionAvailable = true;
								System.out.println("Selected dropdown options for element");
								break;
							}
							else
								iIndex++;
						}
						return bOptionAvailable;
					}
					break;
				case "settextwithclear":
					objMobileElement = this.processMobileElement(locator);
					//objMobileElement.clear();
					objMobileElement.click();
					String s = objMobileElement.getAttribute("value");
					if(s.length() > 0)
					{
						for(int i=0;i<s.length();i++)
						{
							objMobileElement.sendKeys(Keys.BACK_SPACE);
						}
					}
					//objWebElement.sendKeys(Keys.chord(Keys.CONTROL,"a", Keys.DELETE));
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);			
					objMobileElement.sendKeys(new CharSequence[]{values[0]});
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
					blnResult =  true;
					System.out.println("Performed clear with set text action on element");
					break;
				}
				break;
			}catch(StaleElementReferenceException exceptionStaleElement){
				System.out.println("----->>>StaleElementReferenceException");
				intAttempts ++;
				if(intAttempts <= maxTries){
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
				}
				else{
					System.out.println("----->>>Exception");				
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exceptionStaleElement.printStackTrace();
					return false;
				}
			}
			catch(NoSuchElementException exceptionNoSuchElementException){
				System.out.println("----->>>NoSuchElementException");
				intAttempts ++;
				if(intAttempts <= maxTries){
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
				}
				else{
					System.out.println("----->>>Exception");				
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exceptionNoSuchElementException.printStackTrace();
					return false;
				}
			}
			catch(WebDriverException exceptionWebDriverException){
				System.out.println("----->>>WebDriverException");
				intAttempts ++;
				if(intAttempts <= maxTries){
					if(exceptionWebDriverException.getMessage().contains("Other element would receive the click:")){
						waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
					}
					else if(exceptionWebDriverException.getMessage().contains("Element is not clickable")) {
						waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
					}
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
				}
				else{
					System.out.println("----->>>Exception");				
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exceptionWebDriverException.printStackTrace();
					return false;
				}
			}
			catch(Exception exception){
				intAttempts ++;
				if(intAttempts <= maxTries){
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
				}
				else{
					System.out.println("----->>>Exception");				
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exception.printStackTrace();
					return false;
				}
			}
		}  
		return blnResult;
	}

	/**
	 * Perform specific action on mobile element
	 * @author Harshvardhan Yadav (Expleo)
	 */
	public boolean invokeActionOnMobileLocator(By locator, String action, boolean... values){
		int intAttempts = 1;
		int maxTries = Integer.parseInt(configuration.getConfig("maxTriesForElement"));
		boolean blnResult = false;
		while(intAttempts <= maxTries){
			System.out.println("\n **** Attempt - " + intAttempts + " to perform " + action +" on element, MaxAttempts" + maxTries);
			try
			{
				MobileElement objMobileElement;
				action = action.toLowerCase();
				switch(action)
				{
				case "selectcheckbox":
					objMobileElement = this.processMobileElement(locator);
					if (objMobileElement.getAttribute("type").equals("checkbox")) {
						if ((objMobileElement.isSelected() && !values[0]) || (!objMobileElement.isSelected() && values[0]))
							objMobileElement.click();
						blnResult = true;
					} else
						blnResult = false;
					System.out.println("Performed click action on checkbox element");
					break;

				case "selectradiobutton":
					objMobileElement = this.processMobileElement(locator);
					if (objMobileElement.getAttribute("type").equals("radio")) {
						if ((objMobileElement.isSelected() && !values[0]) || (!objMobileElement.isSelected() && values[0]))
							objMobileElement.click();
						blnResult = true;
					} else
						blnResult = false;
					System.out.println("Performed click action on radio element");
					break;
				}
				break;
			}catch(StaleElementReferenceException exceptionStaleElement){
				System.out.println("----->>>StaleElementReferenceException");
				intAttempts ++;
				if(intAttempts <= maxTries){
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
				}
				else{
					System.out.println("----->>>Exception");				
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exceptionStaleElement.printStackTrace();
					return false;
				}
			}
			catch(NoSuchElementException exceptionNoSuchElementException){
				System.out.println("----->>>NoSuchElementException");
				intAttempts ++;
				if(intAttempts <= maxTries){
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
				}
				else{
					System.out.println("----->>>Exception");				
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exceptionNoSuchElementException.printStackTrace();
					return false;
				}
			}
			catch(WebDriverException exceptionWebDriverException){
				System.out.println("----->>>WebDriverException");
				intAttempts ++;
				if(intAttempts <= maxTries){
					if(exceptionWebDriverException.getMessage().contains("Other element would receive the click:")){
						waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
					}
					else if(exceptionWebDriverException.getMessage().contains("Element is not clickable")) {
						waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
					}
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
				}
				else{
					System.out.println("----->>>Exception");				
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exceptionWebDriverException.printStackTrace();
					return false;
				}
			}
			catch(Exception exception){
				intAttempts ++;
				if(intAttempts <= maxTries){
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
				}
				else{
					System.out.println("----->>>Exception");				
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exception.printStackTrace();
					return false;
				}
			}
		}  
		return blnResult;
	}

	public String getAttribute(By locator, String strategy){
		return this.getFromMobileLocator(locator, "getAttribute", strategy);
	}

	public String getText(By locator){
		return this.getFromMobileLocator(locator, "getText");
	}

	/**
	 * Perform specific action on mobile element
	 * @author Harshvardhan Yadav (Expleo)
	 */
	public String getFromMobileLocator(By locator, String action, String... strategy){
		int intAttempts = 1;
		int maxTries = Integer.parseInt(configuration.getConfig("maxTriesForElement"));
		String returnVal = "";
		while(intAttempts <= maxTries){
			System.out.println("Attempt - " + intAttempts + " to " + action +" on element, MaxAttempts" + maxTries);
			try{
				MobileElement objMobileElement;
				action = action.toLowerCase();
				switch(action)
				{
				case "getattribute":
					objMobileElement = this.processMobileElement(locator);
					returnVal = objMobileElement.getAttribute(strategy[0]);
					System.out.println("Performed get attribute action on element");
					break;

				case "gettext":
					objMobileElement = this.processMobileElement(locator);
					returnVal = objMobileElement.getText();
					System.out.println("Performed get text action on element");
					break;
				}
				break;
			}catch(StaleElementReferenceException exceptionStaleElement){
				System.out.println("----->>>StaleElementReferenceException");
				intAttempts ++;
				if(intAttempts <= maxTries){
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
				}
				else{
					System.out.println("----->>>Exception");				
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exceptionStaleElement.printStackTrace();
					return null;
				}
			}
			catch(NoSuchElementException exceptionNoSuchElementException){
				System.out.println("----->>>NoSuchElementException");
				intAttempts ++;
				if(intAttempts <= maxTries){
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
				}
				else{
					System.out.println("----->>>Exception");				
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exceptionNoSuchElementException.printStackTrace();
					return null;
				}
			}
			catch(WebDriverException exceptionWebDriverException){
				System.out.println("----->>>WebDriverException");
				intAttempts ++;
				if(intAttempts <= maxTries){
					if(exceptionWebDriverException.getMessage().contains("Other element would receive the click:")){
						waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
					}
					else if(exceptionWebDriverException.getMessage().contains("Element is not clickable")) {
						waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
					}
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
				}
				else{
					System.out.println("----->>>Exception");				
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exceptionWebDriverException.printStackTrace();
					return null;
				}
			}
			catch(Exception exception){
				intAttempts ++;
				if(intAttempts <= maxTries){
					waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
				}
				else{
					System.out.println("----->>>Exception");				
					if(configuration.getConfigBooleanValue("printStackTraceForActions"))
						exception.printStackTrace();
					return null;
				}
			}
		}  
		return returnVal;
	}

	/**
	 * Process mobile element with all defined synchronization 
	 * @return processed mobile element
	 * @author Harshvardhan Yadav (Expleo)
	 */
	public MobileElement processMobileElement(final By locator)
	{
		System.out.println("Appium processing mobile element");
		try{
			Wait<WebDriver> wait = new FluentWait<WebDriver>(appiumDriverProvider.getAppiumDriver())
					.withTimeout(Duration.ofSeconds(configuration.getConfigIntegerValue("driver.fluentWaitTimeout")))
					.pollingEvery(Duration.ofSeconds(configuration.getConfigIntegerValue("driver.fluentWaitPullingTimeout")))
					.ignoring(NoSuchElementException.class)
					.ignoring(InvalidElementStateException.class)
					.ignoring(StaleElementReferenceException.class)
					.ignoring(WebDriverException.class);

			MobileElement mobileElement = wait.until(new Function<WebDriver, MobileElement>(){
				public MobileElement apply(WebDriver driver){
					return (MobileElement) appiumDriverProvider.getAppiumDriver().findElement(locator);
				}
			}); 

			WebDriverWait waitExplicit = new WebDriverWait(appiumDriverProvider.getAppiumDriver(), Explicit_WAIT_TIMEOUT);

			WebElement webElement = waitExplicit.until(new Function<WebDriver, WebElement>() {
				public WebElement apply(WebDriver driver) {
					return appiumDriverProvider.getAppiumDriver().findElement(locator);
				}
			});


			return (MobileElement)webElement;

		}catch(Exception exception){
			System.out.println("---> Process processMobileElement --->>Exception -- appium driver unable to find element");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			return null;
		} 
	}

	/**
	 * Process mobile element with all defined synchronization 
	 * @return processed mobile element list
	 * @author Harshvardhan Yadav (Expleo)
	 */
	public List<MobileElement> processMobileElements(final By locator)
	{
		System.out.println("Appium processing mobile element");
		try{
			Wait<WebDriver> wait = new FluentWait<WebDriver>(appiumDriverProvider.getAppiumDriver())
					.withTimeout(Duration.ofSeconds(configuration.getConfigIntegerValue("driver.fluentWaitTimeout")))
					.pollingEvery(Duration.ofSeconds(configuration.getConfigIntegerValue("driver.fluentWaitPullingTimeout")))
					.ignoring(NoSuchElementException.class)
					.ignoring(InvalidElementStateException.class)
					.ignoring(StaleElementReferenceException.class);

			List<MobileElement> mobileElements = wait.until(new Function<WebDriver,  List<MobileElement>>(){
				@SuppressWarnings("unchecked")
				public List<MobileElement> apply(WebDriver driver){
					return (List<MobileElement>) appiumDriverProvider.getAppiumDriver().findElements(locator);
				}
			}); 

			//WebDriverWait webDriverWait = new WebDriverWait(appiumDriverProvider.getAppiumDriver(), configuration.getConfigIntegerValue("driver.fluentWaitTimeout"));
			//webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(locator));

			return mobileElements;
		}catch(Exception exception){
			System.out.println("---> Process Elelement --->>Exception");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			return null;
		} 
	}

	public boolean checkElementDisplayedWithMinWait(By locator){
		try{
			WebDriverWait wait = new WebDriverWait(appiumDriverProvider.getAppiumDriver(), (long)FLUENTWAIT_WAIT_MIN_TIMEOUT);
			wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
			appiumDriverProvider.getAppiumDriver().findElement(locator);
			return true;
		}catch(Exception exception){
			System.out.println("---> checkMobileElementDisplayedWithMinWait --->>Exception");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			return false;
		}
	}

	public boolean checkElementDisplayedWithMidWait(By locator){
		try{
			WebDriverWait wait = new WebDriverWait(appiumDriverProvider.getAppiumDriver(), (long)FLUENTWAIT_WAIT_MID_TIMEOUT);
			wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
			appiumDriverProvider.getAppiumDriver().findElement(locator);
			return true;
		}catch(Exception exception){
			System.out.println("---> checkMobileElementDisplayedWithMidWait --->>Exception");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			return false;
		}
	}

	/** android scroll to required text using UIAutomator2 
	 * @author Harshvardhan Yadav (Expleo)
	 */
	public boolean androidScrollToRequiredText(String textToSelect){
		try
		{
			waitMethods.sleep(configuration.getConfigIntegerValue("minwait"));
			appiumDriverProvider.getAppiumDriver().findElement(MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView("
					+ "new UiSelector().text(\""+textToSelect+"\").instance(0));")); 
			waitMethods.sleep(configuration.getConfigIntegerValue("minwait"));
			return true;
		}catch(Exception exception){
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			return false;
		} 
	}

	/** android scroll to child element using UIAutomator2 
	 * @author Harshvardhan Yadav (Expleo)
	 */
	public boolean androidScrollToGetChildByText(String scrollableContainerResourceID, String childIdentifier, String actualTextToSelect){
		try
		{ 	
			waitMethods.sleep(configuration.getConfigIntegerValue("minwait"));
			appiumDriverProvider.getAppiumDriver().findElement(MobileBy.AndroidUIAutomator(
					"new UiScrollable(new UiSelector().resourceId(\""+scrollableContainerResourceID+"\")).getChildByText("
							+ "new UiSelector().className(\""+childIdentifier+"\"), \""+actualTextToSelect+"\")"));
			waitMethods.sleep(configuration.getConfigIntegerValue("minwait"));
			return true;
		}catch(Exception exception){
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			return false;
		} 
	}

	/** android scroll to required text contains using UIAutomator2 
	 * @author Harshvardhan Yadav (Expleo)
	 */
	public boolean androidScrollToRequiredTextContains(String textToSelect){
		try
		{
			waitMethods.sleep(configuration.getConfigIntegerValue("minwait"));
			appiumDriverProvider.getAppiumDriver().findElement(MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView("
					+ "new UiSelector().textContains(\""+textToSelect+"\").instance(0));")); 
			waitMethods.sleep(configuration.getConfigIntegerValue("minwait"));
			return true;
		}catch(Exception exception){
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			return false;
		} 
	}

	/** android scroll using source mobile element to destination mobile element UIAutomator2 
	 * @author Harshvardhan Yadav (Expleo)
	 */
	public boolean scrollUsingTouchActions_ByElements(MobileElement startElement, MobileElement endElement) {
		try
		{
			@SuppressWarnings("rawtypes")
			TouchAction actions = new TouchAction(appiumDriverProvider.getAppiumDriver());
			//actions.press(startElement).waitAction(Duration.ofSeconds(2)).moveTo(endElement).release().perform();
			actions.longPress(new LongPressOptions().withElement(ElementOption.element(startElement))).moveTo(ElementOption.element(endElement)).release().perform();
			return true;
		}catch(Exception exception){
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			return false;
		} 
	}

	public boolean selectFromFixedRadioSpinner(By spinner, String spinnerElementTextToSelect) {
		try
		{
			this.click(spinner);
			this.click(By.xpath("//android.widget.ListView/android.widget.CheckedTextView[@text='" + spinnerElementTextToSelect + "']"));
			return true;
		}catch(Exception exception) {
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			return false;
		}
	}

	public boolean selectFromScrollableRadioSpinner(By spinner, String spinnerElementTextToSelect) {
		try
		{
			this.click(spinner);
			waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
			boolean requiredRadioButton = false;
			do{
				if(this.processMobileElement(By.xpath(".//android.widget.ListView")) != null) {
					List<MobileElement> listView = this.processMobileElements(By.xpath(".//android.widget.ListView/android.widget.CheckedTextView"));
					for(MobileElement mobileElement : listView) {
						if(mobileElement.getText().equals(spinnerElementTextToSelect)) {
							mobileElement.click();
							requiredRadioButton = true;
							break;
						}
					}
					if(!requiredRadioButton) {
						this.scrollUsingTouchActions_ByElements(listView.get(listView.size()-1), listView.get(0));
					}
				}
			}while(!requiredRadioButton);
			return true;
		}catch(Exception exception) {
			System.out.println("Unable to select element from scrollable spinner");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			return false;
		}
	}

	public boolean webPageRefresh() {
		try
		{
			appiumDriverProvider.getAppiumDriver().navigate().refresh();
			return true;
		}catch(Exception exception) {
			System.out.println("Unable to select element from scrollable spinner");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			return false;
		}
	}

	/**
	 *  switch to the Frame by Frame name
	 *  @param : locator - The most common one. You locate your iframe like other
	 *        		elements, then pass it into the method
	 *       		eg.driver.switchTo().frame(driver.findElement(By.xpath(".//iframe[@title='Test']")))
	 *  @author Harshvardhan Yadav (Expleo)
	 */
	public void switchToFrameUsingIframe_Element(By locator) {
		try {
			appiumDriverProvider.getAppiumDriver().switchTo().defaultContent();
			appiumDriverProvider.getAppiumDriver().switchTo().frame(this.processMobileElement(locator));
			System.out.println("Switched to frame");
		} catch (Exception exception) {
			System.out.println("unable to switch to frame");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
		}
	}

	/**
	 * switch to the Frame by Frame name or Id
	 * @param : frameName - Name/Id of frame you want to switch
	 * @author Harshvardhan Yadav (Expleo)
	 */
	public void switchToFrameUsingNameOrId(String frameNameOrID) {
		try {
			appiumDriverProvider.getAppiumDriver().switchTo().defaultContent();
			appiumDriverProvider.getAppiumDriver().switchTo().frame(frameNameOrID);
			System.out.println("Switched to frame");
		} catch (Exception exception) {
			System.out.println("unable to switch to frame");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
		}
	}

	/**
	 * switch to the Frame by index
	 * @param : index - Index on page
	 * @author Harshvardhan Yadav (Expleo)
	 */
	public void switchToFrameUsingIndex(int index) {
		try {
			appiumDriverProvider.getAppiumDriver().switchTo().defaultContent();
			appiumDriverProvider.getAppiumDriver().switchTo().frame(index);
			System.out.println("Switched to frame");
		} catch (Exception exception) {
			System.out.println("unable to switch to frame");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
		}
	}

	/**
	 *	switch to the default content
	 * 	@author Harshvardhan Yadav (Expleo)
	 */
	public void switchToDefaultContent() {
		try {
			appiumDriverProvider.getAppiumDriver().switchTo().defaultContent();
			System.out.println("Switched to default content");
		} catch (Exception exception) {
			System.out.println("unable to switch to default content");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
		}
	}

	/**
	 * Return current window handle
	 * @return current window handle
	 * @author Harshvardhan Yadav (Expleo)
	 */
	public String getCurrentWindowHandle() {
		try {
			return appiumDriverProvider.getAppiumDriver().getWindowHandle();
		}catch(Exception exception){
			System.out.println("Unable to get main window handel");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			return null;
		} 
	}

	/**
	 * switchTo child Window
	 * @author Harshvardhan Yadav (Expleo)
	 **/
	public void switchToChildWindow() 
	{	
		try
		{
			waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
			String parentHandle = this.getCurrentWindowHandle();
			Set<String> windowNames = appiumDriverProvider.getAppiumDriver().getWindowHandles();

			@SuppressWarnings("rawtypes")
			Iterator ite = windowNames.iterator();

			while (ite.hasNext()) 
			{
				String popupHandle = ite.next().toString();
				if (!popupHandle.equals(parentHandle)) {
					appiumDriverProvider.getAppiumDriver().switchTo().window(popupHandle);
					break;
				}
			}
		}catch(Exception exception){
			System.out.println("Unable to siwtch to child window");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
		} 
	}

	/**
	 * switchTo child Window using window handle
	 * @author Harshvardhan Yadav (Expleo)
	 **/
	public void switchToWindowUsingHandle(String windowHandle) 
	{	
		try
		{
			waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
			Set<String> windowNames = appiumDriverProvider.getAppiumDriver().getWindowHandles();

			@SuppressWarnings("rawtypes")
			Iterator ite = windowNames.iterator();

			while (ite.hasNext()) 
			{
				String popupHandle = ite.next().toString();
				if (popupHandle.equals(windowHandle)) {
					appiumDriverProvider.getAppiumDriver().switchTo().window(popupHandle);
					break;
				}
			}
		}catch(Exception exception){
			System.out.println("Unable to siwtch to window using handle");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
		} 
	}

	/**
	 * switch to window using the given title
	 * @param : locator - Window title
	 * @author Harshvardhan Yadav (Expleo)
	 */
	public boolean switchToWindowUsingTitle(String windowTitle) {
		try {
			waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
			String mainWindowHandle = appiumDriverProvider.getAppiumDriver().getWindowHandle();
			Set<String> openWindows = appiumDriverProvider.getAppiumDriver().getWindowHandles();

			if (!openWindows.isEmpty()) {
				for (String windows : openWindows) {
					String window = appiumDriverProvider.getAppiumDriver().switchTo().window(windows).getTitle();
					if (windowTitle.equals(window)) {
						return true;
					}
					else
						appiumDriverProvider.getAppiumDriver().switchTo().window(mainWindowHandle);
				}
			}
			return false;
		} catch (Exception exception) {
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			return false;
		}
	}

	/**
	 * Execute javascript
	 * @author Harshvardhan Yadav (Expleo)
	 */
	public void executeJavascript(String javascript) {
		JavascriptExecutor js = (JavascriptExecutor) appiumDriverProvider.getAppiumDriver();
		js.executeScript(javascript); 
	}

	/**
	 * use to scroll device screen to perticular element
	 * @author Dheerendra Pal (Expleo)
	 */
	public boolean androidScrollToElement(By locator) {
		try {
			WebElement objWebElement = this.processMobileElement(locator);
			((JavascriptExecutor) appiumDriverProvider.getAppiumDriver())
			.executeScript("arguments[0].scrollIntoView();", objWebElement);
			return true;}
		catch(Exception exception){
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			return false;
		} 
	}

	public void clickByTap(By locator) {
		WebElement element = this.processMobileElement(locator);		
		TouchAction action = new TouchAction(appiumDriverProvider.getAppiumDriver());

	}

	/**
	 * switchTo parent Window
	 * @author Harshvardhan Yadav (Expleo)
	 **/
	public void switchToParentWindow() 
	{	
		try
		{
			waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
			String popupHandle = this.getCurrentWindowHandle();
			Set<String> windowNames = appiumDriverProvider.getAppiumDriver().getWindowHandles();

			@SuppressWarnings("rawtypes")
			Iterator ite = windowNames.iterator();

			while (ite.hasNext()) 
			{
				String parentHandle = ite.next().toString();
				if (!popupHandle.equals(parentHandle)) {
					appiumDriverProvider.getAppiumDriver().close();
					appiumDriverProvider.getAppiumDriver().switchTo().window(parentHandle);
					System.out.println("Switched to Parent window . . . . . .");
					break;
				}
			}
		}catch(Exception exception){
			System.out.println("Unable to siwtch to child window");
			if(configuration.getConfig("printStackTraceForActions").equalsIgnoreCase("true"))
				exception.printStackTrace();
		} 
	}

	public boolean pageRefresh() {
		try
		{
			appiumDriverProvider.getAppiumDriver().navigate().refresh();
			return true;
		}catch(Exception exception) {
			System.out.println("Unable to select element from scrollable spinner");
			if(configuration.getConfig("printStackTraceForActions").equalsIgnoreCase("true"))
				exception.printStackTrace();
			return false;
		}
	}

	/**
	 * click using JavaScript Executor
	 * @author Dheerendra Pal (Expleo)
	 * @return 
	 */
	public boolean clickUsingJS(By locator) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) appiumDriverProvider.getAppiumDriver();
			System.out.println("Clicking through JS on element: " + locator);
			js.executeScript("arguments[0].click();", appiumDriverProvider.getAppiumDriver().findElement(locator));
			return true;
		} catch (Exception exception) {			
			System.out.println("Unable to click using javascript");
			if(configuration.getConfig("printStackTraceForActions").equalsIgnoreCase("true"))
				exception.printStackTrace();
			return false;
		} 
	}

	/**
	 * checkElementExists - this returns element is exist or not in DOM
	 * not has relevance for displayed and not displayed.
	 * @author Dayanand Dhange (Expleo)
	 * @return 
	 */
	public boolean checkElementExists(By locator) {
		MobileElement objMobileElement =null;
		boolean flag=false;
		try {
			objMobileElement = (MobileElement) appiumDriverProvider.getAppiumDriver().findElement(locator);
			if (objMobileElement!=null)
				flag=true;
		}
		catch(NoSuchElementException exceptionNoSuchElementException){
			System.out.println("----->>>NoSuchElementException");
			flag=false;
		}
		return flag;
	}

	public boolean pressKeybordKeys(By locator, String key){
		try{
			WebElement objWebElement = this.processMobileElement(locator);
			if(key.toLowerCase().equals("tab"))
				objWebElement.sendKeys(Keys.TAB);
			if(key.toLowerCase().equals("enter"))
				objWebElement.sendKeys(Keys.ENTER);
			return true;
		}catch(Exception exception){
			System.out.println("---> pressKeybordKeys --->>Exception");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			return false;
		}
	}

	public boolean navigateToInvalidUrl() {
		try {
			String regex = "[a-zA-Z]{6}";
			String randomtext = "/" + new Generex(regex).random();
			String currentUrl = appiumDriverProvider.getAppiumDriver().getCurrentUrl();
			String newUrl = currentUrl + randomtext;
			System.out.println("*********** newurl  "+newUrl);
			appiumDriverProvider.getAppiumDriver().navigate().to(newUrl);
			return true;
		}catch(Exception exception) {
			System.out.println("Failed to navigate");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			return false;
		}

	}
	/**
	 * Get the url 
	 *  @author Harshvardhan Yadav (Expleo)
	 */
	public String getUrl() {
		try {
			waitMethods.sleep(DEFAULT_SLEEP_TIMEOUT);
			return appiumDriverProvider.getAppiumDriver().getCurrentUrl();
		}catch(Exception exception){
			System.out.println("Failed to get current URL");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			return "";
		}

	}
	/**
	 * @author Harshvardhan Yadav (Expleo)
	 */
	public void setBrowserWindowSize(String sWidth, String sHeight) {
		try {
			int iWidth = Integer.parseInt(sWidth);
			int iHeight = Integer.parseInt(sHeight);
			Dimension dimension = new Dimension(iWidth, iHeight);
			appiumDriverProvider.getAppiumDriver().manage().window().setSize(dimension);
			System.out.println("Browser window set to the dimension: " + dimension );
		} catch (Exception exception) {
			System.out.println("Unable to ser Browser window to the dimension: ");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
		}
	}
	/**
	 * Get the size of the element
	 *  @author Harshvardhan Yadav (Expleo)
	 */
	public Dimension getSize(By locator) {

		try {
			this.processMobileElement(locator);
			WebElement webElement = appiumDriverProvider.getAppiumDriver().findElement(locator);
			return webElement.getSize();
		}catch(Exception exception){
			System.out.println("Failed to element size");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			return null;
		}

	}
	/**
	 * Get the Height of the element
	 * @author Harshvardhan Yadav (Expleo)
	 */
	public int getHeight(By locator) {
		try {
			return this.getSize(locator).getHeight();
		}catch(Exception exception){
			System.out.println("Failed to element height");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			return 0;
		}
	}

	public int getWidth(By locator) {
		try {
			return this.getSize(locator).getWidth();
		}catch(Exception exception){
			System.out.println("Failed to element height");
			if(configuration.getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			return 0;
		}

	}
	public boolean verifyAlertIsDisplayedOrNot()
	{
		try{
			WebDriverWait wait = new WebDriverWait(appiumDriverProvider.getAppiumDriver(), (long)FLUENTWAIT_WAIT_MIN_TIMEOUT);
			wait.until(ExpectedConditions.alertIsPresent());
			return true;
		}catch(Exception exception){
			System.out.println("---> checkMobileElementDisplayedWithMinWait --->>Exception");
			if(configuration.getConfig("printStackTraceForActions").equalsIgnoreCase("true"))
				exception.printStackTrace();
			return false;
		}
	}
	public void acceptAlertiOS()
	{
		if(this.verifyAlertIsDisplayedOrNot())
		{	
			Alert alert = appiumDriverProvider.getAppiumDriver().switchTo().alert();		
			String alertMessage= alert.getText();		        		
			// Displaying alert message		
			System.out.println(alertMessage);	
			waitMethods.sleep(6);
			// Accepting alert		
			alert.accept();		}
	}
}