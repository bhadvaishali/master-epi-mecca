/**
 * 
 */
package com.generic.utils;

/**
 * @author Harhvardhan Yadav, Namrata Donikar (Expleo)
 *
 */

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Writer;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import net.masterthought.cucumber.ReportParser;
import net.masterthought.cucumber.json.Element;
import net.masterthought.cucumber.json.Feature;
import net.masterthought.cucumber.json.Step;
//import net.masterthought.cucumber.json.support.Status;
//#import net.masterthought.cucumber.json.support.TagObject;
import net.masterthought.cucumber.util.Util;
import net.masterthought.cucumber.json.Tag;



//import net.masterthought.cucumber.ArtifactProcessor;
//import net.masterthought.cucumber.ConfigurationOptions;
//import net.masterthought.cucumber.ReportParser;
//import net.masterthought.cucumber.ScenarioTag;
//import net.masterthought.cucumber.TagObject;
//import net.masterthought.cucumber.charts.FlashChartBuilder;
//import net.masterthought.cucumber.charts.JsChartUtil;
//import net.masterthought.cucumber.json.Artifact;
//import net.masterthought.cucumber.json.Element;
//import net.masterthought.cucumber.json.Feature;
//import net.masterthought.cucumber.json.Step;
//import net.masterthought.cucumber.util.UnzipUtils;
//import net.masterthought.cucumber.util.Util;

class ReportInformation
{
/*
	private Map<String, List<Feature>> projectFeatureMap;
	private List<Feature> features;
	private int numberOfScenarios;
	private int numberOfSteps;
	private List<Step> totalPassingSteps = new ArrayList<Step>();
	private List<Step> totalFailingSteps = new ArrayList<Step>();
	private List<Step> totalSkippedSteps = new ArrayList<Step>();
	private List<Step> totalUndefinedSteps = new ArrayList<Step>();
	private List<Step> totalMissingSteps = new ArrayList<Step>();
	List<Element> numberPassingScenarios = new ArrayList<Element>();
	List<Element> numberFailingScenarios = new ArrayList<Element>();
	private Long totalDuration = 0l;
	List<TagObject> tagMap = new ArrayList<TagObject>();
	private int totalTagScenarios = 0;
	private int totalTagSteps = 0;
	private int totalTagPasses = 0;
	private int totalTagFails = 0;
	private int totalTagSkipped = 0;
	private int totalTagPending = 0;
	private long totalTagDuration = 0l;
	private int totalPassingTagScenarios = 0;
	private int totalFailingTagScenarios = 0;

	public ReportInformation(Map<String, List<Feature>> projectFeatureMap)
	{
		this.projectFeatureMap = projectFeatureMap;
		this.features = listAllFeatures();
		processFeatures();
	}

	private List<Feature> listAllFeatures()
	{
		List<Feature> allFeatures = new ArrayList<Feature>();
		Iterator it = projectFeatureMap.entrySet().iterator();
		while (it.hasNext())
		{
			Map.Entry pairs = (Map.Entry) it.next();
			List<Feature> featureList = (List<Feature>) pairs.getValue();
			allFeatures.addAll(featureList);
		}
		return allFeatures;
	}

	public List<Feature> getFeatures()
	{
		return this.features;
	}

	public List<TagObject> getTags()
	{
		return this.tagMap;
	}

	public Map<String, List<Feature>> getProjectFeatureMap()
	{
		return this.projectFeatureMap;
	}

	public int getTotalNumberOfScenarios()
	{
		return numberOfScenarios;
	}

	public int getTotalNumberOfFeatures()
	{
		return features.size();
	}

	public int getTotalNumberOfSteps()
	{
		return numberOfSteps;
	}

	public int getTotalNumberPassingSteps()
	{
		return totalPassingSteps.size();
	}

	public int getTotalNumberFailingSteps()
	{
		return totalFailingSteps.size();
	}

	public int getTotalNumberSkippedSteps()
	{
		return totalSkippedSteps.size();
	}

	public int getTotalNumberPendingSteps()
	{
		return totalUndefinedSteps.size();
	}

	public int getTotalNumberMissingSteps()
	{
		return totalMissingSteps.size();
	}

	public String getTotalDurationAsString()
	{
		return Util.formatDuration(totalDuration);
	}

	public Long getTotalDuration()
	{
		return totalDuration;
	}

	public String timeStamp()
	{
		return new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date());
	}

	public String getReportStatusColour(Feature feature)
	{
		return feature.getStatus() == Status.PASSED ? "#C5D88A"
				: "#D88A8A";
	}

	public String getTagReportStatusColour(TagObject tag)
	{
		return tag.getStatus() == Status.PASSED ? "#C5D88A" : "#D88A8A";
	}

	public int getTotalTags()
	{
		return tagMap.size();
	}

	public int getTotalTagScenarios()
	{
		return totalTagScenarios;
	}

	public int getTotalPassingTagScenarios()
	{
		return totalPassingTagScenarios;
	}

	public int getTotalFailingTagScenarios()
	{
		return totalFailingTagScenarios;
	}

	public int getTotalTagSteps()
	{
		return totalTagSteps;
	}

	public int getTotalTagPasses()
	{
		return totalTagPasses;
	}

	public int getTotalTagFails()
	{
		return totalTagFails;
	}

	public int getTotalTagSkipped()
	{
		return totalTagSkipped;
	}

	public int getTotalTagPending()
	{
		return totalTagPending;
	}

	public String getTotalTagDuration()
	{
		return Util.formatDuration(totalTagDuration);
	}

	public int getTotalScenariosPassed()
	{
		return numberPassingScenarios.size();
	}

	public int getTotalScenariosFailed()
	{
		return numberFailingScenarios.size();
	}

	private void processTags()
	{
		for (TagObject tag : tagMap)
		{
			totalTagScenarios = calculateTotalTagScenarios(tag);
			totalPassingTagScenarios = calculateTotalTagScenariosForStatus(
					totalPassingTagScenarios, tag, Status.PASSED);
			totalFailingTagScenarios = calculateTotalTagScenariosForStatus(
					totalFailingTagScenarios, tag, Status.FAILED);
			totalTagPasses += tag.getPassedSteps();
			totalTagFails += tag.getFailedSteps();
			totalTagSkipped += tag.getSkippedSteps();
			totalTagPending += tag.getPendingSteps();

			for (Element scenarioTag : tag.getElements())
			{

				if (Objects.nonNull(scenarioTag.getSteps()) && scenarioTag.getSteps().length > 0)
				{
					Step[] steps = scenarioTag.getSteps();
					List<Step> stepList = new ArrayList<Step>();
					for (Step step : steps)
					{
						stepList.add(step);
						totalTagDuration = totalTagDuration
								+ step.getDuration();
					}
					totalTagSteps += stepList.size();
				}
			}
		}
	}

	private int calculateTotalTagScenariosForStatus(int totalScenarios,
			TagObject tag, Status status)
	{
		List<Element> scenarioTagList = new ArrayList<Element>();
		for (Element scenarioTag : tag.getElements())
		{
			if (!scenarioTag.getKeyword().equals("Background"))
			{
				if (scenarioTag.getStatus().equals(status))
				{
					scenarioTagList.add(scenarioTag);
				}
			}
		}
		return totalScenarios + scenarioTagList.size();
	}

	private int calculateTotalTagScenarios(TagObject tag)
	{
		List<Element> scenarioTagList = new ArrayList<Element>();
		for (Element scenarioTag : tag.getElements())
		{
			if (!scenarioTag.getKeyword().equals("Background"))
			{
				scenarioTagList.add(scenarioTag);
			}
		}
		return totalTagScenarios + scenarioTagList.size();
	}

	private void processFeatures()
	{
		for (Feature feature : features)
		{
			List<Element> scenarioList = new ArrayList<Element>();
			Element[] scenarios = feature.getElements();
			if (scenarios.length !=0) 
			{
				numberOfScenarios = getNumberOfScenarios(scenarios);
				for (Element scenario : scenarios)
				{
					String scenarioName = scenario.getName();

					if (!scenario.getKeyword().equals("Background"))
					{
						if (scenario.getStatus()==Status.PASSED) {
							numberPassingScenarios.add(scenario);
						}
						if (scenario.getStatus()==Status.FAILED) {
							numberFailingScenarios.add(scenario);
						}
					}
					// process tags
					if (Objects.nonNull(feature.getTags()) && feature.getTags().length > 0)
					{
						scenarioList.add(new Element(scenario, feature
								.getReportFileName()));
						tagMap = createOrAppendToTagMap(tagMap,
								feature.getTagList(), scenarioList);
					}

					if (Util.hasScenarios(feature))
					{
						if (scenario.hasTags())
						{
							scenarioList = addScenarioUnlessExists(
									scenarioList, new Element(scenario,
											feature.getFileName()));
						}
						tagMap = createOrAppendToTagMap(tagMap,
								scenario.getTagList(), scenarioList);
					}

					if (Util.hasSteps(scenario))
					{
						Sequence<Step> steps = scenario.getSteps();
						numberOfSteps = numberOfSteps + steps.size();
						for (Step step : steps)
						{
							String stepName = step.getRawName();

							// apply artifacts
							if (ConfigurationOptions.artifactsEnabled())
							{
								Map<String, Artifact> map = ConfigurationOptions
										.artifactConfig();
								String mapKey = scenarioName + stepName;
								if (map.containsKey(mapKey))
								{
									Artifact artifact = map.get(mapKey);
									String keyword = artifact.getKeyword();
									String contentType = artifact
											.getContentType();
									step.setName(stepName.replaceFirst(
											keyword,
											getArtifactFile(mapKey, keyword,
													artifact.getArtifactFile(),
													contentType)));
								}
							}

							Status stepStatus = step.getStatus();
							totalPassingSteps = Util.setStepStatus(
									totalPassingSteps, step, stepStatus,
									Status.PASSED);
							totalFailingSteps = Util.setStepStatus(
									totalFailingSteps, step, stepStatus,
									Status.FAILED);
							totalSkippedSteps = Util.setStepStatus(
									totalSkippedSteps, step, stepStatus,
									Status.SKIPPED);
							totalUndefinedSteps = Util.setStepStatus(
									totalUndefinedSteps, step, stepStatus,
									Status.UNDEFINED);
							totalMissingSteps = Util.setStepStatus(
									totalMissingSteps, step, stepStatus,
									Status.MISSING);
							totalDuration = totalDuration + step.getDuration();
						}
					}
				}
			}
		}
		processTags();
	}

	private int getNumberOfScenarios(Element[] scenarios)
	{
		List<Element> scenarioList = new ArrayList<Element>();
		for (Element scenario : scenarios)
		{
			if (!scenario.getKeyword().equals("Background"))
			{
				scenarioList.add(scenario);
			}
		}
		return numberOfScenarios + scenarioList.size();
	}

	private String getArtifactFile(String mapKey, String keyword,
			String artifactFile, String contentType)
	{
		mapKey = mapKey.replaceAll(" ", "_");
		String link = "";
		if (contentType.equals("xml"))
		{
			link = "<div style=\"display:none;\"><textarea id=\""
					+ mapKey
					+ "\" class=\"brush: xml;\"></textarea></div><a onclick=\"applyArtifact('"
					+ mapKey + "','" + artifactFile + "')\" href=\"#\">"
					+ keyword + "</a>";
		}
		else
		{
			link = "<div style=\"display:none;\"><textarea id=\"" + mapKey
					+ "\"></textarea></div><script>\\$('#" + mapKey
					+ "').load('" + artifactFile
					+ "')</script><a onclick=\"\\$('#" + mapKey
					+ "').dialog();\" href=\"#\">" + keyword + "</a>";
		}
		return link;
	}

	private List<Element> addScenarioUnlessExists(
			List<Element> scenarioList, Element scenarioTag)
	{
		boolean exists = false;
		for (Element scenario : scenarioList)
		{
			if (scenario.getParentFeatureUri().equalsIgnoreCase(
					scenarioTag.getParentFeatureUri())
					&& scenario
							.getElement()
							.getName()
							.equalsIgnoreCase(
									scenarioTag.getElement().getName()))
			{
				exists = true;
				break;
			}
		}

		if (!exists)
		{
			scenarioList.add(scenarioTag);
		}
		return scenarioList;
	}

	private List<TagObject> createOrAppendToTagMap(List<TagObject> tagMap,
			Sequence<String> tagList, List<Element> scenarioList)
	{
		for (String tag : tagList)
		{
			boolean exists = false;
			TagObject tagObj = null;
			for (TagObject tagObject : tagMap)
			{
				if (tagObject.getTagName().equalsIgnoreCase(tag))
				{
					exists = true;
					tagObj = tagObject;
					break;
				}
			}
			if (exists)
			{
				List<Element> existingTagList = tagObj.getElements();
				for (Element scenarioTag : scenarioList)
				{
					existingTagList = addScenarioUnlessExists(existingTagList,
							scenarioTag);
				}
				tagMap.remove(tagObj);
				tagObj.setScenarios(existingTagList);
				tagMap.add(tagObj);
			}
			else
			{
				tagObj = new TagObject(tag, scenarioList);
				tagMap.add(tagObj);
			}
		}
		return tagMap;
	}
*/
}

public class ReportBuilder
{
/*
	ReportInformation ri;
	private File reportDirectory;
	private String buildNumber;
	private String buildProject;
	private String pluginUrlPath;
	private boolean flashCharts;
	private boolean runWithJenkins;
	private boolean artifactsEnabled;
	private boolean highCharts;
	private boolean parsingError;

	private final String VERSION = "cucumber-reporting-0.0.21";

	public ReportBuilder(List<String> jsonReports, File reportDirectory,
			String pluginUrlPath, String buildNumber, String buildProject,
			boolean skippedFails, boolean undefinedFails, boolean flashCharts,
			boolean runWithJenkins, boolean artifactsEnabled,
			String artifactConfig, boolean highCharts) throws Exception
	{

		try
		{
			this.reportDirectory = reportDirectory;
			this.buildNumber = buildNumber;
			this.buildProject = buildProject;
			this.pluginUrlPath = getPluginUrlPath(pluginUrlPath);
			this.flashCharts = flashCharts;
			this.runWithJenkins = runWithJenkins;
			this.artifactsEnabled = artifactsEnabled;
			this.highCharts = highCharts;

			ConfigurationOptions.setSkippedFailsBuild(skippedFails);
			ConfigurationOptions.setUndefinedFailsBuild(undefinedFails);
			ConfigurationOptions.setArtifactsEnabled(artifactsEnabled);
			if (artifactsEnabled)
			{
				ArtifactProcessor artifactProcessor = new ArtifactProcessor(
						artifactConfig);
				ConfigurationOptions.setArtifactConfiguration(artifactProcessor
						.process());
			}

			ReportParser reportParser = new ReportParser(jsonReports);
			this.ri = new ReportInformation(reportParser.getFeatures());

		}
		catch (Exception exception)
		{
			parsingError = true;
			generateErrorPage(exception);
		}
	}

	public boolean getBuildStatus()
	{
		return !(ri.getTotalNumberFailingSteps() > 0);
	}

	public void generateReports() throws Exception
	{
		try
		{
			copyResource("themes", "blue.zip");
			if (flashCharts)
			{
				copyResource("charts", "flash_charts.zip");
			}
			else
			{
				copyResource("charts", "js.zip");
			}
			if (artifactsEnabled)
			{
				copyResource("charts", "codemirror.zip");
			}
			generateFeatureOverview();
			generateFeatureReports();
			generateTagReports();
			generateTagOverview();
		}
		catch (Exception exception)
		{
			if (!parsingError)
			{
				generateErrorPage(exception);
			}
		}
	}

	public void generateFeatureReports() throws Exception
	{
		Iterator it = ri.getProjectFeatureMap().entrySet().iterator();
		while (it.hasNext())
		{
			Map.Entry pairs = (Map.Entry) it.next();
			List<Feature> featureList = (List<Feature>) pairs.getValue();

			for (Feature feature : featureList)
			{
				VelocityEngine ve = new VelocityEngine();
				ve.init(getProperties());
				Template featureResult = ve
						.getTemplate("templates/featureReport.vm");
				VelocityContext context = new VelocityContext();
				context.put("version", VERSION);
				context.put("feature", feature);
				context.put("report_status_colour",
						ri.getReportStatusColour(feature));
				context.put("build_project", buildProject);
				context.put("build_number", buildNumber);
				context.put("scenarios", feature.getElements().toList());
				context.put("time_stamp", ri.timeStamp());
				context.put("jenkins_base", pluginUrlPath);
				context.put("fromJenkins", runWithJenkins);
				context.put("artifactsEnabled",
						ConfigurationOptions.artifactsEnabled());
				generateReport(feature.getFileName(), featureResult, context);
			}
		}
	}

	private void generateFeatureOverview() throws Exception
	{
		int numberTotalPassed = ri.getTotalNumberPassingSteps();
		int numberTotalFailed = ri.getTotalNumberFailingSteps();
		int numberTotalSkipped = ri.getTotalNumberSkippedSteps();
		int numberTotalPending = ri.getTotalNumberPendingSteps();

		VelocityEngine ve = new VelocityEngine();
		ve.init(getProperties());
		Template featureOverview = ve
				.getTemplate("templates/featureOverview.vm");
		VelocityContext context = new VelocityContext();
		context.put("version", VERSION);
		context.put("build_project", buildProject);
		context.put("build_number", buildNumber);
		context.put("features", ri.getFeatures());
		context.put("total_features", ri.getTotalNumberOfFeatures());
		context.put("total_scenarios", ri.getTotalNumberOfScenarios());
		context.put("total_steps", ri.getTotalNumberOfSteps());
		context.put("total_passes", numberTotalPassed);
		context.put("total_fails", numberTotalFailed);
		context.put("total_skipped", numberTotalSkipped);
		context.put("total_pending", numberTotalPending);
		context.put("scenarios_passed", ri.getTotalScenariosPassed());
		context.put("scenarios_failed", ri.getTotalScenariosFailed());
		if (flashCharts)
		{
			context.put("step_data", FlashChartBuilder.donutChart(
					numberTotalPassed, numberTotalFailed, numberTotalSkipped,
					numberTotalPending));
			context.put(
					"scenario_data",
					FlashChartBuilder.pieChart(ri.getTotalScenariosPassed(),
							ri.getTotalScenariosFailed()));
		}
		else
		{
			JsChartUtil pie = new JsChartUtil();
			List<String> stepColours = pie.orderStepsByValue(numberTotalPassed,
					numberTotalFailed, numberTotalSkipped, numberTotalPending);
			context.put("step_data", stepColours);
			List<String> scenarioColours = pie.orderScenariosByValue(
					ri.getTotalScenariosPassed(), ri.getTotalScenariosFailed());
			context.put("scenario_data", scenarioColours);
		}
		context.put("time_stamp", ri.timeStamp());
		context.put("total_duration", ri.getTotalDurationAsString());
		context.put("jenkins_base", pluginUrlPath);
		context.put("fromJenkins", runWithJenkins);
		context.put("flashCharts", flashCharts);
		context.put("highCharts", highCharts);
		generateReport("overview-features.html", featureOverview, context);
	}

	public void generateTagReports() throws Exception
	{
		for (TagObject tagObject : ri.getTags())
		{
			VelocityEngine ve = new VelocityEngine();
			ve.init(getProperties());
			Template featureResult = ve.getTemplate("templates/tagReport.vm");
			VelocityContext context = new VelocityContext();
			context.put("version", VERSION);
			context.put("tag", tagObject);
			context.put("time_stamp", ri.timeStamp());
			context.put("jenkins_base", pluginUrlPath);
			context.put("build_project", buildProject);
			context.put("build_number", buildNumber);
			context.put("fromJenkins", runWithJenkins);
			context.put("report_status_colour",
					ri.getTagReportStatusColour(tagObject));
			generateReport(tagObject.getTagName().replace("@", "").trim()
					+ ".html", featureResult, context);
		}
	}

	public void generateTagOverview() throws Exception
	{
		VelocityEngine ve = new VelocityEngine();
		ve.init(getProperties());
		Template featureOverview = ve.getTemplate("templates/tagOverview.vm");
		VelocityContext context = new VelocityContext();
		context.put("version", VERSION);
		context.put("build_project", buildProject);
		context.put("build_number", buildNumber);
		context.put("tags", ri.getTags());
		context.put("total_tags", ri.getTotalTags());
		context.put("total_scenarios", ri.getTotalTagScenarios());
		context.put("total_passed_scenarios", ri.getTotalPassingTagScenarios());
		context.put("total_failed_scenarios", ri.getTotalFailingTagScenarios());
		context.put("total_steps", ri.getTotalTagSteps());
		context.put("total_passes", ri.getTotalTagPasses());
		context.put("total_fails", ri.getTotalTagFails());
		context.put("total_skipped", ri.getTotalTagSkipped());
		context.put("total_pending", ri.getTotalTagPending());
		if (flashCharts)
		{
			context.put("chart_data",
					FlashChartBuilder.StackedColumnChart(ri.getTags()));
		}
		else
		{
			if (highCharts)
			{
// This is temporary patch needs to investigate. 
//Reason is that following code supports only MasterThought Version 0.0.24
//				context.put("chart_categories",
//						JsChartUtil.getTags(ri.getTags()));
//				context.put("chart_data", JsChartUtil
//						.generateTagChartDataForHighCharts(ri.getTags()));
			}
			else
			{
				context.put("chart_rows",
						JsChartUtil.generateTagChartData(ri.getTags()));
			}
		}
		context.put("total_duration", ri.getTotalTagDuration());
		context.put("time_stamp", ri.timeStamp());
		context.put("jenkins_base", pluginUrlPath);
		context.put("fromJenkins", runWithJenkins);
		context.put("flashCharts", flashCharts);
		context.put("highCharts", highCharts);
		generateReport("tag-overview.html", featureOverview, context);
	}

	public void generateErrorPage(Exception exception) throws Exception
	{
		VelocityEngine ve = new VelocityEngine();
		ve.init(getProperties());
		Template errorPage = ve.getTemplate("templates/errorPage.vm");
		VelocityContext context = new VelocityContext();
		context.put("version", VERSION);
		context.put("build_number", buildNumber);
		context.put("fromJenkins", runWithJenkins);
		context.put("jenkins_base", pluginUrlPath);
		context.put("build_project", buildProject);
		context.put("error_message", exception);
		context.put("time_stamp",
				new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date()));
		generateReport("feature-overview.html", errorPage, context);
	}

	private void copyResource(String resourceLocation, String resourceName)
			throws IOException, URISyntaxException
	{
		final File tmpResourcesArchive = File.createTempFile("temp",
				resourceName + ".zip");

		InputStream resourceArchiveInputStream = ReportBuilder.class
				.getResourceAsStream(resourceLocation + "/" + resourceName);
		if (resourceArchiveInputStream == null)
		{
			resourceArchiveInputStream = ReportBuilder.class
					.getResourceAsStream("/" + resourceLocation + "/"
							+ resourceName);
		}
		OutputStream resourceArchiveOutputStream = new FileOutputStream(
				tmpResourcesArchive);
		try
		{
			IOUtils.copy(resourceArchiveInputStream,
					resourceArchiveOutputStream);
		}
		finally
		{
			IOUtils.closeQuietly(resourceArchiveInputStream);
			IOUtils.closeQuietly(resourceArchiveOutputStream);
		}
		UnzipUtils.unzipToFile(tmpResourcesArchive, reportDirectory);
		FileUtils.deleteQuietly(tmpResourcesArchive);
	}

	private String getPluginUrlPath(String path)
	{
		return path.isEmpty() ? "/" : path;
	}

	private void generateReport(String fileName, Template featureResult,
			VelocityContext context) throws Exception
	{
		Writer writer = new FileWriter(new File(reportDirectory, fileName));
		featureResult.merge(context, writer);
		writer.flush();
		writer.close();
	}

	private Properties getProperties()
	{
		Properties props = new Properties();
		props.setProperty("resource.loader", "class");
		props.setProperty("class.resource.loader.class",
				"org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
		props.setProperty("runtime.log", new File(reportDirectory,
				"velocity.log").getPath());
		return props;
	}

	public boolean generateXMLReports() throws Exception
	{
		try
		{
			GenerateXMLReport.testrun = new TestRun("",
					ri.getTotalNumberOfScenarios(),
					ri.getTotalNumberOfScenarios(),
					ri.getTotalScenariosFailed(), 0, 0);
			List<TestCase> lstTestCases = new ArrayList<TestCase>();
			int i = -1;

			List<Element> lstScenarios = ri.getTags().get(0).getElements();
			List<TagObject> tags = ri.getTags();
			for (TagObject tagObject : ri.getTags())
			{

				try
				{
					String tcID = tagObject.getTagName().replace("@", "")
							.trim();
					if (!tcID.matches("[0-9]+"))
						continue;
					i++;
					String tcName = lstScenarios.get(i).getElement().getName();

					tcName = tcName.substring(
							tcName.indexOf("scenario-name\">") + 15,
							tcName.lastIndexOf("</span>"));

					String tcTime = tagObject.getDurationOfSteps();
					String failure = "";

					if (lstScenarios.get(i).getElement().getStatus() == Status.PASSED)
						failure = "";
					else
						failure = "Test cases Failure .....";
					TestCase tc = new TestCase(tcName + "_" + tcID, "",
							failure, tcTime);

					lstTestCases.add(tc);
				}
				catch (Exception e)
				{
					return false;
				}
			}

			if (lstTestCases.size() > 0)
				GenerateXMLReport.testrun.testSuites.add(new TestSuite(
						lstTestCases.get(0).classname, ri
								.getTotalDurationAsString()));
			else
				GenerateXMLReport.testrun.testSuites.add(new TestSuite("NA",
						"0.00"));

			for (TestCase t : lstTestCases)
			{
				GenerateXMLReport.testrun.testSuites.get(0).testcases.add(t);
			}
		}
		catch (Exception e)
		{
			return false;
		}
		return true;
	}

	public boolean generateAdvanceXMLReports() throws Exception
	{

		GenerateXMLReport.testrun = new TestRun("",
				ri.getTotalNumberOfScenarios(), ri.getTotalNumberOfScenarios(),
				ri.getTotalScenariosFailed(), 0, 0);
		GenerateXMLReport.testrun.testSuites.add(new TestSuite("", ri
				.getTotalDurationAsString()));

		List<Element> lstFailedScenarios = ri.numberFailingScenarios;
		List<Element> lstPassedScenarios = ri.numberPassingScenarios;
		String tcID = "";
		String tcName = "";
		String failure = "";
		String tcTime = ri.getTotalTagDuration();
		for (Element e : lstPassedScenarios)
		{
			tcName = e.getName();
			tcName = tcName.substring(tcName.indexOf("scenario-name\">") + 15,
					tcName.lastIndexOf("</span>"));
			tcID = e.getTagsList();
			tcID = tcID.replaceAll("[^0-9]", "");

			if (tcID.length() > 6)
			{
				tcID = tcID.substring(0, 6);
			}

			TestCase tc = new TestCase(tcName + "_" + tcID, "", failure, tcTime);
			GenerateXMLReport.testrun.testSuites.get(0).testcases.add(tc);

		}
		for (Element e : lstFailedScenarios)
		{
			failure = "Test cases is failed, for more details please go to html report.";
			tcName = e.getName();
			tcName = tcName.substring(tcName.indexOf("scenario-name\">") + 15,
					tcName.lastIndexOf("</span>"));
			tcID = e.getTagsList();
			tcID = tcID.replaceAll("[^0-9]", "");
			if (tcID.length() > 6)
			{
				tcID = tcID.substring(0, 6);
			}
			TestCase tc = new TestCase(tcName + "_" + tcID, "", failure, tcTime);
			GenerateXMLReport.testrun.testSuites.get(0).testcases.add(tc);
		}
		return true;

	}
*/
}

