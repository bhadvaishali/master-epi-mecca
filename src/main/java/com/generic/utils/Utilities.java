package com.generic.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
//import sun.misc.BASE64Decoder;
import java.util.Base64;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.TakesScreenshot;
import org.testng.Assert;

import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.cucumberReporting.CustomFormatter;
import com.generic.webdriver.DriverProvider;
import com.mifmif.common.regex.Generex;

import io.cucumber.datatable.DataTable;

/**
 * @ScriptName : Utilities
 * @Description : This class contains utilities function
 * @Author : Harshvardhan Yadav(SQS)
 * @Modified :Harhvardhan Yadav, Namrata Donikar (Expleo)
 */
public class Utilities {
	private Configuration objConfiguration;
	private DriverProvider objDriverProvider;
	private AppiumDriverProvider objAppiumDriverProvider;
	private String randomEmail;
	private String randomusername;
	private String randompassword;

	public Utilities(Configuration configuration, DriverProvider driverProvider,
			AppiumDriverProvider appiumDriverProvider) {
		this.objConfiguration = configuration;
		this.objDriverProvider = driverProvider;
		this.objAppiumDriverProvider = appiumDriverProvider;
	}

	/**
	 * @Method : getRequiredDate
	 * @Description : This method will give require date
	 * @param : incrfementDateByDays Number by which user want increase date
	 * @param : sExpectedDateFormat - User expected date format eg. 9 april 2014 ---
	 *          dd/MM/yyyy -> 09/04/2015, dd-MM-yyyy -> 09-04-2015
	 * @param : timeZoneId - Time Zone
	 * @author : Harshvardhan Yadav(SQS)
	 */
	public String getRequiredDate(String incrementDays, String expectedDateFormat, String timeZoneId) {
		try {
			DateFormat dateFormat;
			Calendar calendar = Calendar.getInstance();
			dateFormat = new SimpleDateFormat(expectedDateFormat);
			if (timeZoneId != null && !timeZoneId.equals(""))
				dateFormat.setTimeZone(TimeZone.getTimeZone(timeZoneId));
			if (incrementDays != null && !incrementDays.equals(""))
				calendar.add(Calendar.DAY_OF_MONTH, Integer.parseInt(incrementDays));
			Date date = calendar.getTime();
			String formattedDate = dateFormat.format(date);
			return formattedDate;
		} catch (Exception exception) {
			exception.printStackTrace();
			return null;
		}
	}

	/**
	 * @Method : getRequiredTime
	 * @Description : This method will give require time
	 * @param : incrementMin - increment in time by minute
	 * @param : sExpectedDateFormat - User expected date format eg. 9 april 2014 ---
	 *          dd/MM/yyyy -> 09/04/2015, dd-MM-yyyy -> 09-04-2015
	 * @param : timeZoneId - Time Zone
	 * @author : Harshvardhan Yadav(SQS)
	 */
	public String getRequiredTime(String incrementMin, String expectedDateFormat, String timeZoneId) {
		try {
			DateFormat dateFormat;
			Calendar calendar = Calendar.getInstance();
			dateFormat = new SimpleDateFormat(expectedDateFormat);
			if (timeZoneId != null && !timeZoneId.equals(""))
				dateFormat.setTimeZone(TimeZone.getTimeZone(timeZoneId));
			if (incrementMin != null && !incrementMin.equals(""))
				calendar.add(Calendar.MINUTE, Integer.parseInt(incrementMin));
			Date time = calendar.getTime();
			String formattedTime = dateFormat.format(time);
			return formattedTime;
		} catch (Exception exception) {
			exception.printStackTrace();
			return null;
		}
	}

	/**
	 * Harshvardhan Yadav(SQS)
	 * 
	 * @param timeFormat
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public String getDifferanceBetweenTimes(String timeFormat, String startTime, String endTime) {
		try {

			SimpleDateFormat dateFormat = new SimpleDateFormat(timeFormat);
			Date firstParsedDate = dateFormat.parse(startTime);
			Date secondParsedDate = dateFormat.parse(endTime);
			long diff = secondParsedDate.getTime() - firstParsedDate.getTime();

			String timeDifferaceInMinSec = String.format("%02d min, %02d sec", TimeUnit.MILLISECONDS.toMinutes(diff),
					TimeUnit.MILLISECONDS.toSeconds(diff)
					- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(diff)));
			return timeDifferaceInMinSec;
		} catch (Exception exception) {
			exception.printStackTrace();
			return null;
		}
	}

	/**
	 * @Method : getRequiredTime
	 * @Description : This method will give require time
	 * @param : incrementMin - increment in time by minute
	 * @param : sExpectedDateFormat - User expected date format eg. 9 april 2014 ---
	 *          dd/MM/yyyy -> 09/04/2015, dd-MM-yyyy -> 09-04-2015
	 * @param : timeZoneId - Time Zone
	 * @author : Harshvardhan Yadav(SQS)
	 */
	public String getCurrentTimeZone() {
		try {
			Calendar calendar = Calendar.getInstance();
			long milliDiff = calendar.get(Calendar.ZONE_OFFSET);
			// Got local offset, now loop through available timezone id(s).
			String[] ids = TimeZone.getAvailableIDs();
			String timeZone = null;
			for (String id : ids) {
				TimeZone tz = TimeZone.getTimeZone(id);
				if (tz.getRawOffset() == milliDiff) {
					// Found a match.
					timeZone = id;
					break;
				}
			}
			return timeZone;
		} catch (Exception exception) {
			exception.printStackTrace();
			return null;
		}
	}

	/**
	 * @Method : getFormatedDate
	 * @Description : This method will converted date into excepted date format
	 * @author : Harshvardhan Yadav(SQS)
	 */
	public String getFormatedDate(String date, String originalDateFormat, String expectedDateFormat) {
		try {
			DateFormat inputFormatter = new SimpleDateFormat(originalDateFormat);
			Date originalDate = inputFormatter.parse(date);
			DateFormat outputFormatter = new SimpleDateFormat(expectedDateFormat);
			String expectedDate = outputFormatter.format(originalDate);
			return expectedDate;
		} catch (Exception exception) {
			exception.printStackTrace();
			return null;
		}
	}

	/**
	 * @Method : copyFileUsingStream
	 * @Description : copy files
	 * @param : Soure file path
	 * @param : destination file path
	 * @author : Harshvardhan Yadav(SQS)
	 */
	public void copyFileUsingStream(String sourceFilePath, String destinationFilePath) {
		InputStream inputStream = null;
		OutputStream outputStream = null;
		try {
			inputStream = new FileInputStream(new File(sourceFilePath));
			outputStream = new FileOutputStream(new File(destinationFilePath));
			byte[] buffer = new byte[1024];
			int length;
			while ((length = inputStream.read(buffer)) > 0) {
				outputStream.write(buffer, 0, length);
			}
		} catch (Exception exception) {
			exception.printStackTrace();

		} finally {
			try {
				inputStream.close();
				outputStream.close();
			} catch (IOException iOException) {
				iOException.printStackTrace();
			}
		}
	}

	/**
	 * @Description :return random number of specified length
	 */
	public int getRandomNumber(int length) {
		/*
		 * Random random = new Random(); int minRange = 1000, maxRange = 9999; int
		 * randomNumber = random.nextInt(maxRange-minRange) + minRange;
		 */

		Random random = new Random();
		char[] digits = new char[length];
		digits[0] = (char) (random.nextInt(9) + '1');
		for (int i = 1; i < length; i++) {
			digits[i] = (char) (random.nextInt(10) + '0');
		}
		return Integer.parseInt(new String(digits));
	}

	/**
	 * @Method : decryptPassword
	 * @Description : This method is used to decrypt password
	 * @param sPassword
	 * @return Decrypted password as a string
	 * @author GBHOSLE_07Jul2017
	 */
	public String decryptPassword(String sPassword) {
		String sKey = "ZYXWVUTSRQPONMLKJIHGFEDCBA1234567890abcdefghijklmnopqrstuvwxyz";
		try {
			if (sPassword == null || sKey == null)
				return null;
			char[] aKeys = sKey.toCharArray();
			// BASE64Decoder objDecoder = new BASE64Decoder();
			// char[] aMesg=new String(objDecoder.decodeBuffer(sPassword)).toCharArray();
			char[] aMesg = new String(Base64.getDecoder().decode(sPassword)).toCharArray();
			int iMsgLength = aMesg.length;
			int iKeyLength = aKeys.length;
			char[] aNewMsg = new char[iMsgLength];
			for (int iCounter = 0; iCounter < iMsgLength; iCounter++) {
				aNewMsg[iCounter] = (char) (aMesg[iCounter] ^ aKeys[iCounter % iKeyLength]);
			}
			aMesg = null;
			aKeys = null;
			return new String(aNewMsg);
		} catch (Exception e) {
			return null;
		}

	}

	// Convert map to hashmap
	public Map<String, String> convertMapToHashMap(Map<String, String> objMap) {
		Map<String, String> objHashMap = new HashMap<String, String>();
		for (String sKey : objMap.keySet()) {
			objHashMap.put(sKey, objMap.get(sKey));
		}
		return objHashMap;
	}

	public int getTotalRowsFromDataTable(DataTable dt) {

		List<List<String>> data = dt.cells();
		return data.size();
	}

	public List<String> getListDataFromDataTable(DataTable dt) {

		List<String> list = new ArrayList<String>();

		for (Map<String, String> data : dt.asMaps()) {

			for (Map.Entry<String, String> m : data.entrySet()) {
				list.add((String) m.getValue());
			}

		}
		return list;
	}

	public String toConvertHexValue(String rgb1) {

		String rgb[] = rgb1.replaceAll("(rgba)|(rgb)|(\\()|(\\s)|(\\))", "").split(",");
		String hex = String.format("#%s%s%s", toBrowserHexValue(Integer.parseInt(rgb[0])),
				toBrowserHexValue(Integer.parseInt(rgb[1])), toBrowserHexValue(Integer.parseInt(rgb[2])));
		return hex;
	}

	private static String toBrowserHexValue(int number) {
		StringBuilder builder = new StringBuilder(Integer.toHexString(number & 0xff));
		while (builder.length() < 2) {
			builder.append("0");
		}
		return builder.toString().toUpperCase();
	}

	// generate random email
	public String getEmail() {
		String regex = "[a-zA-Z0-9]{5}\\@mailinator\\.com";
		this.randomEmail = new Generex(regex).random();
		return randomEmail;
	}

	// get random generated email
	public String useEmail() {
		return randomEmail;
	}

	// generate random username
	public String getUsername() {
		String regex = "[a-zA-Z]{5}";
		this.randomusername = new Generex(regex).random();
		return randomusername;
	}

	// get random generated username
	public String useUsername() {
		return randomusername;
	}

	// generate random password
	public String getPassword() {
		String regex = "[a-zA-Z1-9]{8}";
		this.randompassword = new Generex(regex).random();
		return randompassword;
	}

	// get random generated password
	public String usePassword() {
		return randompassword;
	}
	/**
	 * return require date with increment/decrement in days from current date
	 * @param 	: incOrDecDays - Number by which user want increase/decrease date
	 * @param 	: sExpectedDateFormat - User expected date format eg. 9 april 2014
	 *        		--- dd/MM/yyyy -> 09/04/2015, dd-MM-yyyy -> 09-04-2015
	 * @param 	: timeZoneId - Time Zone
	 * @Author 	: Harshvardhan Yadav (Expleo)
	 */
	public static String getRequiredDay(String incOrDecDays, String expectedDateFormat, String timeZoneId) {
		try {
			DateFormat dateFormat;
			Calendar calendar = Calendar.getInstance();
			dateFormat = new SimpleDateFormat(expectedDateFormat);
			if (timeZoneId != null && !timeZoneId.equals(""))
				dateFormat.setTimeZone(TimeZone.getTimeZone(timeZoneId));
			calendar.add(Calendar.DAY_OF_MONTH, Integer.parseInt(incOrDecDays));
			Date tomorrow = calendar.getTime();
			String formattedDate = dateFormat.format(tomorrow);
			return formattedDate;
		} catch (Exception exception) {
			exception.printStackTrace();
			return "";
		}
	}
	/* returns Current date in provided date format*/
	public static String getCurrentDate(String format)
	{
		DateFormat dateFormat = new SimpleDateFormat(format);
		Date date = new Date();
		String date1= dateFormat.format(date);
		System.out.println("Current date is " +date1);
		return date1;

	}
	public static String getRandomAlphabeticString(int count) {
		return RandomStringUtils.randomAlphabetic(count);
	}

	public static String getRandomAlphanumericString(int count) {
		return RandomStringUtils.randomAlphanumeric(count);
	}

	public static String getRandomNumeric(int count) {
		return RandomStringUtils.randomNumeric(count);
	}

	public static String getRandomAlphanumericEmailString(int count, String emailAddress) {
		return "e" + RandomStringUtils.randomAlphanumeric(count) + emailAddress;
	}

	public static String getRandomNumericBetweenTwo(int min, int max) {
		Random random = new Random();
		int result = random.nextInt(max-min) + min;
		return String.valueOf(result);
	}
	
	public String genarateRandomPassword()
	{
		String upperCaseLetters = RandomStringUtils.random(2, 65, 90, true, true);
		String lowerCaseLetters = RandomStringUtils.random(2, 97, 122, true, true);
		String numbers = RandomStringUtils.randomNumeric(2);
		//   String specialChar = RandomStringUtils.random(2, 33, 47, false, false);
		String totalChars = RandomStringUtils.randomAlphanumeric(2);
		String combinedChars = upperCaseLetters.concat(lowerCaseLetters)
				.concat(numbers).concat(totalChars);

		List<Character> pwdChars = combinedChars.chars()
				.mapToObj(c -> (char) c)
				.collect(Collectors.toList());
		Collections.shuffle(pwdChars);
		String password = pwdChars.stream()
				.collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
				.toString();

		System.out.println( password);
		return password;
	}

}
