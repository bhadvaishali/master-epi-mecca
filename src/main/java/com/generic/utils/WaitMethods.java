package com.generic.utils;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.generic.webdriver.DriverProvider;
import com.google.common.base.Function;
/**
 * @author Harshvardhan Yadav(SQS)
 */
@SuppressWarnings("unchecked")
public class WaitMethods {
	private DriverProvider objDriverProvider;

	public WaitMethods(DriverProvider driverProvider) {
		this.objDriverProvider = driverProvider;
	}

	
	public void waitForElementToBeClickable(final By locator, int timeout) throws Exception{
		WebDriverWait wait = new WebDriverWait(objDriverProvider.getWebDriver(), (long)timeout);
		wait.until((Function)ExpectedConditions.elementToBeClickable((By)locator));
	} 

	public void waitForElementToNotBeClickable(final By locator, int timeout) throws Exception {
		WebDriverWait wait = new WebDriverWait(objDriverProvider.getWebDriver(), (long)timeout);
		wait.until((Function)ExpectedConditions.not((ExpectedCondition)ExpectedConditions.elementToBeClickable((By)locator)));
	}

	public void  waitForElementToBePresent(final By locator, int timeout) throws Exception{
		WebDriverWait wait = new WebDriverWait(objDriverProvider.getWebDriver(), (long)timeout);
		wait.until((Function)ExpectedConditions.presenceOfElementLocated((By)locator));
	}

	public WebElement waitForElementToBeDisplayed(By by, int timeout) {
		try {
			WebDriverWait wait = new WebDriverWait(objDriverProvider.getWebDriver(), (long)timeout);
			return (WebElement)wait.until((Function)ExpectedConditions.visibilityOfElementLocated((By)by));
		}
		catch (TimeoutException ex) {
			return null;
		}
	}

	public void waitForElementNotPresent(final By by, int timeout) throws Exception{
		new WebDriverWait(objDriverProvider.getWebDriver(), (long)timeout).until((Function)ExpectedConditions.not((ExpectedCondition)ExpectedConditions.presenceOfElementLocated((By)by)));
	}

	public void waitForElementNotStale(WebElement webElement, int timeout){
		try{
			WebDriverWait wait = new WebDriverWait(objDriverProvider.getWebDriver(), (long)timeout);
			//wait.until((Function)ExpectedConditions.stalenessOf((WebElement)webElement));
			wait.until((Function)ExpectedConditions.not((ExpectedCondition)ExpectedConditions.stalenessOf((WebElement)webElement)));
		}catch(Exception exception){

		}
	}

	public void waitForPresenceOfNestedElementLocated(WebElement webElement, By sub_locator, int timeout) {
		WebDriverWait wait = new WebDriverWait(objDriverProvider.getWebDriver(), (long)timeout);
		wait.until(ExpectedConditions.presenceOfNestedElementLocatedBy(webElement, sub_locator));
	} 

	public void waitForElementToNotBeDisplayed(final By by, int timeout) throws Exception {
		WebDriverWait wait = new WebDriverWait(objDriverProvider.getWebDriver(), (long)timeout);
		wait.until((Function)ExpectedConditions.invisibilityOfElementLocated((By)by));
	}

	public WebElement getElementFluent(final By locator, int timeout, int pullingTimeout) 
	{
		try{
			@SuppressWarnings("deprecation")
			Wait<WebDriver> wait = new FluentWait<WebDriver>(objDriverProvider.getWebDriver())
					.withTimeout(pullingTimeout, TimeUnit.SECONDS)
					.pollingEvery(pullingTimeout, TimeUnit.SECONDS)
					.ignoring(NoSuchElementException.class)
					.ignoring(InvalidElementStateException.class);

			WebElement webElement = wait.until(new Function<WebDriver, WebElement>(){
				public WebElement apply(WebDriver driver){
					return objDriverProvider.getWebDriver().findElement(locator);
				}
			});
			return webElement;
		}catch(Exception exception){
			System.out.println("Unable to find element with FluentWait - return null");
			return null;
		}
	}

	public WebElement getNestedElementFluent(final WebElement parentWebElement, final By locator, int timeout, int pullingTimeout) throws Exception
	{
		@SuppressWarnings("deprecation")
		Wait<WebDriver> wait = new FluentWait<WebDriver>(objDriverProvider.getWebDriver())
				.withTimeout(timeout, TimeUnit.SECONDS)
				.pollingEvery(pullingTimeout, TimeUnit.SECONDS)
				.ignoring(NoSuchElementException.class)
				.ignoring(InvalidElementStateException.class);

		WebElement webElement = wait.until(new Function<WebDriver, WebElement>(){
			public WebElement apply(WebDriver driver){
				return parentWebElement.findElement(locator);
			}
		});

		return webElement;
	}
	
	/*
	 * public void waitForAngularToFinishedProcessing(int timeout) throws Exception{
	 * WebDriverWait wait = new WebDriverWait(objDriverProvider.getWebDriver(),
	 * (long)timeout); wait.until(AngularConditions.angularHasFinishedProcessing());
	 * }
	 */

	public void sleep(int timeUnitSeconds) 
	{
		try 
		{
			Thread.sleep(TimeUnit.MILLISECONDS.convert(timeUnitSeconds, TimeUnit.SECONDS));
		}
		catch(Exception objException)
		{

		}
	}
}

