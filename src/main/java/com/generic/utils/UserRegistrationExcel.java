package com.generic.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class UserRegistrationExcel {
	private String excelFilePath;
	private XSSFWorkbook workbook;
	private XSSFSheet sheet;
	private XSSFRow row;
 
	public void updateExcelReport(String userName, String password, String email){
		try {
			excelFilePath = System.getProperty("user.dir") + "/src/test/resources/testResult/Execution_Report.xlsx";
			File reportFile = new File(excelFilePath);

			if (!reportFile.exists()) 
			{
				reportFile.createNewFile();
				workbook = new XSSFWorkbook();
				sheet = workbook.createSheet("RunReport");
				row = sheet.createRow(0);
				this.createSummaryHeaderCell(0, "UserName");
				this.createSummaryHeaderCell(1, "Password");
				this.createSummaryHeaderCell(2, "Email");
				row = sheet.createRow(1);
			}
			else 
			{
				workbook = (XSSFWorkbook) WorkbookFactory.create(new FileInputStream(excelFilePath));
				sheet = workbook.getSheet("RunReport");
				row = sheet.createRow(sheet.getLastRowNum() + 1); 

			}
			this.createSheetCell(0, userName);
			this.createSheetCell(1, password);
			this.createSheetCell(2, email);
			
			this.autoSetColumnWidth();
			FileOutputStream fileOutputStream = new FileOutputStream(excelFilePath);
			workbook.write(fileOutputStream);
			fileOutputStream.flush();
			fileOutputStream.close();
		}
		catch(Exception exception) {
			exception.printStackTrace();
		}
	}
  
	public void updateExcelReport(String userName, String password, String email,String dob,String address,String number,String Name){
		try {
			excelFilePath = System.getProperty("user.dir") + "/src/test/resources/testResult/Execution_Report.xlsx";
			File reportFile = new File(excelFilePath);

			if (!reportFile.exists()) 
			{
				reportFile.createNewFile();
				workbook = new XSSFWorkbook();
				sheet = workbook.createSheet("RunReport");
				row = sheet.createRow(0);
				this.createSummaryHeaderCell(0, "UserName");
				this.createSummaryHeaderCell(1, "Password");
				this.createSummaryHeaderCell(2, "Email");
				this.createSummaryHeaderCell(3, "DOB");
				this.createSummaryHeaderCell(4, "Address");
				this.createSummaryHeaderCell(5, "Number");
				this.createSummaryHeaderCell(6, "Name");
				row = sheet.createRow(1);
			}
			else 
			{
				workbook = (XSSFWorkbook) WorkbookFactory.create(new FileInputStream(excelFilePath));
				sheet = workbook.getSheet("RunReport");
				row = sheet.createRow(sheet.getLastRowNum() + 1); 

			}
			this.createSheetCell(0, userName);
			this.createSheetCell(1, password);
			this.createSheetCell(2, email);
			this.createSheetCell(3, dob);
			this.createSheetCell(4, address);
			this.createSheetCell(5, number);
			this.createSheetCell(6, Name);
			
			this.autoSetColumnWidth();
			FileOutputStream fileOutputStream = new FileOutputStream(excelFilePath);
			workbook.write(fileOutputStream);
			fileOutputStream.flush();
			fileOutputStream.close();
		}
		catch(Exception exception) {
			exception.printStackTrace();
		}
	}
	public void createSummaryHeaderCell(int cellNumber, String value) {
		XSSFCell cell = row.createCell(cellNumber);
		cell.setCellValue(new XSSFRichTextString(value));
		cell.setCellStyle(this.getHeaderCellStyle());
	}

	public void createSheetCell(int cellNumber, String value) {
		XSSFCell cell = row.createCell(cellNumber);
		cell.setCellValue(new XSSFRichTextString(value));
	}

	private XSSFCellStyle getHeaderCellStyle() {
		XSSFFont headerFont = workbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontName("Arial");
		XSSFCellStyle cellStyle = workbook.createCellStyle();
		cellStyle.setFont(headerFont);
		cellStyle.setAlignment(HorizontalAlignment.CENTER);
		cellStyle.setAlignment(HorizontalAlignment.CENTER);
		cellStyle.setBorderBottom(BorderStyle.THIN);
		cellStyle.setBorderLeft(BorderStyle.THIN);
		cellStyle.setBorderRight(BorderStyle.THIN);
		cellStyle.setBorderTop(BorderStyle.THIN);

		cellStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.index);
		cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		return cellStyle;
	}

	public void autoSetColumnWidth() {
		sheet.autoSizeColumn(0);
		sheet.autoSizeColumn(1);
	}
}