package com.generic.appiumDriver;

import java.io.FileInputStream;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.generic.interfaces._AppiumDriverCreation;
import com.generic.utils.CommandPrompt;
import com.generic.utils.Configuration;
import com.generic.utils.DeviceConfigurationAndroid;
import com.google.common.collect.ImmutableMap;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.IOSMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;


/**
 * used to initiate appium driver for automation
 * 
 *  @author Harshvardhan Yadav(Expleo)
 *  @modified 	  :Harhvardhan Yadav, Namrata Donikar (Expleo)
 */

public class AppiumDriverFactory implements _AppiumDriverCreation {

	/* initiate android driver instance using npm and node js on windows */
	public AndroidDriver<?> setNPMAndroidDriver(Configuration objConfiguration, String appiumServerIP,
			String appiumServerPort) throws Exception {

		// Set the capabilities for AndroidDriver
		DesiredCapabilities capabilities = DesiredCapabilities.android();			
		DeviceConfigurationAndroid objDeviceConfigurationAndroid = new DeviceConfigurationAndroid();
		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, objDeviceConfigurationAndroid.getDeviceName());
		capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, objDeviceConfigurationAndroid.getDevicePlatForm());
		capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, objDeviceConfigurationAndroid.getDevicePlatFormVersion());
		capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "uiAutomator2");
		capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "Chrome");			
		capabilities.setCapability("appium:chromeOptions", ImmutableMap.of("w3c", false));
		capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		ChromeOptions options= new ChromeOptions();			
		capabilities.setCapability("initialBrowserUrl", "about:blank");
		capabilities.setCapability(ChromeOptions.CAPABILITY, options);

		capabilities.setCapability(MobileCapabilityType.ORIENTATION, objConfiguration.getConfig("mobile.orientation"));
		capabilities.setCapability("newCommandTimeout", objConfiguration.getConfig("appium.NewCommandTimeout"));
		capabilities.setCapability("printPageSourceOnFindFailure", true);
		//capabilities.setCapability("appWaitDuration",objConfiguration.getConfig("mobile.appWaitDuration"));
		capabilities.setCapability("unicodeKeyboard", true); // to hide keyboard permanantly through the app
		capabilities.setCapability("resetKeyboard", true); // to hide keyboard permanantly through the app
		capabilities.setCapability("autoGrantPermissions", true);
		capabilities.setCapability("chromedriverExecutableDir", System.getProperty("user.dir") + "/src/main/resources/mobileChromeDriver" ); // C:\\Users\\7038\\Desktop\\cdrivers
		

		capabilities.setCapability("uiautomator2ServerLaunchTimeout", 40000);
		capabilities.setCapability("skipServerInstallation", false);
		capabilities.setCapability("appWaitDuration", 80000);
		capabilities.setCapability("deviceReadyTimeout", 5);

		System.out.println("Appium server connected..............");
		System.out.println("appiumServerIP---------->" + appiumServerIP);
		System.out.println("appiumServerPort---------->" + appiumServerPort);

		AndroidDriver<?> objAndroidDriver = new AndroidDriver<>(
				new URL("http://" + appiumServerIP + ":" + appiumServerPort + "/wd/hub"), capabilities);

		objAndroidDriver.manage().timeouts().implicitlyWait(
				Integer.parseInt(objConfiguration.getConfig("driver.implicitlyWait")), TimeUnit.SECONDS);
		objAndroidDriver.manage().timeouts().pageLoadTimeout(
				Integer.parseInt(objConfiguration.getConfig("driver.pageLoadTimeout")), TimeUnit.SECONDS);
		objAndroidDriver.manage().timeouts().setScriptTimeout(
				Integer.parseInt(objConfiguration.getConfig("driver.scriptTimeoutWait")), TimeUnit.SECONDS);
		return objAndroidDriver;
	}

	// * initiate android driver instance using desktop appium application on
	// windows*/
	public AndroidDriver<?> setDesktopAppiumAndroidDriver(Configuration objConfiguration) throws Exception {
		// Load APK/IPA properties file
		Properties objAppConfig = new Properties();
		DeviceConfigurationAndroid objDeviceConfigurationAndroid = new DeviceConfigurationAndroid();

		// Set the capabilities for AndroidDriver
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, objDeviceConfigurationAndroid.getDeviceName());
		capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, objDeviceConfigurationAndroid.getDevicePlatForm());
		capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, objDeviceConfigurationAndroid.getDevicePlatFormVersion());
		capabilities.setCapability(MobileCapabilityType.ENABLE_PERFORMANCE_LOGGING, true);
		capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "UiAutomator2");

		capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "Chrome"); 
		capabilities.setCapability("appium:chromeOptions", ImmutableMap.of("w3c", false));		
		ChromeOptions options= new ChromeOptions();			
		//options.addArguments("--incognito");			
		capabilities.setCapability(ChromeOptions.CAPABILITY, options);

		capabilities.setCapability(MobileCapabilityType.ORIENTATION, objConfiguration.getConfig("mobile.orientation"));
		capabilities.setCapability("newCommandTimeout", objConfiguration.getConfig("appium.NewCommandTimeout"));
		capabilities.setCapability(MobileCapabilityType.FULL_RESET, false);
		capabilities.setCapability(MobileCapabilityType.NO_RESET, true);
		capabilities.setCapability("printPageSourceOnFindFailure", true);
		// capabilities.setCapability("appWaitDuration",
		// objConfiguration.getConfigIntegerValue("mobile.appWaitDuration"));
		capabilities.setCapability("unicodeKeyboard", true); // to hide keyboard permanantly throught the app
		capabilities.setCapability("resetKeyboard", true); // to hide keyboard permanantly throught the app
		// capabilities.setCapability("autoGrantPermissions", true);

  		@SuppressWarnings("rawtypes")
		AndroidDriver<?> objAndroidDriver = new AndroidDriver(
				new URL("http://" + objConfiguration.getConfig("desktop.appium.ip") + ":"
						+ objConfiguration.getConfig("desktop.appium.port") + "/wd/hub"),
				capabilities);
		System.out.println("Appium connected..............");
		System.out.println("appiumServerIP---------->" + objConfiguration.getConfig("desktop.appium.ip"));
		System.out.println("appiumServerPort---------->" + objConfiguration.getConfig("desktop.appium.port"));

		objAndroidDriver.manage().timeouts().implicitlyWait(
				Integer.parseInt(objConfiguration.getConfig("driver.implicitlyWait")), TimeUnit.SECONDS);
		objAndroidDriver.manage().timeouts().pageLoadTimeout(
				Integer.parseInt(objConfiguration.getConfig("driver.pageLoadTimeout")), TimeUnit.SECONDS);
		objAndroidDriver.manage().timeouts().setScriptTimeout(
				Integer.parseInt(objConfiguration.getConfig("driver.scriptTimeoutWait")), TimeUnit.SECONDS);

		return objAndroidDriver;
	}

	public IOSDriver<?> setDesktopAppiumIOSDriver(Configuration objConfiguration) throws Exception {
		System.out.println("####Into setDesktop Appium IOS Driver");
		Properties objAppConfig = new Properties();
		objAppConfig.load(new FileInputStream(System.getProperty("user.dir") + "/src/main/resource/mobileResources/devices/iOS/" + objConfiguration.getConfig("ios.deviceName") + ".properties"));

		// Set the capabilities for AndroidDriver
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, objAppConfig.getProperty("device.name"));
		capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, objAppConfig.getProperty("device.platformName"));
		capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, objAppConfig.getProperty("device.platformVersion"));
		capabilities.setCapability(MobileCapabilityType.UDID, objAppConfig.getProperty("device.udid"));
		capabilities.setCapability(MobileCapabilityType.ENABLE_PERFORMANCE_LOGGING, true);
		//capabilities.setCapability("newCommandTimeout", objConfiguration.getConfig("test.appium.newCommandTimeout"));
		capabilities.setCapability(MobileCapabilityType.FULL_RESET, false);
		capabilities.setCapability(MobileCapabilityType.NO_RESET, true);
		capabilities.setCapability("unicodeKeyboard", true); // to hide keyboard permanantly throught the app
		capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest"); //XCUITEST
		capabilities.setCapability(IOSMobileCapabilityType.BROWSER_NAME, "Safari");
		capabilities.setCapability(IOSMobileCapabilityType.SAFARI_ALLOW_POPUPS, true);
		capabilities.setCapability("initialBrowserUrl", "about:blank");

		@SuppressWarnings("rawtypes")
		IOSDriver<?> iOSDriver = new IOSDriver(new URL("http://" + objAppConfig.getProperty("device.appium.ip") + ":"
				+ objAppConfig.getProperty("device.appium.port") + "/wd/hub"), capabilities);

		return iOSDriver;
	}
}