package com.generic.appiumDriver;

import java.io.File;
import java.io.IOException;
import java.util.Base64;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.logging.LogEntries;

import com.generic.utils.Configuration;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.screenrecording.CanRecordScreen;
import io.cucumber.java.Scenario;
/**
 * used to initiate selenium webdriver for automation  
 * @author Harshvardhan Yadav(Expleo)
 *  @modified 	  : Harhvardhan Yadav
 *
 */
public class AppiumDriverProvider extends AppiumDriverFactory 
{

	By btnCookieContinue = By.xpath("//button[contains(text(),'Continue')]");
	// Local variables
	private AppiumDriver<?> appiumDriver;
	private LogEntries appiumLogEntries;

	/**
	 *  initialize mobile appium driver for automation
	 */
	public void initializeConsoleBaseAppium(String mobileEnvironment, Configuration objConfiguration, String appiumServerIP, String appiumServerPort) throws Exception
	{
		switch (mobileEnvironment.toLowerCase())
		{
		case "android":
			appiumDriver = (AppiumDriver<?>) this.setNPMAndroidDriver(objConfiguration, appiumServerIP, appiumServerPort);
			appiumDriver.manage().deleteAllCookies();
			appiumDriver.get(objConfiguration.getConfig("web.Url"));			
			/*
			 * WebElement cookiesContinue = appiumDriver.findElement(btnCookieContinue);
			 * System.out.println("Clicking on cookies continue element:" +
			 * btnCookieContinue); cookiesContinue.click();
			 */
			break;

		case "ios":
			appiumDriver = (AppiumDriver<?>) this.setDesktopAppiumIOSDriver(objConfiguration);
			appiumDriver.get(objConfiguration.getConfig("web.Url"));
			break;
		}
		//appiumLogEntries = appiumDriver.manage().logs().get(LogType.DRIVER);

	}

	public void startRecording()
	{
		((CanRecordScreen) appiumDriver).startRecordingScreen();
	}
	/**
	 *  initialize mobile appium driver for automation
	 */
	public void initializeDesktopAppium(String mobileEnvironment, Configuration objConfiguration) throws Exception
	{
		switch (mobileEnvironment.toLowerCase())
		{
		case "android":
			appiumDriver = (AppiumDriver<?>) this.setDesktopAppiumAndroidDriver(objConfiguration);
			break;
		case "ios":
			appiumDriver = (AppiumDriver<?>) this.setDesktopAppiumIOSDriver(objConfiguration);
			appiumDriver.manage().deleteAllCookies();
			appiumDriver.manage().timeouts().implicitlyWait(Integer.parseInt(objConfiguration.getConfig("driver.implicitlyWait")), TimeUnit.SECONDS);
			appiumDriver.manage().timeouts().pageLoadTimeout(Integer.parseInt(objConfiguration.getConfig("driver.pageLoadTimeout")), TimeUnit.SECONDS);
			appiumDriver.get(objConfiguration.getConfig("web.Url"));
			break;
		}

		//appiumLogEntries = appiumDriver.manage().logs().get(LogType.DRIVER);
	}

	/**
	 *  initialize appiumDriver for automation
	 */
	public void tearDown(){
		System.out.println("**************** into ssssssssssssssssssssss");
		appiumDriver.quit();
	}

	public void stopRecording(String string,String featurename)
	{
		try 
		{
			String video = ((CanRecordScreen) appiumDriver).stopRecordingScreen();
			byte[] decode = Base64.getDecoder().decode(video);
			FileUtils.writeByteArrayToFile(new File("./recordings/"+"Mobile/"+featurename+"/"+string+".mp4"), decode);
		} catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @return - appiumDriver instance
	 */
	public AppiumDriver<?> getAppiumDriver(){
		return appiumDriver;
	}

	/**
	 * @return - appiumDriver instance
	 */
	public LogEntries getAppumLogEntries(){
		return appiumLogEntries;
	}
}
