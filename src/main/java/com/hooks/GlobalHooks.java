package com.hooks;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.generic.MyScreenRecorder;
import com.generic.StepBase;

import com.generic.cucumberReporting.CustomFormatter;
import com.generic.utils.Configuration;

import io.cucumber.core.gherkin.Feature;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;

/**
 * @ScriptName : Hooks
 * @Description : This class contains global hooks required for scenarios
 * @Author : Harhvardhan Yadav, Namrata Donikar (Expleo)
 * 
 */
public class GlobalHooks {
	private StepBase objStepBase;
	private Configuration objConfiguration;
	//private MyScreenRecorder objScreenRecorder ;
	String featureName;
	public GlobalHooks(StepBase stepBase, Configuration configuration ) {
		this.objStepBase = stepBase;
		this.objConfiguration = configuration;
		//	this.objScreenRecorder  = objScreenRecorder ;
	}


	@Before
	public void applyHook(Scenario scenario) {

		featureName = scenario.getId().split(".*/")[1];
		featureName =  featureName.substring(0, featureName.indexOf("."));
		System.out.println("Executing Cucumber Feature :- " +featureName );
		System.out.println("Tags Applied :- " + scenario.getSourceTagNames());
		System.out.println("Sceanrio  " + scenario.getName());
		objConfiguration.loadConfigProperties();
		System.err.println("---Initializing environment---");
		if (objConfiguration.getConfig("mobile.android").equalsIgnoreCase("true")) {
			objStepBase.initializeAndroidEnvironment(scenario);

		} else if (objConfiguration.getConfig("mobile.ios").equalsIgnoreCase("true")) {
			objStepBase.initializeIOSEnvironment();
		} else {
			objStepBase.initializeEnvironment(scenario);
			if((objConfiguration.getConfig("web.scriptVideoRecording").equalsIgnoreCase("true")))
			{
				System.out.println("****inro desktop ss");
				MyScreenRecorder.startRecording(scenario.getName(),featureName);}
		}
	}

	@After
	public void removeHook(Scenario scenario) {
		if (objConfiguration.getConfig("mobile.android").equalsIgnoreCase("true")) {
			objStepBase.tearDownAppiumEnvironmentAndroid(scenario);
		} else if (objConfiguration.getConfig("mobile.ios").equalsIgnoreCase("true")) {
			objStepBase.tearDownAppiumEnvironmentIOS(scenario);
		} else
		{
			if((objConfiguration.getConfig("web.scriptVideoRecording").equalsIgnoreCase("true")))
			{ 	
				System.out.println("****inro desktop stop");
				MyScreenRecorder.stopRecording();
				System.out.println("****inro fgdfgdfg stop");
			}

			objStepBase.tearDownEnvironment(scenario);
		}
	}

}
