/**
 * 
 */
package com.meccabingo.desktop.page.Mecca;

import org.openqa.selenium.By;

import com.generic.WebActions;
import com.generic.logger.LogReporter;
import com.generic.utils.Configuration;
import com.generic.utils.Utilities;
import com.generic.utils.WaitMethods;

/**
 * @author vaishali bhad
 *
 */
public class LogInPage {

	By closeXicon = By.xpath("//a[contains(@class,'icon-close')]");
	By tbUserName = By.xpath("//input[@id='input-username']");
	By tbPassword = By.xpath("//input[@id='input-password']");
	By rememberMeCheckbox = By.xpath("//*[contains(text(),'Remember Me')]/preceding-sibling::input[@type='checkbox']");
	By btnLogIn = By.xpath("//span[@class='button-content']/parent::button[@type='submit']"); // button[contains(text(),'Login')//
																								// and//
																								// @type='submit'],
																								// //*[@id=\"header\"]/header/div[2]/button
	By forgotUserNameLink = By.xpath("//a[contains(text(),'Forgot username?')]");
	By forgotPasswordLink = By.xpath("//a[contains(text(),'Forgot password')]");

	By contactUsSection = By.xpath("//div[contains(@class,'contacts-wrapper')]/span/p[contains(text(),'Contact')]"); // p[contains(text(),'Contact
																														// Us')]
	By contactUsCallNumber = By.xpath("//div[contains(@class,'contacts-wrapper')]/span/p[contains(text(),'Call')]"); // p[contains(text(),'Call:
																														// 08000831988')]
	By contactUsemail = By.xpath("//a[contains(text(),'Support@meccabingo.com')]");

	By sendResetInstructions = By.xpath("//*[@class='login-wrapper-form']/button/child::span");
	By resetPasswordSuccessHeading = By.xpath("//h4[contains(text(),'Success')]");
	By resetPasswordSuccessMessage = By.xpath("//h4[contains(text(),'Success')]/following-sibling::p");
	By txtEmailAddress = By.xpath("//input[@id='input-email']");
	By sendUsernameReminder = By.xpath("//*[@class='login-wrapper-form']/button/child::span");
	By btnCTALogIn = By.xpath("//button[@type='submit']");


	By MyAccountButton = By.xpath("//a[contains(@class,'open-myaccount')]/i");
	By LogoutButton = By.xpath("//a[contains(text(),'Logout')]");

	private WebActions webActions;
	private LogReporter logReporter;
	private RegistrationPage registrationPage;
	private Configuration configuration;
	private WaitMethods wait;
	public LogInPage(WebActions webActions, LogReporter logReporter,Configuration configuration,WaitMethods wait) {
		this.webActions = webActions;
		this.logReporter = logReporter;
		this.configuration= configuration;
		this.wait = wait;
	}

	public void verifyLoginHeaderTitle() {
		By loginHeaderText = By.xpath("//h4[contains(text(),'Login')]"); // h4[starts-with(text(),'Login')]
		logReporter.log("Check Login window header > >", webActions.checkElementDisplayed(loginHeaderText));
		//webActions.useEmail();
	}

	public void clickJoinNowButton() {
		By btnJoinNow = By.xpath("//button[contains(text(),'Join Now')]");
		logReporter.log("click 'join now button' > >", webActions.click(btnJoinNow));

	}

	public void enterUserName(String userName) {
		logReporter.log("Enter Value in userName > >", webActions.setText(tbUserName, userName));
	}

	public void enterPassword(String password) {
		logReporter.log("Enter Value in password > >", webActions.setText(tbPassword, password));
	}

	public void clickLogin() {
		logReporter.log("click 'login button' > >", webActions.click(btnLogIn));
	}

	public void verifyLoginDisabled() {
		By btnLogInDisabled = By.xpath("//button[contains(text(),'Login') and contains(@class,'button-disabled')]");
		logReporter.log("check element > >", webActions.checkElementDisplayed(btnLogInDisabled));
	}

	public void verifyLogInErrorMessage(String errorMessage) {
		By errormessage = By.xpath("//h4[contains(text(),'" + errorMessage + "')]");
		logReporter.log("check element > >", webActions.checkElementDisplayed(errormessage));
	}

	public void verifyLoginPage() {
		By loginHeader = By.xpath("//div[contains(@class,'slideout-overlay-heading')]/h4[contains(text(),'Login')]");
		logReporter.log("Check login page displayed", webActions.checkElementDisplayed(loginHeader));

	}

	public void verifyCloseXicon() {

		logReporter.log("Check Close X icon displayed", webActions.checkElementDisplayed(closeXicon));
	}

	public void clickOnCloseXicon() {

		logReporter.log("Check Close X icon displayed", webActions.click(closeXicon));
	}

	public void verifyTBUserNameDisplayed() {
		logReporter.log("Check Username textbox displayed", webActions.checkElementDisplayed(tbUserName));

	}

	public void verifyTBPasswordDisplayed() {
		logReporter.log("Check Password textbox displayed", webActions.checkElementDisplayed(tbPassword));
	}

	public void verifyPasswordToggle() {
		By passwordToggle = By.xpath("//input[@id='input-password']/following::button[contains(text(),'Show')]");
		logReporter.log("Check Password toggle displayed", webActions.checkElementDisplayed(passwordToggle));

	}

	public void verifyRememberMeCheckbox() {
		logReporter.log("Check Remember Me checkbox displayed",
				webActions.checkElementDisplayed(rememberMeCheckbox));
	}

	public void verifyLoginCTA() {
		logReporter.log("Check login Button displayed", webActions.checkElementDisplayed(btnCTALogIn));
	}

	public void veryForgotUserNameLink() {

		logReporter.log("Check Forgot UserName Link displayed",
				webActions.checkElementDisplayed(forgotUserNameLink));

	}

	public void veryForgotPasswordLink() {

		logReporter.log("Check Forgot Password Link displayed",
				webActions.checkElementDisplayed(forgotPasswordLink));

	}

	public void verifyNewToMeccaText() {
		By newToMeccaText = By
				.xpath("//a[contains(text(),'Sign Up')]/parent::p[contains(text(),'New to Mecca bingo')]");
		logReporter.log("Check New to Mecca text displayed", webActions.checkElementDisplayed(newToMeccaText));
	}

	public void verifySignUpLinkDisplayed() {
		By linkSignUP = By.xpath("//a[contains(text(),'Sign Up')]");
		logReporter.log("Check Sign Up Link displayed", webActions.checkElementDisplayed(linkSignUP));

	}

	public void verifyHelpContactDetails() {
		logReporter.log("Check Contact Us section displayed", webActions.checkElementDisplayed(contactUsSection));
		logReporter.log("Check Contact Us Call NUmber displayed",
				webActions.checkElementDisplayed(contactUsCallNumber));
		logReporter.log("Check Contact Us Email displayed", webActions.checkElementDisplayed(contactUsemail));
	}

	public void verifyLiveChat() {
		By liveChat = By.xpath("//p[contains(text(),'Live Help')]/preceding-sibling::span");
		logReporter.log("Check Live Chat displayed", webActions.checkElementDisplayed(liveChat));
	}

	public void verifyLogInEnabled() {
		if (!webActions.checkElementDisplayed(
				By.xpath("//span[@class='button-content']/parent::button[@type='submit' and  @disabled]")))
			logReporter.log("Check login button Enabled", true);

		// logReporter.log("Check login button Enabled",
		// objWebActions.checkElementEnabled(btnLogIn));
	}

	public void clickOnRememberMe() {
		logReporter.log("Clcik Remember Me checkbox", webActions.click(rememberMeCheckbox));
	}

	public void VerifyTextFromUserName(String useName) {
		logReporter.log("Verify Text from UserName", useName, webActions.getText(tbUserName, "value"));
	}

	public void verifyForgottenPasswordHeader() {
		By forgottenPasswordPageHeader = By
				.xpath("//div[contains(@class,'slideout-overlay-heading')]/h4[contains(text(),'Forgot your password')]");
		logReporter.log("Check 'Forgotten Password' header",
				webActions.checkElementDisplayed(forgottenPasswordPageHeader));
	}

	public void clickOnForgotPasswordLink() {
		logReporter.log("Click 'Forgot Password'", webActions.click(forgotPasswordLink));
	}

	public void verifyBackArrow() {
		By backArrow = By.xpath("//a[contains(@class,'icon-arrow-left')]");
		logReporter.log("Check 'Back Arrow'", webActions.checkElementDisplayed(backArrow));

	}

	public void verifyHelperTextFromForgottenPasswordPage(String text) {
		By forgottenPasswordPageHelperText = By.xpath("//div[contains(@class,'slideout-overlay-content')]/p");
		String currentText = webActions.getText(forgottenPasswordPageHelperText, "text");
		System.out.println("***********currentText   "+currentText);
		logReporter.log("Check 'Forgotten Password additional info'", text,
				text.equalsIgnoreCase(currentText));
	}

	public void verifySendResetInstructionsCTA() {
		logReporter.log("Check 'Send Reset Instructions 'CTA'",
				webActions.checkElementDisplayed(sendResetInstructions));
	}

	public void clickOnSendResetInstructions() {
		logReporter.log("Click 'Send reset instructions' Link", webActions.click(sendResetInstructions));
	}

	public void verifyPasswordResetSuccessMessage() {
		logReporter.log("Check 'Reset Password Sucess'",
				webActions.checkElementDisplayed(resetPasswordSuccessHeading));
		logReporter.log("Check 'Reset Password Sucess Message'",
				webActions.checkElementDisplayed(resetPasswordSuccessMessage));
	}

	public void verifyIDidnotReceiveAnEmailLink() {
		By iDidnotReceiveAnEmail = By.xpath("//div[contains(@class,'login-wrapper-links')]");
		logReporter.log("Check 'I didnot receive an email' link",
				webActions.checkElementDisplayed(iDidnotReceiveAnEmail));
	}

	public void clickOnForgotUsernameLink() {
		logReporter.log("Click 'Forgot username' Link", webActions.click(forgotUserNameLink));
	}

	public void verifyForgottenUsernameHeader() {
		By forgottenUsernamePageHeader = By.xpath("//h4[contains(text(),'Forgotten your username')]"); // div[@class='slideout-overlay-heading']/h4[contains(text(),'Forgotten
																										// your
																										// Username')]
		logReporter.log("Check 'Forgotten Username' header",
				webActions.checkElementDisplayed(forgottenUsernamePageHeader));
	}

	public void verifyHelperTextFromForgottenUsernamePage(String text) {
		By forgottenUsernamePageHelperText = By.xpath(
				"//div[contains(@class,'slideout-overlay-content')]/p[contains(text(),'Please supply the following account')]");
		logReporter.log("Check 'Forgotten username additional info'", text,
				webActions.checkElementDisplayed(forgottenUsernamePageHelperText));
	}

	public void verifyEmailFieldFromForgottenUsername() {
		logReporter.log("Check 'Email' from Forgotten Username", webActions.checkElementDisplayed(txtEmailAddress));
	}

	public void verifySendUsernameReminderCTA() {
		logReporter.log("Check 'Send Username Reminder 'CTA'",
				webActions.checkElementDisplayed(sendUsernameReminder));
	}

	public void enterEmailAddressInForgotUsernameSection(String emailAddress) {
		logReporter.log("Enter Value in email address > >", webActions.setText(txtEmailAddress, emailAddress));
	}

	public void clickOnSendUsernameReminder() {
		logReporter.log("Click 'Send Username Reminder'", webActions.click(sendUsernameReminder));
	}

	public void clickOnLoginCTAbutton() {

		logReporter.log("Click on CTA login button", webActions.click(btnCTALogIn));
	}

	public void clearUsernameText() {
		logReporter.log("Clear username", webActions.clearText(tbUserName));
	}

	public void clearPasswordText() {
		logReporter.log("Clear password", webActions.clearText(tbPassword));
	}

	public void clickOnMyAccountButtonFromHeader() {
		logReporter.log("Click on My Account button", webActions.click(MyAccountButton));

	}

	public void clickOnLogoutbutton() {
		logReporter.log("Click on logout button", webActions.click(LogoutButton));
	}
	
	//mailinator
	
	public void enterEmailId(String email)
	{	
		By mailinatoremail = By.xpath("");
		logReporter.log("", webActions.setText(mailinatoremail, email));
	}
	

	public void setUserNameFromConfig() {
		String userName = configuration.getConfig("web.userName");
		logReporter.log("Enter Value in userName > >", webActions.setTextWithClear(tbUserName, userName));
		}

	public void setPasswordFromConfig() {
		String password = configuration.getConfig("web.password");
		logReporter.log("Enter Value in password > >", webActions.setTextWithClear(tbPassword, password));}
	
	
	
	///
	// Reset Password Link from email
		public void verifyResetPasswordHdrDisplayed()
		{
			By resetPasswordHdr = By.xpath("//h4[text()='Reset password']");

			logReporter.log("Verify Reset Password header is displayed when link is invoked", 
					webActions.checkElementDisplayed(resetPasswordHdr));

		}

		public void setNewPassword(String newpwd)
		{
			By inpnewpwd = By.xpath("//input[@id=\"input-password\" and @type=\"password\"]");
			logReporter.log("Set new password as : ", newpwd,
					webActions.setText(inpnewpwd, newpwd));
			webActions.pressKeybordKeys(inpnewpwd, "tab");

		}
		public void clickSubmitForNewPassword()
		{
			By submitNewPasswordBtn = By.xpath("//button[@type='submit']//span[text()='Reset password']");

			logReporter.log("Click 'Submit' button for reset password", 
					webActions.click(submitNewPasswordBtn));
		}

		public void verifyGoToHomepageBTNDisplayed()
		{
			By locator = By.xpath("//a[@class='btn' and text() ='GO TO THE HOMEPAGE']");

			logReporter.log(" Verify 'GO TO THE HOMEPAGE' button displayed on reset password success screen", 
					webActions.checkElementDisplayed(locator));
		}

		public void clickContinueToLoginBtn()
		{
			By ContinueToLoginBtn = By.xpath("//a[@class='btn' and text() ='Continue to login']");

			logReporter.log("Click 'Continue to login' button on reset password", 
					webActions.click(ContinueToLoginBtn));
		}

		public void verifyPasswordResetSuccessMessageDisplayed() {

			//By locatorYourPasswordReset = By.xpath("//p[contains(text(),'You have successfully reset your password.')]");
			By locatorYourPasswordReset = By.xpath("//i[contains(@class,'icon-check-mark')]//following-sibling::h4[contains(.,'Your password has been reset. Log in to play')]");
			logReporter.log("Verify 'Your password has been reset. Log in to play' message displayed", 
					webActions.checkElementDisplayed(locatorYourPasswordReset));
		}
}
